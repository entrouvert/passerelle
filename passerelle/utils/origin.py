# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.parse


def get_url_origin(url):
    '''Build origin of an URL'''
    parsed = urllib.parse.urlparse(url)
    if ':' in parsed.netloc:
        host, port = parsed.netloc.split(':', 1)
        if parsed.scheme == 'http' and port == 80:
            port = None
        if parsed.scheme == 'https' and port == 443:
            port = None
    else:
        host, port = parsed.netloc, None
    result = '%s://%s' % (parsed.scheme, host)
    if port:
        result += ':%s' % port
    return result


def is_same_origin(url1, url2):
    if 'null' in [url1, url2] or not url1 or not url2:
        return False

    try:
        origin1 = get_url_origin(url1)
        origin2 = get_url_origin(url2)
    except (ValueError, TypeError):
        return False

    return origin1 == origin2
