# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
from contextlib import contextmanager


@contextmanager
def ignore_loggers(*loggers_names):
    propagate = {}
    disabled = {}

    for logger_name in loggers_names:
        logger = logging.getLogger(logger_name)
        propagate[logger] = logger.propagate
        disabled[logger] = logger.disabled
        logger.propagate = False
        logger.disabled = True

    yield

    for logger_name in loggers_names:
        logger = logging.getLogger(logger_name)
        logger.propagate = propagate[logger]
        logger.disabled = disabled[logger]
