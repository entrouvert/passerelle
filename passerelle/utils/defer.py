# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import contextlib
from contextvars import ContextVar

from django.core.management import BaseCommand
from django.db import connection

# See https://docs.python.org/3/library/contextvars.html
# ContextVar are concurrency-safe variables, they are thread safe (like
# threading.local()) and coroutine (asyncio) safe.
run_later_context: ContextVar[list] = ContextVar('run_later_context')


def is_in_transaction():
    return getattr(connection, 'in_atomic_block', False)


@contextlib.contextmanager
def run_later_scope():
    try:
        run_later_context.get()
    except LookupError:
        callbacks = []
        token = run_later_context.set(callbacks)
        try:
            yield
        finally:
            run_later_context.reset(token)
            for func, args, kwargs in callbacks:
                func(*args, **kwargs)
    else:
        # nested scopes have not effect, callbacks will always be called by the
        # most enclosing scope, i.e. in this case:
        # with run_later_scope():
        #     with run_later_scope():
        #         run_later(f)
        #     (1)
        #     ..other statements..
        #     (2)
        #
        # the function will be called at point (2), not (1)
        yield


def run_later(func, *args, **kwargs):
    try:
        callbacks = run_later_context.get()
    except LookupError:
        # no scope, run immediately
        return func(*args, **kwargs)
    else:
        callbacks.append((func, args, kwargs))
        return None


def run_later_if_in_transaction(func, *args, **kwargs):
    if is_in_transaction():
        return run_later(func, *args, **kwargs)
    else:
        return func(*args, **kwargs)


class run_later_middleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        with run_later_scope():
            return self.get_response(request)


# monkeypatch BaseCommand execute to provide the same service to commands
old_BaseCommand_execute = BaseCommand.execute


def BaseCommand_execute(self, *args, **kwargs):
    with run_later_scope():
        return old_BaseCommand_execute(self, *args, **kwargs)


BaseCommand.execute = BaseCommand_execute
