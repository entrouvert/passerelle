# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2018 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import hashlib
import hmac
import time
from urllib import parse as urlparse
from uuid import uuid4

from django.core.cache import cache
from django.utils.encoding import force_bytes, force_str
from requests.auth import AuthBase
from requests.exceptions import RequestException

from .jsonresponse import APIError


class HawkAuth(AuthBase):
    def __init__(self, id, key, algorithm='sha256', ext=''):
        self.id = id.encode('utf-8')
        self.key = key.encode('utf-8')
        self.algorithm = algorithm
        self.timestamp = str(int(time.time()))
        self.nonce = uuid4().hex
        self.ext = ext

    def get_payload_hash(self, req):
        p_hash = hashlib.new(self.algorithm)
        p_hash.update(force_bytes('hawk.1.payload\n'))
        p_hash.update(force_bytes(req.headers.get('Content-Type', '') + '\n'))
        p_hash.update(force_bytes(req.body or ''))
        p_hash.update(force_bytes('\n'))
        return force_str(base64.b64encode(p_hash.digest()))

    def get_authorization_header(self, req):
        url_parts = urlparse.urlparse(req.url)
        uri = url_parts.path
        if url_parts.query:
            uri += '?' + url_parts.query
        if url_parts.port is None:
            if url_parts.scheme == 'http':
                port = '80'
            elif url_parts.scheme == 'https':
                port = '443'
        hash = self.get_payload_hash(req)
        data = [
            'hawk.1.header',
            self.timestamp,
            self.nonce,
            req.method.upper(),
            uri,
            url_parts.hostname,
            port,
            hash,
            self.ext,
            '',
        ]
        digestmod = getattr(hashlib, self.algorithm)
        result = hmac.new(force_bytes(self.key), force_bytes('\n'.join(data)), digestmod)
        mac = force_str(base64.b64encode(result.digest()))
        authorization = 'Hawk id="%s", ts="%s", nonce="%s", hash="%s", mac="%s"' % (
            force_str(self.id),
            self.timestamp,
            self.nonce,
            hash,
            mac,
        )
        if self.ext:
            authorization += ', ext="%s"' % self.ext
        return authorization

    def __call__(self, r):
        r.headers['Authorization'] = self.get_authorization_header(r)
        return r


class HttpBearerAuth(AuthBase):
    def __init__(self, token):
        self.token = token

    def __eq__(self, other):
        return self.token == getattr(other, 'token', None)

    def __ne__(self, other):
        return not self == other

    def __call__(self, r):
        r.headers['Authorization'] = 'Bearer ' + self.token
        return r


class TokenAuth(AuthBase):
    """A class to handle token based authentication"""

    def __init__(self, session, token_url, **kwargs):
        self.session = session
        self.token_url = token_url
        self.cache_delay = kwargs.pop('cache_delay', 300)
        self.request_args = kwargs

    def get_cache_key(self):
        prefix = f'{self.__class__.__name__}:'
        if self.session.resource:
            prefix += f'{self.session.resource.pk}:'
        suffix = f'{self.token_url}'

        auth = self.request_args.get('auth')
        headers = self.request_args.get('headers') or {}

        if isinstance(auth, tuple):
            suffix += f'-{auth}'
        elif 'Authorization' in headers:
            # we may have to use a non standard header
            suffix += f'-{headers["Authorization"]}'

        return prefix + hashlib.sha256(suffix.encode()).hexdigest()

    def clear_cache(self):
        """clear cache (when a 401 is returned)"""
        cache.delete(self.get_cache_key())

    def extract_token_header(self, data):
        """Allow to override token extraction from json response"""
        token_type = data.get('token_type', 'Bearer')
        return f'{token_type} {data["access_token"]}'

    def extract_cache_delay(self, data):
        """Allow to override token cache policy"""
        if 'expires_in' in data:
            cache_delay = int(data['expires_in'])
            # reduce the delay if > 60. just in case
            return cache_delay - 60 if cache_delay > 60 else cache_delay
        return self.cache_delay

    def fetch_token_header(self):
        """Carefully fetch a token, generate an Authorization header and cache the result"""
        key = self.get_cache_key()
        access_token = cache.get(key)
        if not access_token:
            try:
                resp = self.session.post(self.token_url, **self.request_args)
            except RequestException:
                raise APIError('Could not obtain a token. Service is unavailable.')
            if not resp.ok:
                raise APIError('Could not obtain a token. Service is unavailable')
            try:
                data = resp.json()
            except (ValueError, KeyError, TypeError):
                raise APIError('Could not obtain a token. Invalid json response.')
            try:
                access_token = self.extract_token_header(data)
            except Exception:
                msg = 'Could not extract a token from the json response.'
                self.session.logger.exception(msg)
                raise APIError(msg)
            try:
                cache_delay = self.extract_cache_delay(data)
            except Exception as e:
                self.session.logger.warning('Could not extract the cache delay from the response: %s', e)
                cache_delay = self.cache_delay
            cache.set(key, access_token, cache_delay)
        return access_token

    def __call__(self, req):
        """AuthBase api. Add the correct header"""
        req.headers['Authorization'] = self.fetch_token_header()
        return req


class AmazonCognito(TokenAuth):
    def __init__(self, token_key=None, **kwargs):
        super().__init__(**kwargs)
        self.token_key = token_key

    def extract_cache_delay(self, data):
        # AWS says that the token is valid for 1h by default. We cache for 59mn
        # We take care of ExpiresIn value anyway but it may never be revelant (test value: 14400)
        data = data['AuthenticationResult']
        cache_delay = int(data.get('ExpiresIn', 3600))
        return min(cache_delay, 3600) - 60

    def extract_token_header(self, data):
        data = data['AuthenticationResult']
        return f'{data["TokenType"]} {data[self.token_key]}'
