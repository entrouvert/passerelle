# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core import validators
from django.db import models

from .forms import LDAPURLField as LDAPURLFormField


def resource_file_upload_to(instance, filename):
    return '%s/%s/%s' % (instance.get_connector_slug(), instance.slug, filename)


class LDAPURLField(models.URLField):
    default_validators = [validators.URLValidator(schemes=['ldap', 'ldaps'])]

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                'form_class': LDAPURLFormField,
                **kwargs,
            }
        )
