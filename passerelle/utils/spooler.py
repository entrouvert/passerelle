# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess

from uwsgidecorators import spool  # pylint: disable=import-error


@spool
def run_job(args):
    from django.conf import settings

    cmd_args = [
        settings.PASSERELLE_MANAGE_COMMAND,
    ]

    if args.get('domain'):
        # multitenant installation
        cmd_args.append('tenant_command')

    cmd_args += ['runjob', '--job-id', args['job_id']]

    if args.get('domain'):
        # multitenant installation
        cmd_args.append('--domain')
        cmd_args.append(args['domain'])

    # pylint: disable=subprocess-run-check
    subprocess.run(cmd_args)
