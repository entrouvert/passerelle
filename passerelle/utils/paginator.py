from django.core.paginator import EmptyPage, Page, Paginator
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _


class InfinitePaginator(Paginator):
    def page(self, number):
        number = self.validate_number(number)
        offset = (number - 1) * self.per_page

        window_items = list(self.object_list[offset : offset + self.per_page + 1])
        page_items = window_items[: self.per_page]

        if not page_items:
            if number == 1 and self.allow_empty_first_page:
                pass
            else:
                raise EmptyPage(_('That page contains no results'))

        has_next = len(window_items) > len(page_items)
        return InfinitePage(page_items, number, self, has_next)

    @cached_property
    def count(self):
        return 2**32

    @cached_property
    # pylint: disable=invalid-overridden-method
    def page_range(self):
        return [0]


class InfinitePage(Page):
    def __init__(self, object_list, number, paginator, has_next):
        super().__init__(object_list, number, paginator)
        self._has_next = has_next

    def has_next(self):
        return self._has_next
