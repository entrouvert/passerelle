# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core import exceptions, validators
from django.template import Template, TemplateSyntaxError
from django.utils.translation import gettext_lazy as _


class LDAPURLField(forms.URLField):
    default_validators = [validators.URLValidator(schemes=['ldap', 'ldaps'])]


def validate_condition_template(condition_template):
    real_template = f'{{% if {condition_template} %}}OK{{% endif %}}'
    try:
        Template(real_template)
    except (TemplateSyntaxError, OverflowError) as e:
        raise exceptions.ValidationError(_('syntax error: %s') % e, code='syntax-error')


class ConditionField(forms.CharField):
    default_validators = [validate_condition_template]


def validate_template(template):
    try:
        Template(template)
    except (TemplateSyntaxError, OverflowError) as e:
        raise exceptions.ValidationError(_('syntax error: %s') % e, code='syntax-error')


class TemplateField(forms.CharField):
    default_validators = [validate_template]
    widget = forms.Textarea
