# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import apps
from django.urls import include, path, re_path

from .urls_utils import app_enabled, manager_required, required, trust_required


def register_apps_urls(urlpatterns):
    """Call get_before_urls and get_after_urls on all apps providing them,
    add those urls to the given urlpatterns (before or after).
    """
    before_urls = []
    after_urls = []
    for app in apps.get_app_configs():
        if not hasattr(app, 'get_connector_model'):
            continue
        connector_model = app.get_connector_model()
        connector_slug = connector_model.get_connector_slug()
        for obj in (connector_model, app):
            if hasattr(obj, 'get_before_urls') or hasattr(obj, 'get_after_urls'):
                if hasattr(obj, 'get_before_urls'):
                    urls = obj.get_before_urls()
                    if urls:
                        before_urls.append(path('', include(urls)))
                if hasattr(obj, 'get_after_urls'):
                    urls = obj.get_after_urls()
                    if urls:
                        after_urls.append(path('', include(urls)))
            elif hasattr(obj, 'get_urls'):
                url_prefix = '^%s/' % connector_slug
                urls = obj.get_urls()
                if urls:
                    urls = required(app_enabled(app.label), urls)
                    after_urls.append(
                        re_path(url_prefix, include(urls), kwargs={'connector': connector_slug})
                    )
            if hasattr(obj, 'get_management_urls'):
                url_prefix = '^manage/%s/' % connector_slug
                urls = obj.get_management_urls()
                if urls:
                    urls = required(app_enabled(app.label), urls)
                    urls = required(manager_required, urls)
                    after_urls.append(
                        re_path(url_prefix, include(urls), kwargs={'connector': connector_slug})
                    )
            if hasattr(obj, 'get_statistics_urls'):
                url_prefix = '^api/%s/' % connector_slug
                urls = obj.get_statistics_urls()
                if urls:
                    urls = required(app_enabled(app.label), urls)
                    urls = required(trust_required, urls)
                    after_urls.append(
                        re_path(url_prefix, include(urls), kwargs={'connector': connector_slug})
                    )

    return before_urls + urlpatterns + after_urls
