# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _


class GenericConnectorForm(forms.ModelForm):
    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = slugify(self.instance.title)
        return super().save(commit=commit)

    def get_initial_for_field(self, field, field_name):
        if field_name == 'title':
            # if title and slug name are in sync, keep them that way
            if slugify(self.initial.get('title')) == slugify(self.initial.get('slug')):
                field.widget.attrs['data-slug-sync'] = 'slug'
        return super().get_initial_for_field(field, field_name)


class ResourceLogSearchForm(forms.Form):
    log_level = forms.ChoiceField(
        label=_('Log level'),
        choices=(
            ('', _('All levels')),
            ('DEBUG', _('Debug')),
            ('INFO', _('Info')),
            ('WARNING', _('Warning')),
            ('ERROR', _('Error')),
            ('CRITICAL', _('Critical')),
        ),
        required=False,
    )
    q = forms.CharField(required=False)
