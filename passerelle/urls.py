from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from django.views.i18n import JavaScriptCatalog

from .api.urls import urlpatterns as api_urls
from .base.urls import access_urlpatterns, import_export_urlpatterns
from .base.views import GenericJobView, GenericRestartJobView, GenericViewJobsConnectorView
from .plugins import register_apps_urls
from .urls_utils import decorated_includes, manager_required
from .views import (
    GenericConnectorView,
    GenericCreateConnectorView,
    GenericDeleteConnectorView,
    GenericEditConnectorView,
    GenericEndpointView,
    GenericExportConnectorView,
    GenericLogView,
    GenericViewLogsConnectorView,
    HomePageView,
    ManageAddView,
    ManageView,
    MediaView,
    login,
    logout,
    menu_json,
)

admin.autodiscover()

urlpatterns = [
    path('', HomePageView.as_view(), name='homepage'),
    path('manage/', manager_required(ManageView.as_view()), name='manage-home'),
    re_path(r'^manage/menu.json$', manager_required(menu_json), name='menu-json'),
    path('manage/add', manager_required(ManageAddView.as_view()), name='add-connector'),
    re_path(r'^media/(?P<path>.*)$', manager_required(MediaView.as_view()), name='media'),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^manage/access/', decorated_includes(manager_required, include(access_urlpatterns))),
    re_path(r'^manage/', decorated_includes(manager_required, include(import_export_urlpatterns))),
    path('api/', include(api_urls)),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
]

# add patterns from apps
urlpatterns = register_apps_urls(urlpatterns)

# add authentication patterns
urlpatterns += [
    path('logout/', logout, name='auth_logout'),
    path('login/', login, name='auth_login'),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns += [
        path(
            'accounts/mellon/',
            include('mellon.urls'),
            kwargs={
                'template_base': 'passerelle/base.html',
            },
        )
    ]


urlpatterns += [
    re_path(
        r'^manage/(?P<connector>[\w,-]+)/',
        decorated_includes(
            manager_required,
            include(
                [
                    path('add', GenericCreateConnectorView.as_view(), name='create-connector'),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/delete$',
                        GenericDeleteConnectorView.as_view(),
                        name='delete-connector',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/edit$', GenericEditConnectorView.as_view(), name='edit-connector'
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/logs/$',
                        GenericViewLogsConnectorView.as_view(),
                        name='view-logs-connector',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/logs/(?P<log_pk>\d+)/$',
                        GenericLogView.as_view(),
                        name='view-log',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/jobs/$',
                        GenericViewJobsConnectorView.as_view(),
                        name='view-jobs-connector',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/jobs/(?P<job_pk>\d+)/$',
                        GenericJobView.as_view(),
                        name='view-job',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/jobs/(?P<job_pk>\d+)/restart/$',
                        GenericRestartJobView.as_view(),
                        name='restart-job',
                    ),
                    re_path(
                        r'^(?P<slug>[\w,-]+)/export$',
                        GenericExportConnectorView.as_view(),
                        name='export-connector',
                    ),
                ]
            ),
        ),
    )
]

urlpatterns += [
    re_path(
        r'^(?P<connector>[\w,-]+)/(?P<slug>[\w,-]+)/$', GenericConnectorView.as_view(), name='view-connector'
    ),
    re_path(
        r'^(?P<connector>[\w,-]+)/(?P<slug>[\w,-]+)/(?P<endpoint>[\w,-.]+)(?:/(?P<rest>.*))?$',
        GenericEndpointView.as_view(),
        name='generic-endpoint',
    ),
]

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar  # pylint: disable=import-error

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

urlpatterns += staticfiles_urlpatterns()
