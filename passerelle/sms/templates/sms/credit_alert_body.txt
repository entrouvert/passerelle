{% extends "emails/body_base.txt" %}
{% load i18n %}

{% block content %}{% autoescape off %}{% trans "Hi," %}

{% blocktrans trimmed with name=connector.title credit_left=connector.credit_left %}
  There are only {{ credit_left }} credits left for connector {{ name }}.
{% endblocktrans %}

{% blocktrans trimmed %}
  Please add more credit as soon as possible for your account.
{% endblocktrans %}

{% trans "View connector page:" %} {{ connector_url }}

{% endautoescape %}
{% endblock %}

