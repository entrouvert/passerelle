# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django import forms
from django.utils.translation import gettext_lazy as _

from passerelle.forms import GenericConnectorForm


class SmsTestSendForm(forms.Form):
    number = forms.CharField(label=_('To'), max_length=12)
    sender = forms.CharField(label=_('From'), max_length=12)
    message = forms.CharField(label=_('Message'), max_length=128)


class SMSConnectorForm(GenericConnectorForm):
    def __init__(self, *args, **kwargs):
        from passerelle.sms.models import SMSResource

        super().__init__(*args, **kwargs)
        self.fields['authorized'] = forms.MultipleChoiceField(
            choices=SMSResource.AUTHORIZED,
            widget=forms.CheckboxSelectMultiple,
            initial=[SMSResource.ALL],
            label=_('Authorized Countries'),
        )
