import collections
import datetime

from django.contrib import messages
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce, TruncDay
from django.http import JsonResponse
from django.utils.timezone import make_aware
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, View

from passerelle.base.models import SkipJob
from passerelle.utils.jsonresponse import APIError
from passerelle.views import GenericConnectorMixin

from .forms import SmsTestSendForm
from .models import SMSLog


class SmsTestSendView(GenericConnectorMixin, FormView):
    template_name = 'passerelle/manage/messages_service_test_send.html'

    def get_form_class(self):
        return SmsTestSendForm

    def get_object(self):
        return self.model.objects.get(slug=self.kwargs['slug'])

    def get_success_url(self):
        connector = self.get_object()
        return connector.get_absolute_url()

    def form_valid(self, form):
        number = form.cleaned_data['number']
        sender = form.cleaned_data['sender']
        message = form.cleaned_data['message']
        connector = self.get_object()
        try:
            warnings = []
            numbers = connector.clean_numbers([number], warnings=warnings)
            if not numbers:
                raise APIError(warnings)
            connector.send_msg(text=message, sender=sender, destinations=numbers[:1], stop=False)
        except (APIError, SkipJob) as exc:
            messages.error(self.request, _('Sending SMS fails: %s' % exc))
        else:
            messages.success(self.request, _('An SMS was just sent'))
        return super().form_valid(form)


class SmsStatisticsView(View):
    def get(self, request, *args, **kwargs):
        if 'time_interval' in request.GET and request.GET['time_interval'] != 'day':
            return JsonResponse({'err': 1, 'err_desc': 'unsupported time interval'})

        logs = SMSLog.objects.filter(appname=kwargs['connector'], slug=kwargs['slug'])
        if 'start' in request.GET:
            start = make_aware(datetime.datetime.strptime(request.GET['start'], '%Y-%m-%d'))
            logs = logs.filter(timestamp__gte=start)
        if 'end' in request.GET:
            end = make_aware(datetime.datetime.strptime(request.GET['end'], '%Y-%m-%d'))
            logs = logs.filter(timestamp__lte=end)

        if request.GET.get('measure') == 'credits':
            logs = logs.annotate(credit_spent=Coalesce('credits', 1))
            total_aggregate = Sum('credit_spent')
        else:
            total_aggregate = Count('id')

        counter = request.GET.get('counter')
        if counter == '_none':
            logs = logs.filter(counter='')
        elif counter:
            logs = logs.filter(counter=counter)

        logs = logs.annotate(day=TruncDay('timestamp'))

        if not request.GET.get('group_by'):
            logs = logs.values('day').annotate(total=total_aggregate).order_by('day')
            days = [log['day'] for log in logs]
            series = [{'label': _('SMS Count'), 'data': [log['total'] for log in logs]}]
        else:
            group_by = request.GET['group_by']
            logs = logs.values('day', group_by).annotate(total=total_aggregate).order_by('day')

            days = logs_by_day = collections.OrderedDict(
                # day1: {group1: total_11, group2: total_12},
                # day2: {group1: total_21}
            )
            seen_group_values = set(
                # group1, group2
            )
            for log in logs:
                totals_by_group = logs_by_day.setdefault(log['day'], {})
                group_value = log[group_by] or _('None')
                totals_by_group[group_value] = log['total']
                seen_group_values.add(group_value)

            logs_by_group = {
                # group1: [total_11, total_21],
                # group2: [total_12, None],
            }
            for group in seen_group_values:
                logs_by_group[group] = [logs.get(group) for logs in logs_by_day.values()]

            series = [{'label': group, 'data': data} for group, data in logs_by_group.items() if any(data)]
            series.sort(key=lambda x: x['label'])

        return JsonResponse(
            {
                'data': {
                    'x_labels': [day.strftime('%Y-%m-%d') for day in days],
                    'series': series,
                }
            }
        )
