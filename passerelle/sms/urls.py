from django.urls import re_path

from . import views

management_urlpatterns = [
    re_path(r'^(?P<slug>[\w,-]+)/test-send/$', views.SmsTestSendView.as_view(), name='sms-test-send'),
]
