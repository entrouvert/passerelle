# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
import itertools
import logging
import urllib.parse

import phonenumbers
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.core.mail import send_mail
from django.core.validators import RegexValidator
from django.db import models
from django.template.loader import render_to_string
from django.urls import re_path, reverse
from django.utils import timezone
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.sms.forms import SMSConnectorForm
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError

SEND_SCHEMA = {
    'type': 'object',
    'required': ['message', 'from', 'to'],
    'properties': {
        'message': {
            'description': 'String message',
            'type': 'string',
        },
        'from': {
            'description': 'Sender number',
            'type': 'string',
        },
        'to': {
            'description': 'Destination numbers',
            'type': 'array',
            'items': {'type': 'string', 'pattern': r'^\+?[-.\s/\d]+$'},
        },
        'counter': {
            'description': 'Counter name',
            'type': ['string', 'null'],
        },
    },
}


def authorized_default():
    return [SMSResource.ALL]


class SMSResource(BaseResource):
    manager_form_base_class = SMSConnectorForm
    category = _('SMS Providers')
    documentation_url = (
        'https://doc-publik.entrouvert.com/admin-fonctionnel/les-tutos/configuration-envoi-sms/'
    )

    _can_send_messages_description = _('Sending messages is limited to the following API users:')

    default_country_code = models.CharField(
        verbose_name=_('Default country code'),
        max_length=3,
        default='33',
        validators=[RegexValidator('^[0-9]*$', _('The country must only contain numbers'))],
    )
    max_message_length = models.IntegerField(
        _('Maximum message length'), help_text=_('Messages over this limit will be truncated.'), default=2000
    )

    manager_view_template_name = 'passerelle/manage/messages_service_view.html'

    FR_METRO = 'fr-metro'
    FR_DOMTOM = 'fr-domtom'
    BE_ = 'be'
    ALL = 'all'
    AUTHORIZED = [
        (FR_METRO, _('France mainland (+33 [67])')),
        (FR_DOMTOM, _('France and DOM/TOM (+33, +262, etc.)')),
        (BE_, _('Belgian (+32 4[5-9]) ')),
        (ALL, _('All')),
    ]
    authorized = ArrayField(
        models.CharField(max_length=32, null=True, choices=AUTHORIZED),
        verbose_name=_('Authorized Countries'),
        default=authorized_default,
    )

    allow_premium_rate = models.BooleanField(
        _('Allow premium rate numbers'),
        default=False,
    )

    @classmethod
    def get_management_urls(cls):
        return import_string('passerelle.sms.urls.management_urlpatterns')

    @classmethod
    def get_statistics_urls(cls):
        from .views import SmsStatisticsView

        statistics_urlpatterns = [
            re_path(
                r'^(?P<slug>[\w,-]+)/sms-count/$',
                SmsStatisticsView.as_view(),
                name='api-statistics-sms-%s' % cls.get_connector_slug(),
            ),
        ]
        return statistics_urlpatterns

    def _get_authorized_display(self):
        result = []
        for key, value in self.AUTHORIZED:
            if key in self.authorized:
                result.append(str(value))
        return ', '.join(result)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.get_authorized_display = self._get_authorized_display

    zones = {
        FR_METRO: ['FR'],
        FR_DOMTOM: [
            'FR',
            'RE',  # Réunion, Mayotte, Terres australe/antarctiques
            'YT',
            'BL',  # Guadeloupe, Saint-Barthélemy, Saint-Martin
            'GP',
            'MF',
            'GF',  # Guyane
            'MQ',  # Martinique
            'PM',  # Saint-Pierre-et-Miquelon
        ],
        BE_: ['BE'],
    }

    def clean_numbers(self, destinations, warnings=None):
        good_number_types = [phonenumbers.PhoneNumberType.MOBILE]
        if self.allow_premium_rate:
            good_number_types.append(phonenumbers.PhoneNumberType.PREMIUM_RATE)

        region_codes = list(
            itertools.chain.from_iterable(self.zones.get(zone, []) for zone in self.authorized)
        )
        try:
            default_country_code = int(self.default_country_code)
        except ValueError:
            pass
        else:
            default_region_code = phonenumbers.region_code_for_country_code(default_country_code)
            if default_region_code != 'ZZ':
                region_codes = [default_region_code] + region_codes
        all_codes = self.ALL in self.authorized

        cleaned = []
        for dest in destinations:
            pn = None
            for region_code in region_codes:
                try:
                    pn = phonenumbers.parse(dest, region_code)
                except phonenumbers.NumberParseException:
                    continue
                is_valid_country_code = all_codes or (
                    phonenumbers.region_code_for_country_code(pn.country_code) in region_codes
                )
                is_valid = phonenumbers.is_valid_number(pn)
                is_good_type = any(
                    phonenumbers.number_type(pn) == number_type for number_type in good_number_types
                )
                if is_valid and is_valid_country_code and is_good_type:
                    break
                pn = None

            if not pn:
                if warnings is not None:
                    warnings.append('invalid phone number %s' % dest)
                continue

            new_destination = '00' + phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.E164)[1:]
            cleaned.append(new_destination)
        return cleaned

    @endpoint(
        perm='can_send_messages',
        methods=['post'],
        description=_('Send a SMS message'),
        parameters={'nostop': {'description': _('Do not send STOP instruction'), 'example_value': '1'}},
        post={'request_body': {'schema': {'application/json': SEND_SCHEMA}}},
    )
    def send(self, request, post_data, nostop=None):
        post_data['message'] = post_data['message'][: self.max_message_length]
        warnings = []
        destinations = self.clean_numbers(post_data['to'], warnings=warnings)
        logging.info('sending SMS to %r from %r', post_data['to'], post_data['from'])
        stop = nostop is None  # ?nostop in not in query string
        if not destinations:
            raise APIError(
                'no phone number was authorized: %s' % ','.join(post_data['to']), warnings=warnings
            )
        self.add_job(
            'send_job',
            text=post_data['message'],
            sender=post_data['from'],
            destinations=destinations,
            counter=post_data.get('counter') or '',
            stop=stop,
        )
        return {'err': 0, 'warn': warnings}

    def send_job(self, *args, **kwargs):
        counter_name = kwargs.pop('counter', '')
        credits_spent = self.send_msg(**kwargs)
        SMSLog.objects.create(
            appname=self.get_connector_slug(), slug=self.slug, credits=credits_spent, counter=counter_name
        )
        return {'status_info': {'credits-spent': credits_spent}}

    def get_statistics_entries(self, request):
        group_by_options = [
            {'id': 'counter', 'label': _('Counter name')},
        ]

        logs = SMSLog.objects.filter(appname=self.get_connector_slug(), slug=self.slug)
        counter_options = [{'id': '_none', 'label': _('None')}] + [
            {'id': x, 'label': x} for x in logs.values_list('counter', flat=True).distinct() if x
        ]

        return [
            {
                'name': _('SMS Count (%s)') % self.slug,
                'url': request.build_absolute_uri(
                    reverse('api-statistics-sms-%s' % self.get_connector_slug(), kwargs={'slug': self.slug}),
                ),
                'id': 'sms_count_%s_%s' % (self.get_connector_slug(), self.slug),
                'filters': [
                    {
                        'id': 'time_interval',
                        'label': _('Interval'),
                        'options': [{'id': 'day', 'label': _('Day')}],
                        'required': True,
                        'default': 'day',
                    },
                    {
                        'id': 'measure',
                        'label': _('Measure'),
                        'options': [
                            {'id': 'count', 'label': _('Sent SMS count')},
                            {'id': 'credits', 'label': _('Spent SMS credit')},
                        ],
                        'required': True,
                        'default': 'count',
                    },
                    {
                        'id': 'group_by',
                        'label': _('Group by'),
                        'options': group_by_options,
                    },
                    {
                        'id': 'counter',
                        'label': _('Counter name'),
                        'options': counter_options,
                    },
                ],
            }
        ]

    class Meta:
        abstract = True


class SMSLog(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    appname = models.CharField(max_length=128, verbose_name='appname', null=True)
    slug = models.CharField(max_length=128, verbose_name='slug', null=True)
    credits = models.PositiveSmallIntegerField(null=True)
    counter = models.CharField(max_length=256)

    def __str__(self):
        return '%s %s %s' % (self.timestamp, self.appname, self.slug)


class TrackCreditSMSResource(SMSResource):
    credit_threshold_alert = models.PositiveIntegerField(
        verbose_name=_('Credit alert threshold'), default=500
    )
    credit_left = models.PositiveIntegerField(verbose_name=_('Credit left'), default=0, editable=False)
    alert_emails = ArrayField(
        models.EmailField(blank=True),
        blank=True,
        null=True,
        verbose_name=_('Email addresses list to send credit alerts to, separated by comma'),
    )
    credit_alert_timestamp = models.DateTimeField(null=True, editable=False)

    def update_credit_left(self):
        raise NotImplementedError()

    def send_credit_alert_if_needed(self):
        if self.credit_left >= self.credit_threshold_alert:
            return
        if self.credit_alert_timestamp and self.credit_alert_timestamp > timezone.now() - datetime.timedelta(
            days=1
        ):
            return  # alerts are sent daily
        ctx = {
            'connector': self,
            'connector_url': urllib.parse.urljoin(settings.SITE_BASE_URL, self.get_absolute_url()),
        }
        subject = render_to_string('sms/credit_alert_subject.txt', ctx).strip()
        body = render_to_string('sms/credit_alert_body.txt', ctx)
        html_body = render_to_string('sms/credit_alert_body.html', ctx)
        send_mail(
            subject,
            body,
            settings.DEFAULT_FROM_EMAIL,
            self.alert_emails,
            html_message=html_body,
        )
        self.credit_alert_timestamp = timezone.now()
        self.save()
        self.logger.warning('credit is too low, alerts were sent to %s', self.alert_emails)

    def check_status(self):
        self.update_credit_left()
        self.send_credit_alert_if_needed()

    class Meta:
        abstract = True
