from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('base', '0005_resourcelog'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResourceStatus',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('resource_pk', models.PositiveIntegerField()),
                ('start_timestamp', models.DateTimeField(auto_now_add=True)),
                (
                    'status',
                    models.CharField(
                        default=b'unknown',
                        max_length=20,
                        choices=[(b'unknown', 'Unknown'), (b'up', 'Up'), (b'down', 'Down')],
                    ),
                ),
                ('message', models.CharField(max_length=500, blank=True)),
                ('resource_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['-start_timestamp'],
            },
        ),
    ]
