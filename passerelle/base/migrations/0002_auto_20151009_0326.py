from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apiuser',
            name='username',
            field=models.CharField(max_length=128, verbose_name='Username'),
            preserve_default=True,
        ),
    ]
