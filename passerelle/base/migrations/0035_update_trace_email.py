import logging

from django.db import migrations


def update_trace_emails(apps, schema_editor):
    try:
        model = apps.get_model('base', 'LoggingParameters')
        # This is a TextField. We may have some trailing \n
        objs = model.objects.filter(trace_emails__startswith='noreply@entrouvert.com')
        to_update = []
        for o in objs:
            if o.trace_emails.strip() == 'noreply@entrouvert.com':
                o.trace_emails = '-'
                to_update.append(o)
        if to_update:
            model.objects.bulk_update(to_update, ['trace_emails'])
    except Exception:
        logging.exception('An error occured while upgrading trace_emails')


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0034_auto_20241022_1753'),
    ]

    operations = [
        migrations.RunPython(update_trace_emails),
    ]
