from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0025_transaction_id'),
    ]

    operations = [
        migrations.RunSQL(
            ["UPDATE base_resourcelog SET transaction_id=(extra->>'transaction_id')::uuid"], reverse_sql=[]
        ),
    ]
