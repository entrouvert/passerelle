from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apiuser',
            name='username',
            field=models.CharField(unique=True, max_length=128, verbose_name='Username'),
            preserve_default=True,
        ),
    ]
