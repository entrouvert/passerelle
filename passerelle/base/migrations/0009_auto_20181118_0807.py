# Generated by Django 1.11.12 on 2018-11-18 14:07

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('base', '0008_auto_20181118_0717'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='loggingparameters',
            unique_together={('resource_type', 'resource_pk')},
        ),
    ]
