from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessRight',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('codename', models.CharField(max_length=100, verbose_name=b'codename')),
                ('resource_pk', models.PositiveIntegerField()),
            ],
            options={
                'permissions': (('see_accessright', 'Can see access right'),),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ApiUser',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('username', models.CharField(max_length=50, verbose_name='Username')),
                ('fullname', models.CharField(max_length=50, verbose_name='Full Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                (
                    'keytype',
                    models.CharField(
                        blank=True,
                        max_length=4,
                        verbose_name='Key Type',
                        choices=[(b'API', b'API Key'), (b'SIGN', b'HMAC Signature')],
                    ),
                ),
                ('key', models.CharField(max_length=256, verbose_name='Key', blank=True)),
                (
                    'ipsource',
                    models.GenericIPAddressField(
                        unpack_ipv4=True, null=True, verbose_name='IP Address', blank=True
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TemplateVar',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=64)),
                ('value', models.CharField(max_length=128)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='accessright',
            name='apiuser',
            field=models.ForeignKey(to='base.ApiUser', verbose_name='API User', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accessright',
            name='resource_type',
            field=models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
