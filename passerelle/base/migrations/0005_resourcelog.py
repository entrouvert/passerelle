from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0004_auto_20170117_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResourceLog',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('appname', models.CharField(max_length=128, null=True, verbose_name=b'appname')),
                ('slug', models.CharField(max_length=128, null=True, verbose_name=b'slug')),
                ('levelno', models.IntegerField(verbose_name=b'log level')),
                ('sourceip', models.GenericIPAddressField(null=True, verbose_name='Source IP', blank=True)),
                ('message', models.TextField(verbose_name=b'message')),
                ('extra', models.JSONField(default={}, verbose_name=b'extras')),
            ],
            options={
                'permissions': (('see_resourcelog', 'Can see resource logs'),),
            },
        ),
    ]
