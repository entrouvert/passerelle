from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0003_auto_20170116_1656'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='accessright',
            unique_together={('codename', 'resource_type', 'resource_pk', 'apiuser')},
        ),
    ]
