from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0026_transaction_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resourcelog',
            name='transaction_id',
            field=models.UUIDField(db_index=True, null=True),
        ),
    ]
