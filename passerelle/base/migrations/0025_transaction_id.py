from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0024_auto_20201103_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='resourcelog',
            name='transaction_id',
            field=models.UUIDField(null=True),
        ),
    ]
