import json
import sys

from django.core.management.base import BaseCommand

from passerelle.utils import export_site


class Command(BaseCommand):
    help = 'Export the site'

    def add_arguments(self, parser):
        parser.add_argument('--slugs', nargs='+', default=None, help='specify resources to export')
        parser.add_argument(
            '--output', metavar='FILE', default=None, help='name of a file to write output to'
        )

    def handle(self, *args, **options):
        if options['output']:
            with open(options['output'], 'w') as output:
                json.dump(export_site(slugs=options['slugs']), output, indent=4)
                return
        output = sys.stdout
        json.dump(export_site(slugs=options['slugs']), output, indent=4)
