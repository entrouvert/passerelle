import json

from django.core.management.base import BaseCommand

from passerelle.utils import import_site


class Command(BaseCommand):
    help = 'Import an exported site'

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME', type=str, help='name of file to import')
        parser.add_argument('--clean', action='store_true', default=False, help='Clean site before importing')
        parser.add_argument(
            '--import-users', action='store_true', default=False, help='Import users and access rights'
        )
        parser.add_argument(
            '--if-empty', action='store_true', default=False, help='Import only if passerelle is empty'
        )
        parser.add_argument(
            '--overwrite', action='store_true', default=False, help='Overwrite existing resources'
        )

    def handle(self, filename, **options):
        with open(filename) as f:
            import_site(
                json.load(f),
                if_empty=options['if_empty'],
                clean=options['clean'],
                overwrite=options['overwrite'],
                import_users=options['import_users'],
            )
