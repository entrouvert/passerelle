# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import traceback

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from passerelle.base.models import ResourceLog
from passerelle.views import get_all_apps


class Command(BaseCommand):
    help = 'Execute scheduled commands'

    def add_arguments(self, parser):
        parser.add_argument(
            'frequency',
            metavar='FREQUENCY',
            type=str,
            help='every5min/hourly/daily/weekly/monthly/availability/jobs',
        )
        parser.add_argument(
            '--connector',
            dest='connector',
            metavar='CONNECTOR',
            type=str,
            help='limit updates to given connector type',
        )
        parser.add_argument(
            '--connector-slug',
            dest='slug',
            metavar='SLUG',
            type=str,
            help='limit updates to given connector slug',
        )

    def handle(self, frequency, **options):
        if frequency not in ('every5min', 'hourly', 'daily', 'weekly', 'monthly', 'availability', 'jobs'):
            raise CommandError('unknown frequency')
        errors = []

        if frequency == 'daily':
            ResourceLog.daily()

        for app in get_all_apps():
            for connector in app.objects.all():
                if options.get('connector') and connector.get_connector_slug() != options.get('connector'):
                    continue
                if options.get('slug') and connector.slug != options.get('slug'):
                    continue
                try:
                    getattr(connector, frequency)()
                except Exception as e:
                    connector.logger.exception(
                        'connector "%s.%s" error running %s job'
                        % (connector.get_connector_slug(), connector.slug, frequency)
                    )
                    errors.append(
                        {'connector': connector, 'exception': e, 'traceback': traceback.format_exc()}
                    )
        if errors:
            for error in errors:
                if options['verbosity'] >= 1:
                    print(
                        repr(error['connector']),
                    )
                    print(
                        '  url:',
                        getattr(settings, 'SITE_BASE_URL', '') + error['connector'].get_absolute_url(),
                    )
                    print('  error:', error['exception'])
                if options['verbosity'] >= 2:
                    print('  traceback:')
                    print(error['traceback'])
                    print()
            raise CommandError('error running jobs')
