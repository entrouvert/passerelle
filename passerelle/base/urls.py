from django.urls import path, re_path

from .views import (
    AccessRightCreateView,
    AccessRightDeleteView,
    ApiUserCreateView,
    ApiUserDeleteView,
    ApiUserListView,
    ApiUserUpdateView,
    ExportSiteView,
    ImportSiteView,
    LoggingParametersUpdateView,
    ManageAvailabilityView,
)

access_urlpatterns = [
    path('', ApiUserListView.as_view(), name='apiuser-list'),
    path('add', ApiUserCreateView.as_view(), name='apiuser-add'),
    re_path(r'^(?P<pk>[\w,-]+)/edit$', ApiUserUpdateView.as_view(), name='apiuser-edit'),
    re_path(r'^(?P<pk>[\w,-]+)/delete$', ApiUserDeleteView.as_view(), name='apiuser-delete'),
    re_path(r'^(?P<pk>[\w,-]+)/remove$', AccessRightDeleteView.as_view(), name='access-right-remove'),
    re_path(
        r'^accessright/add/(?P<resource_type>[\w,-]+)/(?P<resource_pk>[\w,-]+)/(?P<codename>[\w,-]+)/',
        AccessRightCreateView.as_view(),
        name='access-right-add',
    ),
    re_path(
        r'logging/parameters/(?P<resource_type>[\w,-]+)/(?P<resource_pk>[\w,-]+)/$',
        LoggingParametersUpdateView.as_view(),
        name='logging-parameters',
    ),
    re_path(
        r'manage/availability/(?P<resource_type>[\w,-]+)/(?P<resource_pk>[\w,-]+)/$',
        ManageAvailabilityView.as_view(),
        name='manage-availability',
    ),
]

import_export_urlpatterns = [
    path('import', ImportSiteView.as_view(), name='import-site'),
    path('export', ExportSiteView.as_view(), name='export-site'),
]
