{% extends "emails/body_base.txt" %}
{% load i18n %}

{% block content %}{% autoescape off %}{% trans "Hi," %}

{% trans "The service used by the connector" %} «{{ connector_title }} -- {{ connector_slug }}»
{% if is_down %}{% blocktrans trimmed %}
 as been detected as unavailable since {{ start_timestamp|date:"DATETIME_FORMAT" }}
{% endblocktrans %}{% else %}{% trans "is now back up" %}{% endif %}

{% trans "View connector page:" %} {{ connector_url }}

{% endautoescape %}
{% endblock %}
