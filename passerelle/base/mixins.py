# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils import functional


class ResourceChildViewMixin:
    '''Mixin to help implementing view for child objects of resource objets.'''

    @property
    def resource_class(self):
        field = self.model._meta.get_field('resource')
        return field.related_model

    @property
    def model_verbose_name(self):
        return self.model._meta.verbose_name

    @property
    def model_verbose_name_plural(self):
        return self.model._meta.verbose_name_plural

    @functional.cached_property
    def resource(self):
        return self.resource_class.objects.get(slug=self.kwargs['slug'])

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(resource=self.resource)

    def get_success_url(self):
        return self.resource.get_absolute_url()
