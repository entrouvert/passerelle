from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('adict', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adict',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
