from django.db import migrations


def list_to_dict(apps, schema_editor):
    PlanitechConnector = apps.get_model('planitech', 'PlanitechConnector')
    for conn in PlanitechConnector.objects.all():
        if conn.custom_fields and isinstance(conn.custom_fields, list):
            conn.custom_fields = {'places': conn.custom_fields}
            conn.save()


class Migration(migrations.Migration):
    dependencies = [
        ('planitech', '0006_text_to_jsonb'),
    ]

    operations = [
        migrations.RunPython(list_to_dict),
    ]
