# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import UTC

from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, localtime, make_aware, make_naive


def localtz_to_utc(date_time_string):
    datetime = parse_datetime(date_time_string)
    if not is_aware(datetime):
        datetime = make_aware(datetime, is_dst=False)
    aware_dt_in_utc = datetime.astimezone(UTC)
    return aware_dt_in_utc.isoformat()


def utc_to_localtz(date_time_string):
    if date_time_string is None:
        return None
    aware_dt_in_local_tz = localtime(parse_datetime(date_time_string))
    return make_naive(aware_dt_in_local_tz).isoformat()
