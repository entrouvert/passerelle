# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE_SCHEMA = {
    'type': 'object',
    'properties': {
        'slug': {
            'description': 'slug du block de champs intervention',
            'type': 'string',
        },
        'description': {
            'description': 'Description de la demande',
            'type': 'string',
        },
        'lat': {
            'description': 'Latitude',
            'example_value': '45.79689',
        },
        'lon': {
            'description': 'Longitude',
            'example_value': '4.78414',
        },
        'cityId': {
            'description': 'Code INSEE de la commune',
            'type': 'string',
        },
        'safeguardRequired': {
            'description': 'Présence d’un danger ?',
            'type': 'boolean',
        },
        'interventionCreated': {
            'description': 'Date de création de la demande',
            'type': 'string',
            'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}',
        },
        'interventionDesired': {
            'description': 'Date d’intervention souhaitée',
            'type': 'string',
            'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}',
        },
        'submitterFirstName': {
            'description': 'Prénom du demandeur',
            'type': 'string',
        },
        'submitterLastName': {
            'description': 'Nom du demandeur',
            'type': 'string',
        },
        'submitterMail': {
            'description': 'Adresse mail du demandeur',
            'type': 'string',
        },
        'submitterPhone': {
            'description': 'Numéro de téléphone du demandeur',
            'type': 'string',
        },
        'submitterAddress': {
            'description': 'Adresse du demandeur',
            'type': 'string',
        },
        'submitterType': {
            'description': 'Type de demandeur (Commune, élu, usager…)',
            'type': 'string',
        },
        'onPrivateLand': {
            'description': 'Intervention sur le domaine privé ?',
            'type': 'boolean',
        },
        'checkDuplicated': {
            'description': 'Activation de la détection de doublon (laisser à false pour le moment)',
            'type': 'boolean',
        },
        'externalReferences': {
            'description': 'Réference Externe',
            'type': 'string',
        },
        'external_number': {
            'description': 'Numéro externe de la demande (numéro Publik : {{ form_number }})',
            'type': 'string',
            'pattern': '[0-9]+[0-9]+',
        },
        'external_status': {
            'description': 'Statut externe de la demande (statut Publik : {{ form_status }})',
            'type': 'string',
        },
        'address': {
            'description': 'Rue',
            'type': 'string',
        },
        'city': {
            'description': 'Ville',
            'type': 'string',
        },
        'postalCode': {
            'description': 'Code postal',
            'type': 'string',
        },
        'houseNumber': {
            'description': 'Numéro de voierie',
            'type': 'string',
        },
        'form_api_url': {
            'description': "L'adresse vers la vue API du formulaire : {{ form_api_url }}",
            'type': 'string',
        },
        'form_step': {
            'description': 'Étape du traitement de la demande',
            'type': 'string',
            'default': 'initial',
        },
    },
    'required': [
        'slug',
        'description',
        'lat',
        'lon',
        'cityId',
        'interventionCreated',
        'interventionDesired',
        'submitterFirstName',
        'submitterLastName',
        'submitterMail',
        'submitterPhone',
        'submitterAddress',
        'submitterType',
        'external_number',
        'external_status',
        'form_api_url',
    ],
    'merge_extra': True,
}

UPDATE_SCHEMA = {
    'type': 'object',
    'properties': {
        'data': {
            'type': 'object',
            'properties': {
                'status': {
                    'description': "Nouveau Statut de l'intervention",
                    'type': 'string',
                    'maxLength': 20,
                },
                'date_planification': {
                    'description': "Date de planification de l'intervention",
                    'type': 'string',
                    'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}',
                },
                'date_mise_en_securite': {
                    'description': "Date de mise en securite de l'intervention",
                    'type': 'string',
                    'pattern': '[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}',
                },
                'type_retour': {
                    'description': 'Type du retour',
                    'type': 'string',
                },
                'type_retour_cloture': {
                    'description': 'Type du retour de la clôture',
                    'type': 'string',
                },
                'libelle_cloture': {
                    'description': 'Libellé de la clôture',
                    'type': 'string',
                },
                'commentaire_cloture': {
                    'description': 'Commentaire de la clôture',
                    'type': 'string',
                },
            },
            'required': ['status'],
        },
    },
    'required': ['data'],
}


MEDIA_SCHEMA = {
    'type': 'object',
    'properties': {
        'files': {
            'type': 'array',
            'items': {
                'oneOf': [
                    {
                        'type': 'null',
                    },
                    {
                        'type': 'object',
                        'properties': {
                            'filename': {
                                'description': 'Nom du ficher',
                                'type': 'string',
                            },
                            'content_type': {
                                'description': 'Type MIME',
                                'type': 'string',
                            },
                            'content': {
                                'description': 'Contenu',
                                'type': 'string',
                            },
                        },
                        'required': ['filename', 'content_type', 'content'],
                    },
                ],
            },
        },
    },
    'required': ['files'],
    'unflatten': True,
}
