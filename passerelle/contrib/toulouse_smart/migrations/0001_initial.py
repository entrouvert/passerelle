# Generated by Django 2.2.19 on 2021-05-06 22:50

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0029_auto_20210202_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='ToulouseSmartResource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'basic_auth_username',
                    models.CharField(
                        blank=True, max_length=128, verbose_name='Basic authentication username'
                    ),
                ),
                (
                    'basic_auth_password',
                    models.CharField(
                        blank=True, max_length=128, verbose_name='Basic authentication password'
                    ),
                ),
                (
                    'client_certificate',
                    models.FileField(
                        blank=True, null=True, upload_to='', verbose_name='TLS client certificate'
                    ),
                ),
                (
                    'trusted_certificate_authorities',
                    models.FileField(blank=True, null=True, upload_to='', verbose_name='TLS trusted CAs'),
                ),
                (
                    'verify_cert',
                    models.BooleanField(blank=True, default=True, verbose_name='TLS verify certificates'),
                ),
                (
                    'http_proxy',
                    models.CharField(blank=True, max_length=128, verbose_name='HTTP and HTTPS proxy'),
                ),
                ('webservice_base_url', models.URLField(verbose_name='Webservice Base URL')),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Toulouse Smart',
            },
        ),
        migrations.CreateModel(
            name='Cache',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('key', models.CharField(max_length=64, verbose_name='Key')),
                ('timestamp', models.DateTimeField(auto_now=True, verbose_name='Timestamp')),
                ('value', models.JSONField(default=dict, verbose_name='Value')),
                (
                    'resource',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='toulouse_smart.ToulouseSmartResource',
                        verbose_name='Resource',
                    ),
                ),
            ],
        ),
    ]
