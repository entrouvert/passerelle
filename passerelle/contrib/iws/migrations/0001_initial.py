from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0006_resourcestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='IWSConnector',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'wsdl_url',
                    models.URLField(
                        help_text='URL of the SOAP wsdl endpoint',
                        max_length=400,
                        verbose_name='SOAP wsdl endpoint',
                    ),
                ),
                (
                    'operation_endpoint',
                    models.URLField(
                        help_text='URL of SOAP operation endpoint',
                        max_length=400,
                        verbose_name='SOAP operation endpoint',
                    ),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Service username')),
                (
                    'password',
                    models.CharField(max_length=128, null=True, verbose_name='Service password', blank=True),
                ),
                ('database', models.CharField(max_length=128, verbose_name='Service database')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'IWS connector',
            },
        ),
    ]
