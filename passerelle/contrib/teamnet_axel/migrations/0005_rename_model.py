from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('teamnet_axel', '0004_auto_20170920_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='resource',
            field=models.IntegerField(db_index=True),
        ),
        migrations.RenameModel('Management', 'TeamnetAxel'),
    ]
