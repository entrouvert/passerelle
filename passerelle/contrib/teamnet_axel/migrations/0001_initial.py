from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('nameid', models.CharField(max_length=256)),
                ('login', models.CharField(max_length=128)),
                ('pwd', models.CharField(max_length=128)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Management',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'wsdl_url',
                    models.CharField(
                        help_text='Teamnet Axel WSDL URL', max_length=128, verbose_name='WSDL URL'
                    ),
                ),
                (
                    'verify_cert',
                    models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Username', blank=True)),
                ('password', models.CharField(max_length=128, verbose_name='Password', blank=True)),
                (
                    'keystore',
                    models.FileField(
                        help_text='Certificate and private key in PEM format',
                        upload_to=b'teamnet_axel',
                        null=True,
                        verbose_name='Keystore',
                        blank=True,
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'Teamnet Axel',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='link',
            name='resource',
            field=models.ForeignKey(to='teamnet_axel.Management', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
