from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('teamnet_axel', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='management',
            name='billing_regies',
            field=models.JSONField(default=dict, verbose_name='Mapping between regie ids and billing ids'),
            preserve_default=True,
        ),
    ]
