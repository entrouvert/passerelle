from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('teamnet_axel', '0003_management_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='management',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
