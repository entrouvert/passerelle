from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('teamnet_axel', '0005_rename_model'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='resource',
            field=models.ForeignKey(to='teamnet_axel.TeamnetAxel', on_delete=models.CASCADE),
        ),
    ]
