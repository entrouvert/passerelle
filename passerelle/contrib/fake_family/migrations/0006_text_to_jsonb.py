# Generated by Django 1.11.18 on 2020-05-04 12:06

from django.db import migrations

from passerelle.utils.db import EnsureJsonbType


class Migration(migrations.Migration):
    dependencies = [
        ('fake_family', '0005_auto_20200504_1402'),
    ]

    operations = [
        EnsureJsonbType(model_name='FakeFamily', field_name='jsondatabase'),
    ]
