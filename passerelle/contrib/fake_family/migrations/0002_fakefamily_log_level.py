from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fake_family', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='fakefamily',
            name='log_level',
            field=models.CharField(
                default=b'NOTSET',
                max_length=10,
                verbose_name='Log Level',
                choices=[
                    (b'NOTSET', b'NOTSET'),
                    (b'DEBUG', b'DEBUG'),
                    (b'INFO', b'INFO'),
                    (b'WARNING', b'WARNING'),
                    (b'ERROR', b'ERROR'),
                    (b'CRITICAL', b'CRITICAL'),
                ],
            ),
            preserve_default=True,
        ),
    ]
