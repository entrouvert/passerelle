from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('fake_family', '0002_fakefamily_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fakefamily',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
