# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import re
from datetime import datetime, timezone

from django.db import models
from django.utils.translation import gettext_lazy as _
from requests import ConnectionError, RequestException, Timeout

from passerelle.base.models import BaseResource, HTTPResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError


def iso_now():
    now = datetime.now(timezone.utc)
    local_now = now.astimezone()
    return local_now.isoformat()


class IsereESRH(BaseResource, HTTPResource):
    category = _('Business Process Connectors')
    base_url = models.URLField(_('Base API URL'))

    class Meta:
        verbose_name = _('ESRH Isère')

    def _get(self, endpoint, params=None):
        try:
            response = self.requests.get(f'{self.base_url}/api/v2/{endpoint}', params=params)
            response.raise_for_status()
        except (RequestException, ConnectionError, Timeout) as e:
            raise APIError('HTTP request failed', data={'exception': str(e)})

        try:
            response_json = response.json()
        except ValueError as e:
            raise APIError('ESRH returned invalid json', data={'exception': str(e)})

        if (
            not isinstance(response_json, dict)
            or response_json.get('values') is None
            or not isinstance(response_json['values'], list)
            or any(not isinstance(item, dict) for item in response_json['values'])
        ):
            raise APIError(
                'ESRH returned malformed json : expecting a dictionary with a "values" key containing a list of objects.'
            )

        return response_json['values']

    @endpoint(
        description=_('Get official informations'),
        parameters={
            'number': {'description': _('Official registration number'), 'example_value': '500'},
            'authority': {'description': _('Public authority'), 'example_value': 'CG38'},
        },
    )
    def official(self, request, number, authority):
        agents = self._get('Agent', params={'numero': number, 'collectivite': authority})

        if len(agents) == 0:
            return {'data': None}

        agent = agents[0]

        if 'agentId' not in agent:
            raise APIError(
                'Malformed response : values elements are expected to be objects with an "agentId" key'
            )

        agent_id = agent['agentId']

        agent['DossiersStatutaire'] = self._get(
            f'Agent/{agent_id}/DossiersStatutaire', params={'aDate': iso_now()}
        )

        return {'data': agent}

    @endpoint(
        description=_('Get entities'),
        parameters={
            'label_pattern': {
                'description': _('Filter entities whose label matches this regex (case insensitive)'),
                'example_value': '^dir\\..*',
            },
            'code_pattern': {
                'description': _('Filter entities whose code matches this regex (case insensitive)'),
                'example_value': '^6517.*',
            },
        },
    )
    def entities(self, request, label_pattern=None, code_pattern=None):
        entities = self._get('Entite', params={'aDate': iso_now()})

        if label_pattern:
            label_pattern = re.compile(label_pattern, re.IGNORECASE)

        if code_pattern:
            code_pattern = re.compile(code_pattern, re.IGNORECASE)

        result = []
        for entity in entities:
            id = entity.get('entiteId')
            label = entity.get('libelle', '')
            code = entity.get('code', '')

            if label_pattern and not label_pattern.match(label):
                continue

            if code_pattern and not code_pattern.match(code):
                continue

            result.append(entity | {'id': id, 'text': label})

        return {'data': result}

    @endpoint(
        name='job-types',
        description=_('Get job types'),
        parameters={
            'authority': {'description': _('Public authority'), 'example_value': 'CG38'},
        },
    )
    def job_types(self, request, authority):
        job_types = self._get(
            'Poste', params={'codeCollectivite': authority, 'avecLibellePoste': True, 'aDate': iso_now()}
        )

        result = []
        for job_type in job_types:
            id = job_type.get('posteId')
            labels = job_type.get('libelles', [])
            label = 'N/A' if len(labels) == 0 else labels[0]['libelle']

            result.append(job_type | {'id': id, 'text': label})

        return {'data': result}
