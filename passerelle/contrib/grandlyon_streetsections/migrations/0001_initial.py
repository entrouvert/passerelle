from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0004_auto_20170117_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='GrandLyonStreetSections',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Sections of Grand Lyon Streets',
            },
        ),
        migrations.CreateModel(
            name='StreetSection',
            options={'ordering': ['normalized_name', 'nomcommune', 'bornemindroite', 'bornemingauche']},
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('bornemindroite', models.PositiveIntegerField(null=True)),
                ('bornemingauche', models.PositiveIntegerField(null=True)),
                ('bornemaxdroite', models.PositiveIntegerField(null=True)),
                ('bornemaxgauche', models.PositiveIntegerField(null=True)),
                ('gid', models.PositiveIntegerField(null=True)),
                ('nomcommune', models.CharField(max_length=50)),
                ('nom', models.CharField(max_length=200)),
                ('domanialite', models.CharField(max_length=50)),
                ('codefuv', models.CharField(max_length=15)),
                ('codetroncon', models.CharField(max_length=15)),
                ('normalized_name', models.CharField(max_length=200)),
                ('last_update', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
