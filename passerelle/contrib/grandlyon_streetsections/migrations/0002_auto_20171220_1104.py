from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('grandlyon_streetsections', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grandlyonstreetsections',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
