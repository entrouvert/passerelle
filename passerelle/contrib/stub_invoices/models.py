# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import random
from decimal import Decimal

from django.http import HttpResponse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint


class StubInvoicesConnector(BaseResource):
    category = _('Stub Connectors')

    class Meta:
        verbose_name = _('Invoices')

    # generate a serie of stub invoices
    invoices = {}
    for i in range(15):
        now = timezone.now()
        id_ = '%d%04d' % (now.year, i + 1)
        invoices[id_] = {
            'id': id_,
            'display_id': id_,
            'total_amount': Decimal(random.randint(100, 10000)) / 100,
            'has_pdf': bool(i % 3),
            'created': (now - datetime.timedelta(days=20) + datetime.timedelta(days=i)).date(),
            'label': 'Label %s' % id_,
            'pay_limit_date': (now + datetime.timedelta(days=2 + random.randint(0, 10))).date(),
            'online_payment': bool(i % 2),
            'paid': False,
        }
        if i < 5:
            invoices[id_]['payment_date'] = invoices[id_]['created'] + datetime.timedelta(
                days=1 + random.randint(0, 3)
            )
            invoices[id_]['online_payment'] = False
            invoices[id_]['paid'] = True
        elif invoices[id_]['online_payment'] is False:
            invoices[id_]['no_online_payment_reason'] = random.choice(['autobilling', 'litigation'])

        invoices[id_]['amount'] = invoices[id_]['total_amount']

    def get_invoices(self):
        return self.invoices.values()

    def get_invoice(self, invoice_id):
        return self.invoices.get(invoice_id)

    @endpoint(
        name='invoices',
        pattern='^history/$',
        perm='OPEN',
        description=_('Get list of paid invoices'),
        example_pattern='history/',
    )
    def invoices_history(self, request, NameID=None, **kwargs):
        return {'data': [x for x in self.get_invoices() if x.get('payment_date')]}

    @endpoint(name='invoices', description=_('Get list of unpaid invoices'))
    def invoices_list(self, request, NameID=None, **kwargs):
        return {'data': [x for x in self.get_invoices() if not x.get('payment_date')]}

    @endpoint(
        name='invoice',
        pattern=r'^(?P<invoice_id>\w+)/?$',
        perm='OPEN',
        description=_('Get invoice details'),
        example_pattern='{invoice_id}/',
        parameters={
            'invoice_id': {
                'description': _('Invoice identifier'),
                'example_value': list(invoices.keys())[0],
            }
        },
    )
    def invoice(self, request, invoice_id, NameID=None, **kwargs):
        return {'data': self.get_invoice(invoice_id)}

    @endpoint(
        name='invoice',
        pattern=r'^(?P<invoice_id>\w+)/pdf/?$',
        perm='OPEN',
        description=_('Get invoice as a PDF file'),
        long_description=_('not yet implemented'),
        example_pattern='{invoice_id}/pdf/',
        parameters={
            'invoice_id': {
                'description': _('Invoice identifier'),
                'example_value': list(invoices.keys())[0],
            }
        },
    )
    def invoice_pdf(self, request, invoice_id, NameID=None, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % invoice_id
        response.write('')
        return response

    @endpoint(
        name='invoice',
        methods=['post'],
        pattern=r'^(?P<invoice_id>\w+)/pay/?$',
        description=_('Pay invoice'),
        long_description=_('not yet implemented'),
        example_pattern='{invoice_id}/pay/',
        parameters={
            'invoice_id': {
                'description': _('Invoice identifier'),
                'example_value': list(invoices.keys())[0],
            }
        },
    )
    def invoice_pay(self, request, invoice_id, NameID=None, **kwargs):
        return {'data': None}
