from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('stub_invoices', '0002_stubinvoicesconnector_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stubinvoicesconnector',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
