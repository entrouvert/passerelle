from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Greco',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'NOTSET',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                ('application', models.CharField(max_length=200, verbose_name='Application identifier')),
                ('token_url', models.URLField(max_length=256, verbose_name='Token URL')),
                ('token_authorization', models.CharField(max_length=128, verbose_name='Token Authorization')),
                ('wsdl_url', models.CharField(max_length=256, verbose_name='WSDL or REST URL')),
                (
                    'verify_cert',
                    models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'GRECO Webservices',
            },
        ),
    ]
