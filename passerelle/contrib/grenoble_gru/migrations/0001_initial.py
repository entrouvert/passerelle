from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0006_resourcestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='GrenobleGRU',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'base_url',
                    models.URLField(
                        help_text='Grenoble GRU API base URL', max_length=256, verbose_name='Base URL'
                    ),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Username')),
                ('password', models.CharField(max_length=128, verbose_name='Password')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Grenoble - Gestion des signalements',
            },
        ),
    ]
