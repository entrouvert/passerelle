from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('solis_apa', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solisapa',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
