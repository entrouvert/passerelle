# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _

from passerelle.apps.soap.models import AbstractSOAPConnector
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError

DICT_SCHEMA = {
    'type': 'object',
    'additionalProperties': True,
    'unflatten': True,
}


def lax_int(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        return None


class SOAPScrib(AbstractSOAPConnector):
    category = _('Business Process Connectors')

    class Meta:
        verbose_name = _('Scrib SOAP API')

    @endpoint(
        name='depot',
        description=_('Clean payload and sent it to method/depot'),
        display_order=-1,
        display_category=_('Publik compatible API'),
        methods=['post'],
        post={'request_body': {'schema': {'application/json': DICT_SCHEMA}}},
    )
    def depot(self, request, post_data):
        # clean "bac" list: keep only bacCollecteXXX if bacCollecteXXX is True
        if not isinstance(post_data.get('demandeBacs', {}).get('bac'), list):
            raise APIError('missing demandeBacs/bac list', http_status=400)

        demandeBacs = post_data['demandeBacs']

        cleaned_bacs = []
        for bac in demandeBacs['bac']:
            if not isinstance(bac, dict):
                continue
            cleaned_bac = {}

            # transfer bac* and numBac* keys
            for key, value in bac.items():
                if not key.startswith('bac'):
                    continue

                if value not in (True, 'true', 'True', 1, '1'):
                    continue

                cleaned_bac[key] = 'true'
                numkey = 'numBac' + key[3:]
                num = (bac.get(numkey, '') or '').strip()
                if num:
                    cleaned_bac[numkey] = num

            if not cleaned_bac:
                continue

            # transfer other non empty keys
            for key, value in bac.items():
                if key.startswith(('bac', 'numBac')):
                    continue
                if value is None or value == '':
                    continue
                cleaned_bac[key] = value

            cleaned_bacs.append(cleaned_bac)

        demandeBacs['bac'] = cleaned_bacs

        # remove numeroVoie if empty or non-integer (xsd:int/xsd:short in wsdl)
        for key in ('numeroVoie', 'nombrePersonnesFoyer'):
            if key in demandeBacs:
                demandeBacs[key] = lax_int(demandeBacs[key])
                if demandeBacs[key] is None:  # was not an integer, remove it
                    del demandeBacs[key]

        # remove raisonDemande and typeDegradation if empty (or not string)
        for key in ('raisonDemande', 'typeDegradation'):
            if key in demandeBacs:
                if isinstance(demandeBacs[key], str) and demandeBacs[key].strip():
                    continue
                del demandeBacs[key]

        response = self.method(request, method_name='depot', post_data=post_data)
        codeRetour = response['data'].get('codeRetour')  # should be True
        codeErreur = response['data'].get('codeErreur')  # should be empty
        if not codeRetour or codeErreur:
            response['err'] = 1
        return response
