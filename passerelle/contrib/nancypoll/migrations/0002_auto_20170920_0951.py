from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nancypoll', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nancypoll',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
