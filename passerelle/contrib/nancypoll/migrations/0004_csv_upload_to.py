from django.db import migrations, models

import passerelle.contrib.nancypoll.models
import passerelle.utils.models


class Migration(migrations.Migration):
    dependencies = [
        ('nancypoll', '0003_remove_nancypoll_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nancypoll',
            name='csv_file',
            field=models.FileField(
                upload_to=passerelle.utils.models.resource_file_upload_to, verbose_name='CSV File'
            ),
        ),
    ]
