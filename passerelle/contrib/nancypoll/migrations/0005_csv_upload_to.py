import os

from django.db import migrations


def move_files(apps, schema_editor):
    NancyPoll = apps.get_model('nancypoll', 'NancyPoll')
    NancyPoll.get_connector_slug = lambda *args, **kwargs: 'nancypoll'
    for instance in NancyPoll.objects.all():
        old_name = instance.csv_file.name
        old_path = instance.csv_file.path
        if not old_name.startswith('csv/'):
            continue
        filename = os.path.basename(old_name)
        instance.csv_file.save(filename, instance.csv_file)
        os.unlink(old_path)


class Migration(migrations.Migration):
    dependencies = [
        ('nancypoll', '0004_csv_upload_to'),
    ]

    operations = [
        migrations.RunPython(move_files, reverse_code=migrations.RunPython.noop),
    ]
