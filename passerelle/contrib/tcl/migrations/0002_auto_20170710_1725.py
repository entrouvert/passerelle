from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('tcl', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='stop',
            name='latitude',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='stop',
            name='longitude',
            field=models.FloatField(null=True),
        ),
    ]
