from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('tcl', '0002_auto_20170710_1725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tcl',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
