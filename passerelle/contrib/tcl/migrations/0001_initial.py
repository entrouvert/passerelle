from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0005_resourcelog'),
    ]

    operations = [
        migrations.CreateModel(
            name='Line',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('transport_key', models.CharField(max_length=20)),
                ('indice', models.CharField(max_length=20, blank=True)),
                ('couleur', models.CharField(max_length=20)),
                ('ut', models.CharField(max_length=10)),
                ('ligne', models.CharField(max_length=10)),
                ('code_titan', models.CharField(max_length=10)),
                ('sens', models.CharField(max_length=10)),
                ('libelle', models.CharField(max_length=100)),
                ('infos', models.CharField(max_length=100, blank=True)),
                ('code_titan_short', models.CharField(max_length=10)),
                ('html_bg_color', models.CharField(max_length=10)),
                ('html_fg_color', models.CharField(max_length=10)),
                ('last_update', models.DateTimeField(auto_now=True, verbose_name='Last update')),
            ],
        ),
        migrations.CreateModel(
            name='Stop',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('id_data', models.CharField(max_length=10)),
                ('nom', models.CharField(max_length=50)),
                ('desserte', models.CharField(max_length=200)),
                ('pmr', models.BooleanField(default=False)),
                ('escalator', models.BooleanField(default=False)),
                ('ascenseur', models.BooleanField(default=False)),
                ('last_update', models.DateTimeField(auto_now=True, verbose_name='Last update')),
            ],
        ),
        migrations.CreateModel(
            name='Tcl',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'TCL',
            },
        ),
    ]
