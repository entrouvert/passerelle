# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import copy
import datetime
import difflib
import json
import pickle
import re
from decimal import Decimal
from operator import itemgetter
from urllib.parse import urljoin, urlparse

import zeep
from dateutil import rrule
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models, transaction
from django.db.models import JSONField
from django.http import Http404, HttpResponse
from django.template.loader import render_to_string as render_template_to_string
from django.utils import dateformat
from django.utils.dateparse import parse_date, parse_datetime
from django.utils.text import slugify
from django.utils.timezone import localtime, now
from requests.exceptions import RequestException
from zeep.helpers import serialize_object
from zeep.wsse.username import UsernameToken

from passerelle.apps.base_adresse.models import CityModel
from passerelle.base.models import BaseResource, HTTPResource, SkipJob
from passerelle.base.signature import sign_url
from passerelle.utils.api import endpoint
from passerelle.utils.conversion import normalize, simplify
from passerelle.utils.jsonresponse import APIError
from passerelle.utils.soap import SOAPError, SOAPFault, SOAPServiceUnreachable
from passerelle.utils.templates import render_to_string
from passerelle.utils.validation import is_number
from passerelle.utils.wcs import WcsApi, WcsApiError

from . import activity_schemas, family_schemas, invoice_schemas, schemas, utils


class UpdateError(Exception):
    pass


class ToulouseMaelis(BaseResource, HTTPResource):
    # noqa pylint: disable=too-many-public-methods

    base_wsdl_url = models.CharField(
        max_length=128,
        blank=False,
        verbose_name='URL de base des WSDL',
        default='https://demo-toulouse.sigec.fr/maelisws-toulouse/services/',
    )
    zeep_wsse_username = models.CharField(
        max_length=64, blank=True, default='', verbose_name='Identifiant utilisateur WSSE'
    )
    zeep_wsse_password = models.CharField(
        max_length=64, blank=True, default='', verbose_name='Mot de passe WSSE'
    )
    perisco_nature_codes = models.TextField(
        blank=True,
        default='A,R',
        verbose_name='Codes des natures des activités péri-scolaires, séparés par des virgules',
    )
    extrasco_nature_codes = models.TextField(
        blank=True,
        default='X',
        verbose_name='Codes des natures des activités extra-scolaires, séparés par des virgules',
    )
    loisir_nature_codes = models.TextField(
        blank=True,
        default='P,L,S,' + ','.join(str(x) for x in range(1, 10)),
        verbose_name='Codes des natures des activités loisirs, séparés par des virgules',
    )
    cancel_invoice_delay = models.PositiveIntegerField(
        default='30',
        verbose_name="Délai de conservation des factures issues d'un panier (en minutes)",
    )
    max_payment_delay = models.PositiveIntegerField(
        default='20',
        verbose_name='Délai maximum pour payer une facture via Lingo (en minutes)',
    )

    category = 'Connecteurs métiers'
    _category_ordering = ['Famille', 'Activités']
    soap_client_wsdl_ttl = 300  # 5 minutes of cache for zeep.Client.wsdl

    class Meta:
        verbose_name = 'Toulouse Maelis'

    def get_perisco_nature_codes(self):
        return [x.strip() for x in self.perisco_nature_codes.split(',') if x.strip()]

    def get_extrasco_nature_codes(self):
        return [x.strip() for x in self.extrasco_nature_codes.split(',') if x.strip()]

    def get_loisir_nature_codes(self):
        return [x.strip() for x in self.loisir_nature_codes.split(',') if x.strip()]

    def get_client(self, wsdl_short_name):
        wsse = UsernameToken(self.zeep_wsse_username, self.zeep_wsse_password)
        wsdl_name = wsdl_short_name + 'Service?wsdl'
        wsdl_url = urljoin(self.base_wsdl_url, wsdl_name)
        settings = zeep.Settings(strict=False, xsd_ignore_sequence_order=True)
        return self.soap_client(wsdl_url=wsdl_url, wsse=wsse, settings=settings, api_error=True)

    def call(self, wsdl_short_name, service, **kwargs):
        client = self.get_client(wsdl_short_name)
        method = getattr(client.service, service)
        response = method(**kwargs)
        return serialize_object(response)

    def check_status(self):
        errors = []
        for service in ('Family', 'Activity', 'Invoice', 'Site', 'Ape'):
            if not self.call(service, 'isWSRunning'):
                errors.append(service)
        if errors:
            raise Exception('Maelis services (%s) not running' % ', '.join(errors))

    def assert_family_or_link(self, family_id, NameID):
        # self.get_link will actually raise APIError if there is no link
        return family_id or self.get_link(NameID).family_id

    def update_referential(self, referential_name, data, id_key, text_key, delete=True):
        last_update = now()
        for item in data or []:
            text = item[text_key] or ''
            if isinstance(text, int):
                text = str(text)
            text = text.strip()
            self.referential.update_or_create(
                resource_id=self.id,
                referential_name=referential_name,
                item_id=item[id_key],
                defaults={
                    'item_text': text,
                    'item_unaccent_text': simplify(text),
                    'item_data': dict({'id': item[id_key], 'text': text}, **item),
                    'updated': last_update,
                },
            )
        if delete:
            # delete extraneous items
            self.referential.filter(referential_name=referential_name, updated__lt=last_update).delete()

    def update_catalog_referential(self):
        last_update = now()
        ref_date = last_update.date()
        try:
            data = self.call(
                'Activity',
                'readActivityList',
                # pass schoolyear as '1970', it's not actually used and activities will be
                # returned according to dateStartCalend/dateEndCalend.
                schoolyear='1970',
                dateStartCalend=(ref_date - datetime.timedelta(days=365)).isoformat(),
                dateEndCalend=(ref_date + datetime.timedelta(days=365)).isoformat(),
            )
        except Exception as e:
            raise UpdateError('Service indisponible : %s' % str(e))

        for item in data or []:
            activity_data = {}
            activity_data['id'] = item['activityPortail']['idAct']
            activity_data['text'] = (
                item['activityPortail'].get('libelle2') or item['activityPortail']['libelle'] or ''
            ).strip()
            activity_data['service_id'] = item['activityPortail'].get('idService')
            activity_data['service'] = (
                self.get_referential_item_data('Service', activity_data['service_id']) or {}
            )
            activity_data['type'] = item['activityPortail'].get('activityType')
            if item['activityPortail']['weeklyCalendarActivityList']:
                activity_data['weekly_calendar'] = item['activityPortail']['weeklyCalendarActivityList'][0][
                    'dayWeekInfoList'
                ]
            else:
                activity_data['weekly_calendar'] = []

            activity_data['activityPortail'] = {}
            activity_data['activityPortail']['blocNoteList'] = item['activityPortail'].get('blocNoteList')

            activity_data['unitPortailList'] = []
            for unit in item['unitPortailList']:
                unit_data = {}
                unit_data['id'] = unit['idUnit']
                unit_data['text'] = unit['libelle']

                for key in 'birthDateStart', 'birthDateEnd', 'dateStart', 'dateEnd':
                    unit_data[key] = unit[key]

                unit_data['placeList'] = []
                for place in unit['placeList']:
                    place_data = {}
                    place_data['id'] = place['id']
                    place_data['text'] = place['lib2'] or place['lib']
                    place_data['capacityInfo'] = place['capacityInfo']

                    unit_data['placeList'].append(place_data)
                activity_data['unitPortailList'].append(unit_data)

            self.referential.update_or_create(
                resource_id=self.id,
                referential_name='Activity',
                item_id=activity_data['id'],
                defaults={
                    'item_text': activity_data['text'],
                    'item_data': activity_data,
                    'updated': last_update,
                },
            )
        # delete extraneous items
        self.referential.filter(referential_name='Activity', updated__lt=last_update).delete()

    def get_referential_data(self, service_name, referential_name):
        try:
            return self.call(service_name, 'read' + referential_name + 'List')
        except Exception as e:
            raise UpdateError('Service indisponible : %s' % str(e))

    def update_family_referentials(self):
        # local referentials
        complement_data = [
            {'id': 'A', 'text': 'bâtiment A'},
            {'id': 'B', 'text': 'bâtiment B, ou bis'},
            {'id': 'C', 'text': 'bâtiment C'},
            {'id': 'Q', 'text': 'quater'},
            {'id': 'T', 'text': 'ter'},
        ]
        sex_data = [
            {'id': 'F', 'text': 'Féminin'},
            {'id': 'X', 'text': 'Indéterminé'},
            {'id': 'M', 'text': 'Masculin'},
        ]
        self.update_referential('Complement', complement_data, 'id', 'text')
        self.update_referential('Sex', sex_data, 'id', 'text')

        # remote referentials
        for referential_name in (
            'Category',
            'ChildIndicator',
            'Civility',
            'Country',
            'County',
            'CSP',
            'DietCode',
            'Document',
            'Organ',
            'PAI',
            'ProfessionalSituation',
            'Quality',
            'Quotient',
            'RLIndicator',
            'Situation',
            'Street',
            'Town',
            'Vaccin',
        ):
            id_key, text_key = 'code', 'libelle'
            data = self.get_referential_data('Family', referential_name)
            if referential_name == 'Organ':
                id_key, text_key = 'id', 'code'
            elif referential_name == 'Street':
                id_key, text_key = 'idStreet', 'libelleStreet'

            self.update_referential(referential_name, data, id_key, text_key)

    def update_site_referentials(self):
        for referential_name in ('YearSchool', 'Level', 'DerogReason'):
            id_key, text_key = 'code', 'libelle'
            data = self.get_referential_data('Site', referential_name)
            if referential_name == 'YearSchool':
                id_key, text_key = 'schoolYear', 'schoolYear'
            self.update_referential(referential_name, data, id_key, text_key)

    def update_activity_referentials(self):
        for referential_name in ('ActivityNatureType', 'Direct', 'Service'):
            id_key, text_key = 'code', 'libelle'
            data = self.get_referential_data('Activity', referential_name)
            if referential_name in ['Direct', 'Service']:
                id_key, text_key = 'id', 'lib1'
            self.update_referential(referential_name, data, id_key, text_key)

        # add Direct data on Service entries
        for service in self.referential.filter(referential_name='Service'):
            service.item_data['direction'] = self.get_referential_item_data(
                'Direct', service.item_data['idDir']
            )
            service.save()

        self.update_catalog_referential()

    def update_ape_referentials(self):
        indicators = self.call('Ape', 'readApeIndicatorList')
        self.update_referential('ApeIndicator', indicators, 'level', 'level')

        nurseries = self.call('Ape', 'readNurseryList', request={})
        self.update_referential('Nursery', nurseries, 'idActivity', 'libelle')

    def update_invoice_referentials(self):
        data = self.get_referential_data('Invoice', 'Regie')
        self.update_referential('Regie', data, 'code', 'libelle')

    def merge_zipcodes(self):
        city = CityModel.objects.first()
        if not city:
            return
        cities_by_insee = {x.code: x for x in CityModel.objects.filter(resource=city.resource)}
        for town in self.referential.filter(referential_name='Town').all():
            zipcode = None
            if 'ARRONDISSEMENT' in town.item_text:
                city, district = re.match(r'([A-Z]+).*(\d+)', town.item_text).groups()
                zipcode = {'PARIS': '75', 'LYON': '69', 'MARSEILLE': '13'}.get(city) + district.zfill(3)
            else:
                city = cities_by_insee.get(town.item_id)
                if city:
                    zipcode = city.zipcode
            if zipcode:
                town.item_data['zipcode'] = zipcode
                town.item_data['zip_and_text'] = '%s %s' % (zipcode, town.item_text)
                town.save()

    @classmethod
    def earliest_updated(cls, qs):
        timestamp = qs.aggregate(updated=models.Min('updated'))['updated']
        if timestamp:
            timestamp = localtime(timestamp)
        return timestamp

    def get_activity_referentials_earliest_timestamp(self):
        referentials = self.referential.filter(
            referential_name__in=['Activity', 'ActivityNatureType', 'Direct', 'Service']
        )
        return self.earliest_updated(referentials)

    def is_activity_referentials_too_old(self):
        timestamp = self.get_activity_referentials_earliest_timestamp()
        return not timestamp or (now() - timestamp >= datetime.timedelta(hours=6))

    def every5min(self):
        try:
            self.update_activity_referentials()
        except UpdateError as e:
            if self.is_activity_referentials_too_old():
                self.logger.error('Erreur sur la mise à jour (depuis plus de 6 heures): %s' % e)
            else:
                self.logger.warning('Erreur sur la mise à jour: %s' % e)
        else:
            self.logger.info('Réferentiels mis à jour.')

    def get_referentials_earliest_timestamp(self):
        referentials = self.referential.exclude(
            referential_name__in=['Activity', 'ActivityNatureType', 'Direct', 'Service']
        )
        return self.earliest_updated(referentials)

    def should_update_referentials(self):
        timestamp = self.get_referentials_earliest_timestamp()
        # try to update every 24 hours, then do it as long as it fails
        if not timestamp:
            return True
        if now() - timestamp >= datetime.timedelta(hours=24):
            return True
        # also after a failure do it prefentially after midnight
        if (now() - timestamp > datetime.timedelta(hours=7)) and (
            datetime.time(0, 0) < localtime(now()).time() < datetime.time(6, 0)
        ):
            return True
        return False

    def is_referentials_too_old(self):
        # referentials are tool old if any entry is more than 30 hours old
        timestamp = self.get_referentials_earliest_timestamp()
        return not timestamp or (now() - timestamp >= datetime.timedelta(hours=30))

    def update_referentials(self):
        try:
            self.update_family_referentials()
            self.update_site_referentials()
            self.update_ape_referentials()
            self.update_invoice_referentials()
            # merge zip codes from base adresse into town referential
            self.merge_zipcodes()
        except UpdateError as e:
            if self.is_referentials_too_old():
                self.logger.error('Erreur sur la mise à jour (depuis plus de 30 heures): %s' % e)
            else:
                self.logger.warning('Erreur sur la mise à jour: %s' % e)
        else:
            self.logger.info('Réferentiels mis à jour.')

    def notify_invoices_paid(self):
        invoices = self.invoice_set.filter(
            maelis_notification_date__isnull=True,
            lingo_notification_date__lt=now() - datetime.timedelta(minutes=15),
        )
        for invoice in invoices:
            invoice.notify()

    def cancel_basket_invoices(self):
        invoices = self.invoice_set.filter(
            lingo_notification_date__isnull=True,
            basket_generation_date__isnull=False,
            maelis_cancel_notification_date__isnull=True,
        )
        for invoice in invoices:
            if (
                invoice.start_payment_date is not None
                and invoice.start_payment_date <= now() - datetime.timedelta(minutes=self.max_payment_delay)
            ) or invoice.created <= now() - datetime.timedelta(
                minutes=(self.cancel_invoice_delay + self.max_payment_delay)
            ):
                invoice.cancel()

    def trigger_subscriptions_cron(self):
        # find subscriptions removed from baskets
        pending_basket_subscriptions = self.subscription_set.filter(invoice__isnull=True)
        family_ids = {x.family_id for x in pending_basket_subscriptions}
        for family_id in family_ids:
            self.get_baskets_raw(family_id)

        subscriptions = self.subscription_set.filter(
            wcs_trigger_date__isnull=True,
            wcs_trigger_payload__isnull=False,
        )
        for subscription in subscriptions:
            if subscription.trigger_status() == 'triggering':
                subscription.trigger()

    def hourly(self):
        super().hourly()
        self.notify_invoices_paid()
        self.cancel_basket_invoices()
        self.trigger_subscriptions_cron()
        if self.should_update_referentials():
            self.update_referentials()
        self.cache_clean()

    def get_referential(self, referential_name, id=None, q=None, limit=None, distinct=True, **kwargs):
        if id is not None:
            queryset = self.referential.filter(referential_name=referential_name, item_id=id)
        else:
            queryset = self.referential.filter(referential_name=referential_name).all()
            if q:
                queryset = queryset.filter(item_unaccent_text__icontains=simplify(q))

        if kwargs:
            queryset = queryset.filter(**kwargs)

        if distinct:
            queryset = queryset.distinct('resource', 'referential_name', 'item_text')

        if limit:
            try:
                limit = int(limit)
            except ValueError:
                pass
            else:
                queryset = queryset[:limit]

        return [x.item_data for x in queryset]

    def get_referential_value(self, referential_name, key, default='key'):
        try:
            return self.referential.get(referential_name=referential_name, item_id=key).item_text
        except Referential.DoesNotExist:
            if key != 'NUMPERS_AXEL':
                self.logger.warning("No '%s' key into Maelis '%s' referential", key, referential_name)
            return key if default == 'key' else None

    def get_referential_item_data(self, referential_name, key):
        try:
            return self.referential.get(referential_name=referential_name, item_id=key).item_data
        except Referential.DoesNotExist:
            return None

    def get_link(self, NameID):
        try:
            return self.link_set.get(name_id=NameID)
        except Link.DoesNotExist:
            raise APIError('User not linked to family')

    def get_family_raw(self, family_id):
        cache_key = f'f-{family_id}'
        family = self.cache_get(cache_key)
        if family:
            return family
        family = self.call('Family', 'readFamily', dossierNumber=family_id)
        self.cache_set(cache_key, family)
        return family

    def get_rl_raw(self, family_id, rl_id):
        data = self.get_family_raw(family_id)
        if data['RL1']['num'] == rl_id:
            return data['RL1']
        elif data['RL2'] and data['RL2']['num'] == rl_id:
            return data['RL2']
        raise APIError("no '%s' RL on '%s' family" % (rl_id, family_id))

    def get_person_raw(self, family_id, person_id):
        data = self.get_family_raw(family_id)
        for person in data['emergencyPersonList']:
            if str(person['numPerson']) == person_id:
                return person
        raise APIError("no '%s' emergency person on '%s' family" % (person_id, family_id))

    def get_child_raw(self, family_id, child_id):
        data = self.get_family_raw(family_id)
        for child in data['childList']:
            if child['num'] == child_id:
                return child
        raise APIError("no '%s' child on '%s' family" % (child_id, family_id))

    def get_rl_or_child_raw(self, family_id, person_id):
        data = self.get_family_raw(family_id)
        if data['RL1']['num'] == person_id:
            return data['RL1']
        elif data['RL2'] and data['RL2']['num'] == person_id:
            return data['RL2']
        for child in data['childList']:
            if child['num'] == person_id:
                return child
        raise APIError("no '%s' RL or child on '%s' family" % (person_id, family_id))

    def get_child_person_raw(self, family_id, child_id, person_id):
        data = self.get_child_raw(family_id, child_id)
        for person in data['authorizedPersonList']:
            if str(person['personInfo']['num']) == person_id:
                return person
        raise APIError("no '%s' authorized person on '%s' child" % (person_id, child_id))

    def update_family_raw(self, family_id, **kwargs):
        response = self.call('Family', 'updateFamily', dossierNumber=family_id, **kwargs)
        self.cache_delete(f'f-{family_id}')
        return response

    def add_text_value(self, referential_name, data, keys):
        '''add text from referentials'''
        last_key = keys.pop()
        for key in keys:
            if not isinstance(data, dict) or not key in data:
                return
            data = data[key]
        if isinstance(data, dict) and last_key in data and data[last_key] is not None:
            data[last_key + '_text'] = self.get_referential_value(referential_name, data[last_key])

    def add_indicators_field(self, referential_name, data):
        active_indicators = {x['code']: x for x in data['indicatorList']}
        indicators = self.get_referential(referential_name)
        result = {}
        for item in indicators:
            active_indicator = active_indicators.get(item['id']) or {}
            item['isActive'] = bool(active_indicator)
            if item['typeDesc'] == 'NOTE':
                item['note'] = active_indicator.get('note')
            del item['choiceList']  # no list based indicator on parsifal project
            result[item['id']] = item
        data['indicators'] = result

    def add_quotients_field(self, data):
        quotients = {}
        for item in data['quotientList']:
            key = item['cdquo']
            if key not in quotients:
                quotients[key] = []
            quotients[key].append(item)
        data['quotients'] = {
            x: sorted(y, key=lambda z: (z['yearRev'], z['dateStart'], z['dateEnd']), reverse=True)
            for x, y in quotients.items()
        }

    def add_nature_subscriptions(self, data):
        subscribe_natures = {}
        for item in data['subscribeActivityList'] or []:
            activity_type = item.get('typeActivity')
            activity_nature = activity_type.get('natureSpec') if activity_type else None
            if not activity_nature:
                continue
            for unit in item['subscribesUnit']:
                start_date = unit.get('dateStart')
                end_date = unit.get('dateEnd')
                if not start_date or not end_date:
                    continue
                start_year = utils.get_reference_year_from_date(start_date)
                end_year = utils.get_reference_year_from_date(end_date)
                for year in range(start_year, end_year + 1):
                    school_year = '%s-%s' % (year, year + 1)
                    if not subscribe_natures.get(school_year):
                        subscribe_natures[school_year] = set()
                    subscribe_natures[school_year].add(activity_nature['code'])
        data['subscribe_natures'] = {x: sorted(list(y)) for x, y in subscribe_natures.items()}

    def add_text_value_to_birth(self, data):
        self.add_text_value('Town', data, ['birth', 'communeCode'])
        self.add_text_value('County', data, ['birth', 'cdDepartment'])
        self.add_text_value('Country', data, ['birth', 'countryCode'])
        if data['birth'] and data['birth'].get('communeCode'):
            city = CityModel.objects.filter(code=data['birth']['communeCode']).first()
            if city:
                data['birth']['zipCode'] = city.zipcode

    def add_text_value_to_rl_indicator(self, data):
        self.add_text_value('RLIndicator', data, ['code'])

    def add_text_value_to_child_indicator(self, data):
        self.add_text_value('ChildIndicator', data, ['code'])

    def add_text_value_to_child_person(self, data):
        self.add_text_value('Civility', data, ['personInfo', 'civility'])
        self.add_text_value('Quality', data, ['personQuality', 'code'])
        self.add_text_value('Sex', data, ['personInfo', 'sexe'])

    def add_text_value_to_child(self, data):
        self.add_text_value('Sex', data, ['sexe'])
        self.add_text_value('DietCode', data, ['dietcode'])
        self.add_text_value('PAI', data, ['paiInfoBean', 'code'])
        for person in data['authorizedPersonList']:
            self.add_text_value_to_child_person(person)
        for indicator in data['indicatorList']:
            self.add_text_value_to_child_indicator(indicator)
        self.add_indicators_field('ChildIndicator', data)
        self.add_nature_subscriptions(data)
        self.add_text_value_to_birth(data)

    def add_text_value_to_person(self, data):
        self.add_text_value('Civility', data, ['civility'])
        self.add_text_value('Quality', data, ['quality'])
        self.add_text_value('Sex', data, ['sexe'])

    def add_text_value_to_rl(self, data):
        self.add_text_value('Civility', data, ['civility'])
        self.add_text_value('Quality', data, ['quality'])
        self.add_text_value('Complement', data, ['adresse', 'numComp'])
        self.add_text_value('Street', data, ['adresse', 'idStreet'])
        self.add_text_value('CSP', data, ['profession', 'codeCSP'])
        self.add_text_value('ProfessionalSituation', data, ['profession', 'situation'])
        self.add_text_value('Organ', data, ['CAFInfo', 'organ'])
        for indicator in data['indicatorList']:
            self.add_text_value_to_rl_indicator(indicator)
        for quotient in data['quotientList']:
            self.add_text_value('Quotient', quotient, ['cdquo'])
        self.add_indicators_field('RLIndicator', data)
        self.add_quotients_field(data)
        self.add_nature_subscriptions(data)
        self.add_text_value_to_birth(data)

    def add_text_value_to_family(self, data):
        self.add_text_value('Category', data, ['category'])
        self.add_text_value('Situation', data, ['situation'])
        for rlg in 'RL1', 'RL2':
            if data.get(rlg):
                self.add_text_value_to_rl(data[rlg])
        for child in data['childList']:
            self.add_text_value_to_child(child)
            for person in data['emergencyPersonList']:
                self.add_text_value_to_person(person)

    def get_child_person(self, family_id, child_id, person_id):
        data = self.get_child_person_raw(family_id, child_id, person_id)
        self.add_text_value_to_child_person(data)
        return data

    def get_child(self, family_id, child_id):
        data = self.get_child_raw(family_id, child_id)
        self.add_text_value_to_child(data)
        return data

    def get_person(self, family_id, person_id):
        data = self.get_person_raw(family_id, person_id)
        self.add_text_value_to_person(data)
        return data

    def get_rl(self, family_id, rl_id):
        data = self.get_rl_raw(family_id, rl_id)
        self.add_text_value_to_rl(data)
        return data

    def get_family(self, family_id):
        data = self.get_family_raw(family_id)
        self.add_text_value_to_family(data)
        return data

    def assert_key_in_referential(self, referential_name, key_value, keys_text, required=True):
        if not key_value:
            if required:
                raise APIError('%s is required and could not be None' % keys_text)
            return
        try:
            self.referential.get(referential_name=referential_name, item_id=key_value)
        except Referential.DoesNotExist:
            ref_text = 'required ' if required else ''
            ref_text = ref_text + 'referential'
            raise APIError(
                "%s key value '%s' do not belong to '%s' %s"
                % (keys_text, key_value, referential_name, ref_text)
            )

    def assert_post_data_in_referential(self, referential_name, data, keys, required=True):
        key_value = None
        for key in keys:
            if not (isinstance(data, list) and isinstance(key, int)) and not (
                isinstance(data, dict) and key in data
            ):
                break
            data = data[key]
        else:
            key_value = data
        self.assert_key_in_referential(referential_name, key_value, '/'.join(str(x) for x in keys), required)

    def encode_bool(self, obj):
        if obj is True or str(obj).lower() in ['true', 'oui', '1']:
            return True
        if obj is False or str(obj).lower() in ['false', 'non', '0']:
            return False

    def check_and_adapt_update_indicator_payload_in_referential(
        self, referential, post_data, parent_keys=None
    ):
        keys = parent_keys or []
        data = post_data
        for key in keys:
            data = data[key]

        for i, item in enumerate(data.get('indicatorList', [])):
            self.assert_post_data_in_referential(
                referential, post_data, keys + ['indicatorList', i, 'code'], required=True
            )
            item['isActive'] = self.encode_bool(item['isActive'])

    def check_and_adapt_child_medical_record_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        data = post_data
        for key in keys:
            data = data[key]
        if 'isAuthHospital' in data:
            data['isAuthHospital'] = self.encode_bool(data['isAuthHospital'])

        for i in range(0, len(data.get('vaccinList', []))):
            self.assert_post_data_in_referential(
                'Vaccin', post_data, keys + ['vaccinList', i, 'code'], required=False
            )

    def check_and_adapt_birth_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential(
            'Town', post_data, keys + ['birth', 'communeCode'], required=False
        )
        self.assert_post_data_in_referential(
            'County', post_data, keys + ['birth', 'cdDepartment'], required=False
        )
        self.assert_post_data_in_referential(
            'Country', post_data, keys + ['birth', 'countryCode'], required=False
        )

    def check_and_adapt_child_pai_payoad_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential('PAI', post_data, keys + ['code'])

    def check_and_adapt_child_person_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential(
            'Civility', post_data, keys + ['personInfo', 'civility'], required=False
        )
        self.assert_post_data_in_referential('Sex', post_data, keys + ['personInfo', 'sexe'], required=False)
        self.assert_post_data_in_referential('Quality', post_data, keys + ['personQuality', 'code'])

    def check_and_adapt_child_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential('Sex', post_data, keys + ['sexe'])

        data = post_data
        for key in keys:
            data = data[key]
        for key in ('bPhoto', 'bLeaveAlone'):
            if key in data:
                data[key] = self.encode_bool(data[key])

        if 'dietcode' in data:
            self.assert_post_data_in_referential('DietCode', post_data, keys + ['dietcode'], required=False)
        if 'paiInfoBean' in data:
            self.check_and_adapt_child_pai_payoad_in_referential(post_data, keys + ['paiInfoBean'])
        if 'medicalRecord' in data:
            # dead code as updateFamily seems not to modify medicalRecord
            self.check_and_adapt_child_medical_record_payload_in_referential(
                post_data, keys + ['medicalRecord']
            )
        self.check_and_adapt_update_indicator_payload_in_referential('ChildIndicator', post_data, keys)
        self.check_and_adapt_birth_payload_in_referential(post_data, keys)

    def check_and_adapt_person_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential('Civility', post_data, keys + ['civility'], required=False)
        self.assert_post_data_in_referential('Sex', post_data, keys + ['sexe'], required=False)
        self.assert_post_data_in_referential('Quality', post_data, keys + ['quality'])

    def check_and_adapt_update_coordinate_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential(
            'Street', post_data, keys + ['adresse', 'idStreet'], required=False
        )
        self.assert_post_data_in_referential(
            'Complement', post_data, keys + ['adresse', 'numComp'], required=False
        )
        self.assert_post_data_in_referential(
            'CSP', post_data, keys + ['profession', 'codeCSP'], required=False
        )
        self.assert_post_data_in_referential(
            'ProfessionalSituation', post_data, keys + ['profession', 'situation'], required=False
        )
        self.assert_post_data_in_referential('Organ', post_data, keys + ['CAFInfo', 'organ'], required=False)

        data = post_data
        for key in keys:
            data = data[key]
        if 'contact' in data:
            data = data['contact']
            for key in ('isContactMail', 'isContactSms', 'isInvoicePdf'):
                if key in data:
                    data[key] = self.encode_bool(data[key])

    def check_and_adapt_rl_payload_in_referential(self, post_data, parent_keys=None):
        keys = parent_keys or []
        self.assert_post_data_in_referential('Civility', post_data, keys + ['civility'])
        self.assert_post_data_in_referential('Quality', post_data, keys + ['quality'])
        self.check_and_adapt_update_coordinate_payload_in_referential(post_data, keys)
        self.check_and_adapt_update_indicator_payload_in_referential('RLIndicator', post_data, keys)
        self.check_and_adapt_birth_payload_in_referential(post_data, keys)

    def check_and_adapt_create_rl1_payload_in_referential(self, post_data):
        self.assert_post_data_in_referential('Category', post_data, ['category'])
        self.assert_post_data_in_referential('Situation', post_data, ['situation'])
        self.check_and_adapt_rl_payload_in_referential(post_data, ['rl1'])

    def check_and_adapt_family_payload_in_referential(self, post_data):
        self.assert_post_data_in_referential('Category', post_data, ['category'])
        self.assert_post_data_in_referential('Situation', post_data, ['situation'])
        for rlg in 'rl1', 'rl2':
            if rlg in post_data:
                self.check_and_adapt_rl_payload_in_referential(post_data, [rlg])
        for i, person in enumerate(post_data.get('emergencyPersonList') or []):
            for j in range(0, len(person.get('personList') or [])):
                self.check_and_adapt_person_payload_in_referential(
                    post_data, ['emergencyPersonList', i, 'personList', j]
                )
        for i in range(0, len(post_data.get('childList') or [])):
            self.check_and_adapt_child_payload_in_referential(post_data, ['childList', i])

    def replace_null_values(self, dico):
        '''send null fields as empty SOAP tag to tell maelis to empty the value'''
        for key, value in dico.items():
            if isinstance(value, dict):
                self.replace_null_values(value)
            if value is None:
                dico[key] = ''

    def read_rl_list_raw(self, family_id, text_template):
        result = self.get_family_raw(family_id)

        for rlg in 'RL1', 'RL2':
            item = result.get(rlg)
            if not item:
                break
            self.add_text_value_to_rl(item)
            item['id'] = item['num']
            item['text'] = render_to_string(text_template, item).strip()
            item['family_id'] = family_id
            yield item

    def read_child_list_raw(self, family_id, text_template):
        result = self.get_family_raw(family_id)

        for item in result['childList']:
            self.add_text_value_to_child(item)
            item['id'] = item['num']
            item['text'] = render_to_string(text_template, item).strip()
            item['family_id'] = family_id
            yield item

    def get_person_activity_list_raw(
        self,
        family_id,
        person_id,
        nature=None,
        type_ids=None,
        reference_year=None,
        start_date=None,
        end_date=None,
        activity_label=None,
    ):
        if str(nature).lower() == 'extrasco':
            nature_filter_codes = self.get_extrasco_nature_codes()
        elif str(nature).lower() == 'loisir':
            nature_filter_codes = self.get_loisir_nature_codes()
        else:
            nature_filter_codes = None
        type_filter_codes = [x.strip() for x in str(type_ids or '').split(',') if x.strip()]

        params = {
            'numDossier': family_id,
            'numPerson': person_id,
            'yearSchool': reference_year,
            'dateStartActivity': start_date,
            'dateEndActivity': end_date,
        }
        if activity_label:
            params['activity'] = activity_label
        data = self.call(
            'Activity', 'getPersonCatalogueActivity', getPersonCatalogueActivityRequestBean=params
        )

        activities = []
        for item in data['catalogueActivityList']:
            activity_type = item['activity'].get('activityType')
            activity_nature = activity_type.get('natureSpec') if activity_type else None
            if type_filter_codes:
                if not activity_type or activity_type['code'] not in type_filter_codes:
                    continue
            if nature_filter_codes:
                if not activity_nature or activity_nature['code'] not in nature_filter_codes:
                    continue
            item['activity']['service'] = self.get_referential_item_data(
                'Service', item['activity'].get('idService')
            )
            activities.append(item)
        data['catalogueActivityList'] = activities
        return data

    def get_baskets_raw(self, family_id):
        try:
            baskets = (
                self.call(
                    'Activity',
                    'getFamilyBasket',
                    getFamilyBasketRequestBean={
                        'numFamily': family_id,
                    },
                )
                or []
            )
        except SOAPFault as e:
            fault_message = e.data['soap_fault'].get('message') or ''
            if fault_message.startswith('E02 : '):
                # unknown family
                baskets = []
            else:
                raise

        # remove pending basket subscriptions if they are no more listed into the baskets
        basket_id_ins = {}
        for basket in baskets:
            regie_id = str(basket['codeRegie'])
            basket_id_ins[regie_id] = []
            for line in basket['lignes']:
                basket_id_ins[regie_id].append(line['idIns'])

        for subscription in self.subscription_set.filter(
            basket_removal_date__isnull=True, invoice__isnull=True, family_id=family_id
        ):
            for line in subscription.maelis_data['basket']['lignes']:
                if line['idIns'] in basket_id_ins.get(subscription.regie_id, []):
                    break
            else:
                subscription.basket_removal_date = now()
                subscription.save()
                subscription.set_trigger()

        return baskets

    def get_basket_raw(self, family_id, basket_id):
        for basket in self.get_baskets_raw(family_id):
            if basket['id'] == basket_id:
                return basket
        raise APIError("no '%s' basket on family" % basket_id)

    @endpoint(
        display_category='Famille',
        description='Lister les catégories',
        name='read-category-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_category_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Category', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les indicateurs sur le enfants',
        name='read-child-indicator-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_child_indicator_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('ChildIndicator', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les civilités',
        name='read-civility-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_civility_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Civility', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les compléments du numéro de voie',
        name='read-complement-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_complement_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Complement', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les pays',
        name='read-country-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_country_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Country', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les départements',
        name='read-county-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_county_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('County', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les communes',
        name='read-town-list',
        parameters={
            'id': {'description': 'Identifiant de la commune (code INSEE)'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_town_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Town', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les catégories socio-professionnelles',
        name='read-csp-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_csp_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('CSP', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les régimes alimentaires',
        name='read-dietcode-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_dietcode_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('DietCode', id, q, limit, distinct)}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les directions de la ville',
        name='read-direction-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_direction_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Direct', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les pièces jointes',
        name='read-document-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_document_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Document', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les organismes (CAF)',
        name='read-organ-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_organ_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Organ', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description="Lister les Projets d'Accueils Individualisés (PAI)",
        name='read-pai-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_pai_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('PAI', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les situations professionnelles',
        name='read-professional-situation-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Supression des doublons'},
        },
    )
    def read_professional_situation_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('ProfessionalSituation', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les qualités',
        name='read-quality-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_quality_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Quality', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les quotients',
        name='read-quotient-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_quotient_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Quotient', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les indicateurs sur les responsables légaux',
        name='read-rl-indicator-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_rl_indicator_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('RLIndicator', id, q, limit, distinct)}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les services de la ville',
        name='read-service-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
            'direction_id': {'description': 'Numéro de la direction sur laquelle filtrer'},
        },
    )
    def read_service_list(self, request, id=None, q=None, limit=None, distinct=True, direction_id=None):
        kwargs = {}
        if direction_id:
            kwargs['item_data__idDir'] = direction_id
        return {'data': self.get_referential('Service', id, q, limit, distinct, **kwargs)}

    @endpoint(
        display_category='Famille',
        description='Lister les sexes',
        name='read-sex-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_sex_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Sex', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les situations',
        name='read-situation-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_situation_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Situation', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les voies',
        name='read-street-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_street_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Street', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lister les vaccins',
        name='read-vaccin-list',
        parameters={
            'id': {'description': 'Identifiant de l’enregistrement'},
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats; doit être inférieur à 20.'},
            'distinct': {'description': 'Ne pas inclure les valeurs dupliquées'},
        },
    )
    def read_vaccin_list(self, request, id=None, q=None, limit=None, distinct=True):
        return {'data': self.get_referential('Vaccin', id, q, limit, distinct)}

    @endpoint(
        display_category='Famille',
        description='Lier un compte usager à une famille',
        parameters={'NameID': {'description': 'Publik NameID'}},
        post={'request_body': {'schema': {'application/json': schemas.LINK_SCHEMA}}},
    )
    def link(self, request, NameID, post_data):
        if not NameID:
            raise APIError('NameID is required')
        family_id = post_data['family_id']
        family_id = utils.normalize_id(family_id)
        response = self.get_family_raw(family_id)

        for rlg in 'RL1', 'RL2':
            if not response.get(rlg):
                continue
            if not (
                response[rlg]['birth']
                and isinstance(response[rlg]['birth'].get('dateBirth'), datetime.datetime)
            ):
                self.logger.warning(
                    "Maelis provides an invalid dateBirth for %s on '%s' family", rlg, family_id
                )
                continue
            if (
                response[rlg]['firstname'] == normalize(post_data['firstname']).upper()
                and response[rlg]['lastname'] == normalize(post_data['lastname']).upper()
                and response[rlg]['birth']['dateBirth'].strftime('%Y-%m-%d') == post_data['dateBirth']
            ):
                break
        else:
            raise APIError("Data provided does not match any RL on '%s' family" % family_id)

        # put invoices into cache
        for regie in self.get_referential('Regie'):
            self.get_invoices(family_id, regie['id'], use_cache=False)

        Link.objects.update_or_create(resource=self, name_id=NameID, defaults={'family_id': family_id})
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Supprimer une liaison entre un compte usager et une famille',
        methods=['post'],
        parameters={
            'NameID': {'description': 'Publik NameID'},
        },
    )
    def unlink(self, request, NameID):
        link = self.get_link(NameID)
        link.delete()
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Obtenir les comptes usager liés à une famille',
        name='get-link-list',
        perm='can_access',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def get_link_list(self, request, NameID=None, family_id=None, text_template=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = [
            {'id': x['name_id'], 'context': {'link': x}}
            for x in self.link_set.filter(family_id=family_id).values()
        ]
        for item in data:
            del item['context']['link']['id']
            del item['context']['link']['resource_id']

        # call authentic to add user data to context
        if getattr(settings, 'KNOWN_SERVICES', {}).get('authentic'):
            idp_service = list(settings.KNOWN_SERVICES['authentic'].values())[0]
            for item in data:
                api_url = sign_url(
                    urljoin(
                        idp_service['url'], 'api/users/%s/?orig=%s' % (item['id'], idp_service.get('orig'))
                    ),
                    key=idp_service.get('secret'),
                )
                try:
                    response = self.requests.get(api_url)
                except RequestException:
                    pass
                else:
                    if response.status_code == 200:
                        item['context']['user'] = response.json()
                        for key in 'date_joined', 'last_login':
                            value = item['context']['user'].get(key)
                            if value:
                                item['context']['user'][key] = parse_datetime(value)
                        if item['context']['user'].get('password'):
                            del item['context']['user']['password']

        for item in data:
            item['text'] = (
                render_template_to_string('toulouse_maelis/family_link_template.txt', item['context'])
                .replace('\n', '')
                .strip()
            )
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Obtenir un numéro de dossier famille',
        name='get-family-id',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def get_family_id(self, request, NameID=None, family_id=None):
        if NameID and family_id:
            raise APIError('NameID and family_id both provided')
        try:
            family_id = self.assert_family_or_link(family_id, NameID)
        except APIError:
            return {'data': None}
        return {'data': family_id}

    @endpoint(
        display_category='Famille',
        description='Rechercher un dossier famille',
        name='search-family',
        parameters={
            'q': {'description': 'Recherche en texte intégral'},
            'limit': {'description': 'Nombre maximal de résultats.'},
        },
    )
    def search_family(self, request, q=None, limit=None):
        data = []
        if limit:
            try:
                limit = int(limit)
            except ValueError:
                raise APIError('invalid limit parameter')
        if q and len(q) >= 4:  # speedup maelis reply
            data = self.call('Family', 'readFamilyListFromFullName', fullname=q)
            if limit:
                data = data[:limit]
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Rechercher un dossier famille par son numéro de DUI',
        name='search-family-dui',
        parameters={
            'q': {'description': 'Numéro de DUI'},
        },
    )
    def search_family_dui(self, request, q=None):
        if not q:
            return {'data': []}
        try:
            dui = int(q)
            if dui >= 2**32:
                raise ValueError
        except (TypeError, ValueError):
            return {'data': []}
        try:
            response = self.call('Family', 'readFamilyList', dossierNumber=dui)
        except APIError:
            return {'data': []}
        return {'data': response}

    @endpoint(
        display_category='Famille',
        description='Obtenir les informations sur la famille',
        name='read-family',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_family(self, request, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = self.get_family(family_id)
        data['family_id'] = family_id
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Lister les responsables légaux',
        name='read-rl-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
        },
    )
    def read_rl_list(
        self,
        request,
        NameID=None,
        family_id=None,
        text_template='{{ lastname }} {{ firstname }}',
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        return {'data': list(self.read_rl_list_raw(family_id, text_template))}

    @endpoint(
        display_category='Famille',
        description="Lister les personnes à prévenir en cas d'urgence",
        name='read-person-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
        },
    )
    def read_person_list(
        self, request, NameID=None, family_id=None, text_template='{{ lastname }} {{ firstname }}'
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        result = self.get_family_raw(family_id)
        data = []
        for item in result['emergencyPersonList']:
            self.add_text_value_to_person(item)
            item['id'] = item['numPerson']
            item['text'] = render_to_string(text_template, item).strip()
            item['family_id'] = family_id
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Lister les enfants',
        name='read-child-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
        },
    )
    def read_child_list(
        self, request, NameID=None, family_id=None, text_template='{{ lastname }} {{ firstname }}'
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        return {'data': list(self.read_child_list_raw(family_id, text_template))}

    @endpoint(
        display_category='Famille',
        description='Lister les enfants et les responsables légaux',
        name='read-rl-and-child-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'rl_text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
            'child_text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
        },
    )
    def read_rl_and_child_list(
        self,
        request,
        NameID=None,
        family_id=None,
        rl_text_template='{{ lastname }} {{ firstname }}',
        child_text_template='{{ lastname }} {{ firstname }}',
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        return {
            'data': list(self.read_rl_list_raw(family_id, rl_text_template))
            + list(self.read_child_list_raw(family_id, child_text_template))
        }

    @endpoint(
        display_category='Famille',
        description="Lister les personnes autorisées à récupérer l'enfant",
        name='read-child-person-list',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text',
            },
        },
    )
    def read_child_person_list(
        self,
        request,
        child_id,
        NameID=None,
        family_id=None,
        text_template='{{ personInfo.lastname }} {{ personInfo.firstname }}',
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        result = self.get_child_raw(family_id, child_id)
        data = []
        for item in result['authorizedPersonList']:
            self.add_text_value_to_child_person(item)
            item['id'] = item['personInfo']['num']
            item['text'] = render_to_string(text_template, item).strip()
            item['family_id'] = family_id
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Obtenir les informations sur un responsable légal',
        name='read-rl',
        parameters={
            'rl_id': {'description': 'Numéro du responsable légal'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_rl(
        self,
        request,
        rl_id,
        NameID=None,
        family_id=None,
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = self.get_rl(family_id, rl_id)
        data['family_id'] = family_id
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description="Obtenir les informations sur une personne à prévenir en cas d'urgence",
        name='read-person',
        parameters={
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_person(self, request, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = self.get_person(family_id, person_id)
        data['family_id'] = family_id
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Obtenir les informations sur un enfant',
        name='read-child',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_child(self, request, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = self.get_child(family_id, child_id)
        data['family_id'] = family_id
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description="Obtenir les informations sur une personne autorisée à venir chercher l'enfant",
        name='read-child-person',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_child_person(self, request, child_id, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = self.get_child_person(family_id, child_id, person_id)
        data['family_id'] = family_id
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description="Vérifier qu'un responsable légal existe",
        name='is-rl-exists',
        post={'request_body': {'schema': {'application/json': family_schemas.ISEXISTS_SCHEMA}}},
    )
    def is_rl_exists(self, request, post_data):
        response = self.call('Family', 'isRLExists', **post_data)
        return {'data': response}

    @endpoint(
        display_category='Famille',
        description="Vérifier qu'un enfant existe",
        name='is-child-exists',
        post={'request_body': {'schema': {'application/json': family_schemas.ISEXISTS_SCHEMA}}},
    )
    def is_child_exists(self, request, post_data):
        response = self.call('Family', 'isChildExists', **post_data)
        return {'data': response}

    @endpoint(
        display_category='Famille',
        description='Lister les activités auxquelles un RL ou un enfant est inscrit',
        name='read-subscribe-activity-list',
        parameters={
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'nature': {
                'description': 'Natures des activités : PERICSO, EXTRASCO ou LOISIR (toutes par défaut)',
            },
            'type_ids': {
                'description': 'Codes des types des activités (tous par défaut), séparés par des virgules',
                'example_value': 'ACCSOIR,RESTSCOL',
            },
            'school_year': {
                'description': 'Année scolaire (ex: 2022-2023)',
                'example_value': '2022-2023',
            },
        },
    )
    def read_subscribe_activity_list(
        self, request, person_id, NameID=None, family_id=None, nature=None, type_ids=None, school_year=None
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        result = self.get_rl_or_child_raw(family_id, person_id)
        if str(nature).lower() == 'perisco':
            nature_filter_codes = self.get_perisco_nature_codes()
        elif str(nature).lower() == 'extrasco':
            nature_filter_codes = self.get_extrasco_nature_codes()
        elif str(nature).lower() == 'loisir':
            nature_filter_codes = self.get_loisir_nature_codes()
        else:
            nature_filter_codes = None
        type_filter_codes = [x.strip() for x in str(type_ids or '').split(',') if x.strip()]

        data = []
        for item in result['subscribeActivityList'] or []:
            activity_type = item.get('typeActivity')
            activity_nature = activity_type.get('natureSpec') if activity_type else None
            if type_filter_codes:
                if not activity_type or activity_type['code'] not in type_filter_codes:
                    continue
            if nature_filter_codes:
                if not activity_nature or activity_nature['code'] not in nature_filter_codes:
                    continue
            if school_year:
                school_years = set()
                for unit in item['subscribesUnit']:
                    start_date = unit.get('dateStart')
                    end_date = unit.get('dateEnd')
                    if not start_date or not end_date:
                        continue
                    start_year = utils.get_reference_year_from_date(start_date)
                    end_year = utils.get_reference_year_from_date(end_date)
                    for year in range(start_year, end_year + 1):
                        school_years.add('%s-%s' % (year, year + 1))
                if school_year not in school_years:
                    continue
            item['id'] = item['idActivity']
            item['text'] = item.get('libelle2') or item['libelle']
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Famille',
        description='Créer la famille',
        name='create-family',
        parameters={'NameID': {'description': 'Publik NameID'}},
        post={'request_body': {'schema': {'application/json': family_schemas.CREATE_FAMILY_SCHEMA}}},
    )
    def create_family(self, request, post_data, NameID=None):
        if self.link_set.filter(name_id=NameID).exists():
            raise APIError(
                'User already linked to family',
                extra_dict={'link': self.link_set.filter(name_id=NameID).values()[0]},
            )
        self.check_and_adapt_family_payload_in_referential(post_data)

        response = self.call('Family', 'createFamily', **post_data)
        family_id = response.get('number')
        if not family_id:
            errors = response.get('rl1ErrorList') + response.get('childErrorList')
            raise APIError(' ; '.join(errors))

        if NameID:
            Link.objects.create(resource=self, name_id=NameID, family_id=family_id)
        return {'data': response}

    @endpoint(
        display_category='Famille',
        description='Modifier la famille',
        name='update-family',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_FAMILY_SCHEMA}}},
    )
    def update_family(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_family_payload_in_referential(post_data)
        self.replace_null_values(post_data)

        # adapt payload to use same input as create_family
        if len(post_data.get('emergencyPersonList', [])):
            persons = post_data.pop('emergencyPersonList')
            post_data['emergencyPersonList'] = [{'personList': persons}]

        response = self.update_family_raw(family_id, **post_data)
        family_id = response.get('number')
        errors = response.get('childErrorList')
        if errors:
            raise APIError(' ; '.join(errors))
        return {'data': response}

    @endpoint(
        display_category='Famille',
        description='Créer le RL1',
        name='create-rl1',
        parameters={'NameID': {'description': 'Publik NameID'}},
        post={'request_body': {'schema': {'application/json': family_schemas.CREATE_RL1_SCHEMA}}},
    )
    def create_rl1(self, request, post_data, NameID=None):
        if self.link_set.filter(name_id=NameID).exists():
            raise APIError(
                'User already linked to family',
                extra_dict={'link': self.link_set.filter(name_id=NameID).values()[0]},
            )
        self.check_and_adapt_create_rl1_payload_in_referential(post_data)

        response = self.call('Family', 'createFamily', **post_data)
        family_id = response.get('number')
        if not family_id:
            errors = response.get('rl1ErrorList') or []
            raise APIError(' ; '.join(errors))

        if NameID:
            Link.objects.create(resource=self, name_id=NameID, family_id=family_id)
        return {'data': {'family_id': family_id}}

    @endpoint(
        display_category='Famille',
        description='Modifier le RL1',
        name='update-rl1',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_RL1_SCHEMA}}},
    )
    def update_rl1(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_rl_payload_in_referential(post_data)
        self.replace_null_values(post_data)
        family = self.get_family_raw(family_id)

        rl1 = post_data
        rl1['adresse'] = family['RL1']['adresse']
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'rl1': rl1,
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Créer le RL2',
        name='create-rl2',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.CREATE_RL2_SCHEMA}}},
    )
    def create_rl2(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_rl_payload_in_referential(post_data)
        family = self.get_family_raw(family_id)
        if family['RL2']:
            raise APIError('RL2 already defined on family')

        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'rl2': post_data,
        }
        response = self.update_family_raw(family_id, **payload)
        return {'data': {'id': response['RL2']['num']}}

    @endpoint(
        display_category='Famille',
        description='Modifier le RL2',
        name='update-rl2',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_RL2_SCHEMA}}},
    )
    def update_rl2(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_rl_payload_in_referential(post_data)
        self.replace_null_values(post_data)
        family = self.get_family_raw(family_id)
        if not family['RL2']:
            raise APIError('No RL2 to update on family')

        rl2 = post_data
        rl2['adresse'] = family['RL2']['adresse']
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'rl2': rl2,
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Ajouter un enfant',
        name='create-child',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'force': {
                'description': 'boolean to bypass doublon error',
                'type': 'bool',
                'example_value': 'false',
            },
        },
        post={'request_body': {'schema': {'application/json': family_schemas.CREATE_CHILD_SCHEMA}}},
    )
    def create_child(self, request, post_data, NameID=None, family_id=None, force=False):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_payload_in_referential(post_data)

        payload = {
            'numDossier': family_id,
            'isForceCreateChild': force,
            'child': post_data,
        }
        response = self.call('Family', 'createChild', **payload)
        self.cache_delete(f'f-{family_id}')

        child_id = response.get('number')
        if not child_id:
            errors = response.get('childErrorList') or []
            raise APIError(' ; '.join(errors))
        return {'data': {'child_id': child_id}}

    @endpoint(
        display_category='Famille',
        description='Modifier un enfant',
        name='update-child',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_CHILD_SCHEMA}}},
    )
    def update_child(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_payload_in_referential(post_data)
        self.replace_null_values(post_data)
        family = self.get_family_raw(family_id)

        child = post_data
        child['num'] = child_id
        for known_child in family['childList']:
            if str(known_child['num']) == child_id:
                child['authorizedPersonList'] = known_child['authorizedPersonList']
                break
        else:
            raise APIError('No child %s to update on family' % child_id)
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'childList': [child],
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Mettre à jour les coordonnées d'un responsable légal",
        name='update-coordinate',
        parameters={
            'rl_id': {'description': 'Numéro du responsable légal'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_COORDINATE_SCHEMA}}},
    )
    def update_coordinate(self, request, post_data, rl_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_update_coordinate_payload_in_referential(post_data)
        self.replace_null_values(post_data)

        self.call('Family', 'updateCoordinate', numDossier=family_id, numPerson=rl_id, **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Mettre à jour les indicateurs d'un responsable légal",
        name='update-rl-indicator',
        parameters={
            'rl_id': {'description': 'Numéro du responsable légal'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_INDICATOR_SCHEMA}}},
    )
    def update_rl_indicator(self, request, post_data, rl_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_update_indicator_payload_in_referential('RLIndicator', post_data)

        self.call('Family', 'updatePersonIndicatorList', numPerson=rl_id, **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer ou mettre à jour le quotient d'un responsable légal",
        name='update-quotient',
        parameters={
            'rl_id': {'description': 'Numéro du responsable légal'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_QUOTIENT_SCHEMA}}},
    )
    def update_quotient(self, request, post_data, rl_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.assert_post_data_in_referential('Quotient', post_data, ['cdquo'])

        payload = {
            'dossierNumber': family_id,
            'personNumber': rl_id,
            'quotient': post_data,
        }
        self.call('Family', 'createUpdateQuotient', **payload)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer une personne à prévenir en cas d'urgence",
        name='create-person',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.EMERGENCY_PERSON_SCHEMA}}},
    )
    def create_person(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_person_payload_in_referential(post_data)
        family = self.get_family_raw(family_id)

        personList = family['emergencyPersonList']
        personList.append(post_data)
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'emergencyPersonList': [{'personList': personList}],
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Mettre à jour une personne à prévenir en cas d'urgence",
        name='update-person',
        parameters={
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.EMERGENCY_PERSON_SCHEMA}}},
    )
    def update_person(self, request, post_data, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_person_payload_in_referential(post_data)
        family = self.get_family_raw(family_id)

        personList = family['emergencyPersonList']
        for i, person in enumerate(personList):
            if str(person['numPerson']) == person_id:
                personList[i] = post_data
                personList[i]['numPerson'] = person_id
                break
        else:
            raise APIError("no '%s' authorized person on '%s' family" % (person_id, family_id))
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'emergencyPersonList': [{'personList': personList}],
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Supprimer une personne à prévenir en cas d'urgence",
        name='delete-person',
        parameters={
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        methods=['post'],
    )
    def delete_person(self, request, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        family = self.get_family_raw(family_id)

        personList = family['emergencyPersonList']
        for i, person in enumerate(personList):
            if str(person['numPerson']) == person_id:
                del personList[i]
                break
        else:
            raise APIError("no '%s' authorized person on '%s' family" % (person_id, family_id))
        payload = {
            'category': family['category'],
            'situation': family['situation'],
            'nbChild': family['nbChild'],
            'nbTotalChild': family['nbTotalChild'],
            'nbAES': family['nbAES'],
            'emergencyPersonList': [{'personList': personList}],
        }
        self.update_family_raw(family_id, **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer une personne autorisée à venir chercher l'enfant",
        name='create-child-person',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.AUTHORIZED_PERSON_SCHEMA}}},
    )
    def create_child_person(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_person_payload_in_referential(post_data)
        child = self.get_child_raw(family_id, child_id)

        personList = child['authorizedPersonList']
        personList.append(post_data)
        req = {
            'numFamily': family_id,
            'numPerson': child_id,
            'bLeaveAlone': child['bLeaveAlone'],
            'bPhoto': child['bPhoto'],
            'personList': personList,
        }
        self.call('Family', 'updateChildAutorization', updateChildAutorizationRequest=req)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Mettre à jour une personne autorisée à venir chercher l'enfant",
        name='update-child-person',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.AUTHORIZED_PERSON_SCHEMA}}},
    )
    def update_child_person(self, request, post_data, child_id, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_person_payload_in_referential(post_data)
        child = self.get_child_raw(family_id, child_id)

        personList = child['authorizedPersonList']
        for i, person in enumerate(personList):
            if str(person['personInfo']['num']) == person_id:
                personList[i] = post_data
                personList[i]['personInfo']['num'] = person_id
                break
        else:
            raise APIError("No '%s' authorized person on '%s' child" % (person_id, child_id))
        req = {
            'numFamily': family_id,
            'numPerson': child_id,
            'bLeaveAlone': child['bLeaveAlone'],
            'bPhoto': child['bPhoto'],
            'personList': personList,
        }
        self.call('Family', 'updateChildAutorization', updateChildAutorizationRequest=req)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Supprimer une personne autorisée à venir chercher l'enfant",
        name='delete-child-person',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'person_id': {'description': 'Numéro de la personne'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        methods=['post'],
    )
    def delete_child_person(self, request, child_id, person_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        child = self.get_child_raw(family_id, child_id)

        personList = child['authorizedPersonList']
        for i, person in enumerate(personList):
            if str(person['personInfo']['num']) == person_id:
                del personList[i]
                break
        else:
            raise APIError("No '%s' authorized person on '%s' child" % (person_id, child_id))
        req = {
            'numFamily': family_id,
            'numPerson': child_id,
            'bLeaveAlone': child['bLeaveAlone'],
            'bPhoto': child['bPhoto'],
            'personList': personList,
        }
        self.call('Family', 'updateChildAutorization', updateChildAutorizationRequest=req)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer ou mettre à jour le régime alimentaire d'un enfant",
        name='update-child-dietcode',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'dietcode': {'description': 'code du régime alimentaire'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        methods=['post'],
    )
    def update_child_dietcode(self, request, child_id, dietcode, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.assert_key_in_referential('DietCode', dietcode, 'dietcode parameter', required=False)

        self.call('Family', 'createOrUpdateChildDiet', personNumber=child_id, code=dietcode)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer ou mettre à jour les informations relatives au PAI d'un enfant",
        name='update-child-pai',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.PAIINFO_SCHEMA}}},
    )
    def update_child_pai(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_pai_payoad_in_referential(post_data)

        # use None to empty date passed as an empty string by date filter
        for key in ('dateDeb', 'dateFin'):
            if post_data[key] == '':
                post_data[key] = None

        self.call('Family', 'updateChildPAI', personNumber=child_id, **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Créer ou mettre à jour les données médicales d'un enfant",
        name='update-child-medical-record',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.MEDICALRECORD_SCHEMA}}},
    )
    def update_child_medical_record(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_child_medical_record_payload_in_referential(post_data)
        self.replace_null_values(post_data)

        payload = {
            'numPerson': child_id,
            'medicalRecord': post_data,
        }
        self.call('Family', 'updateChildMedicalRecord', updateChildMedicalRecordRequest=payload)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Ajouter une vaccination à un enfant',
        name='update-child-add-vaccination',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.VACCIN_SCHEMA}}},
    )
    def update_child_add_vaccination(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)

        payload = {
            'numPerson': child_id,
            'vaccinList': [{'code': post_data['code'], 'vaccinationDate': post_data['vaccinationDate']}],
        }
        self.call('Family', 'addChildVaccinList', **payload)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description="Mettre à jour des indicateurs d'un enfant",
        name='update-child-indicator',
        parameters={
            'child_id': {'description': "Numéro de l'enfant"},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.UPDATE_INDICATOR_SCHEMA}}},
    )
    def update_child_indicator(self, request, post_data, child_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.check_and_adapt_update_indicator_payload_in_referential('ChildIndicator', post_data)

        self.call('Family', 'updatePersonIndicatorList', numPerson=child_id, **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Ajouter un document pour une famille, un responsable légal ou un enfant',
        name='add-supplied-document',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.SUPPLIED_DOCUMENTS_SCHEMA}}},
    )
    def add_supplied_document(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        for i in range(0, len(post_data.get('documentList', []))):
            self.assert_post_data_in_referential('Document', post_data, ['documentList', i, 'code'])

        for item in post_data['documentList']:
            file = item.pop('file')
            item['filename'] = file['filename']
            item['fileSupplied'] = {
                'dataHandler': base64.b64decode(file['content']),
                'fileType': file['content_type'],
                'name': file['filename'],
            }
        payload = {'addSuppliedDocumentRequestBean': post_data}
        payload['addSuppliedDocumentRequestBean']['numDossier'] = family_id
        response = self.call('Family', 'addSuppliedDocument', **payload)
        if response != 'OK':
            raise APIError('maelis fails to add the supplied document')
        return {'data': 'ok'}

    @endpoint(
        display_category='Famille',
        description='Savoir si un document déjà ajouté est encore valable (renvoie "ok" ou "ko")',
        name='read-supplied-document-validity',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'numPerson': {'description': "Numéro du responsable légal ou de l'enfant"},
            'code': {'description': 'Code de la pièce'},
            'ref_date': {
                'description': 'Date de référence, utilisée pour déduire la validité',
                'type': 'date',
            },
        },
    )
    def read_supplied_document_validity(
        self, request, code, NameID=None, family_id=None, person_id=None, ref_date=None
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.assert_key_in_referential('Document', code, 'code parameter')

        response = self.call(
            'Family',
            'readSuppliedDocumentValidity',
            readSuppliedDocumentValidityRequestBean={
                'numDossier': family_id,
                'numPerson': person_id,
                'code': code,
                'validityDate': ref_date.isoformat() if ref_date else None,
            },
        )
        return {'data': 'ok' if response is True else 'ko'}

    def get_start_and_end_dates(self, start_date, end_date):
        try:
            start_date = datetime.datetime.strptime(start_date, utils.json_date_format).date()
            end_date = datetime.datetime.strptime(end_date, utils.json_date_format).date()
        except ValueError:
            raise APIError('bad date format, should be YYYY-MM-DD', http_status=400)

        if start_date > end_date:
            raise APIError('start_date should be before end_date', http_status=400)
        reference_year = utils.get_reference_year_from_date(start_date)
        end_reference_year = utils.get_reference_year_from_date(end_date)
        if reference_year != end_reference_year:
            raise APIError(
                'start_date and end_date are in different reference year (%s != %s)'
                % (reference_year, end_reference_year),
                http_status=400,
            )
        return start_date, end_date, reference_year

    def get_bookings(self, family_id, child_id, start_date, end_date):
        bookings = []
        for booking_date in rrule.rrule(rrule.MONTHLY, dtstart=start_date.replace(day=1), until=end_date):
            payload = {
                'requestBean': {
                    'numDossier': family_id,
                    'numPerson': child_id,
                    'year': booking_date.year,
                    'month': booking_date.month,
                }
            }
            response = self.call('Activity', 'getPersonScheduleList', **payload)
            for result_data in response or []:
                for schedule in result_data['activityScheduleList']:
                    activity = schedule['activity']
                    if not activity.get('activityType') or not activity['activityType'].get('natureSpec'):
                        continue
                    if (
                        activity['activityType']['natureSpec']['code'] not in self.get_perisco_nature_codes()
                        and activity['activityType']['natureSpec']['code']
                        not in self.get_extrasco_nature_codes()
                    ):
                        continue
                    activity_id = activity['idAct']
                    many_units = len(schedule['unitScheduleList']) > 1
                    for unit in schedule['unitScheduleList']:
                        days = unit['dayInfoList']
                        for day in days:
                            if day['status'] in ['NO_READ', 'NO_CUSTODY']:
                                continue
                            booking = {
                                'id': '%s:%s:%s'
                                % (child_id, activity_id, day['day'].strftime(utils.json_date_format)),
                                'text': dateformat.format(day['day'], 'l j F Y'),
                                'prefill': day['scheduledPresence'] > 0 or day['realPresence'] > 1,
                                'disabled': (
                                    day['status'] != 'WRITABLE'
                                    or activity['activityType']['natureSpec']['code']
                                    in self.get_extrasco_nature_codes()
                                ),
                                'details': day,
                            }
                            color = 'white'
                            if booking['prefill']:
                                color = 'green'
                            booking['details']['status_color'] = color
                            booking['details']['activity_id'] = activity_id
                            booking['details']['activity_type'] = activity['activityType']['code']
                            booking['details']['activity_label'] = activity['libelle2'] or activity['libelle']
                            if many_units:
                                booking['details']['activity_label'] += ' (%s)' % unit['unit']['libelle']
                            booking['details']['child_id'] = child_id
                            booking['details']['day_str'] = day['day'].strftime(utils.json_date_format)
                            booking['details']['unit_id'] = unit['unit']['idUnit']
                            bookings.append(booking)

        # sort bookings
        activity_types = ['ACCMAT', 'RESTSCOL', 'ACCPERI', 'ACCSOIR']
        bookings = [
            (
                b['details']['day'],
                (
                    activity_types.index(b['details']['activity_type'])
                    if b['details']['activity_type'] in activity_types
                    else 0
                ),
                b['details']['activity_label'],
                b,
            )
            for b in bookings
        ]
        bookings = sorted(bookings, key=itemgetter(0, 1, 2))
        bookings = [b for d, a, l, b in bookings]
        return bookings

    @endpoint(
        display_category='Réservation',
        description="Obtenir l'agenda d'un responsable légal ou d'un enfant",
        name='read-person-agenda',
        parameters={
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'start_date': {'description': 'Début de la période'},
            'end_date': {'description': 'Fin de la période'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_person_agenda(self, request, person_id, start_date, end_date, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        start_date, end_date, reference_year = self.get_start_and_end_dates(start_date, end_date)
        bookings = self.get_bookings(family_id, person_id, start_date, end_date)
        return {
            'data': bookings,
            'extra_data': {
                'start_date': start_date,
                'end_date': end_date,
                'school_year': '%s/%s' % (reference_year, reference_year + 1),
            },
        }

    @endpoint(
        display_category='Réservation',
        description="Modifier l'agenda d'un responsable légal ou d'un enfant",
        name='update-person-agenda',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {
                'schema': {
                    'application/json': activity_schemas.BOOKING_SCHEMA,
                }
            }
        },
    )
    def update_person_agenda(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        person_id = post_data['person_id']
        start_date, end_date, dummy = self.get_start_and_end_dates(
            post_data['start_date'], post_data['end_date']
        )
        requested_bookings = post_data['booking_list']

        # build list of existing booked days
        bookings = self.get_bookings(family_id, person_id, start_date, end_date)
        legacy_bookings = [b['id'] for b in bookings if b['prefill'] is True]
        available_bookings = [b['id'] for b in bookings if b['disabled'] is False]

        bookings_to_update = []
        updated = []
        for booking_info in bookings:
            day_id = booking_info['id']
            booked = None
            action = booking_info['details']['action']
            if day_id not in available_bookings:
                # disabled or not available: not bookable
                booked = None
            elif (
                day_id not in legacy_bookings
                and day_id in requested_bookings
                and action in ['ADD_PRES_PREVI', 'ADD_PRES_REAL', 'DEL_ABSENCE']
            ):
                booked = action
            elif (
                day_id in legacy_bookings
                and day_id not in requested_bookings
                and action in ['DEL_PRES_PREVI', 'DEL_PRES_REAL', 'ADD_ABSENCE']
            ):
                booked = action
            if booked is not None:
                # no changes, don't send the day
                bookings_to_update.append(
                    {
                        'numPerson': person_id,
                        'idAct': booking_info['details']['activity_id'],
                        'idUni': booking_info['details']['unit_id'],
                        'date': booking_info['details']['day_str'],
                        'action': booked,
                    }
                )
                updated.append(
                    {
                        'activity_id': booking_info['details']['activity_id'],
                        'activity_type': booking_info['details']['activity_type'],
                        'activity_label': booking_info['details']['activity_label'],
                        'day': booking_info['details']['day_str'],
                        'booked': booked in ['ADD_PRES_PREVI', 'ADD_PRES_REAL', 'DEL_ABSENCE'],
                    }
                )
        if not bookings_to_update:
            # don't call maelis if no changes
            return updated

        payload = {
            'requestBean': {
                'numDossier': family_id,
                'unitPersonDayInfoList': bookings_to_update,
            }
        }
        response = self.call('Activity', 'updatePersonSchedule', **payload)
        if response.get('result') is False:
            raise APIError(' ; '.join(x['errorMessage'] for x in response['unitPersonDayInfoErrorList']))

        # sort changes
        activity_types = ['ACCMAT', 'RESTSCOL']
        updated = [
            (
                not u['booked'],
                activity_types.index(u['activity_type']) if u['activity_type'] in activity_types else 0,
                u['activity_label'],
                u['day'],
                u,
            )
            for u in updated
        ]
        updated = sorted(updated, key=itemgetter(0, 1, 2, 3))
        updated = [u for b, a, l, d, u in updated]
        updated = [
            {
                'booked': u['booked'],
                'activity_id': u['activity_id'],
                'activity_label': u['activity_label'],
                'day': u['day'],
            }
            for u in updated
        ]

        return {
            'updated': True,
            'count': len(updated),
            'changes': updated,
        }

    @endpoint(
        display_category='Réservation',
        description="Obtenir la semaine type d'une activité",
        name='get-recurrent-week',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'activity_id': {'description': "Numéro de l'activité"},
            'ref_date': {
                'description': "Date de référence, utilisée pour interroger l'agenda sur l'année et le mois",
                'type': 'date',
            },
        },
    )
    def get_recurrent_week(self, request, person_id, activity_id, ref_date, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_rl_or_child_raw(family_id, person_id)

        # use activity start date instead of ref_date, if posterior.
        start_datetime = None
        try:
            activity = self.referential.get(referential_name='Activity', item_id=activity_id)
        except Referential.DoesNotExist:
            pass
        else:
            start_datetime = min(
                parse_datetime(x['dateStart']) for x in activity.item_data['unitPortailList']
            )
        if start_datetime and ref_date < start_datetime.date():
            ref_date = start_datetime.date()

        payload = {
            'requestBean': {
                'numDossier': family_id,
                'numPerson': person_id,
                'idAct': activity_id,
                'year': ref_date.year,
                'month': ref_date.month,
            }
        }
        response = self.call('Activity', 'getPersonScheduleList', **payload)

        date_min_prev = None
        warning_msg = "No week calendar for family '%s', person '%s', activity '%s' on %s" % (
            family_id,
            person_id,
            activity_id,
            ref_date.strftime(utils.json_date_format),
        )
        recurrent_week = []
        day_names = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        for result_data in response or []:
            for schedule in result_data['activityScheduleList']:
                if schedule['activity']['idAct'] != activity_id or not schedule.get('weeklyCalendar'):
                    continue
                warning_msg = "No open day for family '%s', person '%s', activity '%s' on %s" % (
                    family_id,
                    person_id,
                    activity_id,
                    ref_date.strftime(utils.json_date_format),
                )
                units = []
                for item in schedule['unitScheduleList']:
                    key = item['unit']['calendarLetter']
                    value = item['unit']['libelle']
                    units.append((key, value))
                    if item.get('datePrevMin'):
                        if not date_min_prev or item['datePrevMin'] > date_min_prev:
                            date_min_prev = item['datePrevMin']
                for item in schedule['weeklyCalendar'].get('dayWeekInfoList') or []:
                    if item['isOpen']:
                        day_num = item['dayNum']
                        day_str = day_names[day_num - 1]
                        for key, value in units:
                            warning_msg = None
                            recurrent_week.append(
                                {
                                    'id': '%s-%s' % (day_num, key),
                                    'day': day_str,
                                    'label': value,
                                    'overlaps': ['%s-%s' % (day_num, k) for k, v in units if k != key],
                                    'text': '%s %s' % (day_str, value),
                                }
                            )
        if date_min_prev:
            date_min_prev = date_min_prev.strftime(utils.json_date_format)
        if warning_msg and "'None'" not in warning_msg:
            # do not raise on 'None' activity
            raise APIError(warning_msg, log_error=True)
        return {'data': recurrent_week, 'meta': {'date_min_prev': date_min_prev, 'warning_msg': warning_msg}}

    @endpoint(
        display_category='Réservation',
        description="Modifier la semaine type d'une inscription",
        name='update-recurrent-week',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {
                'schema': {
                    'application/json': activity_schemas.UPDATE_RECURRENT_WEEK_SCHEMA,
                }
            }
        },
    )
    def update_recurrent_week(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_rl_or_child_raw(family_id, post_data['person_id'])

        recurrent_week = {}
        for i in range(1, 8):
            day_num = str(i)
            recurrent_week[day_num] = {
                'dayNum': day_num,
                'calendarLetter': None,
                'isPresent': False,
            }
        for item in post_data.get('recurrent_week') or []:
            day_num, key = item.split('-')
            recurrent_week[day_num] = {
                'dayNum': day_num,
                'calendarLetter': key,
                'isPresent': True,
            }
        recurrent_week = sorted(recurrent_week.values(), key=lambda x: (x['dayNum']))

        payload = {
            'numFamily': family_id,
            'numPerson': post_data['person_id'],
            'idActivity': post_data['activity_id'],
            'dateStart': post_data['start_date'],
            'dateEnd': post_data.get('end_date'),
            'dayWeekInfoList': recurrent_week,
        }
        self.call('Activity', 'updateWeekCalendar', **payload)
        return {'data': 'ok'}

    @endpoint(
        display_category='Facture',
        description='Ajouter une autorisation de prélèvement',
        name='add-rl1-direct-debit-order',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {'schema': {'application/json': invoice_schemas.ADD_DIRECT_DEBIT_ORDER_SCHEMA}}
        },
    )
    def add_rl1_direct_debit_order(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        family = self.get_family_raw(family_id)

        post_data['numPerson'] = family['RL1']['num']
        self.call('Invoice', 'addDirectDebitOrder', numDossier=family_id, **post_data)
        return {'data': 'ok'}

    @endpoint(
        display_category='Facture',
        description="Obtenir les informations d'autorisation de prélèvement en cours à la date de référence",
        name='get-rl1-direct-debit-order',
        parameters={
            'codeRegie': {'description': 'Code de la régie'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'dateRef': {
                'description': 'Date de référence',
                'type': 'date',
            },
        },
    )
    def get_rl1_direct_debit_order(self, request, codeRegie, dateRef, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        family = self.get_family_raw(family_id)

        payload = {
            'numDossier': family_id,
            'numPerson': family['RL1']['num'],
            'codeRegie': codeRegie,
            'dateRef': dateRef,
        }
        response = self.call('Invoice', 'getDirectDebitOrder', **payload)
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les années scolaires',
        name='read-school-years-list',
        parameters={
            'subscribable': {
                'description': "N'afficher que les années ouvertes aux inscriptions",
                'example_value': '0',
            },
        },
    )
    def read_school_years_list(self, request, subscribable='1'):
        subscribable = utils.strtobool(subscribable)
        referential = self.get_referential('YearSchool')
        data = []
        for item in referential:
            if subscribable is True:
                start_date = item.get('dateStartSubscribeSchool')
                end_date = item.get('dateEndSubscribeSchool')
                if not (start_date and end_date):
                    continue
                start_date = parse_datetime(start_date)
                end_date = parse_datetime(end_date)
                if not (start_date <= now() <= end_date):
                    continue
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les niveaux scolaires',
        name='read-school-levels-list',
        parameters={
            'age': {'description': 'Âge de l\'enfant', 'example_value': '6'},
        },
    )
    def read_school_levels_list(self, request, age=None):
        data = self.get_referential('Level')
        if age and age.isnumeric():
            return {'data': [item for item in data if item.get('age') == int(age)]}
        return {'data': data}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les motifs de dérogation',
        name='read-exemption-reasons-list',
    )
    def read_exemption_reasons_list(self, request):
        return {'data': self.get_referential('DerogReason')}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les écoles pour une adresse et niveau scolaire',
        name='read-schools-for-address-and-level',
        parameters={
            'year': {'description': 'Année', 'example_value': '2022'},
            'id_street': {'description': 'Identifiant de la voie', 'example_value': '2317'},
            'num': {'description': 'Numéro dans la voie', 'example_value': '4'},
            'comp': {'description': 'Complément d\'adresse (bis, ...)'},
            'level': {'description': 'Niveau scolaire'},
        },
    )
    def read_schools_for_address_and_level(self, request, id_street, year, num=None, comp=None, level=None):
        self.assert_key_in_referential('Street', id_street, 'id_street parameter', required=True)
        self.assert_key_in_referential('Complement', comp, 'comp parameter', required=False)
        self.assert_key_in_referential('Level', level, 'level parameter', required=False)
        if num and not is_number(num):
            raise APIError('num parameter should be a number')

        response = self.call(
            'Site',
            'readSchoolForAdressAndLevel',
            readSchoolForAdressAndLevelRequestBean={
                'schoolYear': year,
                'levelCode': level,
                'adresse': {
                    'idStreet': id_street,
                    'num': num,
                    'numComp': comp,
                },
            },
        )
        data = []
        for item in response:
            item['id'] = item['idSchool']
            item['text'] = item['schoolName']
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les écoles pour un enfant et niveau scolaire',
        name='read-schools-for-child-and-level',
        parameters={
            'year': {'description': 'Année', 'example_value': '2023'},
            'child_id': {'description': 'Identifiant de l\'enfant', 'example_value': '190115'},
            'level': {'description': 'Niveau scolaire'},
        },
    )
    def read_schools_for_child_and_level(self, request, child_id, year, level=None):
        data = {
            'numPerson': child_id,
            'schoolYear': year,
        }
        if level:
            data['levelCode'] = level

        response = self.call('Family', 'readSchoolForChildAndLevel', **data)
        data = []
        for item in response:
            item['id'] = item['idSchool']
            item['text'] = item['schoolName']
            data.append(item)
        return {'data': data}

    @endpoint(
        display_category='Inscriptions',
        description="Remonter les informations scolaires d'un enfant",
        name='read-child-school-informations',
        parameters={
            'child_id': {'description': 'Identifiant de l\'enfant', 'example_value': '190115'},
            'level': {'description': 'Niveau scolaire', 'example_value': 'CP'},
            'year': {'description': 'Année scolaire', 'example_value': '2023'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_child_school_informations(self, request, child_id, level, year, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = {'numDossier': family_id, 'numPerson': child_id, 'schoolYear': year, 'level': level}

        response = self.call(
            'Family', 'getChildSubscribeSchoolInformation', getFamilySubscribeSchoolInfoRequestBean=data
        )
        if response['childSubscribeSchoolInformation'].get('subscribeSchoolInformation'):
            schools = response['childSubscribeSchoolInformation']['subscribeSchoolInformation'].get(
                'derogSchoolList'
            )
            for item in schools or []:
                item['id'] = item['idSchool']
                item['text'] = item['schoolName']
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Créer une pré-inscription scolaire pour un enfant',
        name='create-child-school-pre-registration',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {'schema': {'application/json': family_schemas.SCHOOL_PRE_REGISTRATION_SCHEMA}}
        },
    )
    def create_child_school_pre_registration(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        response = self.call('Family', 'preSubscribeSchoolPerim', **post_data)
        if not response.get('subscribeSchoolBean'):
            raise APIError(response.get('returnMessage') or 'no data returned')
        self.cache_delete(f'f-{family_id}')
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Créer une pré-inscription scolaire avec demande de dérogation',
        name='create-child-school-pre-registration-with-exemption',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {
                'schema': {'application/json': family_schemas.SCHOOL_PRE_REGISTRATION_WITH_EXEMPTION_SCHEMA}
            }
        },
    )
    def create_child_school_pre_registration_with_exemption(
        self, request, post_data, NameID=None, family_id=None
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        response = self.call('Family', 'presubscribeSchoolDerog', **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Créer une pré-inscription scolaire avec rapprochement de fratrie',
        name='create-child-school-pre-registration-with-sibling',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {
                'schema': {'application/json': family_schemas.SCHOOL_PRE_REGISTRATION_WITH_SIBLING_SCHEMA}
            }
        },
    )
    def create_child_school_pre_registration_with_sibling(
        self, request, post_data, NameID=None, family_id=None
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        response = self.call('Family', 'presubscribeSchoolSibling', **post_data)
        self.cache_delete(f'f-{family_id}')
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Obtenir le catalogue des activités loisir, avec leurs critères de recherche',
        name='read-activity-list',
    )
    def read_activity_list(self, request):
        labels = {
            'service': 'Service',
            'nature': "Nature de l'activité",
            'type': 'Discipline',
            'public': 'Public',
            'day': 'Jours',
            'place': 'Lieu',
        }
        day_names = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        all_criterias = {key: {'text': value, 'data': {}} for key, value in labels.items()}
        criterias = {key: {'text': value, 'data': {}} for key, value in labels.items()}

        def add_criteria(label_key, criteria_key, criteria_value):
            if not criteria_value:
                return
            criterias[label_key]['data'][criteria_key] = criteria_value
            if criteria_key not in all_criterias[label_key]['data']:
                all_criterias[label_key]['data'][criteria_key] = criteria_value

        def update_criterias_order_field(criterias_dict, label_keys=None):
            if not label_keys:
                label_keys = criterias_dict.keys()
            for label_key in label_keys:
                if label_key in ('public', 'day'):
                    criterias_dict[label_key]['order'] = sorted(x for x in criterias_dict[label_key]['data'])
                else:
                    criterias_dict[label_key]['order'] = [
                        x[0]
                        for x in sorted(criterias_dict[label_key]['data'].items(), key=lambda x: x[1].lower())
                    ]

        data = []
        for activity in self.get_referential('Activity', distinct=False):
            activity_type = activity['type']
            activity_nature = activity_type.get('natureSpec') if activity_type else None
            if not activity_nature or activity_nature['code'] not in self.get_loisir_nature_codes():
                continue
            service_text = activity['service'].get('text')

            for label_key in criterias:
                criterias[label_key]['data'] = {}
            add_criteria('nature', activity_nature['code'], activity_nature['libelle'])
            type_value = activity_type['libelle'].split('-')[0].strip()
            add_criteria('type', slugify(type_value), type_value)
            add_criteria('service', slugify(service_text), service_text)

            for day in activity['weekly_calendar']:
                if day['isOpen']:
                    add_criteria('day', str(day['dayNum']), day_names[day['dayNum'] - 1])

            update_criterias_order_field(criterias, ['service', 'nature', 'type', 'day'])

            many_units = len(activity['unitPortailList']) > 1
            for unit in activity.pop('unitPortailList'):
                unit_text_item = ''
                if many_units:
                    unit_text_item = ' (%s)' % unit['text']

                criterias['public']['data'] = {}
                start_dob = unit['birthDateStart']
                end_dob = unit['birthDateEnd']
                if start_dob:
                    start_dob = parse_datetime(start_dob)
                if end_dob:
                    end_dob = parse_datetime(end_dob)
                for key, value in utils.get_public_criterias(datetime.date.today(), start_dob, end_dob):
                    add_criteria('public', key, value)

                update_criterias_order_field(criterias, ['public'])

                for place in unit.pop('placeList'):
                    criterias['place']['data'] = {}
                    add_criteria('place', place['id'], place['text'])
                    update_criterias_order_field(criterias, ['place'])

                    data.append(
                        {
                            'id': '%s-%s-%s' % (activity['id'], unit['id'], place['id']),
                            'text': '%s%s, %s' % (activity['text'], unit_text_item, place['text']),
                            'activity': activity,
                            'unit': unit,
                            'place': place,
                            'criterias': copy.deepcopy(criterias),
                        }
                    )

        update_criterias_order_field(all_criterias)
        data.sort(key=lambda x: (x['unit']['dateStart'], x['text'], x['unit']['dateEnd']))
        return {
            'data': data,
            'meta': {
                'all_criterias': all_criterias,
                'all_criterias_order': ['service', 'nature', 'type', 'public', 'day', 'place'],
            },
        }

    @endpoint(
        display_category='Inscriptions',
        description="Obtenir le catalogue des activités d'une personne",
        name='get-person-activity-list',
        parameters={
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'nature': {
                'description': 'Nature des activités : EXTRASCO ou LOISIR (toutes par défaut)',
            },
            'type_ids': {
                'description': 'Codes des types des activités, séparées par des virgules',
                'example_value': 'EXTMERC,EXTVAC',
            },
            'start_date': {'description': 'Début de la période'},
            'end_date': {'description': 'Fin de la période'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text (URL encoding)',
                'example_value': '{{ activity.libelle2 }}',
            },
            'activity_label': {'description': "Libellé de l'activité (ex.: SV pour Séjour Vacance)"},
            'reference_year': {'description': 'Année de référence (schoolYear)'},
        },
    )
    def get_person_activity_list(
        self,
        request,
        person_id,
        NameID=None,
        family_id=None,
        nature=None,
        type_ids=None,
        start_date=None,
        end_date=None,
        text_template='{{ activity.libelle2|default:activity.libelle1 }}',
        activity_label=None,
        reference_year=None,
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        if start_date and end_date:
            start_date, end_date, reference_year = self.get_start_and_end_dates(start_date, end_date)

        response = self.get_person_activity_list_raw(
            family_id,
            person_id,
            nature=nature,
            type_ids=type_ids,
            reference_year=reference_year,
            start_date=start_date and start_date.strftime(utils.json_date_format),
            end_date=start_date and end_date.strftime(utils.json_date_format),
            activity_label=activity_label,
        )

        for item in response['catalogueActivityList']:
            item['id'] = item['activity']['idActivity']
            item['text'] = render_to_string(text_template, item).strip()
        return {'data': response['catalogueActivityList'], 'meta': {'person': response['person']}}

    @endpoint(
        display_category='Inscriptions',
        description="Lister les unités d'une activité pour une personne",
        name='get-person-unit-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'activity_id': {'description': "Numéro de l'activité"},
            'start_date': {'description': 'Début de la période'},
            'end_date': {'description': 'Fin de la période'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text (URL encoding)',
            },
        },
    )
    def get_person_unit_list(
        self,
        request,
        person_id,
        activity_id,
        NameID=None,
        family_id=None,
        start_date=None,
        end_date=None,
        text_template='{{ libelle }}',
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        reference_year = None
        if start_date and end_date:
            start_date, end_date, reference_year = self.get_start_and_end_dates(start_date, end_date)

        response = self.get_person_activity_list_raw(
            family_id,
            person_id,
            reference_year=reference_year,
            start_date=start_date and start_date.strftime(utils.json_date_format),
            end_date=start_date and end_date.strftime(utils.json_date_format),
        )
        for activity in response['catalogueActivityList']:
            if activity['activity']['idActivity'] == activity_id:
                break
        else:
            raise APIError('No activity %s for person' % activity_id)
        data = activity.pop('unitInfoList')
        meta = {'person': response['person'], 'activity': activity}
        for item in data:
            item['id'] = item['idUnit']
            context = dict(item)
            context['meta'] = meta
            item['text'] = render_to_string(text_template, context).strip()
        return {'data': data, 'meta': meta}

    @endpoint(
        display_category='Inscriptions',
        description="Lister les lieux d'une unité pour une personne",
        name='get-person-place-list',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'activity_id': {'description': "Numéro de l'activité"},
            'unit_id': {'description': "Numéro de l'unité"},
            'start_date': {'description': 'Début de la période'},
            'end_date': {'description': 'Fin de la période'},
            'text_template': {
                'description': 'Gabarit utilisé pour la valeur text (URL encoding)',
            },
        },
    )
    def get_person_place_list(
        self,
        request,
        person_id,
        activity_id,
        unit_id,
        NameID=None,
        family_id=None,
        start_date=None,
        end_date=None,
        text_template='{{ place.lib2|default:place.lib1 }}',
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        reference_year = None
        if start_date and end_date:
            start_date, end_date, reference_year = self.get_start_and_end_dates(start_date, end_date)

        response = self.get_person_activity_list_raw(
            family_id,
            person_id,
            reference_year=reference_year,
            start_date=start_date and start_date.strftime(utils.json_date_format),
            end_date=start_date and end_date.strftime(utils.json_date_format),
        )
        for activity in response['catalogueActivityList']:
            if activity['activity']['idActivity'] == activity_id:
                break
        else:
            raise APIError('No activity %s for person' % activity_id)
        for unit in activity['unitInfoList']:
            if unit['idUnit'] == unit_id:
                break
        else:
            raise APIError('No unit %s for person' % unit_id)
        data = unit.pop('placeInfoList')
        del activity['unitInfoList']
        meta = {'person': response['person'], 'activity': activity, 'unit': unit}
        place_ids = []
        for item in data:
            item['id'] = item['place']['idPlace']
            context = dict(item)
            context['meta'] = meta
            item['text'] = render_to_string(text_template, context).strip()
            place_ids.append(item['id'])
        meta['place_ids'] = place_ids
        return {'data': data, 'meta': meta}

    @endpoint(
        display_category='Inscriptions',
        description='Obtenir le catalogue geojson des activités pour une personne',
        name='get-person-catalog-geojson',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'nature': {
                'description': 'Nature des activités : EXTRASCO ou LOISIR (toutes par défaut)',
            },
            'type_ids': {
                'description': 'Codes des types des activités, séparées par des virgules',
                'example_value': 'EXTMERC,EXTVAC',
            },
            'start_date': {'description': 'Début de la période'},
            'end_date': {'description': 'Fin de la période'},
            'activity_id': {'description': "Numéro de l'activité"},
            'unit_id': {'description': "Numéro de l'unité"},
            'place_id': {'description': 'Numéro du lieu'},
        },
    )
    def get_person_catalog_geojson(
        self,
        request,
        person_id,
        NameID=None,
        family_id=None,
        start_date=None,
        end_date=None,
        nature=None,
        type_ids=None,
        activity_id=None,
        unit_id=None,
        place_id=None,
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        reference_year = None
        if start_date and end_date:
            start_date, end_date, reference_year = self.get_start_and_end_dates(start_date, end_date)

        response = self.get_person_activity_list_raw(
            family_id,
            person_id,
            nature=nature,
            type_ids=type_ids,
            reference_year=reference_year,
            start_date=start_date and start_date.strftime(utils.json_date_format),
            end_date=start_date and end_date.strftime(utils.json_date_format),
        )

        def places(properties, place_list, place_id=None):
            for place in place_list:
                properties['place'] = place
                properties['place_id'] = place['place']['idPlace']
                if not place['place']['longitude'] or not place['place']['latitude']:
                    continue
                if place_id:
                    if properties['place_id'] == place_id:
                        yield properties
                        break
                else:
                    yield properties

        def units(properties, unit_list, unit_id=None, place_id=None):
            for unit in unit_list:
                place_list = unit.pop('placeInfoList')
                properties['unit'] = unit
                properties['unit_id'] = unit['idUnit']
                if unit_id:
                    if properties['unit_id'] == unit_id:
                        yield from places(properties, place_list, place_id)
                        break
                else:
                    yield from places(properties, place_list, place_id)

        def activities(activity_id=None, unit_id=None, place_id=None):
            for activity in response['catalogueActivityList']:
                unit_list = activity.pop('unitInfoList')
                properties = {
                    'person': response['person'],
                    'activity_id': activity['activity']['idActivity'],
                    'activity': activity,
                }
                if activity_id:
                    if properties['activity_id'] == activity_id:
                        yield from units(properties, unit_list, unit_id, place_id)
                        break
                else:
                    yield from units(properties, unit_list, unit_id, place_id)

        geojson = {
            'type': 'FeatureCollection',
            'features': [],
        }
        for item in activities(activity_id, unit_id, place_id):
            geojson['features'].append(
                {
                    'type': 'Feature',
                    'geometry': {
                        'coordinates': [
                            float(item['place']['place']['longitude']),
                            float(item['place']['place']['latitude']),
                        ],
                        'type': 'Point',
                    },
                    'properties': {
                        'id': '%s:%s:%s' % (item['activity_id'], item['unit_id'], item['place_id']),
                        'text': '%s / %s / %s'
                        % (
                            item['activity']['activity']['libelle2']
                            or item['activity']['activity']['libelle1'],
                            item['unit']['libelle'],
                            item['place']['place']['lib1'],
                        ),
                        **item,
                    },
                }
            )
        return geojson

    @endpoint(
        display_category='Inscriptions',
        description='Lister les natures des activités',
        name='read-activity-nature-list',
        parameters={
            'nature_ids': {
                'description': 'Codes des natures des activités (tous par défaut), séparées par des virgules',
                'example_value': 'P,1,2',
            },
        },
    )
    def read_activity_nature_list(self, request, nature_ids=None):
        data = self.get_referential('ActivityNatureType')
        if nature_ids:
            codes = [x.strip() for x in nature_ids.split(',') if x.strip()]
            data = [x for x in data if x['id'] in codes]

        groups = {}
        for group in data:
            types = []
            for item in group['activityTypeList']:
                types.append(
                    {
                        'id': item['code'],
                        'text': item['libelle'],
                        **item,
                    }
                )
            groups[group['id']] = types
        return {'data': data, 'meta': groups}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les indicateurs pour les activités petite enfance',
        name='read-ape-indicators-list',
    )
    def read_ape_indicators_list(self, request, level=None):
        data = self.get_referential('ApeIndicator')

        levels = {}
        for level in data:
            indicators = []
            for item in level['indicatorList']:
                indicators.append(
                    {
                        'id': item['code'],
                        'text': item['libelle'],
                        **item,
                    }
                )
            levels[level['id']] = indicators
        return {'data': data, 'meta': levels}

    def get_recurrent_info_from_subscription_info(self, response):
        day_names = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        recurrent_week = []
        weekly_calendar = response.get('weeklyCalendarActivity')
        calendar_generation = response.get('calendarGeneration') or {}
        calendar_generation_value = calendar_generation.get('value') or 'I'
        if calendar_generation_value in ('O', 'F') and weekly_calendar:
            units = []
            for item in response.get('unitScheduleList') or []:
                key = item['unit']['calendarLetter']
                value = item['unit']['libelle']
                units.append((key, value))
            if calendar_generation.get('value') == 'O' and len(units) > 1:
                raise APIError(
                    'connector do not manage activity having both calendarGeneration required and several units'
                )

            for item in weekly_calendar.get('dayWeekInfoList') or []:
                if item['isOpen']:
                    day_num = item['dayNum']
                    day = day_names[day_num - 1]
                    for key, value in units:
                        recurrent_week.append(
                            {
                                'id': '%s-%s' % (day_num, key),
                                'day': day,
                                'label': value,
                                'overlaps': ['%s-%s' % (day_num, k) for k, v in units if k != key],
                                'text': '%s %s' % (day, value),
                            }
                        )
        return {
            'calendar_generation': calendar_generation_value,
            'recurrent_week': recurrent_week,
        }

    @endpoint(
        display_category='Inscriptions',
        description="Obtenir les informations pour s'inscrire puis réserver sur l'extra-scolaire ou le loisir",
        name='get-person-subscription-info',
        parameters={
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'activity_id': {'description': "Numéro de l'activité"},
            'unit_id': {'description': "Numéro de l'unité"},
            'place_id': {'description': 'Numéro du lieu'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'ref_date': {'description': 'Date du début du calcul'},
        },
    )
    def get_person_subscription_info(
        self,
        request,
        person_id,
        activity_id,
        unit_id,
        place_id,
        NameID=None,
        family_id=None,
        ref_date=None,
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        if ref_date:
            try:
                ref_date = parse_date(ref_date)
            except ValueError:
                raise APIError('%s is not a valid date' % ref_date, http_status=400)
            if not ref_date:
                raise APIError('bad date format, should be YYYY-MM-DD', http_status=400)
            ref_date = ref_date.strftime(utils.json_date_format)

        params = {
            'numDossier': family_id,
            'numPerson': person_id,
            'activityUnitPlace': {
                'idActivity': activity_id,
                'idUnit': unit_id,
                'idPlace': place_id,
            },
            'dateRef': ref_date,
        }
        response = self.call('Activity', 'getPersonUnitInfo', getPersonUnitInfoRequestBean=params)
        recurrent_info = self.get_recurrent_info_from_subscription_info(response)
        if recurrent_info['calendar_generation'] == 'F':
            response['recurrent_week'] = recurrent_info['recurrent_week']
        else:
            response['recurrent_week'] = []

        if response.get('conveyance'):
            for part_of_day in response['conveyance'].values():
                if not part_of_day:
                    continue
                for bus_stop in part_of_day['depositPlaceList'] or []:
                    place = bus_stop['place']
                    bus_stop['id'] = place['id']
                    bus_stop['text'] = place['lib2'] or place['lib']

        indicators = {}
        for item in response.get('indicatorList') or []:
            item['id'] = item['code']
            item['text'] = item['libelle']
            indicators[item['id']] = item
        response['indicators'] = indicators

        bookings = []
        many_units = len(response.get('unitScheduleList') or []) > 1
        for day in response.get('openDayList') or []:
            day_str = day['day'].strftime(utils.json_date_format)
            for unit in response.get('unitScheduleList') or []:
                if not unit['unit']['dateDeb'] <= day['day'] <= unit['unit']['dateFin']:
                    continue
                booking = {
                    'id': '%s:%s:%s:%s' % (person_id, activity_id, unit['unit']['idUnit'], day_str),
                    'text': dateformat.format(day['day'], 'l j F Y'),
                    'prefill': False,
                    'disabled': day['hasPlace'] is False,
                    'details': day,
                }
                booking['details']['day_str'] = day_str
                booking['details']['status_color'] = 'white'

                booking['details']['activity_label'] = (
                    response['activity']['libelle2'] or response['activity']['libelle1']
                )
                if many_units:
                    booking['details']['activity_label'] += ' (%s)' % unit['unit']['libelle']

                # uncheck other units selected on same day
                booking['details']['activity_id'] = unit['unit']['idUnit']

                bookings.append(copy.deepcopy(booking))
        # sort bookings
        bookings = [
            (
                b['details']['day'],
                b['details']['activity_label'],
                b,
            )
            for b in bookings
        ]
        bookings = sorted(bookings, key=itemgetter(0, 1))
        bookings = [b for d, l, b in bookings]
        response['agenda'] = bookings
        return {'data': response}

    def process_subscription_payload(self, family_id, post_data):
        params = {
            'numDossier': family_id,
            'numPerson': post_data['person_id'],
            'activityUnitPlace': {
                'idActivity': post_data['activity_id'],
                'idUnit': post_data['unit_id'],
                'idPlace': post_data['place_id'],
            },
        }
        subscription_info = self.call('Activity', 'getPersonUnitInfo', getPersonUnitInfoRequestBean=params)
        if post_data.get('conveyanceSubscribe') and (
            post_data['conveyanceSubscribe'].get('idPlaceMorning')
            or post_data['conveyanceSubscribe'].get('idPlaceAfternoon')
        ):
            if not subscription_info.get('conveyance'):
                raise APIError('no conveyance defined on this activity')
            for payload_key, info_key in [
                ('idPlaceMorning', 'morningJourney'),
                ('idPlaceAfternoon', 'afternoonJourney'),
            ]:
                if post_data['conveyanceSubscribe'].get(payload_key):
                    info_bus_ids = [
                        x['place']['id']
                        for x in subscription_info['conveyance'][info_key]['depositPlaceList']
                    ]
                    payload_bus_id = post_data['conveyanceSubscribe'][payload_key]
                    if not payload_bus_id in info_bus_ids:
                        raise APIError(
                            'no "%s" place defined on "%s" conveyance' % (payload_bus_id, info_key)
                        )

        recurrent_info = self.get_recurrent_info_from_subscription_info(subscription_info)
        available_items = [x['id'] for x in recurrent_info['recurrent_week']]
        if recurrent_info['calendar_generation'] == 'O':
            # automaticaly select all recurrent slots
            selected_recurrent_week = available_items
        else:
            selected_recurrent_week = post_data.get('recurrent_week')
        recurrent_week = []
        for item in selected_recurrent_week or []:
            if item not in available_items:
                raise APIError("recurrent item '%s' is not available no on this activity" % item)
            day_num, key = item.split('-')
            recurrent_week.append(
                {
                    'dayNum': day_num,
                    'calendarLetter': key,
                    'isPresent': True,
                }
            )

        expected_codes = [x['code'] for x in subscription_info.get('indicatorList', [])]
        indicators = []
        for i, item in enumerate(post_data.get('indicatorList') or []):
            item['isActive'] = self.encode_bool(item['isActive'])
            if item['isActive'] is None:  # no value, skip indicator
                continue
            if item['code'] not in expected_codes:
                raise APIError(
                    "indicatorList/%i/code key value '%s' do not belong to activity indicators"
                    % (i, item['code'])
                )
            indicators.append(item)

        return recurrent_week, indicators

    @endpoint(
        display_category='Inscriptions',
        description='Ajouter au panier une inscription extra-scolaire ou loisir',
        name='add-person-basket-subscription',
        post={
            'request_body': {
                'schema': {
                    'application/json': activity_schemas.SUBSCRIPTION_SCHEMA,
                }
            }
        },
    )
    def add_person_basket_subscription(
        self,
        request,
        post_data,
        NameID=None,
        family_id=None,
    ):
        family_id = utils.normalize_id(family_id)
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_rl_or_child_raw(family_id, post_data['person_id'])
        recurrent_week, indicators = self.process_subscription_payload(family_id, post_data)

        form_number = post_data.get('form_number')
        if form_number and self.subscription_set.filter(wcs_form_number=form_number):
            raise APIError("'%s' wcs demand is already linked to a subscription" % form_number)

        payload = {
            'addPersonUnitBasketRequestBean': {
                'numFamily': family_id,
                'numPerson': post_data['person_id'],
                'idAct': post_data['activity_id'],
                'idUnit': post_data['unit_id'],
                'idPlace': post_data['place_id'],
                'dateStartSubscribe': post_data['start_date'],
                'dateEndSubscribe': post_data['end_date'],
                'dayWeekInfoList': recurrent_week,
                'conveyanceSubscribe': post_data.get('conveyanceSubscribe'),
                'indicatorList': indicators,
            }
        }
        response = self.call('Activity', 'addPersonUnitBasket', **payload)
        if not response['controlResult']['controlOK']:
            raise APIError(response['controlResult']['message'])
        self.cache_delete(f'f-{family_id}')

        # record subscription in order to trigger w.c.s. demand
        basket = response.get('basket')
        if post_data.get('form_api_url') and form_number and basket:
            regie_id = basket.get('codeRegie')
            if regie_id:
                related_lines = []
                for line in basket.get('lignes') or []:
                    if (
                        str(line['personneInfo']['numPerson']) == post_data['person_id']
                        and line['inscription']['idAct'] == post_data['activity_id']
                        and line['inscription']['idUnit'] == post_data['unit_id']
                        and line['inscription']['idLieu'] == post_data['place_id']
                    ):
                        if line.get('idIns'):
                            # subscription ids will be used to match an invoice
                            related_lines.append(line)
                if related_lines:
                    # remove unrelated basket lines from response
                    response['basket']['lignes'] = related_lines

                    self.subscription_set.create(
                        wcs_form_api_url=post_data['form_api_url'],
                        wcs_form_number=form_number,
                        regie_id=regie_id,
                        family_id=family_id,
                        maelis_data=response,
                    )
                else:
                    response['connector_message'] = (
                        "Pas de lignes en rapport avec la demande '%s' dans le panier généré."
                        % (post_data.get('form_number', '?'))
                    )
                    self.logger.warning(response['connector_message'])
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Ajouter une inscription extra-scolaire ou loisir',
        name='add-person-subscription',
        post={
            'request_body': {
                'schema': {
                    'application/json': activity_schemas.SUBSCRIPTION_SCHEMA,
                }
            }
        },
    )
    def add_person_subscription(
        self,
        request,
        post_data,
        NameID=None,
        family_id=None,
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_rl_or_child_raw(family_id, post_data['person_id'])
        recurrent_week, indicators = self.process_subscription_payload(family_id, post_data)
        payload = {
            'AddPersonSubscribeRequestBean': {
                'numFamily': family_id,
                'numPerson': post_data['person_id'],
                'idAct': post_data['activity_id'],
                'idUnit': post_data['unit_id'],
                'idPlace': post_data['place_id'],
                'dateStartSubscribe': post_data['start_date'],
                'dateEndSubscribe': post_data['end_date'],
                'dayWeekInfoList': recurrent_week,
                'conveyanceSubscribe': post_data.get('conveyanceSubscribe'),
                'indicatorList': indicators,
            }
        }
        response = self.call('Activity', 'addPersonUnitSubscribe', **payload)
        if not response['controlOK']:
            raise APIError(response['message'])
        self.cache_delete(f'f-{family_id}')
        return {'data': response}

    def get_activity_bookings(self, family_id, person_id, activity_id, start_date, end_date):
        bookings = []
        for booking_date in rrule.rrule(rrule.MONTHLY, dtstart=start_date.replace(day=1), until=end_date):
            payload = {
                'requestBean': {
                    'numDossier': family_id,
                    'numPerson': person_id,
                    'year': booking_date.year,
                    'month': booking_date.month,
                    'idAct': activity_id,
                }
            }
            response = self.call('Activity', 'getPersonScheduleList', **payload)
            for result_data in response or []:
                for schedule in result_data['activityScheduleList']:
                    activity = schedule['activity']
                    if activity['idAct'] != activity_id:
                        continue
                    many_units = len(schedule['unitScheduleList'] or []) > 1
                    for unit in schedule['unitScheduleList'] or []:
                        for day in unit['dayInfoList'] or []:
                            if day['status'] in ['NO_READ', 'NO_CUSTODY']:
                                continue
                            booking = {
                                'id': '%s:%s:%s:%s'
                                % (
                                    person_id,
                                    activity_id,
                                    unit['unit']['idUnit'],
                                    day['day'].strftime(utils.json_date_format),
                                ),
                                'text': dateformat.format(day['day'], 'l j F Y'),
                                'prefill': day['scheduledPresence'] > 0,
                                'disabled': day['status'] != 'WRITABLE',
                                'details': day,
                            }
                            color = 'white'
                            if booking['prefill']:
                                color = 'green'

                            booking['details']['day_str'] = day['day'].strftime(utils.json_date_format)
                            booking['details']['status_color'] = color
                            booking['details']['activity_label'] = (
                                unit['unit']['libelle'] if many_units else ''
                            )

                            # uncheck other sub-units selected on same day
                            booking['details']['activity_id'] = unit['unit']['idUnit']

                            # use to call maelis for booking
                            booking['details']['maelis_activity_id'] = activity_id
                            booking['details']['maelis_unit_id'] = unit['unit']['idUnit']

                            # use to compare from maelis errors messages
                            booking['details']['maelis_day_str'] = day['day'].strftime('%d/%m/%Y')
                            booking['details']['maelis_unit_label'] = unit['unit']['libelle']

                            bookings.append(copy.deepcopy(booking))
        # sort bookings
        bookings = [
            (
                b['details']['day'],
                b['details']['activity_label'],
                b,
            )
            for b in bookings
        ]
        bookings = sorted(bookings, key=itemgetter(0, 1))
        bookings = [b for d, l, b in bookings]
        return bookings

    @endpoint(
        display_category='Réservation',
        description="Obtenir l'agenda d'une activité",
        name='read-activity-agenda',
        parameters={
            'person_id': {'description': "Numéro du responsable légal ou de l'enfant"},
            'activity_id': {'description': "Numéro de l'activité"},
            'start_date': {'description': 'Début de la période', 'type': 'date'},
            'end_date': {'description': 'Fin de la période', 'type': 'date'},
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def read_activity_agenda(
        self, request, person_id, activity_id, start_date, end_date, NameID=None, family_id=None
    ):
        family_id = self.assert_family_or_link(family_id, NameID)
        if start_date > end_date:
            raise APIError('start_date should be before end_date', http_status=400)
        bookings = self.get_activity_bookings(family_id, person_id, activity_id, start_date, end_date)
        return {
            'data': bookings,
            'extra_data': {
                'start_date': start_date,
                'end_date': end_date,
            },
        }

    @endpoint(
        display_category='Réservation',
        description="Modifier l'agenda d'un enfant",
        name='update-activity-agenda',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={
            'request_body': {
                'schema': {
                    'application/json': activity_schemas.BOOKING_ACTIVITY_SCHEMA,
                }
            }
        },
    )
    def update_activity_agenda(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        person_id = post_data['person_id']
        activity_id = post_data['activity_id']
        start_date, end_date, dummy = self.get_start_and_end_dates(
            post_data['start_date'], post_data['end_date']
        )
        requested_bookings = post_data['booking_list']

        # build list of existing booked days
        bookings = self.get_activity_bookings(family_id, person_id, activity_id, start_date, end_date)
        legacy_bookings = [b['id'] for b in bookings if b['prefill'] is True]
        available_bookings = [b['id'] for b in bookings if b['disabled'] is False]

        updated = {}
        bookings_to_update = []
        for booking_info in bookings:
            day_id = booking_info['id']
            booked = None
            action = booking_info['details']['action']
            if day_id not in available_bookings:
                # disabled or not available: not bookable
                booked = None
            elif (
                day_id not in legacy_bookings
                and day_id in requested_bookings
                and action in ['ADD_PRES_PREVI']
            ):
                booked = action
            elif (
                day_id in legacy_bookings
                and day_id not in requested_bookings
                and action in ['DEL_PRES_PREVI']
            ):
                booked = action
            if booked is not None:
                # no changes, don't send the day
                bookings_to_update.append(
                    {
                        'numPerson': person_id,
                        'idAct': booking_info['details']['maelis_activity_id'],
                        'idUni': booking_info['details']['maelis_unit_id'],
                        'date': booking_info['details']['day_str'],
                        'action': booked,
                    }
                )
                updated[day_id] = {
                    'activity_label': booking_info['details']['activity_label'],
                    'day': booking_info['details']['day_str'],
                    'booked': booked in ['ADD_PRES_PREVI'],
                }

        if not bookings_to_update:
            # don't call maelis if no changes
            return {'updated': False, 'count': 0, 'changes': [], 'errors': []}
        payload = {
            'requestBean': {
                'numDossier': family_id,
                'unitPersonDayInfoList': bookings_to_update,
            }
        }
        response = self.call('Activity', 'updatePersonSchedule', **payload)

        # booking errors from maelis
        if response.get('result') is False:
            for error in response['unitPersonDayInfoErrorList']:
                day_id = '%s:%s:%s:%s' % (
                    error['unitPersonDayInfoBean']['numPerson'],
                    error['unitPersonDayInfoBean']['idAct'],
                    error['unitPersonDayInfoBean']['idUni'],
                    error['unitPersonDayInfoBean']['date'].strftime(utils.json_date_format),
                )
                if updated.get(day_id):
                    del updated[day_id]

        changes = sorted(updated.values(), key=lambda x: (x['booked'], x['activity_label'], x['day']))
        return {
            'updated': True,
            'count': len(changes),
            'changes': changes,
        }

    @endpoint(
        display_category='Inscriptions',
        description='Obtenir les paniers de la famille',
        name='get-baskets',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
    )
    def get_baskets(self, request, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        baskets = self.get_baskets_raw(family_id)
        for item in baskets:
            item['text'] = self.get_referential_value('Regie', item['codeRegie'])

        # send invoice cancellation order more often than hourly
        self.cancel_basket_invoices()

        return {'data': baskets}

    @endpoint(
        display_category='Inscriptions',
        description='Prolonger la durée de vie du panier',
        name='update-basket-time',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': activity_schemas.BASKET_SCHEMA}}},
    )
    def update_basket_time(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_basket_raw(family_id, post_data['basket_id'])

        self.call('Activity', 'updateBasketTime', idBasket=post_data['basket_id'])
        return {'data': 'ok'}

    @endpoint(
        display_category='Inscriptions',
        description='Supprimer une ligne du panier',
        name='delete-basket-line',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': activity_schemas.BASKET_LINE_SCHEMA}}},
    )
    def delete_basket_line(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        basket = self.get_basket_raw(family_id, post_data['basket_id'])
        for line in basket['lignes']:
            if line['id'] == post_data['line_id']:
                break
        else:
            raise APIError("no '%s' basket line on basket" % post_data['line_id'])

        response = self.call(
            'Activity',
            'deletePersonUnitBasket',
            deletePersonUnitBasketRequestBean={
                'idBasketLine': post_data['line_id'],
            },
        )
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Supprimer le panier de la famille',
        name='delete-basket',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': activity_schemas.BASKET_SCHEMA}}},
    )
    def delete_basket(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        self.get_basket_raw(family_id, post_data['basket_id'])

        self.call(
            'Activity',
            'deleteBasket',
            deleteBasketRequestBean={
                'idBasket': post_data['basket_id'],
                'idUtilisat': NameID or 'Middle-office',
            },
        )
        return {'data': 'ok'}

    @endpoint(
        display_category='Inscriptions',
        description='Valider le panier de la famille',
        name='validate-basket',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': activity_schemas.BASKET_SCHEMA}}},
    )
    def validate_basket(self, request, post_data, NameID=None, family_id=None):
        family_id = utils.normalize_id(family_id)
        family_id = self.assert_family_or_link(family_id, NameID)
        basket_id = post_data['basket_id']
        self.get_basket_raw(family_id, basket_id)

        # prevent maelis creating an invoice if the is no related new subscriptions
        subscriptions = self.subscription_set.filter(maelis_data__basket__id=basket_id)
        if not any(x.status() == 'pending_basket' for x in subscriptions):
            raise APIError("Basket '%s' has no related pending subscription" % basket_id)

        response = self.call(
            'Activity',
            'validateBasket',
            validateBasketRequestBean={
                'idBasket': basket_id,
            },
        )

        # only one invoice should be returned, create it now to manage cancellation
        invoice = None
        if response:
            for item in response.get('factureLst') or []:
                regie_id = item['regie']['code']
                invoice = self.invoice_set.create(
                    regie_id=regie_id,
                    invoice_id=item['numInvoice'],
                    family_id=family_id,
                    basket_generation_date=now(),
                    maelis_data=item,
                    maelis_data_update_date=now(),
                )
                self.logger.info("Ajout de %s sur la famille '%s'", repr(invoice), family_id)
                invoice.match_subscriptions()
                self.cache_delete(f'i-{regie_id}-{family_id}')

        if not invoice:
            inscriptions = [
                y['inscription'] for x in subscriptions for y in x.maelis_data['basket']['lignes']
            ]
            activities = {'%s / %s' % (x['idAct'], x['libAct2'] or x['libAct']) for x in inscriptions}
            text = 'les activités' if len(activities) > 1 else "l'activité"
            text += ' : ' + ', '.join(sorted(activities))
            response['connector_message'] = (
                "Pas de facture à la validation du panier '%s' pour la famille '%s' sur %s."
                % (
                    basket_id,
                    family_id,
                    text,
                )
            )
            self.logger.error(response['connector_message'])
        return {'data': response}

    @endpoint(
        display_category='Inscriptions',
        description='Lister les crèches',
        name='read-nursery-list',
        parameters={
            'activity_type': {'description': "Type de l'activité.", 'example_value': 'CRECHCO'},
            'code_psu': {'description': 'Code PSU.', 'example_value': 'REGULAR'},
            'service_ids': {
                'description': 'Codes des services à filtrer, séparées par des virgules.',
                'example_value': 'A10054639474, A10054639473',
            },
        },
    )
    def read_nursery_list(self, request, activity_type=None, code_psu=None, service_ids=None):
        nurseries = self.get_referential('Nursery')
        if service_ids:
            service_codes = [x.strip() for x in str(service_ids or '').split(',') if x.strip()]
            nurseries = [n for n in nurseries if n.get('idService') in service_codes]
        if activity_type:
            nurseries = [n for n in nurseries if n['activityType']['code'] == activity_type]
        if code_psu:
            nurseries = [n for n in nurseries if code_psu in [u['typeAcc'] for u in n['unitList']]]

        for item in nurseries:
            item['activity_id'] = item['idActivity']
            item['place_id'] = item['place']['idPlace']
            item['unit_ids'] = {}
            for unit in item['unitList'] or []:
                item['unit_ids'][unit['typeAcc']] = unit['idUnit']
            if code_psu:
                item['unit_id'] = item['unit_ids'].get(code_psu)
            if item['libelle2']:
                item['text'] = item['libelle2']
            item['service'] = self.get_referential_item_data('Service', item.get('idService'))
        return {'data': nurseries}

    @endpoint(
        display_category='Inscriptions',
        description='Obtenir un geojson avec la liste des crèches',
        name='get-nursery-geojson',
        parameters={
            'activity_type': {'description': "Type de l'activité.", 'example_value': 'CRECHCO'},
            'code_psu': {'description': 'Code PSU. (REGULAR par défaut)'},
            'service_ids': {
                'description': 'Codes des services à filtrer, séparées par des virgules.',
                'example_value': 'A10054639474, A10054639473',
            },
        },
    )
    def get_nursery_geojson(self, request, activity_type=None, code_psu='REGULAR', service_ids=None):
        nurseries = self.get_referential('Nursery')
        geojson = {
            'type': 'FeatureCollection',
            'features': [],
        }

        service_codes = []
        if service_ids:
            service_codes = [x.strip() for x in str(service_ids or '').split(',') if x.strip()]

        for item in nurseries:
            if service_codes and item.get('idService') not in service_codes:
                continue
            if activity_type and item['activityType']['code'] != activity_type:
                continue
            if not item['place']['longitude'] or not item['place']['latitude']:
                continue

            item['activity_id'] = item['idActivity']
            item['place_id'] = item['place']['idPlace']
            for unit in item['unitList'] or []:
                if code_psu and unit['typeAcc'] != code_psu:
                    continue

                item['unit_id'] = unit['idUnit']
                geojson['features'].append(
                    {
                        'type': 'Feature',
                        'geometry': {
                            'coordinates': [
                                float(item['place']['longitude']),
                                float(item['place']['latitude']),
                            ],
                            'type': 'Point',
                        },
                        'properties': {
                            **item,
                            'id': '%s:%s:%s' % (item['activity_id'], item['unit_id'], item['place_id']),
                            'text': unit['libelle'],
                            'unit': unit,
                            'service': self.get_referential_item_data('Service', item.get('idService')),
                        },
                    }
                )
        return geojson

    @endpoint(
        display_category='Inscriptions',
        description='Créer une demande de place en crèche pour un enfant',
        name='create-nursery-demand',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
        },
        post={'request_body': {'schema': {'application/json': family_schemas.NURSERY_DEMAND_SCHEMA}}},
    )
    def create_nursery_demand(self, request, post_data, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        apeIndicators = self.get_referential('ApeIndicator')
        for group in apeIndicators:
            if group['id'] == 'INDI_APE_ENF':
                key = 'child_indicators'
            elif group['id'] == 'INDI_APE_FAM':
                key = 'family_indicators'
            else:
                key = 'demand_indicators'
            expected_codes = [x['code'] for x in group['indicatorList']]
            for i, item in enumerate(post_data.get(key) or []):
                if item['code'] not in expected_codes:
                    raise APIError(
                        "%s/%i/code key value '%s' do not belong to APE '%s' indicators"
                        % (key, i, item['code'], group['id'][-3:])
                    )
                item['isActive'] = self.encode_bool(item['isActive'])

        child_data = {}
        if post_data.get('child_id'):
            child_data['numPerson'] = post_data['child_id']
        else:
            child_data.update(
                {
                    'firstname': post_data.get('child_first_name'),
                    'lastname': post_data.get('child_last_name'),
                    'sexe': post_data.get('child_gender'),
                    'birth': {'dateBirth': post_data.get('child_birthdate')},
                }
            )
        if post_data.get('child_indicators'):
            child_data['indiPersList'] = post_data['child_indicators']

        book_data = {'dateDepot': now().strftime(utils.json_date_format), 'datStart': post_data['start_date']}

        if post_data.get('number_of_days'):
            book_data['nbDayByWeek'] = post_data.get('number_of_days')

        for day in ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'):
            if post_data.get('start_hour_%s' % day):
                book_data['startHour%s' % day] = post_data.get('start_hour_%s' % day)

            if post_data.get('end_hour_%s' % day):
                book_data['endHour%s' % day] = post_data.get('end_hour_%s' % day)

        if 'accept_other_nurseries' in post_data:
            book_data['isAcceptOtherNursery'] = bool(post_data['accept_other_nurseries'])

        if post_data.get('comment'):
            book_data['description'] = post_data['comment']

        if post_data.get('comment'):
            book_data['description'] = post_data['comment']

        if post_data.get('demand_indicators'):
            book_data['indiResapeList'] = post_data['demand_indicators']

        for i in range(1, 4):
            if post_data.get('nursery%i' % i):
                book_data['choice%s' % i] = post_data['nursery%s' % i]

        data = {
            'numDossier': post_data['family_id'],
            'child': child_data,
            'apeBook': book_data,
        }

        if post_data.get('family_indicators'):
            data['indiFamList'] = post_data['family_indicators']

        response = self.call('Ape', 'addApeBook', request=data)
        self.cache_delete(f'f-{family_id}')
        return {'data': response}

    @endpoint(
        display_category='Facture',
        description='Lister les régies',
        name='read-regie-list',
    )
    def read_regie_list(self, request):
        return {'data': self.get_referential('Regie')}

    def get_invoices(self, family_id, regie_id, use_cache=True):
        family_id = utils.normalize_id(family_id)
        cache_key = f'i-{regie_id}-{family_id}'
        if self.cache_get(cache_key) and use_cache:
            qs = self.invoice_set.filter(
                regie_id=regie_id, family_id=family_id, maelis_no_more_returned_date__isnull=True
            )
            qs.known_invoice_ids = True
            return qs
        self.assert_key_in_referential('Regie', regie_id, 'regie_id parameter')
        known_invoice_ids = set()
        try:
            result = self.call(
                'Invoice',
                'readInvoices',
                numDossier=family_id,
                codeRegie=regie_id,
                dateStart='1970-01-01',
                dateEnd=now().strftime(utils.json_date_format),
            )
        except SOAPServiceUnreachable:
            known_invoice_ids = None
        except SOAPFault as e:
            logger = self.logger.exception
            if 'E02 :' in str(e):
                logger = self.logger.warning
            logger('unexpected exception in get_invoices (%s)', e)
            known_invoice_ids = None
        else:
            last_update = now()
            bulk_update_data = []
            seen_pks = set()
            for item in result:
                invoice_id = item['numInvoice']
                if not invoice_id:
                    continue
                known_invoice_ids.add(invoice_id)
                invoice, created = self.invoice_set.get_or_create(
                    regie_id=regie_id,
                    invoice_id=invoice_id,
                    defaults={
                        'family_id': family_id,
                        'maelis_data': item,
                        'maelis_data_update_date': last_update,
                    },
                )
                seen_pks.add(invoice.pk)
                if created:
                    self.logger.info("Ajout de %s sur la famille '%s'", repr(invoice), family_id)
                else:
                    if invoice.family_id != family_id:
                        self.logger.error(
                            "%s sur la famille '%s' existe déjà sur la famille '%s'",
                            repr(invoice),
                            family_id,
                            invoice.family_id,
                        )
                        continue
                    content_one = json.dumps(invoice.maelis_data, sort_keys=True, indent=2)
                    content_two = json.dumps(item, sort_keys=True, indent=2, cls=DjangoJSONEncoder)
                    if content_one != content_two:
                        invoice.maelis_data = item
                        invoice.maelis_data_update_date = last_update
                        complete_diff = difflib.ndiff(content_one.split('\n'), content_two.split('\n'))
                        diff = [x for x in complete_diff if x[0] in ['-', '+']]
                        self.logger.info(
                            "Mise à jour de %s sur la famille '%s': %s", repr(invoice), family_id, diff
                        )
                    if content_one != content_two or invoice.maelis_no_more_returned_date is not None:
                        invoice.maelis_no_more_returned_date = None
                        invoice.updated = last_update
                        bulk_update_data.append(invoice)

                Invoice.objects.bulk_update(
                    bulk_update_data,
                    fields=[
                        'updated',
                        'maelis_data',
                        'maelis_data_update_date',
                        'maelis_no_more_returned_date',
                    ],
                )

            # remind invoices that are no more returned by maelis
            no_more_returned_invoices = self.invoice_set.filter(
                regie_id=regie_id,
                family_id=family_id,
                maelis_no_more_returned_date__isnull=True,
            ).exclude(pk__in=seen_pks)
            no_more_returned_invoices.update(maelis_no_more_returned_date=last_update)

        if known_invoice_ids is not None:
            self.cache_set(
                cache_key, True, ttl=120
            )  # do not retrieve invoices for 2 minutes after a succesful read
        qs = self.invoice_set.filter(
            regie_id=regie_id, family_id=family_id, maelis_no_more_returned_date__isnull=True
        )
        qs.known_invoice_ids = known_invoice_ids
        return qs

    def make_no_online_payment_reason(self, qs, invoice):
        if qs.known_invoice_ids is None:
            # soap service is down
            return 'Le service est temporairement indisponible.'

        pay_limit_date = invoice.maelis_data.get('dateDeadlinePayment') or invoice.maelis_data.get(
            'dateDeadline'
        )
        if pay_limit_date:
            pay_limit_date = parse_datetime(pay_limit_date)
        else:
            pay_limit_date = parse_datetime(invoice.maelis_data.get('dateInvoice')) + datetime.timedelta(
                days=15
            )

        if pay_limit_date and now() > pay_limit_date:
            return 'La date limite de paiement en ligne est dépassée.'

        return None

    @endpoint(
        display_category='Facture',
        name='regie',
        pattern=r'^(?P<regie_id>[\w-]+)/invoices/?$',
        example_pattern='{regie_id}/invoices',
        description='Obtenir les factures à payer',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'regie_id': {'description': 'Identifiant de la régie', 'example_value': '102'},
        },
    )
    def invoices(self, request, regie_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        data = []
        qs = self.get_invoices(family_id, regie_id)
        for invoice in qs:
            if invoice.status() not in ['created', 'for_payment']:
                continue
            invoice.no_online_payment_reason = self.make_no_online_payment_reason(qs, invoice)
            data.append(invoice.format_content())
        return {'has_invoice_for_payment': True, 'data': data}

    @endpoint(
        display_category='Facture',
        name='regie',
        pattern=r'^(?P<regie_id>\w+)/invoices/history/?$',
        example_pattern='{regie_id}/invoices/history',
        description='Obtenir les factures déjà payées',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'regie_id': {'description': 'Identifiant de la régie', 'example_value': '102'},
        },
    )
    def invoices_history(self, request, regie_id, NameID=None, family_id=None):
        family_id = self.assert_family_or_link(family_id, NameID)
        invoices = [
            i.format_content()
            for i in self.get_invoices(family_id, regie_id)
            if i.status() in ['paid', 'notified']
        ]
        return {'has_invoice_for_payment': True, 'data': invoices}

    def get_invoice(self, regie_id, invoice_id):
        real_invoice_id = invoice_id.split('-')[-1]
        family_id = invoice_id[: -(len(real_invoice_id) + 1)]
        qs = self.get_invoices(family_id, regie_id, use_cache=False)
        try:
            invoice = qs.get(invoice_id=real_invoice_id)
        except Invoice.DoesNotExist:
            raise APIError('Invoice not found')
        invoice.no_online_payment_reason = self.make_no_online_payment_reason(qs, invoice)
        return invoice

    @endpoint(
        display_category='Facture',
        name='regie',
        pattern=r'^(?P<regie_id>\w+)/invoice/(?P<invoice_id>\d+-\d+)/?$',
        example_pattern='{regie_id}/invoice/{invoice_id}',
        description='Obtenir les détails d’une facture',
        parameters={
            'regie_id': {'description': 'Identifiant de la régie', 'example_value': '102'},
            'invoice_id': {'description': 'Identifiant de facture', 'example_value': 'IDFAM-42'},
            'payment': {
                'description': "Si présent, annuler la facture panier à l'expiration du delai maximum de paiement depuis la date de l'appel"
            },
        },
    )
    def invoice(self, request, regie_id, invoice_id, payment=None, **kwargs):
        invoice = self.get_invoice(regie_id, invoice_id)
        if invoice.status() == 'cancelled':
            raise APIError('Invoice cancelled')

        if payment is not None and invoice.status() in ['created']:
            invoice.start_payment_date = now()
            invoice.save(update_fields=['start_payment_date'])
        if invoice.status() == 'cancelling':
            raise APIError('Invoice cancelling')
        return {
            'data': invoice.format_content(no_online_payment_reason=invoice.no_online_payment_reason),
        }

    @endpoint(
        display_category='Facture',
        name='regie',
        pattern=r'^(?P<regie_id>\w+)/invoice/(?P<invoice_id>\d+-\d+)/pay/?$',
        example_pattern='{regie_id}/invoice/{invoice_id}/pay',
        description='Notifier le paiement de la facture',
        parameters={
            'regie_id': {'description': 'Identifiant de la régie', 'example_value': '102'},
            'invoice_id': {'description': 'Identifiant de facture', 'example_value': 'IDFAM-42'},
        },
        post={
            'request_body': {
                'schema': {
                    'application/json': invoice_schemas.PAYMENT_SCHEMA,
                }
            }
        },
    )
    def pay_invoice(self, request, regie_id, invoice_id, post_data, **kwargs):
        invoice = self.get_invoice(regie_id, invoice_id)
        with transaction.atomic():
            invoice = Invoice.objects.select_for_update().get(pk=invoice.pk)
            if invoice.status() in ['paid', 'notified']:
                raise APIError('Invoice already paid')
            if invoice.status() == 'cancelled':
                raise APIError('Invoice cancelled')
            invoice.lingo_data = post_data
            invoice.lingo_notification_date = now()
            invoice.save(update_fields=['updated', 'lingo_notification_date', 'lingo_data'])

        self.add_job(
            'notify_invoice_paid_job',
            regie_id=regie_id,
            invoice_id=invoice.invoice_id,
            natural_id='%s/%s' % (regie_id, invoice.invoice_id),
        )
        return {'data': 'ok'}

    def notify_invoice_paid_job(self, regie_id, invoice_id):
        try:
            invoice = self.invoice_set.get(regie_id=regie_id, invoice_id=invoice_id)
        except Invoice.DoesNotExist:
            return
        if not invoice.notify() and (now() - invoice.lingo_notification_date) < datetime.timedelta(
            minutes=45
        ):
            # retry in 5 minutes...
            raise SkipJob(5 * 60)

    def trigger_subscription_job(self, pk):
        try:
            subscription = self.subscription_set.get(pk=pk)
        except Invoice.DoesNotExist:
            return
        subscription.trigger()

    @endpoint(
        display_category='Facture',
        name='regie',
        pattern=r'^(?P<regie_id>\w+)/invoice/(?P<invoice_id>\d+-\d+)/pdf/?$',
        example_pattern='{regie_id}/invoice/{invoice_id}/pdf',
        description='Obtenir une facture au format PDF',
        parameters={
            'NameID': {'description': 'Publik NameID'},
            'family_id': {'description': 'Numéro de DUI'},
            'regie_id': {'description': 'Identifiant de la régie', 'example_value': '102'},
            'invoice_id': {'description': 'Identifiant de facture', 'example_value': 'IDFAM-42'},
        },
    )
    def invoice_pdf(self, request, regie_id, invoice_id, NameID=None, family_id=None, **kwargs):
        try:
            self.assert_key_in_referential('Regie', regie_id, 'regie_id parameter')
            family_id = self.assert_family_or_link(family_id, NameID)
        except APIError:
            raise Http404('Fichier PDF non trouvé')

        real_invoice_id = invoice_id.split('-')[-1]
        if invoice_id[: -(len(real_invoice_id) + 1)] != family_id:
            raise Http404('Fichier PDF non trouvé')

        try:
            result = self.call(
                'Invoice',
                'getInvoicePDF',
                getInvoicePDFRequestBean={
                    'codeRegie': regie_id,
                    'numInvoice': real_invoice_id,
                },
            )
        except SOAPFault:
            raise Http404('Fichier PDF non trouvé')

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % invoice_id
        response.write(result)
        return response

    def cache_set(self, key, data, ttl=None):
        ttl = ttl or Cache.DEFAULT_TTL
        expire = now() + datetime.timedelta(seconds=ttl)
        self.cache.update_or_create(
            key=key,
            defaults={'data': Cache.dumps(data), 'expire': expire},
        )

    def cache_get(self, key, default=None):
        try:
            cache_line = self.cache.get(key=key, expire__gt=now())
        except Cache.DoesNotExist:
            return default
        return Cache.loads(cache_line.data)

    def cache_delete(self, key):
        self.cache.filter(key=key).delete()

    def cache_clean(self):
        self.cache.filter(expire__lt=now()).delete()

    def cache_clear(self):
        self.cache.all().delete()


class Link(models.Model):
    resource = models.ForeignKey(ToulouseMaelis, on_delete=models.CASCADE)
    name_id = models.CharField(blank=False, max_length=256)
    family_id = models.CharField(blank=False, max_length=128)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('resource', 'name_id')


class Referential(models.Model):
    resource = models.ForeignKey(
        verbose_name='Resource',
        to=ToulouseMaelis,
        on_delete=models.CASCADE,
        related_name='referential',
    )
    referential_name = models.TextField('Name')
    item_id = models.TextField('Key')
    item_text = models.TextField('Text')
    item_unaccent_text = models.TextField('Text', null=True)
    item_data = JSONField('Data', encoder=DjangoJSONEncoder)
    created = models.DateTimeField('Created', auto_now_add=True)
    updated = models.DateTimeField('Updated', auto_now=True)

    def __repr__(self):
        return '<Referential "%s/%s">' % (self.referential_name, self.item_id)

    class Meta:
        ordering = ('resource', 'referential_name', 'item_text', 'item_id')
        unique_together = [['resource', 'referential_name', 'item_id']]


class Invoice(models.Model):
    '''Family invoices list by regie'''

    resource = models.ForeignKey(ToulouseMaelis, on_delete=models.CASCADE)
    regie_id = models.CharField(blank=False, max_length=128)
    family_id = models.CharField(blank=False, max_length=128)
    invoice_id = models.CharField(blank=False, max_length=128)
    maelis_data = JSONField('Data', encoder=DjangoJSONEncoder)
    lingo_data = JSONField('Data', encoder=DjangoJSONEncoder, null=True)
    maelis_notification_data = JSONField(encoder=DjangoJSONEncoder, null=True)
    created = models.DateTimeField('Created', auto_now_add=True)
    updated = models.DateTimeField('Updated', auto_now=True)
    maelis_data_update_date = models.DateTimeField(null=True)
    start_payment_date = models.DateTimeField(null=True)
    lingo_notification_date = models.DateTimeField(null=True)
    maelis_notification_date = models.DateTimeField(null=True)
    basket_generation_date = models.DateTimeField(null=True)
    maelis_cancel_notification_date = models.DateTimeField(null=True)
    maelis_no_more_returned_date = models.DateTimeField(null=True)

    no_online_payment_reason = None

    def __repr__(self):
        return '<Invoice "%s/%s">' % (self.regie_id, self.invoice_id)

    def status(self):
        if (
            Decimal(self.maelis_data['amountInvoice'] or 0) == Decimal(self.maelis_data['amountPaid'] or 0)
            or self.maelis_notification_date is not None
        ):
            if isinstance(self.maelis_notification_data, dict) and 'error' in self.maelis_notification_data:
                return 'error'
            # payInvoice was sent to Maelis
            return 'notified'
        if self.lingo_notification_date is not None:
            # pay order received from Lingo
            return 'paid'

        # basket invoice only
        if self.basket_generation_date is not None:
            if self.maelis_cancel_notification_date is not None:
                # cancelInvoiceAndDeleteSubscribeList was sent to Maelis
                return 'cancelled'
            if self.lingo_notification_date is None:
                if self.start_payment_date is not None:
                    # displayed into Lingo but no more payable
                    return 'for_payment'
                if self.created <= now() - datetime.timedelta(minutes=self.resource.cancel_invoice_delay):
                    # hide invoice to Lingo
                    return 'cancelling'

        if self.maelis_no_more_returned_date is not None:
            # invoice cancelled by an agent into Maelis
            return 'cancelled_by_agent'
        # new invoice
        return 'created'

    def format_content(self, no_online_payment_reason=None):
        item = self.maelis_data
        paid = self.status() in ['paid', 'notified']
        amount_paid = item['amountInvoice'] if paid else item['amountPaid']
        invoice = {
            'id': '%s-%s' % (item['numFamily'], item['numInvoice']),
            'created': item['dateInvoice'][:10],
            'pay_limit_date': (item['dateDeadline'] or '')[:10],
            'display_id': str(item['numInvoice']),
            'total_amount': str(Decimal(item['amountInvoice'] or 0)),
            'amount': str(Decimal(item['amountInvoice'] or 0) - Decimal(amount_paid or 0)),
            'amount_paid': str(amount_paid or 0),
            'label': item['libelleTTF'],
            'has_pdf': bool(item['pdfName']),
            'online_payment': not bool(self.no_online_payment_reason),
            'paid': paid,
            'payment_date': None,
            'no_online_payment_reason': self.no_online_payment_reason,
            'reference_id': item['numInvoice'],
            'maelis_item': item,
        }
        if paid or self.status() == 'for_payment':
            invoice.update(
                {
                    'pay_limit_date': '',
                    'online_payment': False,
                    'no_online_payment_reason': None,
                }
            )
        if self.status() == 'for_payment':
            invoice['no_online_payment_reason'] = 'Transation de payement en cours'
        return invoice

    @transaction.atomic
    def notify(self):
        # refresh invoice status
        self.resource.get_invoices(self.family_id, self.regie_id)

        obj = Invoice.objects.select_for_update().get(pk=self.pk)
        if obj.maelis_notification_date is not None:
            return True

        amount = Decimal(obj.maelis_data['amountInvoice'] or 0) - Decimal(obj.maelis_data['amountPaid'] or 0)
        if amount <= 0:
            self.resource.logger.warning('Invoice %s looks already notified, stopping.', repr(obj))
            result = {'warning': 'Pas de montant à notifier'}
        else:
            try:
                result = obj.resource.call(
                    'Invoice',
                    'payInvoices',
                    numDossier=obj.family_id,
                    codeRegie=obj.regie_id,
                    amount=str(amount),
                    datePaiement=obj.lingo_data['transaction_date'],
                    refTransaction=obj.lingo_data['transaction_id'],
                    numInvoices=[obj.invoice_id],
                    numPerson=obj.maelis_data['payer']['num'],
                )
            except SOAPServiceUnreachable as e:
                self.resource.logger.warning(
                    'fails to notify paid invoice %s (%s), will retry later.', repr(obj), e
                )
                return False
            except SOAPError as e:
                self.resource.logger.error('fails to notify paid invoice %s (%s), stopping.', repr(obj), e)
                result = {'error': str(e)}

        obj.maelis_notification_date = now()
        obj.maelis_notification_data = result
        obj.save()

        if obj.basket_generation_date is not None:
            # match paid invoice with subscriptions and trigger w.c.s.
            obj.set_trigger_subscriptions()

        # let invoice cache be refreshed on next call to invoices
        self.resource.cache_delete(f'i-{self.regie_id}-{self.family_id}')
        return True

    def match_subscriptions(self):
        invoice_subscription_ids = []
        for line in self.maelis_data['lineInvoiceList'] or []:
            subscription_id = line.get('idIns')
            if subscription_id:
                invoice_subscription_ids.append(subscription_id)
        if not invoice_subscription_ids:
            return

        for subscription in Subscription.objects.filter(
            regie_id=self.regie_id,
            family_id=self.family_id,
            invoice__isnull=True,
            basket_removal_date__isnull=True,
        ):
            for line in subscription.maelis_data['basket']['lignes']:
                subscription_id = line.get('idIns')
                if subscription_id and subscription_id in invoice_subscription_ids:
                    subscription.invoice = self
                    subscription.save()
                    break

    def set_trigger_subscriptions(self):
        for subscription in self.subscription_set.filter(wcs_trigger_payload__isnull=True):
            if subscription.trigger_status() == 'triggering':
                subscription.set_trigger()

    @transaction.atomic
    def cancel(self):
        obj = Invoice.objects.select_for_update().get(pk=self.pk)
        if obj.lingo_notification_date is not None or obj.basket_generation_date is None:
            return False
        try:
            obj.resource.call(
                'Activity',
                'cancelInvoiceAndDeleteSubscribeList',
                idInvoice=obj.maelis_data['idInvoice'],
            )
        except SOAPServiceUnreachable:
            return False
        obj.maelis_cancel_notification_date = now()
        obj.save()
        obj.resource.logger.info("Annulation de %s sur la famille '%s'", repr(obj), obj.family_id)

        # match cancelled invoice with subscriptions and trigger w.c.s.
        obj.set_trigger_subscriptions()
        return True

    class Meta:
        ordering = ('resource', 'regie_id', 'invoice_id')
        unique_together = [['resource', 'regie_id', 'invoice_id']]
        indexes = [
            models.Index(fields=['resource', 'regie_id', 'family_id'], name='invoice_regie_family_idx')
        ]


class Subscription(models.Model):
    '''WCS demand performing basket subscription'''

    resource = models.ForeignKey(ToulouseMaelis, on_delete=models.CASCADE)
    wcs_form_number = models.CharField(max_length=16)
    wcs_form_api_url = models.CharField(max_length=256)
    regie_id = models.CharField(blank=False, max_length=128)
    family_id = models.CharField(blank=False, max_length=128)
    maelis_data = JSONField(encoder=DjangoJSONEncoder)
    basket_removal_date = models.DateTimeField(null=True)
    invoice = models.ForeignKey(Invoice, null=True, on_delete=models.CASCADE)
    wcs_trigger_payload = JSONField(encoder=DjangoJSONEncoder, null=True)
    wcs_trigger_response = JSONField(null=True)
    wcs_trigger_date = models.DateTimeField(null=True)
    created = models.DateTimeField('Created', auto_now_add=True)
    updated = models.DateTimeField('Updated', auto_now=True)

    def __repr__(self):
        return '<Subscription "%s/%s">' % (self.wcs_form_number, self.pk)

    def status(self):
        if self.invoice is not None:
            if self.invoice.status() == 'notified':
                # related invoice is paid
                return 'paid'
            if self.invoice.status() == 'cancelled':
                # related invoice is cancelled
                return 'cancelled'
            if self.invoice.status() == 'error':
                # related invoice cound not be notified
                return 'error'
            # there is a related invoice
            return 'pending_invoice'
        else:
            if self.basket_removal_date is not None:
                # no basket validation (no related invoice generated)
                return 'removed'
            else:
                # subscription is into the maelis basket
                return 'pending_basket'

    def trigger_status(self):
        if self.wcs_trigger_date is not None:
            # wcs demand was triggered
            return 'triggered'
        if self.status() in ['paid', 'cancelled', 'removed', 'error']:
            # wcs demand can be triggered
            return 'triggering'
        else:
            # waiting for a definive subscription status
            return 'pending'

    def set_trigger(self):
        if self.trigger_status() != 'triggering':
            return
        if self.wcs_trigger_payload:
            return
        self.wcs_trigger_payload = {
            'err': 1 if self.status() in ['cancelled', 'removed'] else 0,
            'data': {
                'regie_id': self.regie_id,
                'regie_text': self.resource.get_referential_value('Regie', self.regie_id),
                'invoice_id': self.invoice.invoice_id if self.invoice else None,
                'invoice_status': self.invoice.status() if self.invoice else None,
                'invoice_data': self.invoice.maelis_data if self.invoice else None,
                'invoice_maelis_notification_date': (
                    self.invoice.maelis_notification_date if self.invoice else None
                ),
                'invoice_maelis_notification_data': (
                    self.invoice.maelis_notification_data if self.invoice else None
                ),
                'subscription_id': self.pk,
                'subscription_status': self.status(),
                'subscription_data': self.maelis_data,
            },
        }
        if self.status() == 'removed':
            self.wcs_trigger_payload['err_desc'] = "Le panier n'a pas été validé"
        if self.status() == 'cancelled':
            self.wcs_trigger_payload['err_desc'] = 'La facture a été annulée'
        self.save()
        self.resource.add_job(
            'trigger_subscription_job',
            pk=self.pk,
            natural_id='%s/%s' % (self.wcs_form_number, self.pk),
        )

    def get_wcs_api(self, base_url):
        scheme, netloc, dummy, dummy, dummy, dummy = urlparse(base_url)
        services = settings.KNOWN_SERVICES.get('wcs', {})
        service = None
        for service in services.values():
            remote_url = service.get('url')
            r_scheme, r_netloc, dummy, dummy, dummy, dummy = urlparse(remote_url)
            if r_scheme == scheme and r_netloc == netloc:
                return WcsApi(
                    base_url,
                    orig=service.get('orig'),
                    key=service.get('secret'),
                    session=self.resource.requests,
                )

    @transaction.atomic
    def trigger(self):
        obj = Subscription.objects.select_for_update().get(pk=self.pk)
        if obj.trigger_status() != 'triggering' or not obj.wcs_trigger_payload:
            return
        base_url = '%shooks/update_subscription/' % (obj.wcs_form_api_url)
        wcs_api = obj.get_wcs_api(base_url)
        if not wcs_api:
            err_desc = 'Cannot find wcs service for %s' % obj.wcs_form_api_url
            self.resource.logger.warning(err_desc)
            result = err_desc
        else:
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            self.resource.logger.info(
                'trigger wcs: %s -> %s' % (base_url, self.wcs_trigger_payload['data']['subscription_status'])
            )
            try:
                result = wcs_api.post_json(obj.wcs_trigger_payload, [], headers=headers)
            except WcsApiError as e:
                if e.json_content is not None and e.request_response.status_code == 404:
                    # stop triggering a removed wcs demand
                    self.resource.logger.info(e)
                    obj.wcs_trigger_date = now()
                    obj.wcs_trigger_response = e.json_content
                    obj.save()
                else:
                    # continue triggering on other wcs errors
                    self.resource.logger.warning(e)
                return
        obj.wcs_trigger_date = now()
        obj.wcs_trigger_response = result
        obj.save()

    class Meta:
        ordering = ('resource', 'wcs_form_number')
        unique_together = [['resource', 'wcs_form_number']]


class Cache(models.Model):
    resource = models.ForeignKey(ToulouseMaelis, on_delete=models.CASCADE, related_name='cache')
    key = models.TextField('Cache key')
    data = models.BinaryField('Data')
    expire = models.DateTimeField('Expiration date')

    class Meta:
        unique_together = [
            ('resource', 'key'),
        ]

    DEFAULT_TTL = 3600

    @classmethod
    def dumps(cls, value):
        return pickle.dumps(value)

    @classmethod
    def loads(cls, value):
        if not value:
            raise ValueError('empty pickled')
        try:
            return pickle.loads(value)
        except (TypeError, pickle.UnpicklingError):
            raise ValueError('invalid pickled')
