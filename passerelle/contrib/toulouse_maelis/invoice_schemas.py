# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


PERSON_BANK_SCHEMA = {
    'title': 'Person bank',
    'description': 'Coordonnées du compte à débiter (coordonnées bancaires)',
    'type': 'object',
    'required': ['bankBIC', 'bankIBAN', 'bankRUM', 'dateStart', 'bankAddress', 'lastName', 'firstName'],
    'properties': {
        'bankBIC': {
            'description': 'BIC',
            'type': 'string',
        },
        'bankIBAN': {
            'description': 'IBAN',
            'type': 'string',
        },
        'bankRUM': {
            'description': 'RUM',
            'type': 'string',
        },
        'dateStart': {
            'description': 'date de début',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'bankAddress': {
            'description': 'Adresse de la banque',
            'type': 'string',
        },
        'civility': {
            'description': 'civility',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'lastName': {
            'description': 'Nom',
            'type': 'string',
        },
        'firstName': {
            'description': 'Prénom',
            'type': 'string',
        },
    },
}


ADD_DIRECT_DEBIT_ORDER_SCHEMA = {
    'title': 'Add direct debit order',
    'description': "Ajout d'autorisation de prélèvement",
    'type': 'object',
    'required': ['bank', 'codeRegie'],
    'properties': {
        'bank': PERSON_BANK_SCHEMA,
        'codeRegie': {
            'description': 'Code de la régie',
            'type': 'string',
        },
    },
    'additionalProperties': False,
    'unflatten': True,
}


PAYMENT_SCHEMA = {
    'type': 'object',
    'properties': {
        'transaction_date': {
            'type': 'string',
        },
        'transaction_id': {
            'type': 'string',
        },
    },
    'required': ['transaction_date', 'transaction_id'],
}
