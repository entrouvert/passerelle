# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


BOOLEAN_TYPES = [
    {'type': 'boolean'},
]

BASIC_ID_PROPERTIES = {
    'firstname': {
        'description': 'Prénom',
        'type': 'string',
    },
    'lastname': {
        'description': 'Nom',
        'type': 'string',
    },
    'dateBirth': {
        'description': 'Date de naissance',
        'type': 'string',
        'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
    },
}

LINK_SCHEMA = {
    'title': 'Link',
    'description': "Appairage d'un usager Publik à une famille dans Maelis",
    'type': 'object',
    'required': ['family_id', 'firstname', 'lastname', 'dateBirth'],
    'properties': {
        'family_id': {
            'description': 'Numéro DUI',
            'type': 'string',
        },
    },
    'additionalProperties': False,
}
LINK_SCHEMA['properties'].update(BASIC_ID_PROPERTIES)
