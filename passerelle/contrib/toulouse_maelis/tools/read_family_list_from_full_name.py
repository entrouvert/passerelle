#!/usr/bin/python3

import argparse

import utils


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Family')

    result = client.service.readFamilyListFromFullName(
        fullname=args.query,
    )
    if args.verbose > 1:
        print(result)
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument(
        'query', help='Recherche en texte intégral (plus ou moins)', nargs='?', default='TEST_'
    )
    args = parser.parse_args()
    check(args)
