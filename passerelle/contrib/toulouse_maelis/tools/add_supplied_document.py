#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '334649'  # TEST_TOOLS
PERSON_ID = None
TYPE_ID = '46'
DATE = '2022-09-13'
VISA = '2022-09-14'
VALIDITY = '2022-09-15'
FILENAME = 'test.txt'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Family')

    result = client.service.addSuppliedDocument(
        addSuppliedDocumentRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'documentList': [
                {
                    'code': args.type,
                    'depositDate': args.date,
                    'visaDate': args.visa,
                    'validityDate': args.validity,
                    'filename': args.filename,
                    'fileSupplied': {
                        'dataHandler': open(args.filename, 'rb').read(),
                        'fileType': None,
                        'name': None,
                    },
                }
            ],
        }
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument(
        '--type', '-t', default=TYPE_ID, help='document type (cf ./read_referential_list.py document)'
    )
    parser.add_argument('--date', '-d', default=DATE, help='date (ex: 2023-02-01)')
    parser.add_argument('--visa', '-V', default=VISA, help='date (ex: 2023-02-02)')
    parser.add_argument('--validity', '-W', default=VALIDITY, help='date (ex: 2023-02-03)')
    parser.add_argument('--filename', '-F', default=FILENAME, help='file to upload')
    check(parser.parse_args())
