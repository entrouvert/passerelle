#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
ACTIVITY_ID = 'A10055227963'
START_DATE = '2023-02-01'
END_DATE = '2023-07-01'
WEEK = '_XB____'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    if len(args.week) != 7:
        raise Exception('week must contain 7 letters (on per week day)')
    week = []
    for i, unit_letter in enumerate(args.week):
        if unit_letter in ['_', ' ']:
            letter = None
        else:
            letter = unit_letter
        week.append(
            {
                'dayNum': i + 1,
                'calendarLetter': letter,
                'isPresent': bool(letter is not None),
            }
        )

    result = client.service.updateWeekCalendar(
        numPerson=args.person,
        idActivity=args.activity,
        dateStart=args.start,
        dateEnd=args.end,
        dayWeekInfoList=week,
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--activity', '-a', default=ACTIVITY_ID, help='activity id')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 2023-02-01)')
    parser.add_argument('--end', '-E', default=START_DATE, help='end date (ex: 2023-07-01)')
    parser.add_argument('--week', '-W', default=WEEK, help='7 unit lettres or "_" (ex: _XB____)')
    check(parser.parse_args())
