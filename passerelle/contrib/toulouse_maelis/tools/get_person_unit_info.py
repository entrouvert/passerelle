#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
ACTIVITY_ID = 'A10055227963'
UNIT_ID = 'A10055227965'
PLACE_ID = 'A10055129635'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    results = client.service.getPersonUnitInfo(
        getPersonUnitInfoRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'activityUnitPlace': {
                'idActivity': args.activity,
                'idUnit': args.unit,
                'idPlace': args.place,
            },
            'dateRef': None,  # si nulle, date du jour
        }
    )

    if args.dump:
        print(results)
        exit(0)

    print(
        '* %s / %s / %s'
        % (
            results['activity']['idActivity'],
            results['activity']['libelle1'],
            results['activity']['libelle2'],
        )
    )
    print(
        '  control: %s / %s'
        % (
            results['controlResult']['controlOK'],
            results['controlResult']['message'],
        )
    )
    print('  calendarGeneration: %s' % results['calendarGeneration']['code'])
    if results['activity']['activityType']:
        print(
            '  type: %s / %s'
            % (
                results['activity']['activityType']['code'],
                results['activity']['activityType']['natureSpec']['code'],
            )
        )
    else:
        print("'  type: no activity type")
    print('  weeklyCalendarActivityList: %s' % bool('weeklyCalendarActivityList' in results))

    print('  openDayList: %s' % len(results['openDayList']))

    print('  unit: %s / %s' % (results['unit']['idUnit'], results['unit']['libelle']))
    print('    start: %s' % (results['unit']['dateStart'].strftime('%Y-%m-%d')))
    print('    end:   %s' % (results['unit']['dateEnd'].strftime('%Y-%m-%d')))
    print('  place: %s / %s' % (results['place']['idPlace'], results['place']['lib1']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--dump', '-d', default=False, action='store_true', help='dump')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--activity', '-a', default=ACTIVITY_ID, help='activity id')
    parser.add_argument('--unit', '-u', default=UNIT_ID, help='unit id')
    parser.add_argument('--place', '-p', default=PLACE_ID, help='place id')
    check(parser.parse_args())
