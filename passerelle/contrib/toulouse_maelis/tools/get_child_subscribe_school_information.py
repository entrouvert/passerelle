#!/usr/bin/python3

import argparse

import utils

YEAR = '2023'
LEVEL = None
PERSON_ID = '263781'  # BART NICO
FAMILY_ID = '322573'  # NICO


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Family')

    results = client.service.getChildSubscribeSchoolInformation(
        getFamilySubscribeSchoolInfoRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'schoolYear': args.year,
            'level': args.level,
        }
    )

    print(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--year', '-y', default=YEAR, help='year (ex: 2023)')
    parser.add_argument('--level', '-l', default=LEVEL, help='level (facultatif, ex: CP)')
    check(parser.parse_args())
