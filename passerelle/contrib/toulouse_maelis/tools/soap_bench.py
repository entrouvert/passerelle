#!/usr/bin/python3

import argparse
import copy
import functools
import random
import statistics
import threading
import time
from multiprocessing import Lock, Pool, Process, Queue
from multiprocessing.sharedctypes import Value

import utils

FAMILY_ID = '322423'  # NICO TEST / UDAVE INTEG
PERSON_ID = '176658'  # INTEG

duis = [str(i) for i in range(330120, 33151)]


client = None

# utils.configure_logging(0)

_client = None


def get_client_label(args):
    if args.test in ['read-family', 'search-family', 'update-family']:
        return 'Family'
    elif args.test in ['person-catalog', 'global-catalog']:
        return 'Activity'
    else:
        raise Exception('unknown test')


def check(client, i, args):
    client = client or utils.get_client(args.env, get_client_label(args))

    if args.test == 'read-family':
        result = client.service.readFamily(dossierNumber=args.family)
    elif args.test == 'search-family':
        result = client.service.readFamilyListFromFullName(fullname=args.query)
    elif args.test == 'update-family':
        result = client.service.updateFamily(
            dossierNumber=args.family,
            category='BI',
            situation='VIEM',
            nbChild='1',
            nbTotalChild='2',
            nbAES='3',
        )
    elif args.test == 'person-catalog':
        result = client.service.getPersonCatalogueActivity(
            getPersonCatalogueActivityRequestBean={
                'numDossier': args.family,
                'numPerson': args.person,
                'yearSchool': '2022',
                'dateStartActivity': '2022-09-01',
                'dateEndActivity': '2023-08-31',
            }
        )
    elif args.test == 'global-catalog':
        result = client.service.readActivityList(
            schoolyear='1970',
            dateStartCalend='2022-01-01',
            dateEndCalend='2024-12-31',
        )
    else:
        raise Exception('unknown test')
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--reuse', '-r', default=False, help='reuse zeep client', action='store_true')
    parser.add_argument('--number', '-n', type=int, default=300, help='number of requests')
    parser.add_argument('--concurrency', '-c', type=int, default=100, help='number of parallel processes')
    parser.add_argument('--test', '-t', default='read-family', help='WS to test')
    parser.add_argument('family', help=FAMILY_ID, nargs='?', default=FAMILY_ID)
    parser.add_argument(
        'query', help='Recherche en texte intégral (plus ou moins)', nargs='?', default='SIMP'
    )
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    args = parser.parse_args()

    if args.reuse:
        _client = utils.get_client(args.env, get_client_label(args))

    done = 0
    count = args.number
    concurrency = args.concurrency
    errors = 0
    error_types = set()
    durations = []

    barrier = threading.Barrier(concurrency + 1)
    done_lock = threading.Lock()

    def f(i):
        global done, durations, errors

        __client = None
        if args.reuse:
            __client = _client or utils.get_client(args.env, get_client_label(args))
        barrier.wait()

        while done < count:
            with done_lock:
                if done >= count:
                    break
                current_done = done
                done += 1

            try:
                start = time.time()
                check(__client, i, args)
                duration = time.time() - start
                durations.append(duration)
            except Exception as e:
                error_types.add(repr(e))
                errors += 1

    if 1:
        done_value = Value('i', 0, lock=True)
        result_queue = Queue(count)

        def target(result_queue, done_value):
            _client = None
            if args.reuse:
                _client = utils.get_client(args.env, get_client_label(args))
            while done_value.value < count:
                with done_value.get_lock():
                    if done_value.value >= count:
                        break
                    done_value.value += 1
                try:
                    start = time.time()
                    check(_client or utils.get_client(args.env, get_client_label(args)), i, args)
                    duration = time.time() - start
                    result_queue.put((True, duration))
                except Exception as e:
                    result_queue.put((False, repr(e)))

        begin = time.time()
        processes = []
        for i in range(concurrency):
            processes.append(Process(target=target, args=(result_queue, done_value)))
            processes[-1].start()

        while done < count:
            ok, value = result_queue.get()
            done += 1
            print('Done %05d' % done, end='\r')
            if ok:
                durations.append(value)
            else:
                errors += 1
                error_types.add(value)
        print('Done %05d' % done, end='\r')
        print()
        for process in processes:
            process.join()

    else:
        # Refait le script avec multiprocessing plutôt que threading
        # pour être plus proche des processus uwsgi
        # (la création des zeep.Client coûte parce qu'un seul thread peut le faire à la fois)
        print('obsolete')
        threads = [threading.Thread(target=f, args=(i,)) for i in range(concurrency)]

        for thread in threads:
            thread.start()

        barrier.wait()

        begin = time.time()

        while done != count:
            print('Done %05d' % done, end='\r')
            time.sleep(0.5)
        print('Done %05d' % done, end='\r')

        for thread in threads:
            thread.join()

        print()

    print('Number of requests', count)
    print('Concurrency', concurrency)
    print('Errors', errors, 'on', count, 'types: ', list(error_types))
    print('RPS', float(count - errors) / (time.time() - begin))
    print('Min', min(durations))
    print('Max', max(durations))
    print('Average', statistics.fmean(durations))
    print('Quantiles', statistics.quantiles(durations, n=10))
