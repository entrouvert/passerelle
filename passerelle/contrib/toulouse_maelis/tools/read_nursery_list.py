#!/usr/bin/python3

import argparse
import json

import utils
from zeep.helpers import serialize_object


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Ape')

    results = client.service.readNurseryList(
        request={
            #'activityTypeCode': '', #'CRECHFAM',  # 'CRECHCO'
            #'codePSU': '', #'REGULAR',  # 'OCCASIONAL'
        }
    )

    print(json.dumps(serialize_object(results), cls=utils.DjangoJSONEncoder, indent=2))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    check(parser.parse_args())
