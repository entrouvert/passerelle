#!/usr/bin/python3

from math import inf

from dateutil.relativedelta import relativedelta
from django.utils.dateparse import parse_date


def get_ages_criteria(today, start_dob, end_dob):
    ages_txt = ['<=2', '3-11', '12-17', '18-25', '26-59', '>=60']
    ages = [0, 3, 12, 18, 26, 60, 62]

    data = []
    max_age = relativedelta(today, parse_date(start_dob[:10])).years if start_dob else inf
    min_age = relativedelta(today, parse_date(end_dob[:10])).years if end_dob else 0
    print('activity: [%s:%s]' % (min_age, max_age))

    for i in range(0, len(ages) - 1):
        print('check for laps [%s:%s]' % (ages[i], ages[i + 1] - 1))
        for age in range(ages[i], ages[i + 1]):
            # print('check for age %s <= %s <= %s (%s)' % (min_age, age, max_age, bool(min_age <= age <= max_age)))
            if min_age <= age <= max_age:
                print('found %s' % ages_txt[i])
                data.append((i, ages_txt[i]))
                break
    return data


today = parse_date('2023-01-01')
print(get_ages_criteria(today, '1964-01-01', '1964-01-01'))
