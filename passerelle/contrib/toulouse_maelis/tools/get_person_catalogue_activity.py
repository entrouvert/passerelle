#!/usr/bin/python3

import argparse
import json

import utils
from zeep.helpers import serialize_object

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
YEAR = '2022'
START_DATE = '2022-01-01'
END_DATE = '2023-12-31'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    results = client.service.getPersonCatalogueActivity(
        getPersonCatalogueActivityRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'yearSchool': args.year,  # required to match sector (on extrasco only)
            'dateStartActivity': args.start,
            'dateEndActivity': args.end,
        }
    )

    if args.dump:
        print(json.dumps(serialize_object(results), cls=utils.DjangoJSONEncoder, indent=2))
        return

    for activity in results['catalogueActivityList']:
        if args.nature:
            if args.nature.lower() == 'loisir':
                if activity['activity']['activityType'] and activity['activity']['activityType'][
                    'natureSpec'
                ]['code'] in ['A', 'R', 'X']:
                    continue
            elif args.nature and not (
                activity['activity']['activityType']
                and activity['activity']['activityType']['natureSpec']['code'] == args.nature
            ):
                continue

        print(
            '* %s / %s'
            % (  # / %s' % (
                activity['activity']['idActivity'],
                activity['activity']['libelle1'],
                # activity['activity']['libelle2'],
            )
        )

        # print('  calendarGeneration: %s' % activity['activity']['calendarGeneration']['code'])
        if activity['activity']['activityType']:
            print(
                '  type: %s / %s'
                % (
                    activity['activity']['activityType']['code'],
                    activity['activity']['activityType']['natureSpec']['code'],
                )
            )
        else:
            print("'  type: no activity type")
        # print('  weeklyCalendarActivityList: %s' %
        #      bool('weeklyCalendarActivityList' in activity['activity']))
        # print('  openDayList: %s' % len(activity['openDayList']))

        for unit in activity['unitInfoList']:
            print('  * %s / %s' % (unit['idUnit'], unit['libelle']))
            print('    start: %s' % (unit['dateStart'].strftime('%Y-%m-%d') if unit['dateStart'] else None))
            print('    end:   %s' % (unit['dateEnd'].strftime('%Y-%m-%d') if unit['dateEnd'] else None))

            # print('    places: %s' % len(unit['placeInfoList']))
            for item in unit['placeInfoList']:
                place = item['place']
                print(
                    '    * %s / %s: (x/y: %s/%s)'
                    % (place['idPlace'], place['lib1'], place['longitude'], place['latitude'])
                )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--dump', '-d', default=False, action='store_true', help='dump')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--nature', '-n', default=None, help='code nature, ex: "X"')
    parser.add_argument('--year', '-y', default=YEAR, help='year (ex: 2022)')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 2022-01-01)')
    parser.add_argument('--end', '-E', default=END_DATE, help='end date (ex: 2023-12-31)')
    check(parser.parse_args())
