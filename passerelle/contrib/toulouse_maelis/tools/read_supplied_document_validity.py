#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '334649'  # TEST_TOOLS
PERSON_ID = None
TYPE_ID = '46'
VALIDITY = None


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Family')

    result = client.service.readSuppliedDocumentValidity(
        readSuppliedDocumentValidityRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'code': args.type,
            'validityDate': args.validity,
        }
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument(
        '--type', '-t', default=TYPE_ID, help='document type (cf ./read_referential_list.py document)'
    )
    parser.add_argument('--validity', '-W', default=VALIDITY, help='date (ex: 2023-02-03)')
    check(parser.parse_args())
