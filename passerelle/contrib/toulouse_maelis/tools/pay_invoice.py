#!/usr/bin/python3

import argparse
from decimal import Decimal

import utils

FAMILY_ID = '322573'  # NICO
REGIE_ID = '100'  # CANTINE / CLAE
START_DATE = '1970-01-01'
END_DATE = '2222-02-22'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Invoice')

    invoices = client.service.readInvoices(
        numDossier=args.family,
        codeRegie=args.regie,
        dateStart=args.start,
        dateEnd=args.end,
    )
    obj = [x for x in invoices if x['invoice_id'] == arg.invoice][0]

    result = client.service.payInvoices(
        numDossier=args.family,
        codeRegie=args.regie,
        amount=str(Decimal(obj.maelis_data['amountInvoice']) - Decimal(obj.maelis_data['amountPaid'])),
        datePaiement=obj.lingo_data['transaction_date'],
        refTransaction=obj.lingo_data['transaction_id'],
        numInvoices=[obj.invoice_id],
        numPerson=obj.maelis_data['payer']['num'],
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--regie', '-r', default=REGIE_ID, help='person id')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 1970-01-01)')
    parser.add_argument('--end', '-E', default=END_DATE, help='end date (ex: 2222-02-22)')
    parser.add_argument('--invoice', '-i', default=198, help='invoice id (ex: 198)')
    check(parser.parse_args())
