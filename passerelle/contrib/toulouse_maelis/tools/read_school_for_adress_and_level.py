#!/usr/bin/python3

import argparse

import utils

YEAR = '2023'
LEVEL = None
STREET = '2317'
NUM = '4'
NUM_COMP = None


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Site')
    results = client.service.readSchoolForAdressAndLevel(
        readSchoolForAdressAndLevelRequestBean={
            'schoolYear': args.year,
            'adresse': {
                'idStreet': args.street,
                'num': args.num,
                'numComp': args.num_comp,
            },
            'levelCode': args.level,
        }
    )

    print(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--year', '-y', default=YEAR, help='year (ex: 2023)')
    parser.add_argument('--level', '-l', default=LEVEL, help='level (facultatif, ex: CP)')
    parser.add_argument('--street', '-s', default=STREET, help='street id (ex: 2317)')
    parser.add_argument('--num', '-n', default=NUM, help='house number (ex: 4)')
    parser.add_argument('--num_comp', '-c', default=NUM_COMP, help='house number complement (ex: B)')
    check(parser.parse_args())
