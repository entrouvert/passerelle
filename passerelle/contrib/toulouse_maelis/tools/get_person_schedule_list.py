#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
ACTIVITY_ID = 'A10055227963'
YEAR = '2023'
MONTH = '4'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    res = client.service.getPersonScheduleList(
        requestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'year': args.year,
            'month': args.month,
            'idAct': args.activity,
        }
    )
    if args.dump:
        print(res)
        return
    for person in res:
        for activity in person['activityScheduleList']:
            print('*  %s / %s' % (activity['activity']['idAct'], activity['activity']['libelle']))
            for unit in activity['unitScheduleList']:
                print('  * %s / %s' % (unit['unit']['idUnit'], unit['unit']['libelle']))
                for day in unit['dayInfoList']:
                    if day['status'] in ['NO_READ', 'NO_CUSTODY']:
                        continue
                    print(day)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--dump', '-d', default=False, action='store_true', help='dump')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--activity', '-a', default=ACTIVITY_ID, help='activity id')
    parser.add_argument('--year', '-y', default=YEAR, help='year (ex: 2023)')
    parser.add_argument('--month', '-m', default=MONTH, help='month (ex: 4)')
    parser.add_argument('activity', nargs='?', default=None, help='activity id')
    check(parser.parse_args())
