#!/usr/bin/python3

import argparse

import utils


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, args.service.title())

    ref = args.referential.title()
    if ref == 'Csp':
        ref = 'CSP'
    elif ref == 'Dietcode':
        ref = 'DietCode'
    elif ref == 'Pai':
        ref = 'PAI'
    elif ref == 'Childindicator':
        ref = 'ChildIndicator'
    elif ref == 'Rlindicator':
        ref = 'RLIndicator'
    elif ref == 'Activitynaturetype':
        ref = 'ActivityNatureType'
    elif ref == 'Yearschool':
        ref = 'YearSchool'
    elif ref == 'Derogreason':
        ref = 'DerogReason'
    elif ref == 'Activitynaturetype':
        ref = 'ActivityNatureType'
    elif ref == 'Apeindicator':
        ref = 'ApeIndicator'

    method = getattr(client.service, 'read%sList' % ref)
    results = method()
    print(results)
    assert len(results) > 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--service', '-s', default='Family', help='Family, Activity, Site, Invoice, Ape')
    parser.add_argument(
        'referential',
        help='category childIndicator civility country county csp dietcode direct document organ pai quality quotient rlIndicator service situation street town vaccin'
        + ' activityNatureType derogReason level yearschool'
        + ' regie'
        + ' apeIndicator',
        nargs='?',
        default='civility',
    )

    args = parser.parse_args()
    check(args)
