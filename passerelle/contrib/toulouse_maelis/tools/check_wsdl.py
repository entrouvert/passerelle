#!/usr/bin/python3

import argparse
import os
import subprocess
import sys

import requests
import utils


def check_one(args, service):
    wsdl_url = utils.get_wsdl_url(args.env, service)
    print(wsdl_url)

    # previous result
    wsdl_file_path = 'wsdl/%sService.wsdl' % service.title()
    if not os.path.exists(wsdl_file_path):
        with open(wsdl_file_path, 'w'):
            pass
    with open(wsdl_file_path, 'rb') as fd:
        existing_wsdl_content = fd.read()

    result = requests.get(wsdl_url, verify=False)
    assert result.status_code == 200
    proc = subprocess.run(
        ['xmllint', '--format', '-'],
        input=result.content,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        check=True,
    )
    new_wsdl_content = proc.stdout

    # result
    new_wsdl_file_path = wsdl_file_path
    if not args.replace:
        new_wsdl_file_path += '.new'
    with open(new_wsdl_file_path, 'wb') as wsdl_new_file:
        wsdl_new_file.write(new_wsdl_content)

    # diff
    if args.diff:
        if args.replace:
            cmd = ['git', 'diff', wsdl_file_path]
        else:
            cmd = ['diff', '-u', wsdl_file_path, new_wsdl_file_path]
        output = subprocess.run(cmd, check=False)

    if existing_wsdl_content != new_wsdl_content:
        print('WSDL change on %s' % service)
        return 1
    return 0


def check(args):
    returncode = 0

    utils.configure_logging(args.verbose)
    if args.service == 'all':
        for service in ['family', 'activity', 'invoice', 'ape', 'site']:
            result = check_one(args, service)
            returncode = returncode or result
    else:
        returncode = check_one(args, args.service)
    sys.exit(returncode)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--replace', action='store_true', help='replace files')
    parser.add_argument('--diff', action='store_true', help='display diff')
    parser.add_argument(
        'service', help='family, activity, invoice, ape, site or all', nargs='?', default='all'
    )
    args = parser.parse_args()
    check(args)
