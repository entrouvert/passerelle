#!/usr/bin/python3

import argparse
import json
import pprint

import utils
from zeep.helpers import serialize_object

YEAR = '1970'
START_DATE = '2022-01-01'
END_DATE = '2023-12-31'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    results = client.service.readActivityList(
        schoolyear=args.year,
        dateStartCalend=args.start,
        dateEndCalend=args.end,
    )

    if args.dump:
        print(json.dumps(serialize_object(results), cls=utils.DjangoJSONEncoder, indent=2))
        exit(0)

    for activity in serialize_object(results):
        nature_code = None
        nature_lib = None
        type_code = None
        type_lib = None
        if activity['activityPortail'].get('activityType'):
            nature_code = activity['activityPortail']['activityType']['natureSpec']['code']
            nature_lib = activity['activityPortail']['activityType']['natureSpec']['libelle']
            type_code = activity['activityPortail']['activityType']['code']
            type_lib = activity['activityPortail']['activityType']['libelle']
        print(
            '* %s / %s (nature: (%s) %s / type: (%s) %s)'
            % (
                activity['activityPortail']['idAct'],
                activity['activityPortail']['libelle'],
                nature_code,
                nature_lib,
                type_code,
                type_lib,
            )
        )

        # # # print('  schoolYear: %s' % activity['activityPortail']['schoolYear'])
        # print('  calendarGeneration: %s' % activity['activityPortail']['calendarGeneration']['code'])
        # if activity['activityPortail']['activityType']:
        #     print('  type: %s / %s' % (
        #         activity['activityPortail']['activityType']['code'],
        #         activity['activityPortail']['activityType']['libelle'],
        #     ))
        # # print('  weeklyCalendarActivityList: %s' %
        # #       bool('weeklyCalendarActivityList' in activity['activityPortail']))
        # # print('  openDayList: %s' % len(activity['openDayList']))

        # for unit in activity['unitPortailList']:
        #     print('  * %s / %s' % (unit['idUnit'], unit['libelle']))
        #     #print('    age: %s -> %s' % (unit['birthDateStart'], unit['birthDateEnd']))
        #     #print('    places: %s' % len(unit['placeList']))
        #     #for place in unit['placeList']:
        #     #    print('    * %s : (%s/%s)' % (place['lib'], place['longitude'], place['latitude']))
    exit(0)

    for activity in serialize_object(results):
        if activity['activityPortail']['activityType']['natureSpec']['code'] not in ('P', 'L', 'S'):
            continue
        print(
            '* %s / %s'
            % (
                activity['activityPortail']['idAct'],
                activity['activityPortail']['libelle'],
            )
        )
        print(
            '  nature: %s: %s'
            % (
                activity['activityPortail']['activityType']['natureSpec']['code'],
                activity['activityPortail']['activityType']['natureSpec']['libelle'],
            )
        )
        print(
            '  type: %s: %s'
            % (
                activity['activityPortail']['activityType']['code'],
                activity['activityPortail']['activityType']['libelle'],
            )
        )
    exit(0)

    data = []
    for a, activity in enumerate(serialize_object(results)):
        if activity['activityPortail']['activityType']['natureSpec']['code'] not in ('P', 'L', 'S', 'V'):
            continue
        results[a]['openDayList'] = 'hidden (%s)' % len(activity['openDayList'])
        for u, unit in enumerate(activity['unitPortailList']):
            # del result[a]['unitPortailList'][u]['placeList']
            results[a]['unitPortailList'][u]['placeList'] = 'hidden (%s)' % len(unit['placeList'])
        data.append(activity)
    pprint.pprint(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--dump', '-d', default=False, action='store_true', help='dump')
    parser.add_argument('--year', '-y', default=YEAR, help='year (ex: 2022)')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 2022-01-01)')
    parser.add_argument('--end', '-E', default=END_DATE, help='end date (ex: 2023-12-31)')
    args = parser.parse_args()
    check(args)
