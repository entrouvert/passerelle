#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322423'  # NICO


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    result = client.service.updateBasketTime(
        idBasket=args.basket,
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('basket', nargs='?', help='basket id')
    check(parser.parse_args())
