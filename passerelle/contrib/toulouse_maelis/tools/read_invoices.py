#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
REGIE_ID = '100'  # CANTINE / CLAE
START_DATE = '1970-01-01'
END_DATE = '2222-02-22'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Invoice')

    result = client.service.readInvoices(
        numDossier=args.family,
        codeRegie=args.regie,
        dateStart=args.start,
        dateEnd=args.end,
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--regie', '-r', default=REGIE_ID, help='person id')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 1970-01-01)')
    parser.add_argument('--end', '-E', default=END_DATE, help='end date (ex: 2222-02-22)')
    check(parser.parse_args())
