#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322423'  # NICO


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Family')

    result = client.service.readFamily(
        dossierNumber=args.family,
        # schoolYear=
        # incomeYear=2020,  # <-- pour avoir les quotients
        # referenceYear=2020,
    )
    if args.verbose > 1:
        print(result)
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('family', help=FAMILY_ID, nargs='?', default=FAMILY_ID)
    args = parser.parse_args()
    check(args)
