#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
ACTIVITY_ID = 'A10055227963'
UNIT_ID = 'A10055227965'
PLACE_ID = 'A10055129635'
START_DATE = '2023-02-01'
END_DATE = '2023-07-01'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    result = client.service.addPersonUnitBasket(
        addPersonUnitBasketRequestBean={
            'numFamily': args.family,
            'numPerson': args.person,
            'idAct': args.activity,
            'idUnit': args.unit,
            'idPlace': args.place,
            'dateStartSubscribe': args.start,
            'dateEndSubscribe': args.end,
        }
    )
    print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--activity', '-a', default=ACTIVITY_ID, help='activity id')
    parser.add_argument('--unit', '-u', default=UNIT_ID, help='unit id')
    parser.add_argument('--place', '-p', default=PLACE_ID, help='place id')
    parser.add_argument('--start', '-S', default=START_DATE, help='start date (ex: 2023-02-01)')
    parser.add_argument('--end', '-E', default=END_DATE, help='end date (ex: 2023-07-01)')
    check(parser.parse_args())
