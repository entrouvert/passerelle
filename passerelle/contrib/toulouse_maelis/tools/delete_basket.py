#!/usr/bin/python3

import argparse

import utils


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    results = client.service.deleteBasket(
        deleteBasketRequestBean={
            'idBasket': args.basket,
            'idUtilisat': "Entr'ouvert",
        }
    )
    print(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('basket', help='basket id')
    check(parser.parse_args())
