#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART
ACTIVITY_ID = 'A10055227963'
UNIT_ID = 'A10055227965'
DATE = '2023-04-12'
YEAR = '2023'
ACTION = 'ADD_PRES_PREVI'


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    res = client.service.updatePersonSchedule(
        requestBean={
            'numDossier': args.family,
            'unitPersonDayInfoList': [
                {
                    'numPerson': args.person,
                    'idAct': args.activity,
                    'idUni': args.unit,
                    'date': args.date,
                    'action': args.action,
                },
            ],
        }
    )
    print(res)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    parser.add_argument('--activity', '-a', default=ACTIVITY_ID, help='activity id')
    parser.add_argument('--unit', '-u', default=UNIT_ID, help='unit id')
    parser.add_argument('--date', '-D', default=DATE, help='date (ex: 2023-04-12)')
    parser.add_argument('--action', '-A', default=ACTION, help='ADD_PRES_PREVI, DEL_PRES_PREVI, ...')
    check(parser.parse_args())
