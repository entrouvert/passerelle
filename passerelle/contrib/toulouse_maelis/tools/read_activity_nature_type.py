#!/usr/bin/python3

import argparse

import utils


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')
    results = client.service.readActivityNatureTypeList()

    for nature in results:
        print('* %s: %s' % (nature['code'], nature['libelle']))
        for a_type in nature['activityTypeList']:
            print('  * %s: %s' % (a_type['code'], a_type['libelle']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    check(parser.parse_args())
