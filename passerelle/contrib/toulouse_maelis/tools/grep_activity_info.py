#!/usr/bin/python3

import argparse

import utils

FAMILY_ID = '322573'  # NICO
PERSON_ID = '263781'  # BART


def activity_info(args, client, activity_id, unit_id, text_id):
    results = client.service.getPersonUnitInfo(
        getPersonUnitInfoRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            'activityUnitPlace': {
                'idActivity': activity_id,
                'idUnit': unit_id,
                'idPlace': text_id,
            },
            'dateRef': None,  # si nulle, date du jour
        }
    )
    print('* %s / %s / %s\n' % (activity_id, unit_id, text_id))
    if args.dump:
        print(results)


def check(args):
    utils.configure_logging(args.verbose)
    client = utils.get_client(args.env, 'Activity')

    results = client.service.getPersonCatalogueActivity(
        getPersonCatalogueActivityRequestBean={
            'numDossier': args.family,
            'numPerson': args.person,
            # facultatif, pour fitrer sur une nature d'activité
            'codeNatureActivity': None,  # 'P' ou 'V'
            # facultatif, ne fait que retirer des lignes
            'yearSchool': '2022',  # aucune influence ici
            'dateStartActivity': '2022-09-01',
            'dateEndActivity': '2023-08-01',
        }
    )

    for activity in results['catalogueActivityList']:
        activity_id = activity['activity']['idActivity']
        activity_text = activity['activity']['libelle1']

        for unit in activity['unitInfoList']:
            unit_id = unit['idUnit']
            unit_text = unit['libelle']
            i = 0
            for place in unit['placeInfoList']:
                place_id = place['place']['idPlace']
                place_text = place['place']['lib1']
                print('\n=== %s - %s - %s ===' % (activity_text, unit_text, place_text))
                activity_info(args, client, activity_id, unit_id, place_id)
                i += 1
                if i == 3:
                    print('(...)')
                    break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', type=int, default=2, help='display errors')
    parser.add_argument('--env', '-e', default='integ', help='dev, test, integ, prod')
    parser.add_argument('--dump', '-d', default=False, action='store_true', help='dump')
    parser.add_argument('--family', '-f', default=FAMILY_ID, help='family id')
    parser.add_argument('--person', '-P', default=PERSON_ID, help='person id')
    check(parser.parse_args())
