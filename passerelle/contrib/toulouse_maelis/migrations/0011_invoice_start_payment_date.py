# Generated by Django 3.2.18 on 2023-04-21 13:52

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('toulouse_maelis', '0010_toulousemaelis_max_payment_delay'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='start_payment_date',
            field=models.DateTimeField(null=True),
        ),
    ]
