# Generated by Django 3.2.18 on 2023-04-13 15:57

import django.core.serializers.json
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('toulouse_maelis', '0007_invoice'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='lingo_notification_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='maelis_data_update_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='maelis_notification_data',
            field=models.JSONField(encoder=django.core.serializers.json.DjangoJSONEncoder, null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='maelis_notification_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterUniqueTogether(
            name='invoice',
            unique_together={('resource', 'regie_id', 'invoice_id')},
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='canceled',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='notified',
        ),
    ]
