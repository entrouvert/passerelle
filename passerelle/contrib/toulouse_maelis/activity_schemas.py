# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . import schemas

INDICATOR_SCHEMA = {
    'type': 'object',
    'required': ['code', 'isActive'],
    'properties': {
        'code': {
            'description': "Code de l'indicateur (depuis référentiel)",
            'type': 'string',
            'pattern': '.+',
        },
        'note': {
            'description': 'Commentaire pour les indicateurs de type NOTE',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'isActive': {
            'description': "True pour ajouter/modifier l'indicateur ou False pour le retirer",
            'oneOf': schemas.BOOLEAN_TYPES
            + [
                {'type': 'string', 'pattern': '^$'},
                {'type': 'null'},
            ],
        },
    },
}

BOOKING_SCHEMA = {
    'type': 'object',
    'properties': {
        'person_id': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 8,
        },
        'booking_list': {
            'type': 'array',
            'items': {
                'type': 'string',
                'pattern': '[A-Za-z0-9]+:[- A-Za-z0-9]+:[0-9]{4}-[0-9]{2}-[0-9]{2}',
            },
        },
        'start_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'end_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
    },
    'required': [
        'person_id',
        'booking_list',
        'start_date',
        'end_date',
    ],
}

BOOKING_ACTIVITY_SCHEMA = {
    'type': 'object',
    'properties': {
        'person_id': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 8,
        },
        'activity_id': {
            'type': 'string',
            'pattern': '^[A-Za-z0-9]+$',
        },
        'booking_list': {
            'type': 'array',
            'items': {
                'type': 'string',
                'pattern': '^[A-Za-z0-9]+:[A-Za-z0-9]+:[A-Za-z0-9]+:[0-9]{4}-[0-9]{2}-[0-9]{2}$',
            },
        },
        'start_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'end_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
    },
    'required': [
        'person_id',
        'activity_id',
        'booking_list',
        'start_date',
        'end_date',
    ],
}

RECURRENT_WEEK_SCHEMA = {
    'oneOf': [
        {
            'type': 'array',
            'items': {
                'type': 'string',
                'pattern': '^[1-7]-[A-Z]$',
            },
        },
        {'type': 'null'},
        {
            'type': 'string',
            'pattern': '^$',
        },
    ],
}

UPDATE_RECURRENT_WEEK_SCHEMA = {
    'type': 'object',
    'properties': {
        'person_id': {
            'type': 'string',
            'pattern': '^[0-9]+$',
        },
        'activity_id': {
            'type': 'string',
            'pattern': '^[A-Za-z0-9]+$',
        },
        'start_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'end_date': {
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'recurrent_week': RECURRENT_WEEK_SCHEMA,
    },
    'required': [
        'person_id',
        'activity_id',
        'start_date',
        'end_date',
        'recurrent_week',
    ],
}

SUBSCRIPTION_SCHEMA = {
    'type': 'object',
    'properties': {
        'person_id': {
            'type': 'string',
            'minLength': 1,
            'maxLength': 8,
        },
        'activity_id': {
            'type': 'string',
            'pattern': '[A-Za-z0-9]+',
        },
        'unit_id': {
            'type': 'string',
            'pattern': '[A-Za-z0-9]+',
        },
        'place_id': {
            'type': 'string',
            'pattern': '[A-Za-z0-9]+',
        },
        'start_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'end_date': {
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'recurrent_week': RECURRENT_WEEK_SCHEMA,
        'conveyanceSubscribe': {
            'type': 'object',
            'properties': {
                'idPlaceMorning': {
                    'oneOf': [
                        {
                            'type': 'string',
                            'pattern': '^[A-Za-z0-9]*$',
                        },
                        {'type': 'null'},
                    ],
                },
                'idPlaceAfternoon': {
                    'oneOf': [
                        {
                            'type': 'string',
                            'pattern': '^[A-Za-z0-9]*$',
                        },
                        {'type': 'null'},
                    ],
                },
            },
        },
        'form_api_url': {
            'description': 'Adresse de la vue API du formulaire Publik : {{ form_api_url }}',
            'oneOf': [
                {'type': 'string'},
                {'type': 'null'},
            ],
        },
        'form_number': {
            'description': 'Numéro de la demande Publik : {{ form_number }}',
            'oneOf': [
                {
                    'type': 'string',
                    'pattern': '^[0-9]+-[0-9]+$',
                },
                {'type': 'null'},
            ],
        },
        'indicatorList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
    },
    'required': [
        'person_id',
        'activity_id',
        'unit_id',
        'place_id',
        'start_date',
        'end_date',
    ],
    'unflatten': True,
}

BASKET_SCHEMA = {
    'type': 'object',
    'properties': {
        'basket_id': {
            'type': 'string',
            'pattern': '^[A-Za-z0-9]+$',
        },
    },
    'required': [
        'basket_id',
    ],
}

BASKET_LINE_SCHEMA = {
    'type': 'object',
    'properties': {
        'basket_id': {
            'type': 'string',
            'pattern': '^[A-Za-z0-9]+$',
        },
        'line_id': {
            'type': 'string',
            'pattern': '^[A-Za-z0-9]+$',
        },
    },
    'required': [
        'basket_id',
        'line_id',
    ],
}
