# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy

from . import schemas

ISEXISTS_SCHEMA = {
    'title': 'Exist',
    'description': "Recherche d'un responsable légal ou d'un enfant dans Maelis",
    'type': 'object',
    'required': ['firstname', 'lastname', 'dateBirth'],
    'properties': schemas.BASIC_ID_PROPERTIES,
    'additionalProperties': False,
}


CONTACTLIGHT_SCHEMA = {
    'title': 'Contact light',
    'description': "Informations de contact pour les personnes autorisées à récupérer les enfants ou à prévenir en cas d'urgence",
    'type': 'object',
    'properties': {
        'phone': {
            'description': 'Téléphone',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'mobile': {
            'description': 'Portable',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'mail': {
            'description': 'Mail',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}


PERSON_PROPERTIES = {
    'civility': {
        'description': 'civilité (depuis référentiel)',
        'oneOf': [{'type': 'string'}, {'type': 'null'}],
    },
    'sexe': {
        'description': 'Sexe (depuis référentiel)',
        'oneOf': [{'type': 'string'}, {'type': 'null'}],
    },
    'contact': {'oneOf': [CONTACTLIGHT_SCHEMA, {'type': 'null'}]},
}
PERSON_PROPERTIES.update(schemas.BASIC_ID_PROPERTIES)


EMERGENCY_PERSON_SCHEMA = {
    'title': 'Emergency person',
    'description': "Personnes à prévenir en cas d'urgence",
    'type': 'object',
    'required': ['firstname', 'lastname', 'quality'],
    'properties': {
        'quality': {
            'description': 'Qualité',
            'type': 'string',
            'pattern': '.+',
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}
EMERGENCY_PERSON_SCHEMA['properties'].update(PERSON_PROPERTIES)


AUTHORIZED_PERSON_SCHEMA = {
    'title': 'Family persons',
    'description': "Personnes autorisées à venir chercher l'enfant",
    'type': 'object',
    'required': ['personInfo', 'personQuality'],
    'properties': {
        'personInfo': {
            'type': 'object',
            'required': ['firstname', 'lastname'],
            'properties': PERSON_PROPERTIES,
        },
        'personQuality': {
            'type': 'object',
            'required': ['code'],
            'properties': {
                'code': {
                    'description': 'Le code (depuis référentiel)',
                    'type': 'string',
                    'pattern': '.+',
                },
            },
        },
    },
    'additionalProperties': False,
    'unflatten': True,
}


BIRTH_SCHEMA = {
    'title': 'Birth info',
    'description': 'Informations relatives à la naissance',
    'type': 'object',
    'required': ['dateBirth'],
    'properties': {
        'dateBirth': {
            'description': 'Date de naissance',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'place': {
            'description': 'Lieu de naissance',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'communeCode': {
            'description': 'Commune de naissance (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'cdDepartment': {
            'description': 'Département naissance (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'countryCode': {
            'description': 'Pays de naissance (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}

INDICATOR_SCHEMA = {
    'title': 'Indicator',
    'description': 'Indicateurs',
    'type': 'object',
    'required': ['code', 'isActive'],
    'properties': {
        'code': {
            'description': "Code de l'indicateur (depuis référentiel)",
            'type': 'string',
            'pattern': '.+',
        },
        'note': {
            'description': 'Commentaire pour les indicateurs de type NOTE',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'isActive': {
            'description': "True pour ajouter/modifier l'indicateur (défault) ou False pour le retirer",
            'oneOf': schemas.BOOLEAN_TYPES,
        },
    },
}

ID_PROPERTIES = {
    'firstname': {
        'description': 'Prénom',
        'type': 'string',
    },
    'lastname': {
        'description': 'Nom',
        'type': 'string',
    },
    'maidenName': {
        'description': 'Nom de jeune fille ',
        'oneOf': [{'type': 'string'}, {'type': 'null'}],
    },
    'birth': BIRTH_SCHEMA,
}

ADDRESS_SCHEMA = {
    'title': 'Address',
    'description': 'Informations sur une adresse',
    'type': 'object',
    'required': ['street1', 'town', 'zipcode'],
    'properties': {
        'num': {
            'description': "Numéro de l'adresse",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'numComp': {
            'description': 'Complément du numéro (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'idStreet': {
            'description': 'Identifiant de la voie (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'street1': {
            'description': 'Libellé de la voie',
            'type': 'string',
        },
        'street2': {
            'description': 'Complément de la voie',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'town': {
            'description': 'Ville',
            'type': 'string',
        },
        'zipcode': {
            'description': 'Code postal',
            'type': 'string',
        },
    },
}

CONTACT_SCHEMA = {
    'title': 'Contact',
    'description': 'Informations sur le contact',
    'type': 'object',
    'properties': {
        'phone': {
            'description': 'Téléphone',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'mobile': {
            'description': 'Portable',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'mail': {
            'description': 'Mail',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'isContactMail': {
            'description': 'Accepte de recevoir des mails',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
        'isContactSms': {
            'description': 'Accepte de recevoir des sms',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
        'isInvoicePdf': {
            'description': 'Accepte de ne plus recevoir de facture papier',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
    },
}

ADDRESSPROF_SCHEMA = {
    'title': 'Adresse pro',
    'description': "Informations sur l'adresse professionnelle",
    'type': 'object',
    'properties': {
        'num': {
            'description': "Numéro de l'adresse",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'street': {
            'description': 'Nom de la voie',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'town': {
            'description': 'Ville',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'zipcode': {
            'description': 'Code postal',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}

PROFESSION_SCHEMA = {
    'title': 'Profession',
    'description': 'Informations sur la profession',
    'type': 'object',
    'properties': {
        'codeCSP': {
            'description': 'Catégorie socio-professionnelle (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'profession': {
            'description': 'Profession',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'employerName': {
            'description': "Nom de l'employeur",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'phone': {
            'description': 'Téléphone',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'situation': {
            'description': 'Code de la situation professionnelle (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'weeklyHours': {
            'description': 'horaires de travail hebdomadaire',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'addressPro': ADDRESSPROF_SCHEMA,
    },
}

CAFINFO_SCHEMA = {
    'title': 'CAF',
    'description': 'Informations sur la CAF',
    'type': 'object',
    'properties': {
        'number': {
            'description': "Numéro d'allocataire",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'organ': {
            'description': "Nom de l'organisme (depuis référentiel)",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}

FISCINFO_SCHEMA = {
    'title': 'CAF',
    'description': 'Information fiscale',
    'type': 'object',
    'properties': {
        'spi': {
            'description': 'Numéro fiscal',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}

RLINFO_SCHEMA = {
    'title': 'RL',
    'description': 'Informations sur le responsable légal',
    'type': 'object',
    'required': ['firstname', 'lastname', 'civility', 'quality', 'birth', 'adresse'],
    'properties': {
        'civility': {
            'description': 'civilité (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'quality': {
            'description': 'Qualité (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'adresse': ADDRESS_SCHEMA,
        'contact': {'oneOf': [CONTACT_SCHEMA, {'type': 'null'}]},
        'profession': {'oneOf': [PROFESSION_SCHEMA, {'type': 'null'}]},
        'CAFInfo': {'oneOf': [CAFINFO_SCHEMA, {'type': 'null'}]},
        'fiscalInfo': {'oneOf': [FISCINFO_SCHEMA, {'type': 'null'}]},
        'indicatorList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}
RLINFO_SCHEMA['properties'].update(ID_PROPERTIES)

DOCTORADDRESS_SCHEMA = {
    'title': 'Doctor address',
    'description': "Informations sur l'adresse du docteur",
    'type': 'object',
    'properties': {
        'street1': {
            'description': 'Libellé de la voie',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'town': {
            'description': 'Ville',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'zipcode': {
            'description': 'Code postal',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
}

FAMILYDOCTOR_SCHEMA = {
    'title': 'Family doctor',
    'description': 'Informations sur le docteur',
    'type': 'object',
    'properties': {
        'name': {
            'description': 'Nom',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'phone': {
            'description': 'Téléphone',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'address': DOCTORADDRESS_SCHEMA,
    },
}

VACCIN_SCHEMA = {
    'title': 'Vaccin',
    'description': 'Informations sur le vaccin',
    'type': 'object',
    'required': ['code', 'vaccinationDate'],
    'properties': {
        'code': {
            'description': 'Code du vaccin (depuis référentiel)',
            'type': 'string',
        },
        'vaccinationDate': {
            'description': 'Date du vaccin',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
    },
}

MEDICALRECORD_SCHEMA = {
    'title': 'Medical record',
    'description': 'Informations médicales',
    'oneOf': [
        {
            'type': 'object',
            'properties': {
                'familyDoctor': FAMILYDOCTOR_SCHEMA,
                'allergy1': {
                    'description': 'Allergie 1',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'allergy2': {
                    'description': 'Allergie 2',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'comment1': {
                    'description': 'Commentaire 1',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'comment2': {
                    'description': 'Commentaire 2',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'observ1': {
                    'description': 'Observation 1',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'observ2': {
                    'description': 'Observation 2',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'isAuthHospital': {
                    'description': "Autorisation d'hospitalisation",
                    'oneOf': schemas.BOOLEAN_TYPES,
                },
                'hospital': {
                    'description': 'Hopital',
                    'oneOf': [{'type': 'string'}, {'type': 'null'}],
                },
                'vaccinList': {
                    'oneOf': [
                        {
                            'type': 'array',
                            'items': VACCIN_SCHEMA,
                        },
                        {'type': 'null'},
                    ],
                },
            },
            'additionalProperties': False,
        },
        {'type': 'null'},
    ],
    'unflatten': True,
}

PAIINFO_SCHEMA = {
    'title': 'PAI',
    'description': 'Informations médicales',
    'type': 'object',
    'required': ['code'],
    'properties': {
        'code': {
            'description': 'Code (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'dateDeb': {
            'description': 'Date de début',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'dateFin': {
            'description': 'Date de fin',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'description': {
            'description': 'Texte libre de description (max 500 caractères)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
    },
    'additionalProperties': False,
}

INSURANCE_SCHEMA = {
    'title': 'Insurace',
    'description': "Informations sur l'assurance",
    'type': 'object',
    'properties': {
        'company': {
            'description': "Compagnie d'assurrance",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'contractNumber': {
            'description': 'Numéro du contrat',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'memberNumber': {
            'description': 'Numéro de membre',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'contractStart': {
            'description': 'Date de début du contrat',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'contractEnd': {
            'description': 'Date de fin du contrat',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
    },
}

CHILD_SCHEMA = {
    'title': 'Child',
    'description': "Informations sur la création d'un enfant",
    'type': 'object',
    'required': ['sexe', 'firstname', 'lastname', 'birth'],
    'properties': {
        'num': {
            'description': "Numéro de l'enfant",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'sexe': {
            'description': 'Sexe (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'dietcode': {
            'description': 'Code de régime alimentaire (depuis référentiel)',
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'bPhoto': {
            'description': 'Autorisation photo',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
        'bLeaveAlone': {
            'description': 'Autorisation à partir seul',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
        'medicalRecord': {'oneOf': [MEDICALRECORD_SCHEMA, {'type': 'null'}]},
        'paiInfoBean': {'oneOf': [PAIINFO_SCHEMA, {'type': 'null'}]},
        'insurance': {'oneOf': [INSURANCE_SCHEMA, {'type': 'null'}]},
        'authorizedPersonList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': AUTHORIZED_PERSON_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
        'indicatorList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
    },
    'additionalProperties': False,
}
CHILD_SCHEMA['properties'].update(ID_PROPERTIES)

UPDATE_FAMILY_SCHEMA = {
    'title': 'Family',
    'description': 'Informations pour créer ou mettre à jour une famille',
    'type': 'object',
    'required': ['category', 'situation'],
    'properties': {
        'category': {
            'description': 'Catégorie (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'situation': {
            'description': 'Situation familiale (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'nbChild': {
            'description': "Nombre d'enfants à charge",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'nbTotalChild': {
            'description': "Nombre total d'enfants",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'nbAES': {
            'description': "Nombre d'AES",
            'oneOf': [{'type': 'string'}, {'type': 'null'}],
        },
        'rl1': RLINFO_SCHEMA,
        'rl2': RLINFO_SCHEMA,
        'emergencyPersonList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': EMERGENCY_PERSON_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
        'childList': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': CHILD_SCHEMA,
                },
                {'type': 'null'},
            ],
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}

CREATE_FAMILY_SCHEMA = copy.deepcopy(UPDATE_FAMILY_SCHEMA)
CREATE_FAMILY_SCHEMA['required'] = ['rl1', 'category', 'situation']

CREATE_RL1_SCHEMA = copy.deepcopy(CREATE_FAMILY_SCHEMA)
del CREATE_RL1_SCHEMA['properties']['rl2']
del CREATE_RL1_SCHEMA['properties']['emergencyPersonList']
del CREATE_RL1_SCHEMA['properties']['childList']
del CREATE_RL1_SCHEMA['properties']['rl1']['properties']['contact']
del CREATE_RL1_SCHEMA['properties']['rl1']['properties']['profession']
del CREATE_RL1_SCHEMA['properties']['rl1']['properties']['CAFInfo']
del CREATE_RL1_SCHEMA['properties']['rl1']['properties']['fiscalInfo']

UPDATE_RL1_SCHEMA = copy.deepcopy(RLINFO_SCHEMA)
UPDATE_RL1_SCHEMA['required'] = ['firstname', 'lastname', 'civility', 'quality', 'birth']
del UPDATE_RL1_SCHEMA['properties']['adresse']
del UPDATE_RL1_SCHEMA['properties']['contact']
del UPDATE_RL1_SCHEMA['properties']['profession']
del UPDATE_RL1_SCHEMA['properties']['CAFInfo']
del UPDATE_RL1_SCHEMA['properties']['fiscalInfo']
del UPDATE_RL1_SCHEMA['properties']['indicatorList']

CREATE_RL2_SCHEMA = copy.deepcopy(RLINFO_SCHEMA)
CREATE_RL2_SCHEMA['unflatten'] = True
del CREATE_RL2_SCHEMA['properties']['contact']
del CREATE_RL2_SCHEMA['properties']['profession']
del CREATE_RL2_SCHEMA['properties']['CAFInfo']
del CREATE_RL2_SCHEMA['properties']['fiscalInfo']
del CREATE_RL2_SCHEMA['properties']['indicatorList']

UPDATE_RL2_SCHEMA = copy.deepcopy(UPDATE_RL1_SCHEMA)

CREATE_CHILD_SCHEMA = copy.deepcopy(CHILD_SCHEMA)
CREATE_CHILD_SCHEMA['unflatten'] = True
del CREATE_CHILD_SCHEMA['properties']['dietcode']
del CREATE_CHILD_SCHEMA['properties']['medicalRecord']
del CREATE_CHILD_SCHEMA['properties']['paiInfoBean']
del CREATE_CHILD_SCHEMA['properties']['authorizedPersonList']
del CREATE_CHILD_SCHEMA['properties']['indicatorList']

UPDATE_CHILD_SCHEMA = copy.deepcopy(CREATE_CHILD_SCHEMA)

UPDATE_COORDINATE_SCHEMA = {
    'title': 'Update coordinate',
    'description': "Mise à jour des coordonnées d'un responsable légal",
    'type': 'object',
    'required': ['adresse'],
    'properties': {
        'adresse': ADDRESS_SCHEMA,
        'contact': {'oneOf': [CONTACT_SCHEMA, {'type': 'null'}]},
        'profession': {'oneOf': [PROFESSION_SCHEMA, {'type': 'null'}]},
        'CAFInfo': {'oneOf': [CAFINFO_SCHEMA, {'type': 'null'}]},
        'FiscalInfo': {'oneOf': [FISCINFO_SCHEMA, {'type': 'null'}]},
    },
    'unflatten': True,
    'additionalProperties': False,
}

UPDATE_INDICATOR_SCHEMA = {
    'title': 'Update indicators',
    'description': 'Mise à jour des indicateurs',
    'type': 'object',
    'required': ['indicatorList'],
    'properties': {
        'indicatorList': {
            'type': 'array',
            'items': INDICATOR_SCHEMA,
            'minItems': 1,
        }
    },
    'additionalProperties': False,
    'unflatten': True,
}

UPDATE_QUOTIENT_SCHEMA = {
    'title': 'Family persons',
    'description': 'Mise à jours des quotients sur les responsables légaux',
    'type': 'object',
    'required': ['yearRev', 'dateStart', 'dateEnd', 'mtt', 'cdquo'],
    'properties': {
        'yearRev': {
            'description': 'Année de revenu',
            'type': 'string',
            'pattern': '^[0-9]{4}$',
        },
        'dateStart': {
            'description': 'Date de début',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'dateEnd': {
            'description': 'Date de fin',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'mtt': {
            'description': 'Montant',
            'type': 'string',
            'pattern': r'^[0-9]+\.?[0-9]*$',
        },
        'cdquo': {
            'description': 'Code du quotient (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
    },
    'additionalProperties': False,
}

WCS_FILE_SCHEMA = {
    'title': 'WCS File',
    'description': 'Champ W.C.S. de type ficher',
    'type': 'object',
    'required': ['filename', 'content_type', 'content'],
    'properties': {
        'filename': {
            'description': 'Nom du ficher',
            'type': 'string',
        },
        'content_type': {
            'description': 'Type MIME',
            'type': 'string',
        },
        'content': {
            'description': 'Contenu',
            'type': 'string',
        },
    },
}

SUPPLIED_DOCUMENT_SCHEMA = {
    'title': 'Supplied document',
    'description': 'Ajoute un document pour une famille, un responsable légal ou un enfant',
    'type': 'object',
    'required': ['code', 'file'],
    'properties': {
        'code': {
            'description': 'Code de la pièce (depuis référentiel)',
            'type': 'string',
            'pattern': '.+',
        },
        'depositDate': {
            'description': 'Date de dépôt (date du jour si non transmise) ',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'visaDate': {
            'description': 'Date de visa',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'validityDate': {
            'description': 'Date de validité',
            'type': 'string',
            'pattern': '^([0-9]{4}-[0-9]{2}-[0-9]{2}){0,1}$',
        },
        'file': WCS_FILE_SCHEMA,
    },
    'additionalProperties': False,
}

SUPPLIED_DOCUMENTS_SCHEMA = {
    'title': 'Supplied documents',
    'description': 'Ajoute des documents pour une famille, un responsable légal ou un enfant',
    'type': 'object',
    'required': ['documentList'],
    'properties': {
        'numPerson': {
            'description': "Numéro du responsable légal ou de l'enfant",
            'type': 'string',
        },
        'documentList': {'type': 'array', 'items': SUPPLIED_DOCUMENT_SCHEMA},
    },
    'unflatten': True,
    'additionalProperties': False,
}

SCHOOL_PRE_REGISTRATION_SCHEMA = {
    'title': 'School pre-registration',
    'description': 'Crée une pré-inscription scolaire pour un enfant',
    'type': 'object',
    'required': ['numPerson', 'schoolYear', 'dateSubscribe', 'levelCode'],
    'properties': {
        'numPerson': {
            'description': "Numéro de l'enfant",
            'type': 'string',
        },
        'schoolYear': {
            'description': 'Année scolaire',
            'type': 'string',
        },
        'dateSubscribe': {
            'description': "Date d'inscription",
            'type': 'string',
        },
        'levelCode': {
            'description': 'Le code du niveau scolaire',
            'type': 'string',
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}

SCHOOL_PRE_REGISTRATION_WITH_EXEMPTION_SCHEMA = {
    'title': 'School pre-registration',
    'description': 'Crée une pré-inscription scolaire pour un enfant avec demande de dérogation',
    'type': 'object',
    'required': [
        'numPerson',
        'schoolYear',
        'datePresubscribe',
        'levelCode',
        'idRequestSchool1',
        'derogReasonCode',
        'derogComment',
    ],
    'properties': {
        'numPerson': {
            'description': "Numéro de l'enfant",
            'type': 'string',
        },
        'schoolYear': {
            'description': 'Année scolaire',
            'type': 'string',
        },
        'datePresubscribe': {
            'description': "Date d'inscription",
            'type': 'string',
        },
        'levelCode': {
            'description': 'Le code du niveau scolaire',
            'type': 'string',
        },
        'idRequestSchool1': {
            'description': "Identifiant de l'établissement demandé 1",
            'type': 'string',
        },
        'idRequestSchool2': {
            'description': "Identifiant de l'établissement demandé 2",
            'type': 'string',
        },
        'idRequestSchool3': {
            'description': "Identifiant de l'établissement demandé 3",
            'type': 'string',
        },
        'derogReasonCode': {
            'description': 'Code du motif de dérogation',
            'type': 'string',
        },
        'derogComment': {
            'description': 'Commentaire relatif à la dérogation',
            'type': 'string',
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}


SCHOOL_PRE_REGISTRATION_WITH_SIBLING_SCHEMA = {
    'title': 'School pre-registration',
    'description': 'Crée une pré-inscription scolaire pour un enfant avec rapprochement de fratrie',
    'type': 'object',
    'required': [
        'numPerson',
        'schoolYear',
        'datePresubscribe',
        'levelCode',
        'idSchoolRequested',
        'numPersonSibling',
    ],
    'properties': {
        'numPerson': {
            'description': "Numéro de l'enfant",
            'type': 'string',
        },
        'schoolYear': {
            'description': 'Année scolaire',
            'type': 'string',
        },
        'datePresubscribe': {
            'description': "Date d'inscription",
            'type': 'string',
        },
        'levelCode': {
            'description': 'Le code du niveau scolaire',
            'type': 'string',
        },
        'idSchoolRequested': {
            'description': "Identifiant de l'établissement demandé",
            'type': 'string',
        },
        'numPersonSibling': {
            'description': 'Identifiant du membre de la fratrie pour lequel le rapprochement est demandé',
            'type': 'string',
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}

NURSERY_SCHEMA = {
    'title': 'Nursery',
    'description': 'Crèche',
    'type': 'object',
    'required': ['idActivity', 'idUnit', 'idPlace'],
    'properties': {
        'idActivity': {
            'description': 'Identifiant d\'activité',
            'type': 'string',
        },
        'idUnit': {
            'description': 'Identifiant unité',
            'type': 'string',
        },
        'idPlace': {
            'description': 'Identifiant lieu',
            'type': 'string',
        },
    },
}


NURSERY_DEMAND_SCHEMA = {
    'title': 'Nursery demand',
    'description': 'Crée une demande de place en crèche',
    'type': 'object',
    'required': ['family_id', 'start_date'],
    'properties': {
        'family_id': {
            'description': 'Numéro DUI',
            'type': 'string',
        },
        'family_indicators': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                }
            ],
            'description': 'Liste des indicateurs à positionner sur la famille',
        },
        'child_id': {
            'description': 'Identifiant de l\'enfant',
            'type': 'string',
        },
        'child_first_name': {
            'description': 'Prénom de l\'enfant',
            'type': 'string',
        },
        'child_last_name': {
            'description': 'Nom de l\'enfant',
            'type': 'string',
        },
        'child_gender': {
            'description': 'Sexe de l\'enfant',
            'type': 'string',
        },
        'child_birthdate': {
            'description': 'Date de naissance ou date de naissance prévisionnelle de l\'enfant',
            'type': 'string',
        },
        'child_indicators': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                }
            ],
            'description': 'Liste des indicateurs à positionner sur l\'enfant',
        },
        'demand_indicators': {
            'oneOf': [
                {
                    'type': 'array',
                    'items': INDICATOR_SCHEMA,
                }
            ],
            'description': 'Liste des indicateurs à positionner sur la demande',
        },
        'start_date': {
            'description': 'Date d\'entrée en structure souhaitée',
            'type': 'string',
            'pattern': '^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
        },
        'number_of_days': {
            'description': 'Nombre de jours par semaine',
            'type': 'string',
            'pattern': '^[0-7]{1}$',
        },
        'start_hour_Mon': {
            'description': 'Heure de début le lundi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Mon': {
            'description': 'Heure de fin le lundi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Tue': {
            'description': 'Heure de début le mardi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Tue': {
            'description': 'Heure de fin le mardi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Wed': {
            'description': 'Heure de début le mercedi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Wed': {
            'description': 'Heure de fin le mercredi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Thu': {
            'description': 'Heure de début le jeudi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Thu': {
            'description': 'Heure de fin le jeudi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Fri': {
            'description': 'Heure de début le vendredi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Fri': {
            'description': 'Heure de fin le vendredi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Sat': {
            'description': 'Heure de début le samedi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Sat': {
            'description': 'Heure de fin le samedi',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'start_hour_Sun': {
            'description': 'Heure de début le dimanche',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'end_hour_Sun': {
            'description': 'Heure de fin le dimanche',
            'type': 'string',
            'pattern': '^([0-9]{2}:[0-9]{2}){0,1}$',
        },
        'comment': {
            'description': 'Commentaire à propos du besoin d\'accueil',
            'type': 'string',
        },
        'accept_other_nurseries': {
            'description': 'Accepter d\'autres crèches',
            'oneOf': schemas.BOOLEAN_TYPES,
        },
        'nursery1': {
            'description': 'Premier choix de crèche',
            'oneOf': [NURSERY_SCHEMA],
        },
        'nursery2': {
            'description': 'Deuxième choix de crèche',
            'oneOf': [NURSERY_SCHEMA],
        },
        'nursery3': {
            'description': 'Troisième choix de crèche',
            'oneOf': [NURSERY_SCHEMA],
        },
    },
    'unflatten': True,
    'additionalProperties': False,
}
