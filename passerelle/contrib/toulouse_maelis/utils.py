# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from math import inf

from dateutil.relativedelta import relativedelta

from passerelle.utils.jsonresponse import APIError

json_date_format = '%Y-%m-%d'


def get_reference_year_from_date(some_date):
    if some_date.month <= 8:
        # between january and august, reference year is the year just before
        return some_date.year - 1
    return some_date.year


def get_public_criterias(today, start_dob, end_dob):
    publics_txt = [
        'Petit enfant (- de 3 ans)',
        'Enfant (3-11 ans)',
        'Ado (12-17 ans)',
        'Jeune (18-25 ans)',
        'Adulte (26-59 ans)',
        'Sénior (60 ans et plus)',
    ]
    ages = [0, 3, 12, 18, 26, 60, 62]

    data = []
    max_age = relativedelta(today, start_dob.date()).years if start_dob else inf
    min_age = relativedelta(today, end_dob.date()).years if end_dob else 0
    for i in range(0, len(ages) - 1):
        for age in range(ages[i], ages[i + 1] - 1):
            if min_age <= age <= max_age:
                data.append((str(i), publics_txt[i]))
                break
    return data


def strtobool(val):
    val = val.lower()
    if val in ('y', 'yes', 't', 'true', 'on', '1'):
        return True
    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
        return False
    raise APIError('invalid truth value %r' % val)


def normalize_id(value):
    if not isinstance(value, str):
        return None
    value = str(value)
    value = re.sub(r'\s', '', value)
    value = re.sub(r'^0*', '', value)
    return value
