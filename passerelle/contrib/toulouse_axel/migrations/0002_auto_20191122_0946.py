from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('toulouse_axel', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='link',
            unique_together={('resource', 'name_id')},
        ),
    ]
