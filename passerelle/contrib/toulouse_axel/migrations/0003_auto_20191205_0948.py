import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('toulouse_axel', '0002_auto_20191122_0946'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lock',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('key', models.CharField(max_length=256)),
                ('lock_date', models.DateTimeField(auto_now_add=True)),
                ('locker', models.CharField(blank=True, max_length=256)),
                (
                    'resource',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='toulouse_axel.ToulouseAxel'
                    ),
                ),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='lock',
            unique_together={('resource', 'key')},
        ),
    ]
