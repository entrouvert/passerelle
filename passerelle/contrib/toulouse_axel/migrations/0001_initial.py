import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0016_auto_20191002_1443'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name_id', models.CharField(max_length=256)),
                ('dui', models.CharField(max_length=128)),
                ('person_id', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='ToulouseAxel',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'wsdl_url',
                    models.CharField(
                        help_text='Toulouse Axel WSDL URL', max_length=128, verbose_name='WSDL URL'
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Toulouse Axel',
            },
        ),
        migrations.AddField(
            model_name='link',
            name='resource',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='toulouse_axel.ToulouseAxel'
            ),
        ),
        migrations.AlterUniqueTogether(
            name='link',
            unique_together={('resource', 'name_id'), ('resource', 'name_id', 'dui', 'person_id')},
        ),
    ]
