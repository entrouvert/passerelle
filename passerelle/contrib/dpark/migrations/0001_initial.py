from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0006_resourcestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='DPark',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'wsdl_url',
                    models.URLField(
                        help_text='URL of the SOAP wsdl endpoint',
                        max_length=512,
                        verbose_name='SOAP wsdl endpoint',
                    ),
                ),
                (
                    'operation_url',
                    models.URLField(
                        help_text='URL of the SOAP operation endpoint',
                        max_length=512,
                        verbose_name='SOAP operation endpoint',
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'D-Park connector',
            },
        ),
        migrations.CreateModel(
            name='Pairing',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('nameid', models.CharField(max_length=256)),
                ('lastname', models.CharField(max_length=128)),
                ('firstnames', models.CharField(max_length=128)),
                ('filenumber', models.CharField(max_length=128)),
                ('badgenumber', models.CharField(max_length=128)),
                ('cardnumber', models.CharField(max_length=128)),
                ('resource', models.ForeignKey(to='dpark.DPark', on_delete=models.CASCADE)),
            ],
            options={'ordering': ['filenumber']},
        ),
    ]
