# Generated by Django 1.11.12 on 2018-11-19 13:42

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dpark', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dpark',
            name='log_level',
        ),
    ]
