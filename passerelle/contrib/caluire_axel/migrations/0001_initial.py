import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0029_auto_20210202_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaluireAxel',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'wsdl_url',
                    models.CharField(
                        help_text='Caluire Axel WSDL URL', max_length=128, verbose_name='WSDL URL'
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Caluire Axel',
            },
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name_id', models.CharField(max_length=256)),
                ('family_id', models.CharField(max_length=128)),
                ('person_id', models.CharField(max_length=128)),
                (
                    'resource',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='caluire_axel.CaluireAxel'
                    ),
                ),
            ],
            options={
                'unique_together': {('resource', 'name_id')},
            },
        ),
    ]
