# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def compute_status(data):
    data.update(
        {
            'closed': not data['resourceNumber'],
            'estimatedAvgWaitingTimeInMinutes': None,
        }
    )
    if not data['closed'] and data['estimatedAvgWaitingTime'] != '-':
        values = [int(x) for x in data['estimatedAvgWaitingTime'].split(':')]
        data['estimatedAvgWaitingTimeInMinutes'] = 60 * values[0] + values[1] + 1
