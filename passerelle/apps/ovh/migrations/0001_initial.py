import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OVHSMSGateway',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                ('account', models.CharField(max_length=64, verbose_name='Account')),
                ('username', models.CharField(max_length=64, verbose_name='Username')),
                ('password', models.CharField(max_length=64, verbose_name='Password')),
                (
                    'msg_class',
                    models.IntegerField(
                        choices=[
                            (
                                0,
                                (
                                    'Message are directly shown to users on phone screen at reception. '
                                    'The message is never stored, neither in the phone memory nor in the SIM card. '
                                    'It is deleted as soon as the user validate the display.'
                                ),
                            ),
                            (
                                1,
                                'Messages are stored in the phone memory, or in the SIM card if the memory is full. ',
                            ),
                            (2, 'Messages are stored in the SIM card.'),
                            (3, 'Messages are stored in external storage like a PDA or a PC.'),
                        ],
                        default=1,
                        verbose_name='Message class',
                    ),
                ),
                (
                    'credit_threshold_alert',
                    models.PositiveIntegerField(default=100, verbose_name='Credit alert threshold'),
                ),
                (
                    'default_country_code',
                    models.CharField(
                        default='33',
                        max_length=3,
                        verbose_name='Default country code',
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[0-9]*$', 'The country must only contain numbers'
                            )
                        ],
                    ),
                ),
                ('credit_left', models.PositiveIntegerField(default=0, verbose_name='Credit left')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'db_table': 'sms_ovh',
                'verbose_name': 'OVH',
            },
            bases=(models.Model,),
        ),
    ]
