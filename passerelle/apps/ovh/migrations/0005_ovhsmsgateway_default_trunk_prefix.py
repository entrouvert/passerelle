import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('ovh', '0004_auto_20160407_0456'),
    ]

    operations = [
        migrations.AddField(
            model_name='ovhsmsgateway',
            name='default_trunk_prefix',
            field=models.CharField(
                default='0',
                max_length=2,
                verbose_name='Default trunk prefix',
                validators=[
                    django.core.validators.RegexValidator(
                        '^[0-9]*$', 'The trunk prefix must only contain numbers'
                    )
                ],
            ),
            preserve_default=True,
        ),
    ]
