from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('ovh', '0005_ovhsmsgateway_default_trunk_prefix'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ovhsmsgateway',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
