from django.urls import re_path

from . import views

management_urlpatterns = [
    re_path(
        r'^(?P<slug>[\w,-]+)/request_token/$', views.RequestTokenView.as_view(), name='ovh-request-token'
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/confirm_token/(?P<uuid>[a-z0-9-]+)/$',
        views.ConfirmTokenView.as_view(),
        name='ovh-confirm-token',
    ),
]
