from django.contrib import admin

from .models import OVHSMSGateway


@admin.register(OVHSMSGateway)
class OVHSMSGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = [
        'title',
        'slug',
        'description',
        'username',
        'password',
        'msg_class',
        'credit_threshold_alert',
        'credit_left',
        'default_country_code',
    ]
