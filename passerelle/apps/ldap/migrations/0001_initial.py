# Generated by Django 3.2.14 on 2022-08-02 14:37

from django.db import migrations, models

import passerelle.apps.ldap.forms
import passerelle.utils.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0029_auto_20210202_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                ('ldap_url', passerelle.utils.models.LDAPURLField(max_length=512, verbose_name='Server URL')),
                (
                    'ldap_bind_dn',
                    models.CharField(blank=True, max_length=256, null=True, verbose_name='Bind DN'),
                ),
                (
                    'ldap_bind_password',
                    models.CharField(blank=True, max_length=128, null=True, verbose_name='Bind password'),
                ),
                (
                    'ldap_tls_cert',
                    models.FileField(
                        verbose_name='TLS client certificate',
                        null=True,
                        blank=True,
                        upload_to=passerelle.utils.models.resource_file_upload_to,
                        validators=[passerelle.apps.ldap.forms.validate_certificate],
                    ),
                ),
                (
                    'ldap_tls_key',
                    models.FileField(
                        blank=True,
                        null=True,
                        upload_to=passerelle.utils.models.resource_file_upload_to,
                        validators=[passerelle.apps.ldap.forms.validate_private_key],
                        verbose_name='TLS client key',
                    ),
                ),
                (
                    'ldap_tls_cacert',
                    models.FileField(
                        blank=True,
                        null=True,
                        upload_to=passerelle.utils.models.resource_file_upload_to,
                        validators=[passerelle.apps.ldap.forms.validate_certificate],
                        verbose_name='TLS trusted certificate',
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
                (
                    'ldap_tls_check_hostname',
                    models.BooleanField(
                        verbose_name='TLS check hostname',
                        default=True,
                        help_text='Warning: this option is actually not supported (python-ldap < 3.4)',
                        blank=True,
                    ),
                ),
                (
                    'ldap_tls_check_cert',
                    models.BooleanField(
                        verbose_name='TLS check certificate',
                        default=True,
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'LDAP',
            },
        ),
    ]
