# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django import forms
from django.utils.translation import gettext_lazy as _

from passerelle.forms import GenericConnectorForm


class HolidaysConnectorForm(GenericConnectorForm):
    def __init__(self, *args, **kwargs):
        from .models import HOLIDAYS_CHOICES

        super().__init__(*args, **kwargs)
        self.fields['holidays'] = forms.MultipleChoiceField(
            choices=HOLIDAYS_CHOICES,
            widget=forms.CheckboxSelectMultiple,
            initial=[x[0] for x in HOLIDAYS_CHOICES],
            label=_('Holidays'),
        )
