import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChoositRegisterGateway',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                ('url', models.CharField(max_length=200)),
                ('key', models.CharField(max_length=64)),
            ],
            options={
                'db_table': 'registration_choosit',
                'verbose_name': 'Choosit Registration',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChoositRegisterNewsletter',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=16)),
                ('description', models.CharField(max_length=128, blank=True)),
            ],
            options={
                'db_table': 'newsletter_choosit',
                'verbose_name': 'Choosit Registration',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChoositSMSGateway',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                ('key', models.CharField(max_length=64, verbose_name='Key')),
                (
                    'default_country_code',
                    models.CharField(
                        default='33',
                        max_length=3,
                        verbose_name='Default country code',
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[0-9]*$', 'The country must only contain numbers'
                            )
                        ],
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'db_table': 'sms_choosit',
                'verbose_name': 'Choosit',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='choositregistergateway',
            name='newsletters',
            field=models.ManyToManyField(to='choosit.ChoositRegisterNewsletter', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='choositregistergateway',
            name='users',
            field=models.ManyToManyField(to='base.ApiUser', blank=True),
            preserve_default=True,
        ),
    ]
