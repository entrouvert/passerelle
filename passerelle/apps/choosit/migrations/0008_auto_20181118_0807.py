# Generated by Django 1.11.12 on 2018-11-18 14:07

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('choosit', '0007_auto_20180814_1048'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choositsmsgateway',
            name='log_level',
        ),
    ]
