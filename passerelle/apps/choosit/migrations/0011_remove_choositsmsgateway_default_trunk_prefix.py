# Generated by Django 3.2.25 on 2024-12-12 16:35

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('choosit', '0010_auto_20210202_1304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choositsmsgateway',
            name='default_trunk_prefix',
        ),
    ]
