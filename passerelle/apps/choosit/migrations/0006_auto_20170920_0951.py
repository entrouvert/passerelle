from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('choosit', '0005_choositsmsgateway_default_trunk_prefix'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choositsmsgateway',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
