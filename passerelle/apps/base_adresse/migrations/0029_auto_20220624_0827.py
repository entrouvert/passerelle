from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0028_alter_streetmodel_ban_id'),
    ]

    operations = [
        migrations.RemoveIndex(
            model_name='streetmodel',
            name='base_adress_resourc_e0df5d_idx',
        ),
        migrations.AlterUniqueTogether(
            name='streetmodel',
            unique_together=set(),
        ),
    ]
