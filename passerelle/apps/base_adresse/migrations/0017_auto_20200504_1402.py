# Generated by Django 1.11.18 on 2020-05-04 12:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0016_auto_20200130_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addresscachemodel',
            name='data',
            field=models.JSONField(),
        ),
        migrations.AlterField(
            model_name='baseadresse',
            name='api_geo_url',
            field=models.CharField(
                default='https://geo.api.gouv.fr/',
                help_text='Base Adresse API Geo URL',
                max_length=128,
                verbose_name='API Geo URL',
            ),
        ),
        migrations.AlterField(
            model_name='baseadresse',
            name='service_url',
            field=models.CharField(
                default='https://api-adresse.data.gouv.fr/',
                help_text='Base Adresse Web Service URL',
                max_length=128,
                verbose_name='Service URL',
            ),
        ),
    ]
