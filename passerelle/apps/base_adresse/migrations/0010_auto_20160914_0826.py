from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0009_streetmodel_simple_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='streetmodel',
            name='last_update',
            field=models.DateTimeField(auto_now=True, verbose_name='Last update', null=True),
        ),
    ]
