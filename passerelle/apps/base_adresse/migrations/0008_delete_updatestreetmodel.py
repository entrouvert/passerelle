from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0007_auto_20160729_1540'),
    ]

    operations = [
        migrations.DeleteModel(
            name='UpdateStreetModel',
        ),
    ]
