from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0005_auto_20160407_0456'),
    ]

    operations = [migrations.RenameModel('BaseAddresse', 'BaseAdresse')]
