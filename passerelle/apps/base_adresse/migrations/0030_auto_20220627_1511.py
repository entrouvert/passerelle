# Generated by Django 2.2.28 on 2022-06-27 13:11

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0029_auto_20220624_0827'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regionmodel',
            name='code',
            field=models.CharField(max_length=3, verbose_name='Region code'),
        ),
    ]
