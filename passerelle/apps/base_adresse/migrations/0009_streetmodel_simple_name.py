from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0008_delete_updatestreetmodel'),
    ]

    operations = [
        migrations.AddField(
            model_name='streetmodel',
            name='unaccent_name',
            field=models.CharField(max_length=150, null=True, verbose_name='Street name ascii char'),
        ),
    ]
