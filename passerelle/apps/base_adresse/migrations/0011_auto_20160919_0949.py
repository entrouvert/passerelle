from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0010_auto_20160914_0826'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='streetmodel',
            options={'ordering': ['unaccent_name', 'name']},
        ),
    ]
