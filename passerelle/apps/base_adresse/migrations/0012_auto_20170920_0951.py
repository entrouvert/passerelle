from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base_adresse', '0011_auto_20160919_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseadresse',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
