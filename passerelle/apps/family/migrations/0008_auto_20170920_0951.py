from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('family', '0007_auto_20161122_1816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='genericfamily',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
