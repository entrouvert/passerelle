from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('cmis', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cmisconnector',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
