from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0005_resourcelog'),
    ]

    operations = [
        migrations.CreateModel(
            name='CmisConnector',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'cmis_endpoint',
                    models.URLField(
                        help_text='URL of the CMIS Atom endpoint',
                        max_length=400,
                        verbose_name='CMIS Atom endpoint',
                    ),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Service username')),
                ('password', models.CharField(max_length=128, verbose_name='Service password')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'CMIS connector',
            },
        ),
    ]
