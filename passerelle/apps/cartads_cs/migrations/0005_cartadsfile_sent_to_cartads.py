# Generated by Django 1.11.12 on 2019-09-13 06:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('cartads_cs', '0004_cartadsdossier_cartads_steps_cache'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cartadsfile',
            options={'ordering': ['id']},
        ),
        migrations.AddField(
            model_name='cartadsfile',
            name='sent_to_cartads',
            field=models.DateTimeField(null=True),
        ),
    ]
