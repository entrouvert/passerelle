from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0010_loggingparameters_trace_emails'),
    ]

    operations = [
        migrations.CreateModel(
            name='VivaTicket',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('url', models.URLField(verbose_name='API URL')),
                ('login', models.CharField(max_length=256, verbose_name='API Login')),
                ('password', models.CharField(max_length=256, verbose_name='API Password')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'VivaTicket',
            },
        ),
    ]
