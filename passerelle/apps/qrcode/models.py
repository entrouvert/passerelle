import binascii
import json
import os
import uuid
from collections import defaultdict
from datetime import datetime, timedelta, timezone
from io import BytesIO
from itertools import product
from urllib.parse import urlparse

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.dateparse import parse_datetime as django_parse_datetime
from django.utils.http import urlencode
from django.utils.translation import gettext_lazy as _
from nacl.signing import SigningKey
from qrcode import ERROR_CORRECT_Q, QRCode
from qrcode.image.pil import PilImage
from requests.exceptions import RequestException

from passerelle.base.models import BaseResource
from passerelle.base.signature import sign_url
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError

UUID_PATTERN = '(?P<uuid>[0-9|a-f]{8}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9a-f]{12})'

CAMPAIGN_SCHEMA = {
    '$schema': 'http://json-schema.org/draft-06/schema#',
    'type': 'object',
    'unflatten': True,
    'additionalProperties': False,
    'properties': {
        'metadata': {
            'type': 'object',
            'title': _('Metadata associated to the campaign'),
            'additionalProperties': True,
        },
    },
}

CERTIFICATE_SCHEMA = {
    'type': 'object',
    'unflatten': True,
    'additionalProperties': False,
    'properties': {
        'campaign': {
            'type': 'string',
            'title': _('Campaign'),
            'pattern': UUID_PATTERN,
        },
        'data': {
            'type': 'object',
            'title': _('Data to encode in the certificate'),
            'additionalProperties': {'type': 'string'},
        },
        'metadata': {
            'type': 'object',
            'title': _('Metadata associated to the certificate'),
            'additionalProperties': True,
        },
        'validity_start': {
            'any': [{'type': 'null'}, {'const': ''}, {'type': 'string', 'format': 'date-time'}],
        },
        'validity_end': {
            'any': [{'type': 'null'}, {'const': ''}, {'type': 'string', 'format': 'date-time'}],
        },
    },
}

READER_SCHEMA = {
    'type': 'object',
    'additionalProperties': False,
    'properties': {
        'campaign': {
            'type': 'string',
            'title': _('Campaign'),
            'pattern': UUID_PATTERN,
        },
        'readable_metadatas': {
            'title': _('Comma-separated list of metadata keys that this reader is allowed to read.'),
            'any': [{'type': 'null'}, {'const': ''}, {'type': 'string'}],
        },
        'validity_start': {
            'any': [{'type': 'null'}, {'const': ''}, {'type': 'string', 'format': 'date-time'}],
        },
        'validity_end': {
            'any': [{'type': 'null'}, {'const': ''}, {'type': 'string', 'format': 'date-time'}],
        },
        'metadata': {
            'type': 'object',
            'title': _('Metadata associated with this reader'),
            'additionalProperties': True,
        },
    },
}

EVENT_SCHEMA = {
    '$schema': 'http://json-schema.org/draft-06/schema#',
    'type': 'object',
    'additionalProperties': False,
    'properties': {
        'certificate': {
            'type': 'string',
            'title': _('Certificate'),
            'pattern': UUID_PATTERN,
        },
        'metadata': {
            'type': 'object',
            'title': _('Metadata associated with the event'),
            'additionalProperties': True,
        },
    },
}


def generate_key():
    key = os.urandom(32)
    return ''.join(format(x, '02x') for x in key)


TALLY_SCHEMA = {
    'type': 'object',
    'unflatten': True,
    'additionalProperties': False,
    'properties': {
        'since': {
            'type': 'integer',
            'title': _('Get events since this timestamp'),
        },
        'asynchronous': {
            'type': 'boolean',
            'title': _('Asynchronous tallying (force save events if credit is insufficient)'),
        },
        'events': {
            'type': 'array',
            'title': _('Events to tally'),
            'items': {
                'type': 'object',
                'properties': {
                    'timestamp': {
                        'type': 'integer',
                        'title': _('Timestamp when the QRCode was read'),
                    },
                    'certificate': {
                        'type': 'string',
                        'title': _('Read certificate'),
                        'pattern': UUID_PATTERN,
                    },
                },
            },
        },
    },
}


class QRCodeConnector(BaseResource):
    category = _('Misc')

    class Meta:
        verbose_name = _('QR Code')

    @property
    def certificates(self):
        return Certificate.objects.filter(campaign__resource=self)

    @property
    def readers(self):
        return Reader.objects.filter(campaign__resource=self)

    def daily(self):
        for campaign in self.campaigns.all():
            campaign.merge_old_events()

    @endpoint(
        name='save-campaign',
        pattern=f'^{UUID_PATTERN}?$',
        example_pattern='{uuid}',
        description=_('Create or update a campaign'),
        post={'request_body': {'schema': {'application/json': CAMPAIGN_SCHEMA}}},
        parameters={
            'uuid': {
                'description': _('Campaign identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def save_campaign(self, request, uuid=None, post_data=None):
        metadata = post_data.get('metadata') or {}
        if not uuid:
            campaign = self.campaigns.create(metadata=metadata)
        else:
            campaign = get_object_or_404(self.campaigns, uuid=uuid)
            campaign.metadata = metadata
            campaign.save()

        return {
            'data': {
                'uuid': campaign.uuid,
            }
        }

    @endpoint(
        name='save-certificate',
        pattern=f'^{UUID_PATTERN}?$',
        example_pattern='{uuid}',
        description=_('Create or update a certificate'),
        post={'request_body': {'schema': {'application/json': CERTIFICATE_SCHEMA}}},
        parameters={
            'uuid': {
                'description': _('Certificate identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def save_certificate(self, request, uuid=None, post_data=None):
        if post_data.get('validity_start'):
            validity_start = parse_datetime(post_data['validity_start'])
        else:
            validity_start = None
        if post_data.get('validity_end'):
            validity_end = parse_datetime(post_data['validity_end'])
        else:
            validity_end = None
        data = post_data.get('data') or {}
        metadata = post_data.get('metadata') or {}

        if not uuid:
            if 'campaign' not in post_data:
                raise APIError('Campaign is mandatory when creating new certificate', http_status=400)

            campaign = self.campaigns.get(uuid=post_data['campaign'])
            certificate = campaign.certificates.create(
                campaign=campaign,
                data=data,
                metadata=metadata,
                validity_start=validity_start,
                validity_end=validity_end,
            )
        else:
            certificate = get_object_or_404(self.certificates, uuid=uuid)
            if 'campaign' in post_data and post_data['campaign'] != str(certificate.campaign.uuid):
                raise APIError("You can't change the campaign of an existing certificate", http_status=400)
            certificate.validity_start = validity_start
            certificate.validity_end = validity_end
            certificate.data = data
            certificate.metadata = metadata
            certificate.save()

        return {
            'data': {
                'uuid': certificate.uuid,
                'qrcode_url': certificate.get_qrcode_url(request),
            }
        }

    @endpoint(
        name='get-certificate',
        description=_('Retrieve an existing certificate'),
        pattern=f'^{UUID_PATTERN}$',
        example_pattern='{uuid}',
        parameters={
            'uuid': {
                'description': _('Certificate identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def get_certificate(self, request, uuid):
        certificate = get_object_or_404(self.certificates, uuid=uuid)

        status = {}
        for (account,) in certificate.events.values_list('metadata__account').distinct():
            account_status = merge_events(certificate.get_account_events(account))

            if 'account' in account_status:
                del account_status['account']

            account_key = '_default' if account is None else account

            status[account_key] = account_status

        return {
            'err': 0,
            'data': {
                'uuid': certificate.uuid,
                'data': certificate.data,
                'validity_start': certificate.validity_start and certificate.validity_start.isoformat(),
                'validity_end': certificate.validity_end and certificate.validity_end.isoformat(),
                'qrcode_url': certificate.get_qrcode_url(request),
                'status': status,
            },
        }

    @endpoint(
        name='get-qrcode',
        description=_('Get QR Code'),
        pattern=f'^{UUID_PATTERN}$',
        example_pattern='{uuid}',
        parameters={
            'uuid': {
                'description': _('Certificate identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def get_qrcode(self, request, uuid):
        certificate = get_object_or_404(self.certificates, uuid=uuid)
        qr_code = certificate.generate_qr_code()
        return HttpResponse(qr_code, content_type='image/png')

    @endpoint(
        name='save-reader',
        pattern=f'^{UUID_PATTERN}?$',
        example_pattern='{uuid}',
        description=_('Create or update a qrcode reader'),
        post={'request_body': {'schema': {'application/json': READER_SCHEMA}}},
        parameters={
            'uuid': {
                'description': _('QRCode reader identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def save_reader(self, request, uuid=None, post_data=None):
        if post_data.get('validity_start'):
            validity_start = parse_datetime(post_data['validity_start'])
        else:
            validity_start = None
        if post_data.get('validity_end'):
            validity_end = parse_datetime(post_data['validity_end'])
        else:
            validity_end = None

        readable_metadatas = post_data.get('readable_metadatas', '')
        metadata = post_data.get('metadata', {})

        if not uuid:
            if 'campaign' not in post_data:
                raise APIError(_('campaign parameter is mandatory when creating a reader'))

            campaign = get_object_or_404(self.campaigns, uuid=post_data['campaign'])
            reader = campaign.readers.create(
                campaign=campaign,
                validity_start=validity_start,
                validity_end=validity_end,
                readable_metadatas=readable_metadatas,
                metadata=metadata,
            )
        else:
            reader = get_object_or_404(self.readers, uuid=uuid)
            if 'campaign' in post_data:
                reader.campaign = get_object_or_404(self.campaigns, uuid=post_data['campaign'])
            reader.validity_start = validity_start
            reader.validity_end = validity_end
            reader.readable_metadatas = readable_metadatas
            reader.metadata = metadata
            reader.save()

        return {
            'data': {
                'uuid': reader.uuid,
                'url': reader.get_url(request),
            }
        }

    @endpoint(
        name='get-reader',
        description=_('Get informations about a QRCode reader'),
        pattern=f'^{UUID_PATTERN}$',
        example_pattern='{uuid}',
        parameters={
            'uuid': {
                'description': _('QRCode reader identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def get_reader(self, request, uuid):
        reader = get_object_or_404(self.readers, uuid=uuid)
        return {
            'err': 0,
            'data': {
                'uuid': reader.uuid,
                'validity_start': reader.validity_start and reader.validity_start.isoformat(),
                'validity_end': reader.validity_end and reader.validity_end.isoformat(),
                'url': reader.get_url(request),
            },
        }

    @endpoint(
        name='open-reader',
        perm='OPEN',
        description=_('Open a QRCode reader page.'),
        pattern=f'^{UUID_PATTERN}$',
        example_pattern='{uuid}',
        parameters={
            'uuid': {
                'description': _('QRCode reader identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            }
        },
    )
    def open_reader(self, request, uuid):
        reader = get_object_or_404(self.readers, uuid=uuid)
        now = datetime.now(timezone.utc)
        accounts = reader.merged_metadata.get('accounts')

        return TemplateResponse(
            request,
            'qrcode/qrcode-reader.html',
            context={
                'started': now >= reader.validity_start if reader.validity_start is not None else True,
                'expired': now >= reader.validity_end if reader.validity_end is not None else False,
                'verify_key': reader.campaign.hex_verify_key,
                'reader': reader,
                'metadata_url': reader.get_metadata_url(request),
                'tally_url': reader.get_tally_url(request) if reader.merged_metadata.get('tally') else None,
                'template': reader.merged_metadata.get('template'),
                'accounts': json.dumps(accounts) if accounts else None,
                'service_worker_scope': reverse(
                    'generic-endpoint',
                    kwargs={
                        'slug': self.slug,
                        'connector': self.get_connector_slug(),
                        'endpoint': 'open-reader',
                    },
                ),
            },
        )

    @endpoint(
        name='read-metadata',
        perm='OPEN',
        description=_('Read certificate metadata'),
        pattern=f'^{UUID_PATTERN}$',
        example_pattern='{uuid}',
        parameters={
            'uuid': {
                'description': _('QRCode reader identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            },
            'certificate': {
                'description': _('Certificate identifier'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            },
        },
    )
    def read_metadata(self, request, uuid, certificate):
        reader = get_object_or_404(self.readers, uuid=uuid)
        certificate = get_object_or_404(self.certificates, uuid=certificate)
        now = datetime.now(timezone.utc)
        if reader.validity_start is not None and now < reader.validity_start:
            return {'err': 1, 'err_desc': _("Reader isn't usable yet.")}

        if reader.validity_end is not None and now > reader.validity_end:
            return {'err': 1, 'err_desc': _('Reader has expired.')}

        readable_metatadas = reader.readable_metadatas.split(',')
        return {'err': 0, 'data': {k: v for k, v in certificate.metadata.items() if k in readable_metatadas}}

    @endpoint(
        name='tally',
        perm='OPEN',
        pattern=f'^{UUID_PATTERN}?$',
        description=_('Tally and get tallied events'),
        post={'request_body': {'schema': {'application/json': TALLY_SCHEMA}}},
    )
    def tally(self, request, uuid=None, post_data=None):
        # The asynchronous parameter is injected by the QR code reader's
        # service worker when it sends events that were already validated
        # according to its cache state (cf. qrcode-service-worker.js). When we
        # receive such events, we want to save them regardless of the credit
        # left on the certificate, because we want the DB to reflect what
        # happened in reality.
        #
        # Here the reality is that the person who used this certificate is
        # already playing in the swimming pool even if they didn't have credit
        # left on their card because the QR code reader a the entrance was out
        # of sync when they checked in. Make them pay.
        #
        # An event or some events asynchronously tallied are events created
        # without checking on the backend storage that there was enough credit.
        # So they are always tallied, allowing negative credit on the
        # corresponding accounts.
        asynchronous = post_data.get('asynchronous', False)

        reader = get_object_or_404(self.readers, uuid=uuid)

        if not reader.merged_metadata.get('tally'):
            raise PermissionDenied('Tallying is not enabled for this reader')

        pending_events = defaultdict(lambda: defaultdict(list))
        reader_accounts = [
            None if it == '_default' else it for it in reader.merged_metadata.get('accounts', [None])
        ]
        for event in post_data.get('events', []):
            certificate = event['certificate']
            account = None
            if '/' in certificate:
                certificate, account = certificate.split('/', maxsplit=1)
            if account not in reader_accounts:
                raise APIError(
                    f'Reader {reader.uuid} is not authorized to tally account {account}', http_status=401
                )
            pending_events[certificate][account].append(event)

        certificates = {}

        now = datetime.now(timezone.utc)
        certificates_queryset = Certificate.objects.filter(
            Q(validity_end__gte=now) | Q(validity_end__isnull=True),
            Q(validity_start__lte=now) | Q(validity_start__isnull=True),
            campaign=reader.campaign,
        )

        with transaction.atomic():
            rollback = False
            for certificate in certificates_queryset.filter(
                uuid__in=pending_events.keys()
            ).select_for_update():
                negative_or_revoked_account, account_statuses = certificate.try_to_merge_new_events(
                    reader=reader,
                    accounts_events=pending_events[str(certificate.uuid)],
                    # prevent more event creation if we already know they will be rollbacked
                    create=not rollback,
                    # allow events making the account's credit negative
                    # OR events on already revoked certificates
                    force=asynchronous,
                )
                rollback |= (not asynchronous) and negative_or_revoked_account
                for account, status in account_statuses.items():
                    account_key = certificate.make_account_key(account=account)
                    certificates[account_key] = status
            if rollback:
                transaction.set_rollback(True)

        stale_certificates = certificates_queryset
        since_timestamp = post_data.get('since', 0)
        if since_timestamp != 0:
            since = datetime.fromtimestamp(since_timestamp, timezone.utc) - timedelta(minutes=1)
            stale_certificates = stale_certificates.filter(events__received__gte=since)

            account_filter = Q(events__metadata__account__in=reader_accounts)

            if None in reader_accounts:
                account_filter |= Q(events__metadata__account=None)
                account_filter |= ~Q(events__metadata__has_key='account')

            stale_accounts = (
                stale_certificates.filter(account_filter)
                .values_list('id', 'events__metadata__account')
                .distinct()
            )

        else:
            stale_accounts = product(certificates_queryset.values_list('id', flat=True), reader_accounts)

        for certificate_id, account in stale_accounts:
            certificate = Certificate.objects.prefetch_related('events').get(id=certificate_id)
            account_key = certificate.make_account_key(account=account)
            if account_key in certificates:
                continue
            certificates[account_key] = certificate.merge_account_events(account)

        return {
            'data': {
                'timestamp': int(datetime.timestamp(now)),
                'certificates': certificates,
            }
        }

    @endpoint(
        name='add-event',
        description=_('Add an event to a certificate'),
        post={'request_body': {'schema': {'application/json': EVENT_SCHEMA}}},
    )
    def add_event(self, request, post_data):
        certificate_uuid = post_data['certificate']
        try:
            certificate = Certificate.objects.get(uuid=certificate_uuid)
        except Certificate.DoesNotExist:
            raise APIError(f'Unknown certificate {certificate_uuid}', http_status=400)

        # See merge_events implementation for internal metadata fields
        # semantics
        metadata = post_data.get('metadata', {})
        if 'credit' in metadata:
            try:
                metadata['credit'] = int(metadata['credit'])
            except ValueError:
                raise APIError('Invalid credit value: %s' % metadata['credit'], http_status=400)

        certificate.create_event_from_wcs(metadata=metadata)

    @endpoint(
        name='events',
        description=_('List events'),
        parameters={
            'campaign': {
                'description': _('List only events related to this campaign.'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            },
            'certificate': {
                'description': _('List only events related to this certificate.'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            },
            'reader': {
                'description': _('List only events related to this reader.'),
                'example_value': '12345678-1234-1234-1234-123456789012',
            },
            'since': {
                'description': _('List only event received after this date.'),
                'example_value': '2024-02-01T20:00:00+00:00',
            },
            'until': {
                'description': _('List only event received before this date.'),
                'example_value': '2024-02-01T20:00:00+00:00',
            },
            'tally': {
                'description': _(
                    'Only list event generated by readers (default: list events coming from both readers and direct API calls)'
                ),
                'example_value': 'True',
                'type': 'boolean',
            },
        },
    )
    def events(
        self,
        request,
        campaign=None,
        certificate=None,
        reader=None,
        since=None,
        until=None,
        tally=None,
    ):
        result = []
        for event in self._list_events(
            campaign=campaign, certificate=certificate, reader=reader, since=since, until=until, tally=tally
        ):
            result.append(
                {
                    'uuid': str(event.uuid),
                    'certificate': str(event.certificate.uuid),
                    'reader': str(event.reader.uuid) if event.reader else None,
                    'received': event.received.isoformat(),
                    'happened': event.happened.isoformat(),
                    'metadata': event.metadata,
                }
            )
        return {'data': result}

    def _list_events(
        self,
        campaign=None,
        certificate=None,
        reader=None,
        since=None,
        until=None,
        tally=None,
        summary=False,
    ):
        query = Event.objects.filter(certificate__campaign__resource=self)
        if campaign:
            query = query.filter(certificate__campaign__uuid=parse_uuid(campaign))
        if certificate:
            query = query.filter(certificate__uuid=parse_uuid(certificate))
        if reader:
            query = query.filter(reader__uuid=parse_uuid(reader))
        if since:
            query = query.filter(received__gte=parse_datetime(since))
        if until:
            query = query.filter(received__lte=parse_datetime(until))
        if tally is not None:
            query = query.filter(reader__isnull=not tally)

        return query.order_by('happened')

    def trigger_wcs_job(self, event_id):
        event = Event.objects.select_related().get(id=event_id)
        event.trigger_wcs()


def encode_mime_like(data):
    msg = ''
    for key, value in data.items():
        msg += '%s: %s\n' % (key, value.replace('\n', '\n '))
    return msg.encode()


BASE45_CHARSET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:'
BASE45_DICT = {v: i for i, v in enumerate(BASE45_CHARSET)}


def b45encode(buf: bytes) -> bytes:
    """Convert bytes to base45-encoded string"""
    res = ''
    buflen = len(buf)
    for i in range(0, buflen & ~1, 2):
        x = (buf[i] << 8) + buf[i + 1]
        e, x = divmod(x, 45 * 45)
        d, c = divmod(x, 45)
        res += BASE45_CHARSET[c] + BASE45_CHARSET[d] + BASE45_CHARSET[e]
    if buflen & 1:
        d, c = divmod(buf[-1], 45)
        res += BASE45_CHARSET[c] + BASE45_CHARSET[d]
    return res.encode()


class Campaign(models.Model):
    uuid = models.UUIDField(verbose_name=_('UUID'), unique=True, default=uuid.uuid4)
    resource = models.ForeignKey(QRCodeConnector, on_delete=models.CASCADE, related_name='campaigns')
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_('Last modification'), auto_now=True)
    metadata = models.JSONField(null=True, verbose_name=_('Campaign meta data'))

    key = models.CharField(
        _('Private Key'),
        max_length=64,
        default=generate_key,
        validators=[RegexValidator(r'[a-z|0-9]{64}', 'Key should be a 32 bytes hexadecimal string')],
    )

    @property
    def signing_key(self):
        binary_key = binascii.unhexlify(self.key)
        return SigningKey(seed=binary_key)

    @property
    def hex_verify_key(self):
        verify_key = self.signing_key.verify_key.encode()
        return binascii.hexlify(verify_key).decode('utf-8')

    def merge_old_events(self):
        max_history = int((self.metadata or {}).get('max_event_history', 90))
        max_event_age = datetime.now(timezone.utc) - timedelta(max_history)
        query_set = Event.objects.filter(certificate__campaign=self, received__lt=max_event_age)
        for certificate_pk, account in query_set.values_list('certificate', 'metadata__account').distinct():
            with transaction.atomic():
                certificate = Certificate.objects.get(pk=certificate_pk)
                events_to_merge = (
                    certificate.get_account_events(account)
                    .filter(received__lt=max_event_age)
                    .select_for_update()
                )

                if events_to_merge.count() == 1:
                    continue

                last_event = events_to_merge.last()
                merged_metadata = merge_events(events_to_merge)
                if 'last_tally' in merged_metadata:
                    del merged_metadata['last_tally']
                events_to_merge.all().delete()
                merged_event = Event.objects.create(
                    certificate=certificate, happened=last_event.happened, metadata=merged_metadata
                )
                merged_event.received = last_event.received
                merged_event.save()


class Certificate(models.Model):
    uuid = models.UUIDField(verbose_name=_('UUID'), unique=True, default=uuid.uuid4)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, related_name='certificates')
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_('Last modification'), auto_now=True)
    validity_start = models.DateTimeField(verbose_name=_('Validity Start Date'), null=True)
    validity_end = models.DateTimeField(verbose_name=_('Validity End Date'), null=True)
    data = models.JSONField(null=True, verbose_name='Certificate Data')
    metadata = models.JSONField(null=True, verbose_name='Certificate meta data')

    def to_json(self):
        data = {'uuid': str(self.uuid)}
        if self.validity_start:
            data['validity_start'] = str(self.validity_start.timestamp())
        if self.validity_end:
            data['validity_end'] = str(self.validity_end.timestamp())
        data |= self.data
        return data

    def generate_b45_data(self):
        data = self.to_json()
        msg = encode_mime_like(data)
        signed = self.campaign.signing_key.sign(msg)
        return b45encode(signed).decode()

    def generate_qr_code(self):
        qr_code = QRCode(image_factory=PilImage, error_correction=ERROR_CORRECT_Q)
        data = self.generate_b45_data()
        qr_code.add_data(data)
        qr_code.make(fit=True)
        image = qr_code.make_image(fill_color='black', back_color='white')
        fd = BytesIO()
        image.save(fd)
        return fd.getvalue()

    def get_qrcode_url(self, request):
        qrcode_relative_url = reverse(
            'generic-endpoint',
            kwargs={
                'slug': self.campaign.resource.slug,
                'connector': self.campaign.resource.get_connector_slug(),
                'endpoint': 'get-qrcode',
                'rest': str(self.uuid),
            },
        )
        return request.build_absolute_uri(qrcode_relative_url)

    def try_to_merge_new_events(self, reader, accounts_events, create=True, force=False):
        statuses = {}
        existing_uuids = {str(event_uuid) for event_uuid in self.events.values_list('uuid', flat=True)}
        negative_or_revoked_account = False
        for account, events in accounts_events.items():
            # filter out already saved events
            events = [evt for evt in events if evt['uuid'] not in existing_uuids]

            # keep new account status to return it to caller
            account_status = statuses[account] = self._recompute_account_status_with_events(account, events)

            negative_or_revoked_account |= account_status['credit'] < 0 or account_status['revoked']

            if not create:
                continue

            if negative_or_revoked_account and not force:
                continue

            for event in events:
                credit = event.get('credit', -1)
                metadata = {'credit': 0 if account_status['pass'] else credit}
                if account:
                    metadata['account'] = account

                self.create_event_from_reader(
                    reader=reader,
                    uuid=event.get('uuid'),
                    happened=datetime.fromtimestamp(event['timestamp'], timezone.utc),
                    metadata=metadata,
                )
        return negative_or_revoked_account, statuses

    def _recompute_account_status_with_events(self, account, events):
        '''Merge knowns and new events for an account'''
        account_status = self.merge_account_events(account)

        if not account_status['revoked'] and not account_status['pass']:
            account_status['credit'] += sum(min(-1, event.get('credit', -1)) for event in events)
        return account_status

    def merge_account_events(self, account):
        account_events = self.get_account_events(account)
        # See merge_events implementation for internal metadata fields
        # semantics
        account_status = merge_events(account_events)
        if 'account' in account_status:
            del account_status['account']
        return account_status

    def get_account_events(self, account):
        query_set = self.events
        if account is not None:
            return query_set.filter(metadata__account=account)

        return query_set.filter(~Q(metadata__has_key='account') | Q(metadata__account=None))

    def _sign_wcs_url(self, trigger_url):
        scheme, netloc, _, _, _, _ = urlparse(trigger_url)
        services = settings.KNOWN_SERVICES.get('wcs', {})
        for service in services.values():
            remote_url = service.get('url')
            service_scheme, service_netloc, _, _, _, _ = urlparse(remote_url)
            if service_scheme == scheme and service_netloc == netloc:
                orig = service.get('orig')
                if orig:
                    trigger_url = (
                        trigger_url + ('&' if '?' in trigger_url else '?') + urlencode({'orig': orig})
                    )
                secret = service.get('secret')
                return sign_url(trigger_url, secret)

        self.campaign.resource.logger.error(
            "Can't find a suitable configured WCS service for url %s", trigger_url
        )

        return None

    def get_trigger_wcs_url(self):
        url = self.metadata and self.metadata.get('trigger_url')
        if not url:
            return None
        return self._sign_wcs_url(url)

    def create_event_from_reader(self, reader, uuid, happened, metadata):
        event = self.events.create(uuid=uuid, reader=reader, happened=happened, metadata=metadata)
        if not self.get_trigger_wcs_url():
            return
        event.register_trigger_wcs()
        return event

    def create_event_from_wcs(self, metadata):
        return self.events.create(happened=datetime.now(timezone.utc), metadata=metadata)

    def make_account_key(self, account=None):
        if account is None:
            return str(self.uuid)
        return f'{str(self.uuid)}/{account}'


class Reader(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, related_name='readers')
    uuid = models.UUIDField(verbose_name=_('UUID'), unique=True, default=uuid.uuid4)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_('Last modification'), auto_now=True)
    validity_start = models.DateTimeField(verbose_name=_('Validity Start Date'), null=True)
    validity_end = models.DateTimeField(verbose_name=_('Validity End Date'), null=True)
    readable_metadatas = models.CharField(max_length=128, verbose_name=_('Readable metadata keys'), null=True)
    metadata = models.JSONField(null=True, verbose_name=_('Reader meta data'))

    @property
    def merged_metadata(self):
        return (self.campaign.metadata or {}) | (self.metadata or {})

    def get_url(self, request):
        return self._get_endpoint_url(request, 'open-reader')

    def get_metadata_url(self, request):
        if not self.readable_metadatas:
            return None
        return self._get_endpoint_url(request, 'read-metadata')

    def get_tally_url(self, request):
        return self._get_endpoint_url(request, 'tally')

    def _get_endpoint_url(self, request, endpoint):
        relative_url = reverse(
            'generic-endpoint',
            kwargs={
                'slug': self.campaign.resource.slug,
                'connector': self.campaign.resource.get_connector_slug(),
                'endpoint': endpoint,
                'rest': str(self.uuid),
            },
        )
        return request.build_absolute_uri(relative_url)


class Event(models.Model):
    uuid = models.UUIDField(verbose_name=_('UUID'), unique=True, default=uuid.uuid4)
    certificate = models.ForeignKey(Certificate, on_delete=models.CASCADE, related_name='events')
    reader = models.ForeignKey(Reader, on_delete=models.CASCADE, related_name='events', null=True)
    happened = models.DateTimeField()
    received = models.DateTimeField(auto_now_add=True)
    metadata = models.JSONField(null=True, verbose_name='Event metadata')

    def register_trigger_wcs(self):
        self.certificate.campaign.resource.add_job('trigger_wcs_job', event_id=self.id)

    def trigger_wcs(self):
        url = self.certificate.get_trigger_wcs_url()
        if not url:
            return

        try:
            response = self.certificate.campaign.resource.requests.post(
                url,
                json={
                    'uuid': str(self.uuid),
                    'certificate': str(self.certificate.uuid),
                    'reader': str(self.reader.uuid) if self.reader else None,
                    'received': self.received.isoformat(),
                    'happened': self.happened.isoformat(),
                    'metadata': self.metadata,
                },
            )
            response.raise_for_status()
        except RequestException as exception:
            self.certificate.campaign.resource.logger.error(
                'Error while triggering w.c.s. for certificate %s : %s', self.certificate.uuid, exception
            )
            raise


def parse_datetime(value):
    try:
        result = django_parse_datetime(value)
    except ValueError:
        result = None

    if result is None:
        raise APIError(_('Invalid date %s') % value)

    return result


def merge_events(query_set):
    events_data = query_set.order_by('received').values_list('metadata', 'reader', 'happened')

    credit = 0
    merged_metadata = {}
    last_tally = None
    last_reader = None

    for metadata, reader, happened in events_data:
        metadata = metadata or {}

        # Credit is accumulated, so events saved via add_event can add credit
        # by setting a positive value in the 'credit' field of metadata. Each
        # tallying event decrement it by setting -1 in the same 'credit' field.
        credit += metadata.get('credit', 0)

        # Other metadatas are overwritten in the chronological order, so the
        # last value that was set for a given field is the one taken into
        # account.
        # This behavior allows to revoke / unrevoke a certificate, by adding an
        # event using the save_event endpoint with the 'revoked' metadata set
        # to true resp. false.
        # In the same fashion, the 'pass' boolean metadata allows to enable /
        # disable unlimited credit.
        merged_metadata = merged_metadata | metadata

        if reader and happened and (last_tally is None or happened > last_tally):
            last_tally = happened
            last_reader = reader

    result = {'revoked': False, 'pass': False} | merged_metadata | {'credit': credit}

    if last_tally:
        result['last_tally'] = {
            'timestamp': datetime.timestamp(last_tally),
            'reader': Reader.objects.get(pk=last_reader).merged_metadata,
        }

    return result


def parse_uuid(value):
    try:
        return uuid.UUID(value)
    except ValueError:
        raise APIError(_('Invalid UUID: %s') % value)
