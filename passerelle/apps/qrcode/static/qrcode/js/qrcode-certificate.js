import './nacl.min.js'
import './zxing-browser.min.js'

/* c8 ignore start */
// https://github.com/zxing-js/browser/issues/72
if (window.ZXingBrowser) {
  const patchedMediaStreamIsTorchCompatible
    = window.ZXingBrowser.BrowserCodeReader.mediaStreamIsTorchCompatible
  window.ZXingBrowser.BrowserCodeReader.mediaStreamIsTorchCompatible = (track) => {
    return track.getCapabilities && patchedMediaStreamIsTorchCompatible(track)
  }
}
/* c8 ignore stop */

export async function scanCertificates (videoElement, hexKey, callback) {
  const key = new Uint8Array(hexKey.match(/[\da-f]{2}/gi).map(h => parseInt(h, 16)))
  const codeReader = new window.ZXingBrowser.BrowserQRCodeReader()

  await codeReader.decodeFromVideoDevice(undefined, videoElement, async (result) => {
    if (!result) {
      return
    }

    let signed
    try {
      signed = decodeBase45(result.text)
    } catch (error) {
      await callback({error: gettext('Invalid QR Code.')})
      return
    }

    const opened = window.nacl.sign.open(signed, key)
    if (opened == null) {
      await callback({error: gettext('This reader can\'t read this QR Code.')})
      return
    }

    const decoder = new TextDecoder('utf-8')
    const decoded = decoder.decode(opened)
    const data = decodeMimeLike(decoded)
    const uuid = data.uuid

    const validity = {
      start: data.validity_start && parseFloat(data.validity_start),
      end: data.validity_end && parseFloat(data.validity_end),
    }

    delete data.uuid
    delete data.validity_start
    delete data.validity_end
    await callback({uuid, validity, data})
  })
}

export async function fetchMetadata (uuid, metadataUrl) {
  try {
    const url = `${metadataUrl}?certificate=${uuid}`
    const response = await fetch(url)
    const content = await response.json()

    if (!response.ok || content.err !== 0) {
      return { error: gettext('An api error occured while fetching metadata.') }
    }

    return { items: content.data }
  } catch {
    return {
      error: gettext(
        'A network error occured while fetching metadata : check network connectivity.',
      ),
    }
  }
}

function splitAccountKey (accountKey) {
  const keyElements = accountKey.split('/')
  if (keyElements.length > 1) {
    return [keyElements[0], keyElements.slice(1).join('/')]
  }

  return [accountKey, null]
}

export async function tally (uuid, tallyUrl, accounts) {
  try {
    const now = Math.floor(Date.now() / 1000)
    const events = []

    for (const [accountId, debit] of Object.entries(accounts)) {
      if (debit === 0) {
        continue
      }

      const accountKey = accountId !== '_default' ? `${uuid}/${accountId}` : uuid
      events.push({
        uuid: crypto.randomUUID(),
        certificate: accountKey,
        timestamp: now,
        credit: -debit,
      })
    }

    const response = await fetch(
      tallyUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          since: now,
          events,
        }),
      },
    )

    const json = await response.json()

    if (!response.ok || json.err !== 0) {
      return {error: gettext('An api error occured while tallying the QR code.')}
    }

    const accountsState = {}
    for (const [accountKey, result] of Object.entries(json.data.certificates)) {
      const [certificateUUID, accountId] = splitAccountKey(accountKey)
      if (certificateUUID !== uuid) {
        continue
      }

      accountsState[accountId || '_default'] = result
    }

    return { accounts: accountsState }
  } catch (error) {
    console.log(error)
    return {
      error: gettext('A network error occured while tallying the QR code : check network connectivity.'),
    }
  }
}

function decodeMimeLike (value) {
  const chunks = value.split('\n')
  const data = {}
  let k = null
  let v = null

  for (let i = 0; i < chunks.length; i++) {
    const line = chunks[i]
    if (line.startsWith(' ')) {
      if (k !== null) {
        v += '\n' + line.slice(1)
      }
    } else {
      if (k !== null) {
        data[k] = v
        k = null
        v = null
      }
      if (line.indexOf(': ') !== -1) {
        const parts = line.split(': ', 2)
        k = parts[0]
        v = parts[1]
      }
    }
  }

  if (k !== null) {
    data[k] = v
  }

  return data
}

function divmod (a, b) {
  let remainder = a
  let quotient = 0
  if (a >= b) {
    remainder = a % b
    quotient = (a - remainder) / b
  }
  return [quotient, remainder]
}

const BASE45_CHARSET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:'

function decodeBase45 (str) {
  const output = []
  const buf = []

  for (let i = 0, length = str.length; i < length; i++) {
    const j = BASE45_CHARSET.indexOf(str[i])
    if (j < 0) { throw new Error('Base45 decode: unknown character') }
    buf.push(j)
  }

  for (let i = 0, length = buf.length; i < length; i += 3) {
    const x = buf[i] + buf[i + 1] * 45
    if (length - i >= 3) {
      const [d, c] = divmod(x + buf[i + 2] * 45 * 45, 256)
      output.push(d)
      output.push(c)
    } else {
      output.push(x)
    }
  }
  return new Uint8Array(output)
}
