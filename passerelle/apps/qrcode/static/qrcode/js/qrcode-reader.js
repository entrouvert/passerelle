import { scanCertificates, tally, fetchMetadata } from './qrcode-certificate.js'
import './nunjucks.min.js'

class QRElement extends window.HTMLElement {
  static template

  static init (name, template) {
    this.template = document.createElement('template')
    this.template.innerHTML = template
    window.customElements.define(name, this)
  }

  constructor () {
    super()
    this.attachShadow({ mode: 'open' })
    this.shadowRoot.append(this.constructor.template.content.cloneNode(true))
  }
}


export class QRCodeReader extends QRElement {
  static defaultContentTemplate = `
    {% if error %}
      <qr-title>` + gettext('Invalid Certificate') + `</qr-title>
      <qr-error>{{ error }}</qr-error>
    {% else %}
      <qr-title>` + gettext('Validity') + `</qr-title>
      {% if validity.start and validity.start > now() %}
        <qr-error>` + gettext('QRCode is not valid yet') + `</qr-error>
      {% endif %}
      {% if validity.end and validity.end < now() %}
        <qr-error>` + gettext('QRCode is expired') + `</qr-error>
      {% endif %}
      <qr-label>` + gettext('From:') + `</qr-label>
      <qr-value>{{ validity.start|formatDate or "` + gettext('Always') + `" }}</qr-value>
      <qr-label>` + gettext('To:') + `</qr-label>
      <qr-value>{{ validity.end | formatDate or "` + gettext('Never') + `" }}</qr-value>

      {% if data or metadata %}
        <qr-title loading="{{ metadata.loading }}">` + gettext('Data') + `</qr-title>
        {% if metadata.error %}
          <qr-error>{{ metadata.error }}</qr-error>
          <qr-button action="fetch-metadata">` + gettext('Retry') + `</qr-button>
        {% endif %}
        {% for key, value in data %}
          <qr-label>{{ key }}</qr-label>
          <qr-value>{{ value }}</qr-value>
        {% endfor %}
        {% for key, value in metadata.items %}
          <qr-label>{{ key }}</qr-label>
          <qr-value>{{ value }}</qr-value>
        {% endfor %}
      {% endif %}
      {% if tallyUrl %}
        <qr-tallier tally-url="{{ tallyUrl }}" certificate-uuid="{{ uuid }}" accounts='{{ accounts }}'/>
      {% endif %}
    {% endif %}
  `

  static {
    this.init('qrcode-reader',
      !navigator.mediaDevices
        ? `<p>
          ${gettext('QR code reader isn\'t supported on your platform. Please update your browser.')}
      </p>`
        : `
      <style>
        .sync-status {
          border-radius: var(--border-radius);
          grid-area: 1 / 1 / 2 / 2;
          align-self: start;
          justify-self: end;
          grid-template-columns: auto 1fr;
          display: none;
          align-items: center;
          background: white;
          box-shadow: 0 0 10px 3px #ffffff;
          margin: var(--space-medium);
          padding: var(--space-small) var(--space-medium);
          gap: var(--space-small);
        }

        .sync-status.online, .sync-status.offline, .sync-status.error {
          display: grid;
        }

        .sync-online-icon, .sync-offline-icon, .sync-error-icon {
          grid-area: 1 / 2 / 2 / 3;
          width: 22px;
          height: 22px;
          display: none;
          color: var(--color);
          fill: transparent;
          stroke: var(--color);
        }

        .sync-last-update {
          grid-area: 1 / 1 / 2 / 2;
        }

        .sync-online-icon { --color: var(--color-success); }
        .sync-offline-icon { --color: var(--color-loading); }
        .sync-error-icon { --color: var(--color-error); }

        .sync-status.online:not(.error) .sync-online-icon { display: block; }
        .sync-status.offline:not(.error) .sync-offline-icon { display: block; }
        .sync-status.error .sync-error-icon { display: block; }
        .sync-status.error .sync-last-update {
          color: var(--color-error);
          display: block;
        }

        .video-wrapper {
          grid-area: 1 / 1 / 2 / 2;
          width: fit-content;
          height: fit-content;
          display: grid;
          justify-self: center;
          align-self: center;
        }

        .video {
          grid-area: 1 / 1 / 2 / 2;
          justify-self: center;
          align-self: center;
          width: 100%;
          height: auto;
          max-height: 100%;
        }

        .fullscreen .video {
          height: 100vh;
        }

        .fullscreen-button {
          z-index: 1;
          grid-area: 1 / 1 / 2 / 2;
          width: 1.8rem;
          height: 1.8rem;
          align-self: end;
          justify-self: end;

          border: none;
          fill: white;
          margin: 5px;
        }

        .fullscreen .enter-fullscreen-icon { display: none; }
        .exit-fullscreen-icon { display: none; }
        .fullscreen .exit-fullscreen-icon { display: block; }

        .popup {
          grid-area: 1 / 1 / 2 / 2;
          align-self: center;
          justify-self: stretch;
          z-index: 1;
          overflow: hidden;

          --border-color: var(--color-success);

          border: 2px solid var(--border-color);
          border-radius: var(--border-radius);
          margin: 10px;
          padding-bottom: var(--space-medium);
          box-shadow: 0 0 10px 3px #ffffff;
          background: white;

          display: flex;
          flex-direction: column;
        }

        :host(.popup-closed) .popup {
          display: none;
        }

        .error .popup {
          --border-color: var(--color-error);
        }

        .content {
          display: grid;
          grid-template-columns: auto 1fr;
          margin-bottom: var(--space-medium);
        }
      }
      </style>
      <div class="sync-status">
        <svg class="sync-online-icon">
          <use xlink:href="/static/qrcode/img/icons.svg#sync"></use>
        </svg>
        <svg class="sync-offline-icon">
          <use xlink:href="/static/qrcode/img/icons.svg#sync-disabled"></use>
        </svg>
        <svg class="sync-error-icon">
          <use xlink:href="/static/qrcode/img/icons.svg#sync-problem"></use>
        </svg>
        <div class="sync-last-update"></div>
      </div>
      <div class="video-wrapper">
        <video class="video"></video>
        <div class="fullscreen-button">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            class="enter-fullscreen-icon"
          >
            <path
              d="
                M8 3V5H4V9H2V3H8ZM2 21V15H4V19H8V21H2ZM22 21H16V19H20V15H22V21ZM22
                9H20V5H16V3H22V9Z
              ">
            </path>
          </svg>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            class="exit-fullscreen-icon"
          >
            <path
              d="
                M18 7H22V9H16V3H18V7ZM8 9H2V7H6V3H8V9ZM18 17V21H16V15H22V17H18ZM8
                15V21H6V17H2V15H8Z
              ">
            </path>
          </svg>
        </div>
      </div>
      <div class="popup">
        <div class="content"></div>
        <qr-button class="close-popup-button">${gettext('Close')}</qr-button>
      </div>
    `)
  }

  #contentTemplate
  #context

  constructor () {
    super()

    if (!navigator.mediaDevices) {
      return
    }

    this.classList.add('popup-closed')
    const closePopupButton = this.shadowRoot.querySelector('.close-popup-button')
    closePopupButton.addEventListener('click', () => {
      this.#context = undefined
      this.classList.add('popup-closed')
    })

    const fullScreenButton = this.shadowRoot.querySelector('.fullscreen-button')
    fullScreenButton.addEventListener('click', () => {
      this.#toggleFullScreen()
    })

    this.addEventListener('fullscreenchange', () => {
      this.#onFullScreenChanged()
    })

    this.shadowRoot.addEventListener('fetch-metadata', this.#fetchMetadata.bind(this))
  }

  connectedCallback () {
    const key = this.getAttribute('verify-key')
    if (!navigator.mediaDevices || !key) {
      return
    }

    const contentTemplate = this.innerHTML.trim() || this.constructor.defaultContentTemplate
    this.#contentTemplate = nunjucks.compile(
      contentTemplate,
      (new nunjucks.Environment())
        .addFilter('formatDate', (timestamp) => {
          if (!timestamp) {
            return undefined
          }
          const date = new Date(timestamp * 1000)
          return date.toLocaleString()
        })
        .addGlobal('now', () => (new Date()).getTime() / 1000)
        .addGlobal('gettext', gettext)
      ,
    )

    this.#startServiceWorker()

    const videoElement = this.shadowRoot.querySelector('.video')
    scanCertificates(
      videoElement,
      key,
      async (result) => {
        this.classList.remove('popup-closed')

        if (this.#context && this.#context.uuid === result.uuid) {
          return
        }

        this.#context = result

        const validityStart = this.#context.validity && this.#context.validity.start
        const validityEnd = this.#context.validity && this.#context.validity.end
        const now = (new Date()).getTime() / 1000
        if (this.#context.uuid
          && (!validityStart || now >= validityStart)
          && (!validityEnd || now <= validityEnd)
        ) {
          this.#context.tallyUrl = this.getAttribute('tally-url')
        } else {
          this.#context.tallyUrl = undefined
        }

        this.#context.accounts = this.getAttribute('accounts')
        this.#render()

        await Promise.all([this.#fetchMetadata()])
      },
    )
  }

  async #startServiceWorker () {
    if (!navigator.serviceWorker) {
      return
    }

    const tallyUrl = this.getAttribute('tally-url')
    const serviceWorker = this.getAttribute('service-worker')
    if (!tallyUrl || !serviceWorker) {
      return
    }

    const refreshTally = () => {
      if (document.hidden === false) {
        registration.active.postMessage({refreshTally: tallyUrl})
      }
    }

    navigator.serviceWorker.register(serviceWorker, { scope: '/' })

    navigator.serviceWorker.addEventListener(
      'message',
      this.#onServiceWorkerMessage.bind(this),
    )

    const registration = await navigator.serviceWorker.ready

    refreshTally()
    setInterval(refreshTally, 10000)
  }

  #onServiceWorkerMessage (message) {
    const syncStatus = message.data.syncStatus
    if (syncStatus) {
      const syncStatusElement = this.shadowRoot.querySelector('.sync-status')
      syncStatusElement.classList.toggle('error', syncStatus.error === true)
      syncStatusElement.classList.toggle('online', syncStatus.offline === false)
      syncStatusElement.classList.toggle('offline', syncStatus.offline === true)

      const lastUpdateElement = this.shadowRoot.querySelector('.sync-last-update')
      if (syncStatus.lastUpdate === 0) {
        lastUpdateElement.innerText = gettext('Syncing tally database')
        return
      }

      const delta = Math.floor((new Date().getTime() / 1000) - syncStatus.lastUpdate)
      if (delta === 0) {
        lastUpdateElement.innerText = gettext('Up to date')
        return
      }

      const seconds = delta % 60
      const minutes = ((delta - seconds) % 3600) / 60
      const hours = (delta - minutes * 60 - seconds) / 3600
      const format = (number) => (number).toLocaleString(undefined, {minimumIntegerDigits: 2})
      lastUpdateElement.innerText = `${format(hours)}:${format(minutes)}:${format(seconds)}`
    }
  }

  #render () {
    const contentElement = this.shadowRoot.querySelector('.content')
    contentElement.innerHTML = this.#contentTemplate.render(this.#context)
  }

  async #fetchMetadata () {
    const metadataUrl = this.getAttribute('metadata-url')
    if (!metadataUrl || !this.#context.uuid) {
      return
    }

    const metadata = this.#context.metadata = { loading: true }
    this.#render()
    try {
      Object.assign(
        metadata,
        await fetchMetadata(this.#context.uuid, metadataUrl),
      )
    } finally {
      metadata.loading = false
      this.#render()
    }
  }

  #toggleFullScreen () {
    if (document.fullscreenElement) {
      document.exitFullscreen()
    } else {
      this.requestFullscreen()
    }
  }

  #onFullScreenChanged () {
    if (document.fullscreenElement === this) {
      this.classList.add('fullscreen')
    } else {
      this.classList.remove('fullscreen')
    }
  }
}

export class QRTitle extends QRElement {
  static observedAttributes = ['loading', 'error']

  static {
    this.init(
      'qr-title',
      `
        <style>
          :host(.error) {
            --color: var(--color-error);
            background-color: var(--color-error-background);
          }

          :host(.loading) {
            --color: var(--color-loading);
            background-color: var(--color-loading-background);
          }

          :host {
            --color: var(--color-success);

            grid-column: 1 / 3;
            padding: var(--space-small) var(--space-medium);

            display: grid;
            grid-template-columns: auto 1fr;
            gap: var(--space-xsmall);
            align-items: center;

            background: var(--color-success-background);
            font-weight: bold;
            color: var(--color);
            stroke: var(--color);
            fill: var(--color);
          }

          :host(:not(:first-child)) {
            margin-top: var(--space-large);
          }

          .spinner, .ok-icon, .error-icon {
            grid-area: 1 / 1 / 2 / 2;
          }

          .spinner, .error-icon {
            display: none;
          }

          @keyframes spinner-keyframes {
            100% { transform:rotate(360deg); }
          }

          .spinner-animation {
            transform-origin:center;
            animation:spinner-keyframes .75s infinite linear;
          }

          :host(.error) .ok-icon, :host(.loading) .ok-icon { display: none; }
          :host(.error) .error-icon { display: block; }
          :host(.loading) .spinner { display: block; }

          .content {
            font-size: 1.2rem;
            grid-area: 1 / 2 / 2 / 3;
          }
        </style>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          width="24"
          height="24"
          class="spinner"
        >
          <path
            d="
              M12,1A11,11,0,1,0,23,12,11,11,0,0,0,12,1Zm0,19a8,8,0,1,1,8-8A8,8,0,
              0,1,12,20Z
            "
            opacity=".25"
          />
          <path
            d="
              M10.14,1.16a11,11,0,0,0-9,8.92A1.59,1.59,0,0,0,2.46,12,1.52,1.52,0,
              0,0,4.11,10.7a8,8,0,0,1,6.66-6.61A1.42,1.42,0,0,0,12,2.69h0A1.57,
              1.57,0,0,0,10.14,1.16Z
            "
            class="spinner-animation"
          />
        </svg>
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="24"
            height="24"
            class="ok-icon"
        >
          <path
            d="
              M12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2
              22 6.47715 22 12C22 17.5228 17.5228 22 12 22ZM12 20C16.4183 20 20
              16.4183 20 12C20 7.58172 16.4183 4 12 4C7.58172 4 4 7.58172 4 12C4
              16.4183 7.58172 20 12 20ZM11.0026 16L6.75999 11.7574L8.17421
              10.3431L11.0026 13.1716L16.6595 7.51472L18.0737 8.92893L11.0026 16Z
            "
          />
        </svg>
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            class="error-icon">
        >
          <path
            d="
              M12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228
              2 22 6.47715 22 12C22 17.5228 17.5228 22 12 22ZM12 20C16.4183 20
              20 16.4183 20 12C20 7.58172 16.4183 4 12 4C7.58172 4 4 7.58172 4
              12C4 16.4183 7.58172 20 12 20ZM11 15H13V17H11V15ZM11 7H13V13H11V7Z
            "
          />
        </svg>
        <div class="content">
          <slot />
        </div>
      </div>
    `)
  }

  attributeChangedCallback (name, _, newValue) {
    if (name === 'loading' || name === 'error') {
      if (newValue && newValue.toLowerCase() !== 'false') {
        this.classList.add(name)
      } else {
        this.classList.remove(name)
      }
    }
  }
}

export class QRLabel extends QRElement {
  static {
    this.init(
      'qr-label',
      `
        <style>
          :host {
            font-weight: bold;
            grid-column: 1 / 2;
            padding: 0 var(--space-small) 0 0;
            margin-left: var(--space-medium);
          }
        </style>
        <slot />
      `,
    )
  }
}

export class QRValue extends QRElement {
  static {
    this.init(
      'qr-value',
      `
        <style>
          :host {
            grid-column: 2 / 3;
            padding: 0 var(--space-small);
          }
        </style>
        <slot />
      `,
    )
  }
}

export class QRError extends QRElement {
  static {
    this.init(
      'qr-error',
      `
        <style>
          :host {
            grid-column: 1 / 3;
            color: var(--color-error);
            font-weight: bold;
            padding: var(--space-small) var(--space-medium);
          }
        </style>
        <slot />
      `,
    )
  }

  connectedCallback () {
    let previousTitle = this.previousSibling
    while (previousTitle && !(previousTitle instanceof QRTitle)) {
      previousTitle = previousTitle.previousSibling
    }

    if (previousTitle) {
      previousTitle.setAttribute('error', 'true')
    }

    const reader = this.closest('qrcode-reader')
    if (reader) {
      reader.classList.add('error')
    }
  }
}

export class QRButton extends QRElement {
  static {
    this.init(
      'qr-button',
      `
        <style>
          :host {
            grid-column: 1 / 3;
            padding: var(--space-small) var(--space-medium);
            display: flex
          }

          button {
            padding: 5px;
            font-size: 1.2rem;
            flex-grow: 1;
          }
        </style>
        <button>
          <slot />
        </button>
      `,
    )
  }

  constructor () {
    super()
    this.addEventListener('click', this.#onClick.bind(this))
  }

  #onClick (evt) {
    const action = this.getAttribute('action')
    switch (action) {
    case 'fetch-metadata':
      evt.preventDefault()
      this.dispatchEvent(new CustomEvent('fetch-metadata', { bubbles: true}))
    }
  }
}

export class QRCodeTallyAccount extends QRElement {
  static observedAttributes = [
    'account-id',
    'account-label',
    'account-debit',
    'account-credit',
    'account-revoked',
  ]

  static {
    this.init('qr-tally-account', `
      <style>
        button {
          --color: black;
          display: flex;
          align-items: center;
          justify-content: center;
          padding: var(--space-small);
        }

        .icon {
          width: 22px;
          height: 22px;
          stroke: var(--color);
          fill: var(--color);
        }

        #content {
          display: grid;
          grid-template-columns: 1fr auto 30px auto;
          padding: var(--space-small) var(--space-medium);
        }

        #status-wrapper {
          color: var(--color);
          display: grid;
          grid-template-columns: auto 1fr;
          align-items: center;
        }

        #error-icon, #success-icon {
          grid-area: 1 / 1 / 3 / 2;
          display: none;
          margin-right: var(--space-small);
        }

        #label {
          display: flex;
          align-items: center;
          grid-area: 1 / 2 / 2 / 3;
        }

        #status-text {
          display: none;
          grid-area: 2 / 2 / 3 / 3;
          font-size: 0.9em;
        }

        #debit, #credit {
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 1.2rem;
        }

        :host(.enabled) #content {
          background: var(--color-selected-background);
        }

        :host(.enabled) #label {
          font-weight: bold;
        }

        :host(.error) #error-icon,
        :host(.success) #success-icon,
        :host(.error) #status-text,
        :host(.success) #status-text {
          display: block;
        }

        :host(.error) #content {
          --color: var(--color-error);
          background: var(--color-error-background);
        }

        :host(.success) #content {
          --color: var(--color-success);
          background: var(--color-success-background);
        }

        :host(.tallied) #add-debit-button,
        :host(.tallied) #remove-debit-button {
          display: none;
        }

        :host(.auto-tally) #remove-debit-button,
        :host(.auto-tally) #debit,
        :host(.auto-tally) #add-debit-button {
          display: none
        }
      </style>
      <div id='content'>
        <div id='status-wrapper'>
          <svg id='error-icon' class='icon'>
            <use xlink:href="/static/qrcode/img/icons.svg#forbid"></use>
          </svg>
          <svg id='success-icon' class='icon'>
            <use xlink:href="/static/qrcode/img/icons.svg#checkbox-circle"></use>
          </svg>
          <div id='label'></div>
          <div id='status-text'>
          </div>
        </div>
        <button id='remove-debit-button' disabled>
          <svg class='icon'>
            <use xlink:href="/static/qrcode/img/icons.svg#substract"></use>
          </svg>
        </button>
        <div id='debit'>0</div>
        <button id='add-debit-button'>
          <svg class='icon'>
            <use xlink:href="/static/qrcode/img/icons.svg#add"></use>
          </svg>
        </button>
      </div>
    `)
  }

  #labelElement = this.shadowRoot.querySelector('#label')
  #statusElement = this.shadowRoot.querySelector('#status-text')
  #debitElement = this.shadowRoot.querySelector('#debit')
  #addDebitButton = this.shadowRoot.querySelector('#add-debit-button')
  #removeDebitButton = this.shadowRoot.querySelector('#remove-debit-button')

  get accountId () { return this.getAttribute('account-id') }
  get debit () { return parseInt(this.getAttribute('account-debit') || '0') }
  set debit (value) { this.setAttribute('account-debit', value) }

  constructor () {
    super()
    this.#removeDebitButton.addEventListener('click', () => { this.debit -= 1 })
    this.#addDebitButton.addEventListener('click', () => { this.debit += 1 })
  }

  attributeChangedCallback (name, _, newValue) {
    switch (name) {
    case 'account-label':
      if (this.accountId === '_default' && !newValue) {
        newValue = gettext('Main account')
      }
      this.shadowRoot.querySelector('#label').innerHTML = newValue
      break
    case 'account-debit':
      this.classList.remove('error')
      this.classList.remove('success')
      if (parseInt(newValue) === 0) {
        this.classList.remove('enabled')
        this.#removeDebitButton.disabled = true
      } else {
        this.classList.add('enabled')
        this.#removeDebitButton.disabled = false
      }
      this.#debitElement.innerText = newValue
      this.dispatchEvent(new CustomEvent('debit-changed', { bubbles: true }))
      break
    case 'account-credit':
      this.#updateStatus()
      break
    case 'account-revoked':
      this.#addDebitButton.disabled = this.#revoked
      this.#removeDebitButton.disabled = this.#revoked
      if (this.#revoked) {
        this.setAttribute('account-debit', '0')
      }
      this.#updateStatus()
      break
    }
  }

  get #credit () { return parseInt(this.getAttribute('account-credit') || '0') }
  get #revoked () { return this.getAttribute('account-revoked') === 'true' }

  #updateStatus () {
    this.classList.remove('error')
    this.classList.remove('success')
    this.#statusElement.innerText = ''

    if (this.#revoked) {
      this.classList.add('error')
      this.#statusElement.innerText = gettext('Account revoked')
    } else if (this.#credit < 0) {
      this.classList.add('error')
      this.#statusElement.innerText = gettext('Credit: ') + `${this.#credit + this.debit}`
    } else {
      this.classList.add('success')
      this.#statusElement.innerText = gettext('Credit: ') + `${this.#credit}`
    }
  }
}

export class QRTallier extends QRElement {
  static observedAttributes = [
    'tally-url',
    'certificate-uuid',
    'accounts',
  ]

  static {
    this.init('qr-tallier', `
      <style>
        :host {
          grid-column: 1 / 3;
          display: flex;
          flex-direction: column;
        }

        button {
          padding: 5px;
          margin: var(--space-small) var(--space-medium);
          font-size: 1.2rem;
          flex-grow: 1;
        }

        #accounts-container {
          max-height: 30vh;
          overflow: scroll;
          border-bottom: 1px solid black;
          border-top: 1px solid black;
        }

        #global-error {
          color: var(--color-error);
          font-weight: bold;
          padding: var(--space-small) var(--space-medium);
        }
      </style>
      <qr-title id='title'>` + gettext('Tallying') + `</qr-title>
      <div id="accounts-container"></div>
      <div id='global-error' hidden></div>
      <button id='tally-button'>` + gettext('Tally') + `</button>
    `)
  }

  #titleElement = this.shadowRoot.querySelector('#title')
  #globalErrorElement = this.shadowRoot.querySelector('#global-error')
  #accountsContainer = this.shadowRoot.querySelector('#accounts-container')
  #tallyButton = this.shadowRoot.querySelector('#tally-button')

  get certificateUUID () { return this.getAttribute('certificate-uuid') }
  get tallyUrl () { return this.getAttribute('tally-url') }

  constructor () {
    super()
    this.#tallyButton.addEventListener('click', this.#tally.bind(this))
  }

  attributeChangedCallback (name, _, newValue) {
    switch (name) {
    case 'tally-url':
    case 'certificate-uuid':
      this.hidden = this.certificateUUID && this.tallyUrl
      break
    case 'accounts':
      this.#setAccounts(newValue ? JSON.parse(newValue) : {'_default': ''})
      break
    }
  }

  #setAccounts (accounts) {
    this.#accountsContainer.innerHTML = ''

    for (const [accountId, label] of Object.entries(accounts)) {
      const accountElement = document.createElement('qr-tally-account')
      accountElement.setAttribute('account-id', accountId)
      accountElement.setAttribute('account-label', label)
      accountElement.addEventListener('debit-changed', this.#accountDebitChanged.bind(this))
      this.#accountsContainer.append(accountElement)
    }

    if (this.#accountElements.length === 1) {
      this.#accountElements[0].setAttribute('account-debit', 1)
      this.#accountElements[0].classList.add('auto-tally')
    }
  }

  get #accountElements () {
    return [...this.#accountsContainer.querySelectorAll('qr-tally-account')]
  }

  get #accountDebits () {
    return this.#accountElements.reduce(
      (result, element) => ({...result, [element.accountId]: element.debit }),
      {},
    )
  }

  async #tally () {
    const tallyUrl = this.getAttribute('tally-url')
    const certificateUUID = this.getAttribute('certificate-uuid')

    this.#titleElement.setAttribute('loading', true)
    this.#titleElement.removeAttribute('error')
    this.#globalErrorElement.hidden = true
    this.disabled = true

    try {
      const { error, accounts } = await tally(certificateUUID, tallyUrl, this.#accountDebits)

      let invalidTally = false
      if (accounts) {
        for (const accountElement of this.#accountElements) {
          const accountState = accounts[accountElement.accountId]
          if (accountState) {
            accountElement.setAttribute('account-credit', accountState.credit)
            accountElement.setAttribute('account-revoked', accountState.revoked === true)
            invalidTally |= accountState.credit < 0 || accountState.revoked
          }
        }

        if (!invalidTally) {
          for (const accountElement of this.#accountElements) {
            if (!accounts[accountElement.accountId]) {
              accountElement.hidden = true
            } else {
              accountElement.classList.add('tallied')
            }
          }
          this.#tallyButton.hidden = true
        }
      }

      if (invalidTally || error) {
        this.#globalErrorElement.innerText = error ?? gettext('Invalid tally')
        this.#globalErrorElement.hidden = false
        this.#titleElement.setAttribute('error', true)
        this.#tallyButton.innerText = gettext('Retry')
      }
    } finally {
      this.#titleElement.removeAttribute('loading')
      this.disabled = false
    }
  }

  #accountDebitChanged () {
    const hasPositiveDebit = Object.values(this.#accountDebits).some(debit => debit !== 0)
    this.#tallyButton.disabled = !hasPositiveDebit
  }
}
