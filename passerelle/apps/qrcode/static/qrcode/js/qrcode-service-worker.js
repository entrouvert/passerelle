/* global clients */
const _EVENTS = 'events'
const _CERTIFICATES = 'certificates'

const tallyUrls = new Set()

self.addEventListener('activate', async () => {
  console.log('Activating QR code service worker')
  clients.claim()
})

self.addEventListener('message', async (event) => {
  try {
    const tallyUrl = event.data.refreshTally
    if (tallyUrl) {
      tallyUrls.add(tallyUrl)
      await refreshTally(tallyUrl)
    }
  } catch (error) {
    console.log(error)
  }
})

self.addEventListener('fetch', (event) => {
  if (tallyUrls.has(event.request.url)) {
    event.respondWith(tally(event.request))
  }
})

let DATABASE = null

const openDbRequest = indexedDB.open('QrCodeReader', 2)

openDbRequest.onupgradeneeded = (event) => {
  console.log('Upgrading database')
  const oldVersion = isNaN(event.oldVersion) ? 0 : event.oldVersion

  if (oldVersion < 1) {
    const db = openDbRequest.result
    const eventStore = db.createObjectStore(_EVENTS, { keyPath: 'uuid'})
    eventStore.createIndex('certificate', 'certificate', { unique: false })

    const certificateStore = db.createObjectStore(_CERTIFICATES, { keyPath: 'uuid' })
    certificateStore.createIndex('uuid', 'uuid', { unique: true })
  }

  if (oldVersion < 2) {
    const transaction = event.target.transaction
    const certificateStore = transaction.objectStore(_CERTIFICATES)
    certificateStore.createIndex('updateTimestamp', 'updateTimestamp', { unique: false })
  }
}

openDbRequest.onsuccess = (event) => {
  DATABASE = event.target.result
}

openDbRequest.onerror = () => {
  console.log(`An error occured while opening the indexed DB : ${openDbRequest.error.message}`)
}

async function tally (request) {
  const json = await request.clone().json()
  try {
    const certificates = await addEvents(json.events)
    return new Response(JSON.stringify({
      err: 0,
      data: {
        timestamp: await getLastUpdateTimestamp(),
        certificates,
      },
    }))
  } catch {
    return await fetch(request)
  }
}

async function postSyncStatus ({offline, error, lastUpdate}) {
  for (const client of await clients.matchAll()) {
    await client.postMessage({
      syncStatus: {
        lastUpdate,
        offline,
        error,
      },
    })
  }
}

async function refreshTally (url) {
  console.info(`Refreshing tally from ${url}`)
  const lastUpdate = await getLastUpdateTimestamp()
  try {
    const tallyEvents = await getPendingEvents()
    let response
    try {
      response = await fetch(
        url,
        {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            since: lastUpdate,
            asynchronous: true,
            events: tallyEvents,
          }),
        },
      )
    } catch {
      await postSyncStatus({ offline: true, lastUpdate })
      return
    }

    if (!response.ok) {
      console.log(`Error while refreshing tally : ${response.status} (${response.statusText})`)
      await postSyncStatus({ error: true, lastUpdate})
      return
    }

    const eventsToFlush = [...tallyEvents.map(evt => evt.uuid)]
    const json = await response.json()
    const data = json.data

    for (const [uuid, certificate] of Object.entries(data.certificates)) {
      await saveCertificate({uuid, ...certificate, updateTimestamp: data.timestamp}, eventsToFlush)
    }

    await postSyncStatus({ offline: false, lastUpdate: data.timestamp })
  } catch (error) {
    await postSyncStatus({ error: true, lastUpdate })
    throw error
  }
}

function addEvents (tallyEvents) {
  return new Promise((resolve, reject) => {
    const transaction = DATABASE.transaction([_EVENTS, _CERTIFICATES], 'readwrite')
    const tallyEventStore = transaction.objectStore(_EVENTS)
    const tallyEventIndex = tallyEventStore.index('certificate')
    const certificateStore = transaction.objectStore(_CERTIFICATES)

    const eventsIterator = tallyEvents.values()
    let invalidTally = false
    const result = {}

    function abortTransaction (error) {
      transaction.abort()
      reject(error)
    }

    function addNextEvent () {
      const {value, done } = eventsIterator.next()

      if (done) {
        if (invalidTally) {
          transaction.abort()
        }

        resolve(result)
        return
      }

      const tallyEvent = value
      const certificateUUID = value.certificate
      const certificateRequest = certificateStore.get(certificateUUID)

      certificateRequest.onerror = () => abortTransaction(certificateRequest.error)
      certificateRequest.onsuccess = () => {
        const certificate = certificateRequest.result || { credit: 0 }
        let credit = (certificate.credit || 0) + (tallyEvent.credit || -1)

        const pendingEventsRequest = tallyEventIndex.openCursor(certificateUUID)
        pendingEventsRequest.onerror = () => abortTransaction(pendingEventsRequest.error)
        pendingEventsRequest.onsuccess = (event) => {
          const cursor = event.target.result
          if (cursor) {
            credit += cursor.value.credit || -1
            cursor.continue()
            return
          }

          result[certificateUUID] = {...certificate, credit}
          if (credit < 0 || certificate.revoked) {
            invalidTally = true
            addNextEvent()
          } else {
            const addEventRequest = tallyEventStore.add(tallyEvent)

            addEventRequest.onerror = () => abortTransaction(addEventRequest.error)
            addEventRequest.onsuccess = () => addNextEvent()
          }
        }
      }
    }

    addNextEvent()
  })
}

function getPendingEvents () {
  return new Promise((resolve, reject) => {
    // It is important to create the transaction inside the promise : if
    // execution is given back to the event loop and a transaction has no
    // pending request, the transaction is closed, making following calls fail
    // with InactiveTransactionError.
    const transaction = DATABASE.transaction([_EVENTS], 'readonly')
    const tallyEventStore = transaction.objectStore(_EVENTS)

    // If a stored event was received from the API, it will not have a
    // "pending" field. Even if it was added locally with a "pending" field set
    // to 1, refreshTally will overwrite it with a new event object without
    // that field.
    //
    // IndexedDB indices only return objects that have a value for the field
    // they index, so all that we have to do here is to get all events in the
    // "pending" index : they are those added locally and not yet synchronized
    // with the backend.
    const request = tallyEventStore.getAll()
    request.onsuccess = () => resolve(request.result)
    request.onerror = () => reject(request.error)
  })
}

function saveCertificate (certificate, eventsToFlush) {
  return new Promise((resolve, reject) => {
    const transaction = DATABASE.transaction([_CERTIFICATES, _EVENTS], 'readwrite')
    const certificateStore = transaction.objectStore(_CERTIFICATES)
    const tallyEventStore = transaction.objectStore(_EVENTS)
    const tallyEventIndex = tallyEventStore.index('certificate')
    const saveCertificateRequest = certificateStore.put(certificate)

    saveCertificateRequest.onerror = () => reject(saveCertificateRequest.error)
    saveCertificateRequest.onsuccess = () => {
      const flushEventsRequest = tallyEventIndex.openCursor(certificate.uuid)

      flushEventsRequest.onerror = () => reject(flushEventsRequest.error)
      flushEventsRequest.onsuccess = (event) => {
        const cursor = event.target.result
        if (!cursor) {
          resolve()
          return
        }

        if (eventsToFlush.includes(cursor.value.uuid)) {
          cursor.delete()
        }

        cursor.continue()
      }
    }
  })
}

function getLastUpdateTimestamp () {
  return new Promise((resolve, reject) => {
    const transaction = DATABASE.transaction([_CERTIFICATES], 'readonly')
    const certificateStore = transaction.objectStore(_CERTIFICATES)
    const updateTimestampIndex = certificateStore.index('updateTimestamp')
    const cursorRequest = updateTimestampIndex.openCursor(null, 'prev')

    cursorRequest.onerror = () => reject(cursorRequest.error)
    cursorRequest.onsuccess = (event) => {
      const cursor = event.target.result
      if (!cursor) {
        resolve(0)
      }
      resolve(cursor.value.updateTimestamp)
    }
  })
}
