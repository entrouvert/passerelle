from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0002_auto_20151009_0326'),
        ('contenttypes', '0001_initial'),
        ('clicrdv', '0003_auto_20160920_0903'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewClicRdv',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default='NOTSET',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            ('NOTSET', 'NOTSET'),
                            ('DEBUG', 'DEBUG'),
                            ('INFO', 'INFO'),
                            ('WARNING', 'WARNING'),
                            ('ERROR', 'ERROR'),
                            ('CRITICAL', 'CRITICAL'),
                            ('FATAL', 'FATAL'),
                        ],
                    ),
                ),
                (
                    'server',
                    models.CharField(
                        default='sandbox.clicrdv.com',
                        max_length=64,
                        choices=[
                            ('www.clicrdv.com', 'Production (www.clicrdv.com)'),
                            ('sandbox.clicrdv.com', 'SandBox (sandbox.clicrdv.com)'),
                        ],
                    ),
                ),
                ('group_id', models.IntegerField(default=0)),
                ('apikey', models.CharField(max_length=64)),
                ('username', models.CharField(max_length=64)),
                ('password', models.CharField(max_length=64)),
                ('websource', models.CharField(max_length=64, null=True, blank=True)),
                ('default_comment', models.CharField(max_length=250, null=True, blank=True)),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Clicrdv Agenda',
            },
        ),
    ]
