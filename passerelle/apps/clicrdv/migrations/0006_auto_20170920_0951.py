from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('clicrdv', '0005_auto_20161218_1701'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clicrdv',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
