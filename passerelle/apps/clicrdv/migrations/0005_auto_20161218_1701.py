from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('clicrdv', '0004_newclicrdv'),
    ]

    operations = [
        migrations.RenameModel('NewClicRdv', 'ClicRdv'),
    ]
