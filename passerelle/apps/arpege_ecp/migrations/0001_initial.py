from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0006_resourcestatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArpegeECP',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                ('webservice_base_url', models.URLField(verbose_name='Webservice Base URL')),
                ('hawk_auth_id', models.CharField(max_length=64, verbose_name='Hawk Authentication id')),
                ('hawk_auth_key', models.CharField(max_length=64, verbose_name='Hawk Authentication secret')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'Arpege ECP',
            },
        ),
    ]
