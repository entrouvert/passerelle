# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2024 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import urljoin

from django.db import models
from django.utils.dateparse import parse_datetime
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource, HTTPResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError
from passerelle.utils.templates import evaluate_template, validate_template


class Mgdis(BaseResource, HTTPResource):
    base_url = models.URLField(_('Webservice Base URL'), help_text=_('Example: https://test.mgcloud.fr/'))
    tenant_id = models.CharField(_('Tenant identifier'), max_length=128)
    demand_name_template = models.CharField(
        _('Demand name template'), max_length=128, default='{{ text }}', validators=[validate_template]
    )

    category = _('Business Process Connectors')

    class Meta:
        verbose_name = _('MGDIS')

    log_requests_errors = False

    @endpoint(
        description=_('Returns user forms'),
        parameters={
            'NameID': {'description': _('Publik NameID'), 'example_value': 'xyz'},
            'status': {'description': _('Demands status'), 'example_value': 'all'},
            'tiers_id': {'description': _('Tiers identifier')},
        },
    )
    def demands(self, request, NameID, status='all', tiers_id=None):
        url = urljoin(
            self.base_url, 'pda-semi-public-api/api/tenants/%s/gru-publik/mes-demandes' % self.tenant_id
        )
        if not NameID:
            return {'data': []}

        response = self.requests.get(url, params={'suboidc': NameID, 'size': 50})
        if not response.ok:
            raise APIError('Invalid MGDIS response (%s): %s' % (response.status_code, response.text))

        demands = []
        try:
            json_response = response.json()
            if json_response['err']:
                raise APIError('MGDIS error: %s' % json_response['err_desc'])
            data = json_response.get('data', {})

            for tiers in data['tiers']:
                for demand in tiers.pop('demandes', []):
                    if tiers_id is not None and tiers['id'] != tiers_id:
                        continue
                    if status == 'draft' and not demand.get('draft', False):
                        continue
                    if status == 'done' and not demand.get('form_status_is_endpoint', False):
                        continue
                    if (
                        status == 'pending'
                        and demand.get('draft', False)
                        and demand.get('form_status_is_endpoint', False)
                    ):
                        continue

                    demand['tiers'] = tiers
                    demand['status'] = demand['status']['text']
                    demand['name'] = evaluate_template(self.demand_name_template, demand)
                    demand['datetime'] = parse_datetime(demand['datetime']).strftime('%Y-%m-%d %H:%M:%S')
                    demand['form_receipt_datetime'] = demand['datetime']
                    demands.append(demand)

        except ValueError:
            raise APIError(
                'MGDIS error: unparsable response', data={'content': repr(response.content[:1024])}
            )
        return {'data': demands}
