# Generated by Django 3.2.19 on 2024-04-30 16:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('r2p', '0001_inital'),
    ]

    operations = [
        migrations.AddField(
            model_name='r2p',
            name='hide_spi',
            field=models.BooleanField(blank=True, default=True, verbose_name='Hide SPI'),
        ),
    ]
