# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import uuid
from urllib.parse import urljoin

import requests
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.http_authenticators import TokenAuth
from passerelle.utils.jsonresponse import APIError

BASE_PARAMETERS = {
    'prenom': {
        'description': _('First name'),
        'example_value': 'Jean',
    },
    'nom': {
        'description': _('Last name'),
        'example_value': 'Dubignot',
    },
    'sexe': {
        'description': _('Gender (1 = male, 2 = female)'),
        'example_value': '2',
    },
    'naisDate': {
        'description': _('Birthdate (YYYY or MM/YYYY or DD/MM/YYYY)'),
        'example_value': '15/06/1943',
    },
    'naisCodePays': {
        'description': _('Birth country (5 caracters - INSEE)'),
        'example_value': '99100',
    },
    'naisCodeDept': {
        'description': _('Birth department (2 caracters or 3 caracters for TOM and DOM)'),
        'example_value': '75',
    },
    'naisCodeCommune': {
        'description': _('Birth commune (3 caracters or 2 caracters for TOM and DOM - INSEE)'),
        'example_value': '109',
    },
}

PERSONNE_BY_CRITERIA_PARAMETERS = BASE_PARAMETERS.copy()
PERSONNE_BY_CRITERIA_PARAMETERS.update(
    {
        'adrCodePays': {
            'description': _('Adress country (5 caracters - INSEE)'),
            'example_value': '99100',
        },
        'adrCodeDept': {
            'description': _('Adress department (2 caracters or 3 caracters for TOM and DOM)'),
            'example_value': '75',
        },
        'adrCodeCommune': {
            'description': _('Adress commune (3 caracters or 2 caracters for TOM and DOM - INSEE)'),
            'example_value': '109',
        },
        'adrLibelleVoie': {
            'description': _('Adress street'),
            'example_value': 'danton',
        },
        'adrIndiceRepetition': {
            'description': _('Adress repeat index (B = bis, T = ter, Q = quater)'),
            'example_value': 'B',
        },
        'adrNumVoie': {
            'description': _('Adress street number'),
            'example_value': '22',
        },
    }
)


def parse_spi(value):
    value = value.strip().replace(' ', '')
    if not (value and value.isascii() and value.isdigit()):
        raise APIError(_('invalid spi'))
    return value


class R2P(BaseResource):
    api_url = models.URLField(
        _('DGFIP API base URL'),
        max_length=256,
        default='https://gw.dgfip.finances.gouv.fr',
    )
    oauth_username = models.CharField(pgettext_lazy('r2p', 'DGFIP API Username'), max_length=128)
    oauth_password = models.CharField(pgettext_lazy('r2p', 'DGFIP API Password'), max_length=128)
    hide_spi = models.BooleanField(blank=True, default=True, verbose_name=_('Hide SPI'))

    category = _('Business Process Connectors')

    class Meta:
        verbose_name = _('API Recherche des personnes physiques')

    def make_requests_auth(self, session):
        data = {
            'grant_type': 'client_credentials',
            'scope': (
                'api_r2p_recherche_personne_physique api_r2p_recherche_personne_physique '
                'api_r2p_resolution_spi_degrade'
            ),
        }
        return TokenAuth(
            session=session,
            token_url=urljoin(self.api_url, '/token'),
            auth=(self.oauth_username, self.oauth_password),
            data=data,
            cache_delay=300,
        )

    def _call(self, path, params=None):
        url = urljoin(self.api_url, path)
        headers = {
            'X-Correlation-ID': str(uuid.uuid4().hex),
        }
        params = params if params else {}
        try:
            resp = self.requests.get(url=url, headers=headers, params=params)
        except (requests.Timeout, requests.RequestException) as e:
            raise APIError(str(e))
        try:
            resp.raise_for_status()
        except requests.RequestException as main_exc:
            try:
                err_data = resp.json()
            except (json.JSONDecodeError, requests.exceptions.RequestException):
                err_data = {'response_text': resp.text}
            raise APIError(str(main_exc), data=err_data)

        try:
            json_data = resp.json()
        except (json.JSONDecodeError, requests.exceptions.RequestException) as e:
            raise APIError(str(e))
        if self.hide_spi:
            spi = json_data.get('personnePhysique', {}).get('identifiant', {}).get('spi', '')
            if spi:
                json_data['personnePhysique']['identifiant']['spi'] = 'ANONYMIZED'
        return json_data

    @endpoint(
        name='personne-by-criteria',
        description=_('Search natural person by criteria'),
        parameters=PERSONNE_BY_CRITERIA_PARAMETERS,
    )
    def personne_by_criteria(self, request, **kwargs):
        return {'data': self._call('/r2p/v1/personne', params=kwargs)}

    @endpoint(
        name='personne-by-spi',
        description=_('Search natural person by SPI'),
        parameters={
            'spi': {
                'description': _('Tax number of the person'),
                'example_value': '1257553562447',
            },
        },
    )
    def personne_by_spi(self, request, spi):
        spi = parse_spi(spi)
        return {'data': self._call(f'/r2p/v1/personne/{spi}')}

    @endpoint(name='spi-by-criteria', description=_('Search SPI by criteria'), parameters=BASE_PARAMETERS)
    def spi_by_criteria(self, request, **kwargs):
        return {'data': self._call('/r2p/v1/personne/spi', params=kwargs)}
