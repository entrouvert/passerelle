# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import feedparser
from django.db import models
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint


class Feed(BaseResource):
    url = models.URLField(_('URL'), max_length=1000)

    category = _('Data Sources')

    class Meta:
        verbose_name = _('Feed')

    @endpoint(description=_('Feed'))
    def json(self, request):
        response = self.requests.get(self.url)
        response.raise_for_status()
        feed = feedparser.parse(response.content)
        return {'data': feed}

    def check_status(self):
        if not self.url:
            return
        feed = self.json(None)
        if not feed['data'].get('feed'):
            raise Exception('empty feed')
