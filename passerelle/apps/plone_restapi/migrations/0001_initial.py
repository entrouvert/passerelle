# Generated by Django 2.2.19 on 2021-10-15 10:15

import django.db.models.deletion
from django.db import migrations, models

import passerelle.utils.templates


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0029_auto_20210202_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='PloneRestApi',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'service_url',
                    models.CharField(
                        help_text='ex: https://demo.plone.org', max_length=256, verbose_name='Site URL'
                    ),
                ),
                (
                    'token_ws_url',
                    models.CharField(
                        blank=True,
                        help_text='ex: https://IDP/idp/oidc/token/ (empty for anonymous or basic auth access)',
                        max_length=256,
                        verbose_name='Token webservice URL',
                    ),
                ),
                (
                    'client_id',
                    models.CharField(
                        blank=True,
                        help_text='OIDC id of the connector',
                        max_length=128,
                        verbose_name='OIDC id',
                    ),
                ),
                (
                    'client_secret',
                    models.CharField(
                        blank=True,
                        help_text='Share secret secret for webservice call authentication',
                        max_length=128,
                        verbose_name='Shared secret',
                    ),
                ),
                ('username', models.CharField(blank=True, max_length=128, verbose_name='Username')),
                ('password', models.CharField(blank=True, max_length=128, verbose_name='Password')),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Plone REST API Web Service',
            },
        ),
        migrations.CreateModel(
            name='Query',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('slug', models.SlugField(max_length=128, verbose_name='Slug')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                (
                    'uri',
                    models.CharField(
                        blank=True, help_text='uri to query', max_length=128, verbose_name='Uri'
                    ),
                ),
                (
                    'text_template',
                    models.TextField(
                        blank=True,
                        help_text="Use Django's template syntax. Attributes can be accessed through {{ attributes.name }}",
                        validators=[passerelle.utils.templates.validate_template],
                        verbose_name='Text template',
                    ),
                ),
                (
                    'filter_expression',
                    models.TextField(
                        blank=True,
                        help_text='Specify more URL parameters (key=value) separated by lines',
                        verbose_name='filter',
                    ),
                ),
                (
                    'sort',
                    models.CharField(
                        blank=True,
                        help_text='Sorts results by the specified field',
                        max_length=256,
                        verbose_name='Sort field',
                    ),
                ),
                (
                    'order',
                    models.BooleanField(
                        default=True,
                        help_text='Unset to use descending sort order',
                        verbose_name='Ascending sort order',
                    ),
                ),
                (
                    'limit',
                    models.PositiveIntegerField(
                        default=10,
                        help_text='Number of results to return in a single call',
                        verbose_name='Limit',
                    ),
                ),
                (
                    'resource',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='queries',
                        to='plone_restapi.PloneRestApi',
                        verbose_name='Resource',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Query',
                'ordering': ['name'],
                'abstract': False,
                'unique_together': {('resource', 'name'), ('resource', 'slug')},
            },
        ),
    ]
