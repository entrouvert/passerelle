# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import CreateView, DeleteView, UpdateView

from passerelle.base.mixins import ResourceChildViewMixin

from . import models
from .forms import QueryForm


class QueryNew(ResourceChildViewMixin, CreateView):
    model = models.Query
    form_class = QueryForm
    template_name = 'passerelle/manage/resource_child_form.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = self.model(resource=self.resource)
        return kwargs


class QueryEdit(ResourceChildViewMixin, UpdateView):
    model = models.Query
    form_class = QueryForm
    template_name = 'passerelle/manage/resource_child_form.html'


class QueryDelete(ResourceChildViewMixin, DeleteView):
    model = models.Query
    template_name = 'passerelle/manage/resource_child_confirm_delete.html'
