from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('oxyd', '0005_oxydsmsgateway_default_trunk_prefix'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oxydsmsgateway',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
