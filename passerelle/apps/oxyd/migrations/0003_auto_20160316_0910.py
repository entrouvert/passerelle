from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('oxyd', '0002_oxydsmsgateway_log_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oxydsmsgateway',
            name='log_level',
            field=models.CharField(
                default=b'NOTSET',
                max_length=10,
                verbose_name='Log Level',
                choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')],
            ),
            preserve_default=True,
        ),
    ]
