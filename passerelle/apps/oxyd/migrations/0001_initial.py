import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OxydSMSGateway',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                ('username', models.CharField(max_length=64, verbose_name='Username')),
                ('password', models.CharField(max_length=64, verbose_name='Password')),
                (
                    'default_country_code',
                    models.CharField(
                        default='33',
                        max_length=3,
                        verbose_name='Default country code',
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[0-9]*$', 'The country must only contain numbers'
                            )
                        ],
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'db_table': 'sms_oxyd',
                'verbose_name': 'Oxyd',
            },
            bases=(models.Model,),
        ),
    ]
