from django.contrib import admin

from .models import OxydSMSGateway


@admin.register(OxydSMSGateway)
class OxydSMSGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['title', 'slug', 'description', 'username', 'password', 'default_country_code']
