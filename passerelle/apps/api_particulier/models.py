# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Gateway to API-Particulier web-service from SGMAP:
https://particulier.api.gouv.fr/
'''

import datetime
import re
from collections import OrderedDict
from urllib import parse

import requests

try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.conversion import exception_to_text
from passerelle.utils.jsonresponse import APIError
from passerelle.utils.validation import is_number

from .known_errors import KNOWN_ERRORS

# regular expression for numero_fiscal and reference_avis
FISCAL_RE = re.compile(r'^[0-9a-zA-Z]{13}$')


class APIParticulier(BaseResource):
    PLATFORMS = [
        {'name': 'prod', 'label': _('Production'), 'url': 'https://particulier.api.gouv.fr/api/'},
        {
            'name': 'test',
            'label': _('Test'),
            'url': 'https://staging.particulier.api.gouv.fr/api/',
        },
    ]
    PLATFORMS = OrderedDict([(platform['name'], platform) for platform in PLATFORMS])

    platform = models.CharField(
        verbose_name=_('Platform'),
        max_length=8,
        choices=[(key, platform['label']) for key, platform in PLATFORMS.items()],
    )

    api_key = models.CharField(max_length=9999, default='', blank=True, verbose_name=_('API key'))

    log_requests_errors = False

    accessible_scopes = ArrayField(
        models.CharField(max_length=32),
        null=True,
        blank=True,
        editable=False,
    )

    hide_description_fields = ['accessible_scopes']

    @property
    def url(self):
        return self.PLATFORMS[self.platform]['url']

    def get(self, path, **kwargs):
        user = kwargs.pop('user', None)
        url = parse.urljoin(self.url, path)
        headers = {'X-API-KEY': self.api_key}
        if user:
            headers['X-User'] = user
        try:
            response = self.requests.get(url, headers=headers, timeout=5, **kwargs)
        except requests.RequestException as e:
            raise APIError(
                'API-particulier platform "%s" connection error: %s' % (self.platform, exception_to_text(e)),
                log_error=True,
                data={
                    'code': 'connection-error',
                    'platform': self.platform,
                    'error': str(e),
                },
            )
        try:
            data = response.json()
        except JSONDecodeError as e:
            content = repr(response.content[:1000])
            raise APIError(
                'API-particulier platform "%s" returned non-JSON content with status %s: %s'
                % (self.platform, response.status_code, content),
                log_error=True,
                data={
                    'code': 'non-json',
                    'status_code': response.status_code,
                    'exception': str(e),
                    'platform': self.platform,
                    'content': content,
                },
            )
        if response.status_code != 200:
            # avoid logging http errors about non-transport failure
            message = data.get('message', '')
            if message in KNOWN_ERRORS.get(response.status_code, []):
                error_data = {
                    'code': data.get('error', 'api-error').replace('_', '-'),
                    'status_code': response.status_code,
                    'platform': self.platform,
                    'content': data,
                }
                if response.status_code // 100 == 4:
                    # for 40x errors, allow caching of the response, as it should not change with future requests
                    return {
                        'err': 1,
                        'err_desc': message,
                        'data': error_data,
                    }
                raise APIError(message, data=error_data)
            raise APIError(
                'API-particulier platform "%s" returned a non 200 status %s: %s'
                % (self.platform, response.status_code, data),
                log_error=True,
                data={
                    'code': 'non-200',
                    'status_code': response.status_code,
                    'platform': self.platform,
                    'content': data,
                },
            )
        return {
            'err': 0,
            'data': data,
        }

    def get_scopes(self):
        response = self.get('introspect')
        result = sorted(response['data'].get('scopes'))
        return result

    def clean(self):
        try:
            scopes = self.get_scopes()
        except APIError:
            scopes = []
        self.accessible_scopes = [x[:32] for x in scopes]

    def daily(self):
        super().daily()
        try:
            scopes = self.get_scopes()
        except APIError:
            scopes = []
        self.accessible_scopes = [x[:32] for x in scopes]
        self.save()

    @endpoint(
        description=_('Get scopes available'),
        display_order=1,
    )
    def scopes(self, request):
        scopes = self.get_scopes()
        self.accessible_scopes = [x[:32] for x in scopes]
        self.save()
        return {
            'err': 0,
            'data': scopes,
        }

    @endpoint(
        show=False,
        description=_('Get family allowances recipient informations'),
        parameters={
            'code_postal': {
                'description': _('postal code'),
                'example_value': '99148',
            },
            'numero_allocataire': {
                'description': _('recipient identifier'),
                'example_value': '0000354',
            },
            'user': {
                'description': _('requesting user'),
                'example_value': 'John Doe (agent)',
            },
        },
    )
    def caf_famille(self, request, code_postal, numero_allocataire, user=None):
        # deprecated endpoint
        return self.v2_situation_familiale(request, code_postal, numero_allocataire, user=user)

    @endpoint(
        name='situation-familiale',
        description=_('Get family allowances recipient informations'),
        parameters={
            'code_postal': {
                'description': _('postal code'),
                'example_value': '44100',
            },
            'numero_allocataire': {
                'description': _('recipient identifier'),
                'example_value': '4400100',
            },
            'user': {
                'description': _('requesting user'),
                'example_value': 'John Doe (agent)',
            },
        },
    )
    def v2_situation_familiale(self, request, code_postal, numero_allocataire, user=None):
        numero_allocataire = numero_allocataire.strip()[:7]
        code_postal = code_postal.strip()

        if not code_postal or not numero_allocataire:
            raise APIError('missing code_postal or numero_allocataire')

        if len(numero_allocataire) != 7 or not is_number(numero_allocataire):
            raise APIError('numero_allocataire should be a 7 digits number')
        data = self.get(
            'v2/composition-familiale',
            params={
                'codePostal': code_postal,
                'numeroAllocataire': numero_allocataire,
            },
            user=user,
        )
        data['data']['numero_allocataire'] = numero_allocataire
        data['data']['code_postal'] = code_postal
        for kind in 'allocataires', 'enfants':
            for person in data['data'].get(kind) or []:
                if len(person.get('dateDeNaissance') or '') == 8:
                    birthdate = person['dateDeNaissance']
                    person['dateDeNaissance_iso'] = birthdate[4:] + '-' + birthdate[2:4] + '-' + birthdate[:2]
        return data

    @endpoint(
        name='composition-familiale-v2',
        description='Quotient familial MSA & CAF',
        parameters={
            'nomUsage': {'description': 'Nom d\'usage.'},
            'nomNaissance': {
                'description': 'Nom de naissance.  Le champ est obligatoire',
                'example_value': 'ROUX',
            },
            'prenoms': {
                'description': 'Liste des prénoms, séparés par une virgule.',
                'example_value': 'JEANNE,STEPHANIE',
            },
            'anneeDateDeNaissance': {
                'description': 'Année de naissance. Ne pas la renseigner si celle-ci est inconnue.',
                'example_value': '1987',
            },
            'moisDateDeNaissance': {
                'description': 'Mois de naissance. Ne pas le renseigner si celui-ci est '
                'inconnu. Cette valeur est ignorée si anneeDateDeNaissance est '
                'vide.',
                'example_value': '6',
            },
            'jourDateDeNaissance': {
                'description': 'Jour de naissance. Ne pas le renseigner si celui-ci est '
                'inconnu. Cette valeur est ignorée si moisDateDeNaissance ou '
                'anneeDateDeNaissance est vide.',
            },
            'codeInseeLieuDeNaissance': {
                'description': 'Code Insee à 5 chiffres du lieu de naissance de la '
                'personne sur 5 chiffres. Ne pas remplir si la personne est '
                'née à l\'étranger. En l\'absence du triplet '
                '(nomCommuneNaissance, anneeDateDeNaissance, '
                'codeInseeDepartementNaissance), le champ est obligatoire '
                'si la personne est née en France',
                'example_value': '08480',
            },
            'codePaysLieuDeNaissance': {
                'description': 'Code Insee à 5 chiffres du pays de naissance (France = '
                '99100). Obligatoire.',
                'example_value': '99100',
            },
            'sexe': {
                'description': 'Sexe de la personne, masculin ou féminin. (M/F) Obligatoire.',
                'example_value': 'F',
            },
            'nomCommuneNaissance': {
                'description': 'Nom de la commune de naissance. En l\'absence du '
                'codeInseeLieuDeNaissance, ce paramètre est obligatoire si la '
                'personne est née en France',
            },
            'codeInseeDepartementNaissance': {
                'description': 'Code INSEE du département de naissance. En l\'absence '
                'du codeInseeLieuDeNaissance, ce paramètre est '
                'obligatoire si la personne est née en France',
                'example_value': '51',
            },
            'annee': {
                'description': 'Année du quotient familial recherché. Si l\'année n\'est pas renseignée, '
                'l\'année utilisée par défaut est celle en cours. L\'API permet d\'accéder à un '
                'historique de maximum 2 ans.',
                'example_value': '2023',
            },
            'mois': {
                'description': "Mois du quotient familial recherché. Si le mois n'est pas renseigné, le mois "
                'utilisé par défaut est celui en cours.',
                'example_value': '5',
            },
            'user': {
                'description': _('requesting user'),
                'example_value': 'John Doe (agent)',
            },
        },
    )
    def composition_familiale_v2(
        self,
        request,
        nomNaissance,
        codePaysLieuDeNaissance,
        sexe,
        nomUsage=None,
        prenoms=None,
        anneeDateDeNaissance=None,
        moisDateDeNaissance=None,
        jourDateDeNaissance=None,
        codeInseeLieuDeNaissance=None,
        nomCommuneNaissance=None,
        codeInseeDepartementNaissance=None,
        annee=None,
        mois=None,
        user=None,
    ):
        # build params from endpoint_info parameters
        params = {}
        local_vars = locals()
        for key in self.composition_familiale_v2.endpoint_info.parameters:
            value = (local_vars.get(key, '') or '').strip()
            if value:
                params[key] = value
        params.pop('user', None)

        in_france = params.get('codePaysLieuDeNaissance') == '99100'

        required = ('nomCommuneNaissance', 'anneeDateDeNaissance', 'codeInseeDepartementNaissance')
        if in_france:
            if not all([params.get(key) for key in required]):
                if not params.get('codeInseeLieuDeNaissance'):
                    raise APIError(
                        'Those fields are required when codeInseeLieuDeNaissance is not filled: %s'
                        % ', '.join(required)
                    )

        prenoms = params.pop('prenoms', '')
        if prenoms:
            params['prenoms[]'] = [prenom.strip() for prenom in prenoms.split(',') if prenom.strip()]
        data = self.get(
            'v2/composition-familiale-v2',
            params=params,
            user=user,
        )
        return data

    @endpoint(
        name='scolarites',
        description='Statut élève scolarisé et boursier',
        parameters={
            'nom': {'description': "Nom de l'élève", 'example_value': 'Martin'},
            'prenom': {'description': "Prénom de l'élève", 'example_value': 'Justine'},
            'sexe': {
                'description': "Sexe de l'élève, masculin ou féminin. (M/F)",
                'example_value': 'f',
            },
            'dateNaissance': {
                'description': "Date de naissance de l'élève au format AAAA-MM-JJ",
                'example_value': '2000-01-20',
                'type': 'date',
            },
            'codeEtablissement': {
                'description': "Code d'unité administrative immatriculée (code UAI) de "
                "l'établissement où est scolarisé l'élève. Ce code unique inscrit "
                'au répertoire national des établissements est composé de 7 '
                "chiffres et d'une lettre ; les trois premiers chiffres "
                "correspondent au numéro de département de l'établissement.. Pour "
                "retrouver facilement le code UAI d'un établissement à partir d' "
                'informations plus facilement connues des usagers (commune, code '
                'postal, etc.), vous pouvez utiliser l\'API "Annuaire de '
                "l'éducation "
                'nationale https://api.gouv.fr/les-api/api-annuaire-education . '
                "⚠️ L'établissement 0861288H (CNED Direction générale) n'existe "
                "pas dans l'API annuaire de l'éducation nationale, il faudra donc "
                'compléter la liste des établissements avec une ligne CNED à '
                'laquelle sera associée le code UAI qui sera en passé en entrée.',
                'example_value': '0890003V',
            },
            'anneeScolaire': {
                'description': 'Année scolaire recherchée au format AAAA-AAAA ou AAAA. Les '
                "informations connues par l'API concerne surtout l'année en cours et "
                "parfois l'année scolaire à venir. Pour en savoir plus consulter la "
                'fiche métier : '
                'https://particulier.api.gouv.fr/catalogue/education_nationale/statut_eleve_scolarise'
                '#faq_entry_answer_0_api_particulier_endpoint_education_nationale_statut_eleve_scolarise',
                'example_value': '2022',
            },
            'user': {
                'description': _('requesting user'),
                'example_value': 'John Doe (agent)',
            },
        },
    )
    def scolarites(
        self, request, nom, prenom, sexe, dateNaissance, codeEtablissement, anneeScolaire, user=None
    ):
        # build params from endpoint_info parameters
        params = {}
        local_vars = locals()
        for key in self.scolarites.endpoint_info.parameters:
            value = local_vars.get(key) or ''
            if isinstance(value, datetime.date):
                value = value.isoformat()
            if value:
                params[key] = value.strip()
        params.pop('user', None)
        data = self.get(
            'v2/scolarites',
            params=params,
            user=user,
        )
        return data

    category = _('Business Process Connectors')

    class Meta:
        verbose_name = _('API Particulier')
