# from https://github.com/betagouv/api-caf/blob/master/lib/client/errors.json

KNOWN_ERRORS = {
    404: {
        'Absence ressources ou QF à 0 sur la période de référence. Le document ne peut être édité.',
        'Dossier allocataire inexistant. Le document ne peut être édité.',
        "Dossier en cours d'affiliation sur la période de référence. Le document ne peut être édité.",
        'Dossier radié au cours de la période de référence. Le document ne peut être édité.',
        'Dossier radié. Le document ne peut être édité.',
        'Dossier suspendu au cours de la période de référence. Le document ne peut être édité.',
        'Dossier suspendu. Le document ne peut être édité.',
        'Décès MON/MME. Le document ne peut être édité.',
        "Le dossier n'est pas éligible à la réduction sociale téléphonique - Pas de droit AAH ou RSA à la date de situation.",
        'Non droit. Le document ne peut être édité.',
        'Pas de droit sur la période demandée pour la prestation sélectionnée (entre début période et date du jour)',
        'Pas de droit sur la période demandée pour la prestation sélectionnée (entre début période et fin période)',
        'Pas de droit sur la période demandée pour la prestation sélectionnée et le bénéficiaire choisi',
        'Pas de droit sur la période demandée pour la prestation sélectionnée.',
        "Votre quotient familial (Qf) sur cette période est non disponible. Pour plus d'information, contactez-nous.",
        # API particulier error messages not from the source above
        'Les paramètres fournis sont incorrects ou ne correspondent pas à un avis',
        "L'identifiant indiqué n'existe pas, n'est pas connu ou ne comporte aucune information pour cet appel.",
    },
    400: {
        'Absence de code confidentiel. Le document ne peut être édité.',
        'Absence du nom du responsable dossier. Le document ne peut être édité.',
        'Allocataire non radié à la date de situation demandée',
        'Il existe au moins un enfant pour lequel il existe ou il a existé un droit sur le dossier',
        'Il existe au moins un enfant pour lequel il existe un droit sur le dossier et/ou après la date de situation demandée',
        'Il existe au moins un enfant pour lequel il existe un droit sur le dossier et/ou à la période demandée',
        'Il existe des droits pour la prestation sélectionnée sur le dossier et/ou la période demandée',
        'Il existe des droits pour la prestation sélectionnée sur le dossier et/ou la période demandée (après date du jour)',
        'L’opérateurs téléphonique» ne propose pas de raccordement SMS avec un prestataire externe (raccordement avec un numéro court). ',
        # API particulier error messages not from the source above
        "La référence de l'avis n'est pas correctement formatée",
    },
    500: {
        'Les informations souhaitées sont momentanément indisponibles. Merci de renouveler votre demande ultérieurement.',
        'Problème technique avec le service tiersi',
        'Problème technique avec le service trafic',
        "Votre demande n'a pu aboutir car le numéro de téléphone mentionné ne fait pas partie de la liste des numéros autorisés.",
        "Votre demande n'a pu aboutir en raison d'un incident technique lié à l'appel au service IMC. Des paramètres manquent.",
        (
            "Votre demande n'a pu aboutir en raison d'un incident technique lié à l'appel au service IMC. "
            'La taille du message ne doit pas être supérieure à 160 caractères.'
        ),
        (
            "Votre demande n'a pu aboutir en raison d'un incident technique lié à l'appel au service IMC. "
            "Le SMS a été envoyé mais l'acquittement n'a pas été reçu."
        ),
        "Votre demande n'a pu aboutir en raison d'un incident technique momentané. Merci de renouveler votre demande ultérieurement.",
        "Votre demande n'a pu aboutir en raison d'une erreur fonctionnelle lié à l'appel au service IMC.",
        "Votre demande n'a pu aboutir en raison d'une erreur technique lié à l'appel au service IMC.",
        (
            "Votre demande n’a pu aboutir en raison d'un problème technique lié aux données entrantes du webservice. "
            'Merci de renouveler votre demande ultérieurement.'
        ),
    },
}
