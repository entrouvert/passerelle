# Generated by Django 3.2.18 on 2023-12-13 10:33

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('api_particulier', '0006_api_key_length_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apiparticulier',
            name='api_key',
            field=models.CharField(blank=True, default='', max_length=2048, verbose_name='API key'),
        ),
    ]
