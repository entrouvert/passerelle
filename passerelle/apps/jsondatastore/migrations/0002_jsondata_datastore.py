from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('jsondatastore', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsondata',
            name='datastore',
            field=models.ForeignKey(to='jsondatastore.JsonDataStore', null=True, on_delete=models.CASCADE),
        ),
    ]
