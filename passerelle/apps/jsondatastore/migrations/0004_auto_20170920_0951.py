from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('jsondatastore', '0003_auto_20170623_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jsondatastore',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
