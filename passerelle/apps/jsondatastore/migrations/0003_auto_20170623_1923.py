from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('jsondatastore', '0002_jsondata_datastore'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsondata',
            name='text',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AddField(
            model_name='jsondatastore',
            name='text_value_template',
            field=models.CharField(max_length=256, verbose_name='Template for "text" key value', blank=True),
        ),
    ]
