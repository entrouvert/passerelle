from django.contrib import admin

from .models import MobytSMSGateway


@admin.register(MobytSMSGateway)
class MobytSMSGatewayAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ['title', 'slug', 'description', 'username', 'password', 'quality', 'default_country_code']
