from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mobyt', '0003_auto_20160316_0910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobytsmsgateway',
            name='log_level',
            field=models.CharField(
                default=b'NOTSET',
                max_length=10,
                verbose_name='Log Level',
                choices=[
                    (b'NOTSET', b'NOTSET'),
                    (b'DEBUG', b'DEBUG'),
                    (b'INFO', b'INFO'),
                    (b'WARNING', b'WARNING'),
                    (b'ERROR', b'ERROR'),
                    (b'CRITICAL', b'CRITICAL'),
                ],
            ),
            preserve_default=True,
        ),
    ]
