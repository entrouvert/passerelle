from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mobyt', '0005_mobytsmsgateway_default_trunk_prefix'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobytsmsgateway',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
