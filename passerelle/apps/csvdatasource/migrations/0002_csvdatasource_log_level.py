from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='csvdatasource',
            name='log_level',
            field=models.CharField(
                default=b'NOTSET',
                max_length=10,
                verbose_name='Debug Enabled',
                blank=True,
                choices=[(b'DEBUG', b'DEBUG'), (b'INFO', b'INFO')],
            ),
            preserve_default=True,
        ),
    ]
