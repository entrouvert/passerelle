from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0004_auto_20160407_0456'),
    ]

    operations = [
        migrations.AddField(
            model_name='csvdatasource',
            name='_dialect_options',
            field=models.JSONField(default=dict, editable=False),
            preserve_default=True,
        ),
    ]
