from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0009_auto_20161109_0353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='csvdatasource',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
