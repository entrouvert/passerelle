from django.db import migrations, models

import passerelle.apps.csvdatasource.models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0018_text_to_jsonb'),
    ]

    operations = [
        migrations.AlterField(
            model_name='csvdatasource',
            name='csv_file',
            field=models.FileField(
                help_text='Supported file formats: csv, ods, xls, xlsx',
                upload_to=passerelle.apps.csvdatasource.models.upload_to,
                verbose_name='Spreadsheet file',
            ),
        ),
    ]
