from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0005_csvdatasource__dialect_options'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='csvdatasource',
            name='_dialect_options',
        ),
        migrations.AddField(
            model_name='csvdatasource',
            name='_dialect_options',
            field=models.JSONField(null=True, editable=False),
        ),
    ]
