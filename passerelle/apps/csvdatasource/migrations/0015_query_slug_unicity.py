from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0014_query_set_slug'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='query',
            unique_together={('resource', 'slug')},
        ),
    ]
