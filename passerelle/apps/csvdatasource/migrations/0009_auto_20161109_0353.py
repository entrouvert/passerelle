from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('csvdatasource', '0008_auto_20161025_1156'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='csvdatasource',
            options={'verbose_name': 'Spreadsheet File'},
        ),
        migrations.AddField(
            model_name='csvdatasource',
            name='sheet_name',
            field=models.CharField(max_length=150, verbose_name='Sheet name', blank=True),
        ),
    ]
