from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CsvDataSource',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'csv_file',
                    models.FileField(
                        help_text='Supported file formats: csv, ods, xls, xlsx',
                        upload_to=b'csv',
                        verbose_name='Spreadsheet file',
                    ),
                ),
                (
                    'columns_keynames',
                    models.CharField(
                        default=b'id, text',
                        help_text='ex: id,text,data1,data2',
                        max_length=256,
                        verbose_name='Column keynames',
                        blank=True,
                    ),
                ),
                ('skip_header', models.BooleanField(default=False, verbose_name='Skip first line')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'verbose_name': 'CSV File',
            },
            bases=(models.Model,),
        ),
    ]
