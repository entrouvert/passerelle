# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path

from passerelle.apps.csvdatasource import views

management_urlpatterns = [
    re_path(r'^(?P<connector_slug>[\w,-]+)/download/$', views.CsvDownload.as_view(), name='csv-download'),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/queries/new/$', views.NewQueryView.as_view(), name='csv-new-query'
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/queries/(?P<pk>[\w,-]+)/$',
        views.UpdateQueryView.as_view(),
        name='csv-edit-query',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/queries/(?P<pk>[\w,-]+)/delete$',
        views.DeleteQueryView.as_view(),
        name='csv-delete-query',
    ),
]
