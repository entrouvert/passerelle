# Generated by Django 1.11.20 on 2019-10-06 13:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('astregs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='association_label',
            field=models.CharField(max_length=128, null=True),
        )
    ]
