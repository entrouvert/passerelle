from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('okina', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='okina',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
