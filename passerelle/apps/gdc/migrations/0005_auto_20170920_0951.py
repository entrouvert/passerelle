from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('gdc', '0004_auto_20160407_0456'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gdc',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
