# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2024 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from passerelle.forms import GenericConnectorForm


class ProxyConnectorForm(GenericConnectorForm):
    class Meta:
        # form fields ordering (no defaults)
        fields = [
            'slug',
            'title',
            'description',
            'upstream_base_url',
            'auth_type',
            'basic_auth_username',
            'basic_auth_password',
            'oauth2_token_url',
            'oauth2_username',
            'oauth2_password',
            'oauth2_scopes',
            'amazon_cognito_client_id',
            'client_certificate',
            'verify_cert',
            'http_proxy',
            'http_timeout',
            'forced_headers',
            'status_url',
            'status_text',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not getattr(settings, 'PASSERELLE_APP_PROXY_COGNITO_ENABLED', False):
            # hide field and choices when feature is not enabled
            self.fields['auth_type'].widget.choices = [
                item
                for item in self.fields['auth_type'].widget.choices[:]
                if not item[0].startswith('amazon_cognito')
            ]
            del self.fields['amazon_cognito_client_id']

    def clean(self):
        cleaned_data = super().clean()
        required_fields = []
        auth_type = cleaned_data.get('auth_type') or ''

        if auth_type == 'basic_auth':
            err = _('This field is required for basic authentication')
            required_fields = ['basic_auth_username', 'basic_auth_password']
        elif auth_type.startswith(('oauth2', 'amazon_cognito')):
            err = _('This field is required for OAuth2 authentication')
            required_fields = ['oauth2_token_url', 'oauth2_username', 'oauth2_password']
            if auth_type.startswith('amazon_cognito'):
                err = _('This field is required for Amazon Cognito authentication')
                required_fields.append('amazon_cognito_client_id')

        if required_fields:
            for field in required_fields:
                if not cleaned_data.get(field):
                    self.add_error(field, err)
        return cleaned_data
