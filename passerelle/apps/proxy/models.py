# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import parse_qsl

from django.conf import settings
from django.db import models
from django.http.response import HttpResponse
from django.utils.translation import gettext_lazy as _
from requests.auth import HTTPBasicAuth
from requests.exceptions import HTTPError

from passerelle.base.models import BaseResource, HTTPResource, OAuth2Resource
from passerelle.utils import http_authenticators
from passerelle.utils.api import endpoint

from . import forms

PASS_HEADERS_REQUEST = (
    'accept',
    'accept-encoding',
    'accept-language',
    'cookie',
    'user-agent',
)


class Resource(BaseResource, OAuth2Resource, HTTPResource):
    manager_form_base_class = forms.ProxyConnectorForm

    category = _('Misc')

    auth_types = (
        ('basic_auth', _('Basic Authentication')),
        ('oauth2', _('OAuth 2')),
        ('amazon_cognito_access_token', _('Amazon Cognito (AccessToken)')),
        ('amazon_cognito_id_token', _('Amazon Cognito (IdToken)')),
    )

    auth_type = models.CharField(_('Authentication'), choices=auth_types, max_length=32, blank=True)
    amazon_cognito_client_id = models.CharField(_('Amazon cognito ClientId'), max_length=128, blank=True)
    upstream_base_url = models.URLField(_('Upstream Service Base URL'))
    http_timeout = models.PositiveIntegerField(
        _('Timeout on upstream, in seconds. Use 0 for system default (%ss)') % settings.REQUESTS_TIMEOUT,
        default=5,
    )
    forced_headers = models.TextField(
        _('Headers'),
        blank=True,
        help_text=_('Headers to always add (one per line, format "Header-Name: value")'),
    )
    status_url = models.URLField(
        _('Status url'),
        help_text=_('URL to call to check service status. It must respond 200 to a GET request.'),
        blank=True,
    )
    status_text = models.CharField(
        _('Status text'),
        help_text=_(
            'Text to match in status response. '
            'If set, this text must be contained within the response for the resource to be considered “up”.'
        ),
        max_length=512,
        blank=True,
    )

    log_requests_errors = False

    class Meta:
        verbose_name = _('Proxy')

    def check_status(self):
        response = self.requests.request(
            method='GET',
            url=self.status_url,
            headers=self.get_forced_headers(),
            timeout=self.http_timeout,
        )
        # raise for invalid status
        response.raise_for_status()
        if response.status_code // 100 != 2:
            raise HTTPError('{response.status_code} is not 200', response=response)
        if self.status_text and self.status_text not in response.text:
            raise HTTPError(
                f'{self.status_text:!r} not found in {self.status_url:!r} response', response=response
            )

    def has_check_status(self):
        """check status iif the url is set"""
        return bool(self.status_url)

    def get_forced_headers(self):
        """Extract headers from configuration"""
        headers = {}
        for header in self.forced_headers.splitlines():
            header = header.strip()
            if header.startswith('#'):
                continue
            header = header.split(':', 1)
            if len(header) == 2:
                headers[header[0].strip()] = header[1].strip()
        return headers

    def make_requests_auth(self, session):
        if self.auth_type == 'basic_auth':
            return HTTPBasicAuth(self.basic_auth_username, self.basic_auth_password)
        elif self.auth_type.startswith('amazon_cognito'):
            if self.auth_type == 'amazon_cognito_access_token':
                token_key = 'AccessToken'
            else:
                token_key = 'IdToken'
            return http_authenticators.AmazonCognito(
                session=session,
                token_url=self.oauth2_token_url,
                token_key=token_key,
                json={
                    'AuthParameters': {
                        'USERNAME': self.oauth2_username,
                        'PASSWORD': self.oauth2_password,
                    },
                    'AuthFlow': 'USER_PASSWORD_AUTH',
                    'ClientId': self.amazon_cognito_client_id,
                },
                headers={
                    'X-Amz-Target': 'AWSCognitoIdentityProviderService.InitiateAuth',
                    'Content-Type': 'application/x-amz-json-1.1',
                },
                # Avoid RecursionError in passerelle.utils.Requests.request
                auth=None,
            )
        elif self.auth_type == 'oauth2':
            # standard oauth2
            return super().make_requests_auth(session)

    @endpoint(
        name='request',
        methods=['get', 'post', 'delete', 'put', 'patch'],
        pattern=r'^(?P<path>.*)$',
        description=_('Make a request'),
        example_pattern='{path}',
        parameters={
            'path': {
                'description': _('request will be made on Upstream Service Base URL + path'),
                'example_value': 'foo/bar',
            }
        },
    )
    def request(self, request, path, *args, **kwargs):
        params = parse_qsl(request.META.get('QUERY_STRING'))
        if params and params[-1][0] == 'signature':
            # remove Publik signature parts: orig, algo, timestamp, nonce, signature
            params = [
                (k, v) for k, v in params if k not in ('orig', 'algo', 'timestamp', 'nonce', 'signature')
            ]
        headers = {k: v for k, v in request.headers.items() if v and k.lower() in PASS_HEADERS_REQUEST}
        if request.method != 'GET':
            headers['Content-Type'] = request.headers.get('content-type')
        headers.update(self.get_forced_headers())
        upstream = self.requests.request(
            method=request.method,
            url=self.upstream_base_url + path,
            headers=headers,
            params=params,
            data=request.body,
            timeout=self.http_timeout,
        )
        response = HttpResponse(
            upstream.content,
            content_type=upstream.headers.get('Content-Type'),
            status=upstream.status_code,
            reason=upstream.reason,
        )
        # from django.utils.log:230
        # if this attr is set, responses with status_code >= 400 won't be logged
        response._has_been_logged = True
        return response
