# Generated by Django 3.2.19 on 2024-10-03 14:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('proxy', '0002_auto_20240729_1659'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='status_text',
            field=models.CharField(
                blank=True,
                help_text=(
                    'Text to match in status response. '
                    'If set, this text must be contained within the response for the resource '
                    'to be considered “up”.'
                ),
                max_length=512,
                verbose_name='Status text',
            ),
        ),
        migrations.AddField(
            model_name='resource',
            name='status_url',
            field=models.URLField(
                blank=True,
                help_text='URL to call to check service status. It must respond 200 to a GET request.',
                verbose_name='Status url',
            ),
        ),
    ]
