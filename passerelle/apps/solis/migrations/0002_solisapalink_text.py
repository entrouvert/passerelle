from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('solis', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='solisapalink',
            name='text',
            field=models.CharField(default='inconnu', max_length=256),
            preserve_default=False,
        ),
    ]
