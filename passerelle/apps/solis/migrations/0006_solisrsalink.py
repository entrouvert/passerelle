# Generated by Django 1.11.20 on 2019-05-09 07:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('solis', '0005_remove_solis_log_level'),
    ]

    operations = [
        migrations.CreateModel(
            name='SolisRSALink',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name_id', models.CharField(max_length=256)),
                ('user_id', models.CharField(max_length=64)),
                ('code', models.CharField(max_length=64)),
                ('text', models.CharField(max_length=256)),
                (
                    'resource',
                    models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='solis.Solis'),
                ),
            ],
        ),
    ]
