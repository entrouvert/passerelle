from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0005_resourcelog'),
    ]

    operations = [
        migrations.CreateModel(
            name='Solis',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'service_url',
                    models.URLField(
                        help_text='Solis API base URL', max_length=256, verbose_name='Service URL'
                    ),
                ),
                ('username', models.CharField(max_length=128, verbose_name='Username')),
                ('password', models.CharField(max_length=128, verbose_name='Password')),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'Solis',
            },
        ),
        migrations.CreateModel(
            name='SolisAPALink',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name_id', models.CharField(max_length=256)),
                ('user_id', models.CharField(max_length=64)),
                ('code', models.CharField(max_length=64)),
                ('resource', models.ForeignKey(to='solis.Solis', on_delete=models.CASCADE)),
            ],
        ),
    ]
