from django.db import migrations, models

import passerelle.apps.solis.models


class Migration(migrations.Migration):
    dependencies = [
        ('solis', '0002_solisapalink_text'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solis',
            old_name='password',
            new_name='basic_auth_password',
        ),
        migrations.RenameField(
            model_name='solis',
            old_name='username',
            new_name='basic_auth_username',
        ),
        migrations.AlterField(
            model_name='solis',
            name='basic_auth_password',
            field=models.CharField(max_length=128, verbose_name='HTTP Basic Auth password', blank=True),
        ),
        migrations.AlterField(
            model_name='solis',
            name='basic_auth_username',
            field=models.CharField(max_length=128, verbose_name='HTTP Basic Auth username', blank=True),
        ),
        migrations.AddField(
            model_name='solis',
            name='client_certificate',
            field=models.FileField(
                help_text='Client certificate and private key (PEM format)',
                upload_to=passerelle.apps.solis.models.keystore_upload_to,
                null=True,
                verbose_name='Client certificate',
                blank=True,
            ),
        ),
        migrations.AddField(
            model_name='solis',
            name='http_proxy',
            field=models.CharField(max_length=128, verbose_name='Proxy URL', blank=True),
        ),
        migrations.AddField(
            model_name='solis',
            name='trusted_certificate_authorities',
            field=models.FileField(
                help_text='Trusted CAs (PEM format)',
                upload_to=passerelle.apps.solis.models.trusted_cas_upload_to,
                null=True,
                verbose_name='Trusted CAs',
                blank=True,
            ),
        ),
        migrations.AddField(
            model_name='solis',
            name='verify_cert',
            field=models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
        ),
    ]
