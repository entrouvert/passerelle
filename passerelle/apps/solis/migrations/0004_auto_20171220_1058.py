from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('solis', '0003_auto_20171219_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solis',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
