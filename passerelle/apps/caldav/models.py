# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2024 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import functools
import urllib.parse

import caldav
import requests
from django.db import models
from django.utils.dateparse import parse_date, parse_datetime
from django.utils.translation import gettext_lazy as _

from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.conversion import exception_to_text
from passerelle.utils.jsonresponse import APIError

EVENT_SCHEMA_PART = {
    'type': 'object',
    'description': _('Ical event properties ( VEVENT RFC 5545 3.6.1 )'),
    'properties': {
        'DTSTART': {
            'type': 'string',
            'description': _('Event start (included) ISO-8601 date-time or date (for allday event)'),
        },
        'DTEND': {
            'type': 'string',
            'description': _('Event end (excluded) ISO-8601 date-time or date (for allday event)'),
        },
        'SUMMARY': {
            'type': 'string',
            'description': 'RFC 5545 3.8.1.12',
        },
        'DESCRIPTION': {
            'type': 'string',
            'description': 'RFC 5545 3.8.2.5',
        },
        'LOCATION': {
            'type': 'string',
            'description': 'RFC 5545 3.8.1.7',
        },
        'CATEGORY': {'type': 'string'},
        'TRANSP': {
            'type': 'boolean',
            'description': _('Transparent if true else opaque (RFC 5545 3.8.2.7)'),
        },
        'RRULE': {
            'description': _('Recurrence rule (RFC 5545 3.8.5.3)'),
            'type': 'object',
            'properties': {
                'FREQ': {
                    'type': 'string',
                    'enum': ['WEEKLY', 'MONTHLY', 'YEARLY'],
                },
                'BYDAY': {
                    'type': 'array',
                    'items': {
                        'type': 'string',
                        'enum': ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'],
                    },
                },
                'BYMONTH': {
                    'type': 'array',
                    'items': {
                        'type': 'integer',
                        'minimum': 1,
                        'maximum': 12,
                    },
                },
                'COUNT': {
                    'type': 'integer',
                    'minimum': 1,
                },
                'UNTIL': {
                    'type': 'string',
                    'description': _('Date or date and time indicating the end of recurrence'),
                },
            },
        },
    },
}


USERNAME_PARAM = {
    'description': _('The calendar\'s owner username'),
    'type': 'string',
}

EVENT_UID_PARAM = {
    'description': _('An event UID'),
    'type': 'string',
}


# Action's request body schema
EVENT_SCHEMA = {
    'title': _('Event description schema'),
    'unflatten': True,
    **EVENT_SCHEMA_PART,
}


def clean_egw_response(response, *args, **kwargs):
    '''requests hooks that modify requests's responses deleting
    EGW's SQL log lines when there is some

    SQL log lines are matched by checking that they :
    - startswith "==> SQL =>"
    - endswith "<br>"
    '''
    response._content = b'\n'.join(
        line
        for line in response.content.split(b'\n')
        if not line.startswith(b'==> SQL =>') or not line.endswith(b'<br>')
    )
    return response


class CalDAV(BaseResource):
    dav_url = models.URLField(
        blank=False,
        verbose_name=_('DAV root URL'),
        help_text=_('DAV root URL (such as https://test.egw/groupdav.php/)'),
    )
    dav_login = models.CharField(max_length=128, verbose_name=_('DAV username'), blank=False)
    dav_password = models.CharField(max_length=512, verbose_name=_('DAV password'), blank=False)

    category = _('Misc')

    log_requests_errors = False

    class Meta:
        verbose_name = _('CalDAV')

    @functools.cached_property
    def dav_client(self):
        '''Instanciate a caldav.DAVCLient and return the instance'''
        client = caldav.DAVClient(self.dav_url, username=self.dav_login, password=self.dav_password)
        # Replace DAVClient.session requests.Session instance by our
        # own requests session in order to log DAV interactions
        client.session = self.requests
        # adds EGW response cleaning hook
        self.requests.hooks['response'] = clean_egw_response
        return client

    def check_status(self):
        '''Attempt a propfind on DAV root URL'''
        try:
            rep = self.dav_client.propfind()
            rep.find_objects_and_props()
        except caldav.lib.error.AuthorizationError:
            raise Exception(_('Not authorized: bad login/password ?'))

    @endpoint(
        name='event',
        pattern='^create$',
        example_pattern='create',
        methods=['post'],
        post={'request_body': {'schema': {'application/json': EVENT_SCHEMA}}},
        parameters={
            'username': USERNAME_PARAM,
        },
    )
    def create_event(self, request, username, post_data):
        '''Event creation endpoint'''
        cal = self.get_calendar(username)
        self._process_event_properties(post_data)

        # Sequence is auto-incremented when saved, -1 will lead to the
        # expected SEQUENCE:0 when an event is created
        post_data['SEQUENCE'] = -1
        try:
            evt = cal.save_event(**post_data)
        except requests.exceptions.RequestException as expt:
            raise APIError(
                _('Error sending creation request to caldav server'),
                data={
                    'expt_class': str(type(expt)),
                    'expt': str(expt),
                    'username': username,
                },
            )
        except caldav.lib.error.DAVError as expt:
            raise APIError(
                _('Error creating event'),
                data={'expt_class': str(type(expt)), 'expt': exception_to_text(expt), 'username': username},
            )
        return {'data': {'event_id': evt.id}}

    # Patch do not support request_body validation, using post instead
    @endpoint(
        name='event',
        pattern='^update$',
        example_pattern='update',
        methods=['post'],
        post={'request_body': {'schema': {'application/json': EVENT_SCHEMA}}},
        parameters={
            'username': USERNAME_PARAM,
            'event_id': EVENT_UID_PARAM,
        },
    )
    def update_event(self, request, username, event_id, post_data):
        '''Event update endpoint'''
        ical = self.get_event(username, event_id)

        vevent = ical.icalendar_instance.walk('VEVENT')
        if not len(vevent) == 1:
            raise APIError(
                _('Given event (user:%(user)r uid:%(uid)r) do not contains VEVENT component')
                % {'user': username, 'uid': event_id},
                data={
                    'username': username,
                    'event_uid': event_id,
                    'VEVENT': str(vevent),
                },
            )
        vevent = vevent[0]
        self._process_event_properties(post_data, vevent)
        # vevent.update(post_data) do not convert values as expected
        for k, v in post_data.items():
            vevent.pop(k)
            vevent.add(k, v)
        if 'SEQUENCE' not in vevent:
            # SEQUENCE is auto-incremented when present
            # here after a 1st modification the SEQUENCE will be 1 (not 0)
            vevent['SEQUENCE'] = 0
        try:
            # do not use ical.save(no_create=True) : no_create fails on some calDAV
            ical.save()
        except requests.exceptions.RequestException as expt:
            raise APIError(
                _('Error sending update request to caldav server'),
                data={
                    'expt_class': str(type(expt)),
                    'expt': str(expt),
                    'username': username,
                    'event_id': event_id,
                },
            )
        return {'data': {'event_id': ical.id}}

    @endpoint(
        name='event',
        pattern='^delete$',
        example_pattern='delete',
        methods=['delete'],
        parameters={
            'username': USERNAME_PARAM,
            'event_id': EVENT_UID_PARAM,
        },
    )
    def delete_event(self, request, username, event_id):
        ical = self.get_event(username, event_id)
        try:
            ical.delete()
        except requests.exceptions.RequestException as expt:
            raise APIError(
                _('Error sending deletion request to caldav server'),
                data={
                    'expt_class': str(type(expt)),
                    'expt': str(expt),
                    'username': username,
                    'event_id': event_id,
                },
            )
        return {}

    def get_event(self, username, event_uid):
        '''Fetch an event given a username and an event_uid
        Arguments:
        - username: Calendar owner's username
        - event_uid: The event's UID

        Returns an caldav.Event instance
        '''
        event_path = '%s/calendar/%s.ics' % (urllib.parse.quote(username), urllib.parse.quote(str(event_uid)))
        cal = self.get_calendar(username)
        try:
            ical = cal.event_by_url(event_path)
        except caldav.lib.error.DAVError as expt:
            raise APIError(
                _('Unable to get event %(event)r in calendar owned by %(user)r')
                % {'event': event_uid, 'user': username},
                data={
                    'expt': exception_to_text(expt),
                    'expt_cls': str(type(expt)),
                    'username': username,
                    'event_uid': event_uid,
                },
            )
        except requests.exceptions.RequestException as expt:
            raise APIError(
                _('Unable to communicate with caldav server while fetching event'),
                data={
                    'expt': exception_to_text(expt),
                    'expt_class': str(type(expt)),
                    'username': username,
                    'event_uid': event_uid,
                },
            )
        return ical

    def get_calendar(self, username):
        '''Given a username returns the associated calendar set
        Arguments:
        - username: Calendar owner's username

        Returns A caldav.Calendar instance
        Note: do not raise any caldav exception before a method trying to make
               a request is called
        '''
        path = '%s/calendar' % urllib.parse.quote(username)
        calendar = caldav.Calendar(client=self.dav_client, url=path)
        return calendar

    def _process_event_properties(self, data, vevent=None):
        '''Handles verification & convertion of event properties
        @note Modify given data dict inplace
        '''
        if 'TRANSP' in data:
            data['TRANSP'] = 'TRANSPARENT' if data['TRANSP'] else 'OPAQUE'

        if 'CATEGORY' in data:
            data['CATEGORIES'] = [data.pop('CATEGORY')]

        if 'RRULE' in data and 'UNTIL' in data['RRULE']:
            try:
                data['RRULE']['UNTIL'] = self._parse_date_or_datetime(data['RRULE']['UNTIL'])
            except ValueError:
                raise APIError(
                    _('Unable to convert field %(name)s=%(value)r: not a valid date nor date-time')
                    % {'name': 'RRULE/UNTIL', 'value': data['RRULE']['UNTIL']},
                    http_status=400,
                )

        for dt_field in ('DTSTART', 'DTEND'):
            if dt_field not in data:
                continue
            try:
                data[dt_field] = self._parse_date_or_datetime(data[dt_field])
            except ValueError:
                raise APIError(
                    _('Unable to convert field %(name)s=%(value)r: not a valid date nor date-time')
                    % {'name': dt_field, 'value': data[dt_field]},
                    http_status=400,
                )

        dtstart = None
        if 'DTSTART' in data:
            dtstart = data['DTSTART']
        elif vevent and 'DTSTART' in vevent:
            dtstart = vevent.decoded('DTSTART')
        until = None
        if 'RRULE' in data and 'UNTIL' in data['RRULE']:
            until = data['RRULE']['UNTIL']
        elif vevent and 'RRULE' in vevent and 'UNTIL' in vevent['RRULE']:
            until = vevent.decoded('RRULE')['UNTIL'][0]

        if until and dtstart:
            new_until = None
            if type(dtstart) is not type(until):
                if isinstance(dtstart, datetime.datetime):
                    tzinfo = None
                    if dtstart.tzinfo:
                        tzinfo = datetime.UTC
                    until = datetime.datetime.combine(until, datetime.time(23, 59), tzinfo)
                else:
                    until = until.date()
                new_until = until
            elif isinstance(dtstart, datetime.datetime) and dtstart.tzinfo != until.tzinfo:
                if dtstart.tzinfo:
                    new_until = until.astimezone(datetime.UTC)
                else:
                    # assuming that UNTIL is in same timezone than the implicit DTSTART tz
                    new_until = until.replace(tzinfo=None)
            if new_until:
                if 'RRULE' not in data:
                    data['RRULE'] = {}
                data['RRULE']['UNTIL'] = new_until

    def _parse_date_or_datetime(self, value):
        try:
            ret = parse_date(value) or parse_datetime(value)
        except ValueError:
            ret = None
        if not ret:
            raise ValueError('Invalid value')
        return ret
