# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import urllib.parse

import requests
from django.db import models
from django.utils.translation import gettext_lazy as _

from passerelle.sms.models import TrackCreditSMSResource
from passerelle.utils.jsonresponse import APIError


class SMSFactorSMSGateway(TrackCreditSMSResource):
    auth_token = models.CharField(verbose_name=_('Auth Token'), max_length=255)

    # unecessary field
    allow_premium_rate = None

    class Meta:
        verbose_name = 'SMS Factor'
        db_table = 'sms_factor'

    TEST_DEFAULTS = {
        'create_kwargs': {
            'auth_token': 'yyy',
            'credit_threshold_alert': 1000,
        },
        'test_vectors': [
            {
                'status_code': 200,
                'response': {
                    'status': -7,
                    'message': 'Erreur de données',
                    'details': 'Texte du message introuvable',
                },
                'result': {
                    'err': 1,
                    'err_desc': 'SMS Factor error: some destinations failed',
                    'data': [
                        ['33688888888', 'Texte du message introuvable'],
                        ['33677777777', 'Texte du message introuvable'],
                    ],
                },
            },
            {
                'status_code': 200,
                'response': {
                    'status': 1,
                    'message': 'OK',
                    'ticket': '14672468',
                    'cost': 2,
                    'credits': 642,
                    'total': 2,
                    'sent': 2,
                    'blacklisted': 0,
                    'duplicated': 0,
                    'invalid': 0,
                    'npai': 0,
                },
                'result': {
                    'err': 0,
                    'data': {
                        'status': 1,
                        'message': 'OK',
                        'ticket': '14672468',
                        'cost': 2,
                        'credits': 642,
                        'total': 2,
                        'sent': 2,
                        'blacklisted': 0,
                        'duplicated': 0,
                        'invalid': 0,
                        'npai': 0,
                    },
                },
            },
        ],
    }
    URL = 'https://api.smsfactor.com'

    TEST_CREDIT_LEFT = {
        'create_kwargs': {
            'auth_token': 'yyy',
        },
        'url': urllib.parse.urljoin(URL, 'credits'),
        'get_credit_left_payload': lambda x: {'credits': str(x)},
    }

    def request(self, method, endpoint, **kwargs):
        url = urllib.parse.urljoin(self.URL, endpoint)

        headers = {
            'Authorization': f'Bearer {self.auth_token}',
            'Accept': 'application/json',
        }

        try:
            response = self.requests.request(method, url, headers=headers, **kwargs)
        except requests.RequestException as e:
            raise APIError('SMS Factor: request failed, %s' % e)
        else:
            try:
                result = response.json()
            except ValueError:
                raise APIError('SMS Factor: bad JSON response')
        try:
            response.raise_for_status()
        except requests.RequestException as e:
            raise APIError('SMS Factor: %s "%s"' % (e, result))
        return result

    def send_msg(self, text, sender, destinations, **kwargs):
        """Send a SMS using the SMS Factor provider"""
        # from https://dev.smsfactor.com/en/api/sms/send/send-single
        # and https://dev.smsfactor.com/en/api/sms/send/send-simulate
        # set destinations phone number in E.164 format (without the + prefix)
        # [country code][phone number including area code]
        destinations = [dest[2:] for dest in destinations]

        results = []

        for dest in destinations:
            params = {
                'sender': sender,
                'text': text,
                'to': dest,
                'pushtype': 'alert' if not kwargs.get('stop') else 'marketing',
            }
            data = self.request('get', 'send', params=params)
            logging.info('SMS Factor answered with %s', data)
            results.append(data)

        errors = [f'SMS Factor error: {r["status"]}: {r["message"]}' for r in results if r['status'] != 1]

        consumed_credits = None
        try:
            self.credit_left = results[-1]['credits']
            consumed_credits = sum(r['cost'] for r in results)
        except KeyError:
            # no credits key, there was probably an error with the request
            pass
        else:
            self.save(update_fields=['credit_left'])
        if any(errors):
            raise APIError('SMS Factor error: some destinations failed', data=errors)
        return consumed_credits

    def update_credit_left(self):
        result = self.request('get', endpoint='credits')
        try:
            # SMS Factor returns this as a string, for an unknown reason
            self.credit_left = int(result['credits'])
            self.save(update_fields=['credit_left'])
        except KeyError:
            self.logger.warning('Cannot retrieve credits for sms-factor connector: %s', result)
