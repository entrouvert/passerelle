# Generated by Django 1.11.18 on 2019-05-24 10:25

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('base', '0012_job'),
    ]

    operations = [
        migrations.CreateModel(
            name='ATALConnector',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description')),
                ('slug', models.SlugField(unique=True, verbose_name='Identifier')),
                (
                    'base_soap_url',
                    models.URLField(
                        help_text='URL of the base SOAP endpoint',
                        max_length=400,
                        verbose_name='Base SOAP endpoint',
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        blank=True,
                        related_name='+',
                        related_query_name='+',
                        to='base.ApiUser',
                    ),
                ),
            ],
            options={
                'verbose_name': 'ATAL connector',
            },
        ),
    ]
