# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


INSERT_DEMANDE_COMPLET_BY_TYPE = {
    'type': 'object',
    'required': ['type_demande'],
    'additionalProperties': False,
    'properties': {
        'type_demande': {
            'type': 'string',
        },
        'code_service_demandeur': {
            'type': 'string',
        },
        'date_saisie': {
            'type': 'string',
        },
        'date_demande': {
            'type': 'string',
        },
        'date_souhaite': {
            'type': 'string',
        },
        'date_butoir': {
            'type': 'string',
        },
        'contact_civilite': {
            'type': 'string',
        },
        'contact_nom': {
            'type': 'string',
        },
        'contact_prenom': {
            'type': 'string',
        },
        'contact_tel': {
            'type': 'string',
        },
        'contact_mobile': {
            'type': 'string',
        },
        'contact_email': {
            'type': 'string',
        },
        'contact_info_compl': {
            'type': 'string',
        },
        'demande_type_support': {
            'type': 'string',
        },
        'contact_adresse': {
            'type': 'string',
        },
        'contact_adresse_compl': {
            'type': 'string',
        },
        'contact_code_postal': {
            'type': 'string',
        },
        'contact_ville': {
            'type': 'string',
        },
        'contact_organisme': {
            'type': 'string',
        },
        'contact_titre': {
            'type': 'string',
        },
        'code_equipement': {
            'type': 'string',
        },
        'code_mairie_equipement': {
            'type': 'string',
        },
        'code_sig_equipement': {
            'type': 'string',
        },
        'code_collectivite_equipement': {
            'type': 'string',
        },
        'code_quartier_equipement': {
            'type': 'string',
        },
        'code_type_equipement': {
            'type': 'string',
        },
        'demande_lieu': {
            'type': 'string',
        },
        'coord_x': {
            'type': 'number',
        },
        'coord_y': {
            'type': 'number',
        },
        'demande_priorite': {
            'type': 'string',
        },
        'demande_objet': {
            'type': 'string',
        },
        'demande_description': {
            'type': 'string',
        },
        'demande_commentaire': {
            'type': 'string',
        },
        'remote_adresse': {'type': 'string'},
        'demande_mots_cles': {'type': 'string'},
        'code_thematique': {
            'type': 'string',
        },
        'code_priorite': {'type': 'string'},
        'demande_thematique': {'type': 'string'},
        'code_projet': {'type': 'string'},
    },
}

INSERT_ACTION_COMMENT = {
    'type': 'object',
    'required': ['numero_demande', 'commentaire'],
    'properties': {
        'numero_demande': {
            'type': 'string',
        },
        'commentaire': {
            'type': 'string',
        },
    },
}

UPLOAD = {
    'type': 'object',
    'required': ['file', 'numero_demande'],
    'properties': {
        'file': {
            'type': 'object',
            'required': ['content'],
            'properties': {
                'content': {
                    'type': 'string',
                },
                'filename': {
                    'type': 'string',
                },
            },
        },
        'numero_demande': {
            'type': 'string',
        },
        'nom_fichier': {
            'type': 'string',
        },
    },
}
