# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _

NUMBER_TYPES = [
    {'type': 'number'},
    {
        'type': 'string',
        'pattern_description': _('Decimal number within a string'),
    },
]

GEOJSON_POINT = {
    'title': 'GeoJSON Point',
    'type': 'object',
    'required': ['coordinates'],
    'properties': {
        'type': {'type': 'string', 'enum': ['Point']},
        'coordinates': {
            'type': 'array',
            'minItems': 2,
            'maxItems': 2,
            'items': {
                'oneOf': NUMBER_TYPES,
            },
        },
    },
}

DEMANDES_RECUES = {
    'type': 'object',
    'additionalProperties': False,
    'properties': {
        'fournisseur': {
            'type': 'string',
        },
        'idDemande': {
            'type': 'string',
        },
        'dateEnvoi': {
            'type': 'string',
        },
        'typeModele': {'type': 'string', 'enum': ['DA', 'DPS', 'DPV']},
        'demandeur': {
            'title': 'Demandeur',
            'type': 'object',
            'properties': {
                'nom': {
                    'type': 'string',
                },
                'prenom': {
                    'type': 'string',
                },
                'raisonSociale': {
                    'type': 'string',
                },
                'telephone': {
                    'type': 'string',
                },
                'indicatifTel': {
                    'type': 'string',
                },
                'mail': {
                    'type': 'string',
                },
                'adresse': {
                    'title': 'Adresse',
                    'type': 'object',
                    'properties': {
                        'num': {
                            'type': 'string',
                        },
                        'rue': {
                            'type': 'string',
                        },
                        'cp': {
                            'type': 'string',
                        },
                        'insee': {
                            'type': 'string',
                        },
                        'commune': {
                            'type': 'string',
                        },
                        'pays': {
                            'type': 'string',
                        },
                    },
                    'required': ['rue', 'insee'],
                },
            },
            'required': ['mail', 'adresse'],
        },
        'destinataire': {
            'title': 'Destinataire',
            'type': 'object',
            'properties': {
                'idCollectivite': {
                    'type': 'string',
                },
                'nomCollectivite': {
                    'type': 'string',
                },
                'idAgenceSGLK': {
                    'type': 'string',
                },
            },
            'required': ['idCollectivite', 'nomCollectivite'],
        },
        'localisation': {
            'title': 'Localisation',
            'type': 'object',
            'properties': {
                'num': {
                    'type': 'string',
                },
                'extension': {
                    'type': 'string',
                },
                'rue': {
                    'type': 'string',
                },
                'complement': {
                    'type': 'string',
                },
                'cp': {
                    'type': 'string',
                },
                'localite': {
                    'type': 'string',
                },
            },
        },
        'geom': GEOJSON_POINT,
        'additionalInformation': {
            'title': 'Additionnal informations',
            'type': 'object',
            'properties': {
                'typeDemande': {
                    'type': 'string',
                },
                'dateDebut': {
                    'type': 'string',
                },
                'dateFin': {
                    'type': 'string',
                },
            },
            'required': ['typeDemande', 'dateDebut', 'dateFin'],
        },
    },
    'required': [
        'fournisseur',
        'idDemande',
        'dateEnvoi',
        'typeModele',
        'demandeur',
        'geom',
        'additionalInformation',
    ],
    'unflatten': True,
}

UPLOAD_ANNEXES = {
    'type': 'object',
    'additionalProperties': False,
    'properties': {
        'id_demande': {
            'title': 'Litteralis demand identifier',
            'type': 'string',
        },
        'file': {
            'title': 'File object',
            'type': 'object',
            'properties': {
                'filename': {
                    'type': 'string',
                    'description': 'Filename',
                },
                'content': {
                    'type': 'string',
                    'description': 'Content',
                },
                'content_type': {
                    'type': 'string',
                    'description': 'Content type',
                },
            },
            'required': ['content'],
        },
    },
    'required': ['id_demande', 'file'],
}
