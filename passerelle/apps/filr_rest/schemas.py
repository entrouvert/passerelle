# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext_lazy as _

UPLOAD = {
    'type': 'object',
    'title': _('Upload file'),
    'properties': {
        'file': {
            'title': _('File object'),
            'type': 'object',
            'properties': {
                'filename': {
                    'type': 'string',
                    'description': _('Filename'),
                },
                'content': {
                    'type': 'string',
                    'description': _('Content'),
                },
                'content_type': {
                    'type': 'string',
                    'description': _('Content type'),
                },
            },
            'required': ['content', 'filename'],
        },
        'filename': {
            'type': 'string',
            'description': _('Filename (takes precedence over filename in "file" object)'),
        },
        'folder_name': {
            'type': 'string',
            'description': _('Folder name'),
        },
        'root_folder_id': {
            'type': 'string',
            'description': _('Root folder identifier'),
            'pattern': '^[0-9]+$',
        },
    },
    'required': ['file', 'folder_name', 'root_folder_id'],
}


SHARE_FOLDER = {
    'type': 'object',
    'title': _('Share Folder'),
    'properties': {
        'folder_id': {
            'type': 'string',
            'description': _('Folder identifier'),
            'pattern': '^[0-9]+$',
        },
        'emails': {
            'type': 'array',
            'description': _('Emails'),
            'items': {
                'oneOf': [
                    {'type': 'string'},
                    {'type': 'null'},
                ]
            },
        },
        'days_to_expire': {'type': 'string', 'pattern': '^[0-9]+$'},
    },
    'required': ['folder_id', 'emails', 'days_to_expire'],
    'unflatten': True,
}


DELETE_FOLDER = {
    'type': 'object',
    'title': _('Delete Folder'),
    'properties': {
        'folder_id': {'type': 'string', 'description': _('Folder identifier'), 'pattern': '^[0-9]+$'}
    },
    'required': ['folder_id'],
}
