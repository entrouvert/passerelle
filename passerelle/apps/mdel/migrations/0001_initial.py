from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Demand',
            fields=[
                ('num', models.CharField(max_length=64, unique=True, serialize=False, primary_key=True)),
                ('flow_type', models.CharField(max_length=32)),
                ('status', models.IntegerField(null=True)),
                ('step', models.IntegerField(default=0)),
                ('demand_id', models.CharField(max_length=128, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MDEL',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'NOTSET',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                        ],
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser', related_name='+', related_query_name='+', blank=True
                    ),
                ),
            ],
            options={
                'verbose_name': 'Mes Demarches En Ligne',
            },
        ),
        migrations.AddField(
            model_name='demand',
            name='resource',
            field=models.ForeignKey(to='mdel.MDEL', on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='demand',
            unique_together={('num', 'flow_type')},
        ),
    ]
