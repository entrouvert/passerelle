import datetime

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mdel', '0002_auto_20170123_0311'),
    ]

    operations = [
        migrations.AddField(
            model_name='demand',
            name='created_at',
            field=models.DateTimeField(
                default=datetime.datetime(2016, 12, 31, 12, 0, 0, 0, tzinfo=datetime.UTC),
                auto_now_add=True,
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='demand',
            name='updated_at',
            field=models.DateTimeField(
                default=datetime.datetime(2016, 12, 31, 12, 0, 0, 0, tzinfo=datetime.UTC),
                auto_now=True,
            ),
            preserve_default=False,
        ),
    ]
