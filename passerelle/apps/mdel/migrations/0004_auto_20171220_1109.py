from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mdel', '0003_auto_20170125_0450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mdel',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
