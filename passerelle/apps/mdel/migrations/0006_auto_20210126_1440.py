# Generated by Django 1.11.29 on 2021-01-26 13:40

from django.db import migrations

import passerelle.utils.sftp


class Migration(migrations.Migration):
    dependencies = [
        ('mdel', '0005_remove_mdel_log_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='mdel',
            name='incoming_sftp',
            field=passerelle.utils.sftp.SFTPField(
                blank=True, default=None, verbose_name='SFTP server for incoming files'
            ),
        ),
        migrations.AddField(
            model_name='mdel',
            name='outcoming_sftp',
            field=passerelle.utils.sftp.SFTPField(
                blank=True, default=None, verbose_name='SFTP server for outgoing files'
            ),
        ),
    ]
