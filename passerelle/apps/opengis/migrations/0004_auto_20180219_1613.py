from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('opengis', '0003_auto_20171220_1058'),
    ]

    operations = [
        migrations.AddField(
            model_name='opengis',
            name='projection',
            field=models.CharField(
                choices=[
                    (b'EPSG:2154', 'EPSG:2154 (Lambert-93)'),
                    (b'EPSG:3857', 'EPSG:3857 (WGS 84 / Pseudo-Mercator)'),
                    (b'EPSG:3945', 'EPSG:3945 (CC45)'),
                    (b'EPSG:4326', 'EPSG:4326 (WGS 84)'),
                ],
                default=b'EPSG:3857',
                max_length=16,
                verbose_name='GIS projection',
            ),
        ),
    ]
