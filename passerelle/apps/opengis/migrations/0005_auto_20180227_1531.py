from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('opengis', '0004_auto_20180219_1613'),
    ]

    operations = [
        migrations.AddField(
            model_name='opengis',
            name='search_radius',
            field=models.IntegerField(default=5, verbose_name='Radius for point search'),
        ),
    ]
