from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('opengis', '0002_auto_20171129_1814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='opengis',
            name='slug',
            field=models.SlugField(verbose_name='Identifier', unique=True),
        ),
    ]
