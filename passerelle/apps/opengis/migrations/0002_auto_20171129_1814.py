from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('opengis', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='opengis',
            old_name='service_root_url',
            new_name='wms_service_url',
        ),
        migrations.AddField(
            model_name='opengis',
            name='wfs_service_url',
            field=models.URLField(max_length=256, verbose_name='Web Feature Service (WFS) URL', blank=True),
        ),
        migrations.AlterField(
            model_name='opengis',
            name='wms_service_url',
            field=models.URLField(max_length=256, verbose_name='Web Map Service (WMS) URL', blank=True),
        ),
    ]
