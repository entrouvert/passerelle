from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('opengis', '0014_auto_20210927_1006'),
    ]

    operations = [
        migrations.AddField(
            model_name='query',
            name='computed_properties',
            field=models.JSONField(blank=True, default=dict),
        ),
    ]
