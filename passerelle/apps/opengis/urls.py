# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path

from . import views

management_urlpatterns = [
    re_path(r'^(?P<slug>[\w,-]+)/query/new/$', views.QueryNew.as_view(), name='opengis-query-new'),
    re_path(r'^(?P<slug>[\w,-]+)/query/(?P<pk>\d+)/$', views.QueryEdit.as_view(), name='opengis-query-edit'),
    re_path(
        r'^(?P<slug>[\w,-]+)/query/(?P<pk>\d+)/delete/$',
        views.QueryDelete.as_view(),
        name='opengis-query-delete',
    ),
    re_path(
        r'^(?P<slug>[\w,-]+)/query/(?P<pk>\d+)/test-indexing-template/$',
        views.TestIndexingTemplate.as_view(),
        name='opengis-query-test-indexing-template',
    ),
]
