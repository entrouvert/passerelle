# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core.exceptions import ValidationError

from passerelle.base.forms import BaseQueryFormMixin

from . import models


class QueryForm(BaseQueryFormMixin, forms.ModelForm):
    class Meta:
        model = models.Query
        fields = '__all__'
        exclude = ['resource']

    def add_field_error(self, cleaned_data, field_keys, message):
        for key in field_keys:
            if not cleaned_data.get(key):
                self.add_error(key, message)

    def clean(self):
        cleaned_data = super().clean()
        geo_point = cleaned_data.get('geo_point')
        lat = cleaned_data.get('lat')
        lon = cleaned_data.get('lon')
        output = cleaned_data.get('output')

        try:
            models.OpenDataSoft.clean_lat_lon(geo_point, lat, lon)
        except ValueError as e:
            self.add_field_error(cleaned_data, ['geo_point', 'lat', 'lon'], str(e))
            raise ValidationError(e)

        try:
            models.OpenDataSoft.clean_output(geo_point, output)
        except ValueError as e:
            self.add_field_error(cleaned_data, ['geo_point', 'output'], str(e))
            raise ValidationError(e)

        return cleaned_data

    def clean_dist(self):
        data = self.cleaned_data['dist']
        try:
            cleaned_data = models.OpenDataSoft.clean_dist(data)
        except ValueError as e:
            raise ValidationError(e)

        return cleaned_data
