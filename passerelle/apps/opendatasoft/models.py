# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
from urllib import parse as urlparse

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from requests import RequestException

from passerelle.base.models import BaseQuery, BaseResource
from passerelle.utils.api import endpoint
from passerelle.utils.jsonresponse import APIError
from passerelle.utils.templates import render_to_string, validate_template


class OpenDataSoft(BaseResource):
    service_url = models.CharField(
        _('Site URL'),
        max_length=256,
        blank=False,
        help_text=_('URL without ending "api/..."'),
    )
    api_key = models.CharField(
        _('API key'),
        max_length=128,
        blank=True,
        help_text=_('API key used as credentials'),
    )

    category = _('Data Sources')
    documentation_url = 'https://help.opendatasoft.com/apis/ods-explore-v2/'

    DEFAULT_Q_FILTER = 'search'
    DEFAULT_DIST = '50.0m'
    DEFAULT_OUTPUT = 'json'

    class Meta:
        verbose_name = _('OpenDataSoft Web Service')

    def export_json(self):
        data = super().export_json()
        data['queries'] = [query.export_json() for query in self.queries.all()]
        return data

    @classmethod
    def import_json_real(cls, overwrite, instance, d, **kwargs):
        data_queries = d.pop('queries', [])
        instance = super().import_json_real(overwrite, instance, d, **kwargs)
        queries = []
        if instance and overwrite:
            Query.objects.filter(resource=instance).delete()
        for data_query in data_queries:
            query = Query.import_json(data_query)
            query.resource = instance
            queries.append(query)
        Query.objects.bulk_create(queries)
        return instance

    @classmethod
    def cut_first(cls, value, separator):
        index = value.find(separator)
        if index != -1:
            return value[:index]
        else:
            return value

    @classmethod
    def clean_dist(cls, value):
        match = re.match(r'^(\d+[,.]?\d*)\s*(.*)$', value.strip())
        if not match:
            raise ValueError('Bad dist format, expect float value followed by unit')
        dist_value, dist_unit = match.group(1, 2)
        dist_unit = dist_unit.lower()
        if dist_unit not in ('mi', 'yd', 'ft', 'm', 'cm', 'km', 'mm'):
            raise ValueError('Bad dist unit, expect mi, yd, ft, m, cm, km or mm')
        dist_value = float(dist_value.replace(',', '.'))
        return '%s%s' % (dist_value, dist_unit)

    @classmethod
    def clean_output(cls, geo_point, output):
        if output == 'geojson' and not geo_point:
            raise ValueError('GeoJson output format needs require geo_point parameter')

    @classmethod
    def clean_lat_lon(cls, geo_point, lat, lon):
        if any((lat, lon)) and not all((lat, lon, geo_point)):
            raise ValueError('"geo_point", "lat" and "lon" are requiring each other')

    def api_v2_get(self, url, params):
        headers = {}
        if self.api_key:
            headers = {'Authorization': 'ApiKey %s' % self.api_key}
        try:
            response = self.requests.get(url, headers=headers, params=params)
        except RequestException as e:
            raise APIError('OpenDataSoft: %s' % e)
        try:
            json_response = response.json()
        except ValueError:
            raise APIError('OpenDataSoft: not a JSON response', data={'content': response.text[:1024]})
        try:
            response.raise_for_status()
        except RequestException:
            message = None
            for key in 'message', 'errore_code', 'error':
                message = json_response.get(key)
                if message:
                    break
            raise APIError('OpenDataSoft: %s' % message, json=json_response)
        return json_response

    def call_search(
        self, dataset=None, text_template='', filter_expression='', sort=None, limit=None, id=None, q=None
    ):
        scheme, netloc, path, params, query, fragment = urlparse.urlparse(self.service_url)
        path = urlparse.urljoin(path, 'api/records/1.0/search/')
        url = urlparse.urlunparse((scheme, netloc, path, params, query, fragment))

        params = {'dataset': dataset}
        if id is not None:
            params['q'] = 'recordid:%s' % id
        elif q is not None:
            # remove query language operators, but keep simple quote
            terms = re.split(r'[^\w\']', q)
            terms = [term for term in terms if len(term) > 1 and term.lower() not in ['and', 'or', 'not']]
            params['q'] = ' '.join(terms)
        elif sort:
            params['sort'] = sort
        if self.api_key:
            params['apikey'] = self.api_key
        if limit:
            params['rows'] = limit
        params.update(urlparse.parse_qs(filter_expression))

        try:
            response = self.requests.get(url, params=params)
        except RequestException as e:
            raise APIError('OpenDataSoft error: %s' % e)
        try:
            json_response = response.json()
        except ValueError:
            json_response = None
        if json_response and json_response.get('error'):
            raise APIError(json_response.get('error'))
        try:
            response.raise_for_status()
        except RequestException as e:
            raise APIError('OpenDataSoft error: %s' % e)
        if not json_response:
            raise APIError('OpenDataSoft error: bad JSON response')

        result = []
        for record in json_response.get('records'):
            data = {}
            for key, value in record.get('fields').items():
                if key in ('id', 'text'):
                    key = 'original_%s' % key
                data[key] = value
            data['id'] = record.get('recordid')
            data['text'] = render_to_string(text_template, data).strip()
            result.append(data)

        return {'data': result}

    def call_records(
        self,
        dataset='',
        id=None,
        q=None,
        q_filter=DEFAULT_Q_FILTER,
        geo_point=None,
        lat=None,
        lon=None,
        dist=DEFAULT_DIST,
        text_template='',
        output=DEFAULT_OUTPUT,
        # opendatasoft optional parameters
        select='*',
        where=None,
        group_by=None,
        order_by=None,
        refine=None,
        exclude=None,
        limit=None,
        offset=None,
        lang=None,
        timezone=None,
    ):
        if q_filter not in [x[0] for x in Query.Q_FILTERS]:
            raise APIError('unknown q_filter function: %s' % q_filter)
        if output not in [x[0] for x in Query.OUTPUTS]:
            raise APIError('unknown output format: %s' % output)

        cutted_dataset = self.cut_first(dataset, '/')
        scheme, netloc, path, params, query, fragment = urlparse.urlparse(self.service_url)
        path = urlparse.urljoin(path, 'api/explore/v2.1/catalog/datasets/%s/records/' % cutted_dataset)
        url = urlparse.urlunparse((scheme, netloc, path, params, query, fragment))

        # default opendatasoft parameters
        params = {
            'include_app_metas': True,  # ask for record id (_id) in the reply
            'lang': settings.LANGUAGE_CODE.split('-')[:1][0],
            'timezone': settings.TIME_ZONE,
        }
        # act as a proxy for opendatasoft parameters
        if not id:
            for key in self.records.endpoint_info.parameters.keys():
                if key not in locals() or not locals()[key]:
                    continue  # not provided
                if key in [
                    'dataset',
                    'text_template',
                    'id',
                    'q',
                    'q_filter',
                    'geo_point',
                    'lat',
                    'lon',
                    'dist',
                    'output',
                ]:
                    continue  # ignore publik parameters
                value = str(locals()[key])
                cutted_value = self.cut_first(value, '&')  # truncate on '&' to prevent odsql injection
                params[key] = cutted_value

        # manage publik parameters
        try:
            self.clean_lat_lon(geo_point, lat, lon)
            self.clean_output(geo_point, output)
            formatted_dist = self.clean_dist(dist)
        except ValueError as e:
            raise APIError(e)
        if id is not None:
            url += id
        else:
            select_clauses = [params.get('select', '')]
            where_clauses = [params.get('where', '')]
            order_by_clauses = [params.get('order_by', '')]

            # plain text search
            if q is not None:
                # escape double quote to prevent odsql injection
                escaped_value = q.replace('"', '\\"')
                where_clauses.append('%s("%s")' % (q_filter, escaped_value))

            # spacial search
            if all((geo_point, lat, lon)):
                try:
                    lat, lon = float(lat.replace(',', '.')), float(lon.replace(',', '.'))
                except ValueError:
                    raise APIError('Bad lon/lat coordinates format')

                where_clauses.append(
                    "within_distance(%s, GEOM'POINT(%s %s)', %s)"
                    % (
                        geo_point,
                        lon,
                        lat,
                        formatted_dist,
                    )
                )

                # prioritize results according to coordinates
                select_clauses.append(
                    "distance(%s, GEOM'POINT(%s %s)') as dist"
                    % (
                        geo_point,
                        lon,
                        lat,
                    )
                )
                order_by_clauses.append('dist')

            params['where'] = [x for x in where_clauses if x]
            params['select'] = [x for x in select_clauses if x]
            params['order_by'] = [x for x in order_by_clauses if x]

        json_response = self.api_v2_get(url, params)
        meta = data = []
        if id is not None:
            records = [json_response]
        else:
            records = json_response.pop('results', [])
            meta = json_response
        for record in records:
            entry = {}
            for key, value in record.items():
                if key in ('id', 'text'):
                    key = 'original_%s' % key
                entry[key] = value
            entry['id'] = record.get('_id')
            entry['text'] = render_to_string(text_template, record).strip()
            data.append(entry)

        if output == 'geojson':
            geojson = {
                'type': 'FeatureCollection',
                'features': [],
            }
            for item in data:
                lon = lat = None
                point = item.get(geo_point)
                if point:
                    lon = point.get('lon')
                    lat = point.get('lat')
                if not (lon and lat):
                    continue
                geojson['features'].append(
                    {
                        'type': 'Feature',
                        'geometry': {
                            'coordinates': [lon, lat],
                            'type': 'Point',
                        },
                        'properties': {**item},
                    }
                )
            return geojson
        else:
            return {'meta': meta, 'data': data}

    @endpoint(
        description=_('Search (API v1)'),
        parameters={
            'dataset': {'description': _('Dataset')},
            'text_template': {'description': _('Text template')},
            'sort': {'description': _('Sort field')},
            'limit': {'description': _('Maximum items')},
            'id': {'description': _('Record identifier')},
            'q': {'description': _('Full text query')},
        },
    )
    def search(
        self, request, dataset=None, text_template='', sort=None, limit=None, id=None, q=None, **kwargs
    ):
        return self.call_search(dataset, text_template, '', sort, limit, id, q)

    @endpoint(
        description=_('Records (API v2)'),
        parameters={
            'dataset': {'description': _('Identifier of the dataset to be queried')},
            'id': {'description': _('Record identifier')},
            'q': {'description': _('Full text query')},
            'text_template': {'description': _('Text template')},
            'q_filter': {
                'description': _(
                    'Full text searches function to use: search (default), suggest or startswith'
                ),
            },
            'geo_point': {'description': _('Geo point 2D dataset field')},
            'lat': {'description': _('Geographic limitation and priority')},
            'lon': {'description': _('Geographic limitation and priority')},
            'dist': {
                'description': _('Only include results inside lat/lon/dist circle. Example: 2km'),
            },
            'output': {'description': _('Output format: "json" (defaut) or "geojson"')},
            'select': {
                'description': _('Select expression to add, remove or change the fields to return'),
            },
            'where': {
                'description': _('Filter expression including logical operations and functions'),
            },
            'group_by': {
                'description': _('Expression to defines a grouping function for an aggregation'),
            },
            'order_by': {
                'description': _(
                    'Comma-separated list of field names or aggregations to sort on, followed by an order (asc or desc)'
                ),
            },
            'refine': {
                'description': _('Facet filter used to limit the result set'),
            },
            'exclude': {
                'description': _('Facet filter used to exclude a facet value from the result set'),
            },
            'limit': {'description': _('Number of items to return')},
            'offset': {'description': _('Index of the first item to return (starting at 0)')},
            'lang': {'description': _('If specified, override the default language, which is "fr"')},
            'timezone': {'description': _('Set the timezone for datetime fields')},
        },
    )
    def records(
        self,
        request,
        dataset,
        id=None,
        q=None,
        q_filter=None,
        geo_point=None,
        lat=None,
        lon=None,
        dist=None,
        text_template='',
        output=None,
        # opendatasoft optional parameters
        select=None,
        where=None,
        group_by=None,
        order_by=None,
        refine=None,
        exclude=None,
        limit=None,
        offset=None,
        lang=None,
        timezone=None,
    ):
        params = {}
        for key in self.records.endpoint_info.parameters.keys():
            value = locals().get(key)  # get parameter value
            if value:
                params[key] = value

        return self.call_records(**params)

    @endpoint(
        description=_('Available facet names'),
        parameters={
            'dataset': {'description': _('Identifier of the dataset to be queried')},
        },
    )
    def facets(self, request, dataset):
        cutted_dataset = self.cut_first(dataset, '/')
        scheme, netloc, path, params, query, fragment = urlparse.urlparse(self.service_url)
        path = urlparse.urljoin(path, 'api/explore/v2.1/catalog/datasets/%s/facets/' % cutted_dataset)
        url = urlparse.urlunparse((scheme, netloc, path, params, query, fragment))

        json_response = self.api_v2_get(url, params)
        data = []
        for entry in json_response.get('facets') or []:
            name = entry.get('name')
            if name:
                data.append({'id': slugify(name), 'text': name})
        return {'data': data}

    @endpoint(
        description=_('Available facet values'),
        name='facet-values',
        parameters={
            'dataset': {'description': _('Identifier of the dataset to be queried')},
            'facet': {'description': _('Identifier of the facet')},
        },
    )
    def facet_values(self, request, dataset, facet):
        cutted_dataset = self.cut_first(dataset, '/')
        scheme, netloc, path, params, query, fragment = urlparse.urlparse(self.service_url)
        path = urlparse.urljoin(path, 'api/explore/v2.1/catalog/datasets/%s/facets/' % cutted_dataset)
        url = urlparse.urlunparse((scheme, netloc, path, params, query, fragment))

        json_response = self.api_v2_get(url, params)
        data = []
        for entry in json_response.get('facets') or []:
            name = entry.get('name')
            if name != facet:
                continue
            for sub_entry in entry.get('facets') or []:
                value = sub_entry.get('name')
                if value:
                    data.append({'id': slugify(value), 'text': value})
            break
        else:
            raise APIError('no "%s" facet on "%s" dataset' % (facet, dataset))
        return {'data': data}

    @endpoint(
        name='q',
        description=_('Query'),
        pattern=r'^(?P<query_slug>[\w:_-]+)/$',
        show=False,
    )
    def q(self, request, query_slug, **kwargs):
        query = get_object_or_404(Query, resource=self, slug=query_slug)
        return query.q(request, **kwargs)

    def create_query_url(self):
        return reverse('opendatasoft-query-new', kwargs={'slug': self.slug})


class Query(BaseQuery):
    Q_FILTERS = [
        ('search', _('Case insensitive fuzzy and prefix search (search)')),
        ('suggest', _('Case insensitive search beginning with the searched string (suggest)')),
        (
            'startswith',
            _(
                'Case sensitive search, on the whole string, without splitting it by spaces and forming terms before (startswith)'
            ),
        ),
    ]
    OUTPUTS = [
        ('json', _('Format outputs as JSON (default)')),
        ('geojson', _('Format outputs as GeoJSON')),
    ]
    resource = models.ForeignKey(
        to=OpenDataSoft, related_name='queries', verbose_name=_('Resource'), on_delete=models.CASCADE
    )
    dataset = models.CharField(
        _('Dataset'),
        max_length=128,
        blank=False,
        help_text=_('Dataset to query'),
    )
    text_template = models.TextField(
        verbose_name=_('Text template'),
        help_text=_("Use Django's template syntax. Attributes can be accessed through {{ attributes.name }}"),
        validators=[validate_template],
        blank=True,
    )
    q_filter = models.CharField(
        verbose_name=_('Full text searches function to use (API v2)'),
        choices=Q_FILTERS,
        default=OpenDataSoft.DEFAULT_Q_FILTER,
        max_length=10,
        blank=False,
    )
    geo_point = models.CharField(
        verbose_name=_('Geo point 2D (API v2)'),
        help_text=_('Geographic point field in the dataset'),
        max_length=256,
        blank=True,
    )
    lat = models.FloatField(
        verbose_name=_('Latitude (API v2)'),
        help_text=_('Geographic limitation and priority'),
        null=True,
        blank=True,
    )
    lon = models.FloatField(
        verbose_name=_('Longitude (API v2)'),
        help_text=_('Geographic limitation and priority'),
        null=True,
        blank=True,
    )
    dist = models.CharField(
        verbose_name=_('Distance (API v2)'),
        help_text=_('Only include results inside lat/lon/dist circle. Example: 2km'),
        max_length=128,
        default=OpenDataSoft.DEFAULT_DIST,
        blank=True,
    )
    output = models.CharField(
        verbose_name=_('Output format (API v2)'),
        choices=OUTPUTS,
        default=OpenDataSoft.DEFAULT_OUTPUT,
        max_length=10,
        blank=False,
    )
    select = models.TextField(
        verbose_name=_('Select (API v2)'),
        help_text=_('Select expression to add, remove or change the fields to return'),
        blank=True,
    )
    where = models.TextField(
        verbose_name=_('Where (API v2)'),
        help_text=_('Filter expression including logical operations and functions'),
        blank=True,
    )
    group_by = models.TextField(
        verbose_name=_('Group by (API v2)'),
        help_text=_('Expression to defines a grouping function for an aggregation'),
        blank=True,
    )
    order_by = models.CharField(
        verbose_name=_('Order by (API v2)'),
        help_text=_(
            'Comma-separated list of field names or aggregations to sort on, followed by an order (asc or desc)'
        ),
        max_length=256,
        blank=True,
    )
    refine = models.TextField(
        verbose_name=_('Refine (API v2)'),
        help_text=_('Facet filter used to limit the result set'),
        blank=True,
    )
    exclude = models.TextField(
        verbose_name=_('Exclude (API v2)'),
        help_text=_('Facet filter used to exclude a facet value from the result set'),
        blank=True,
    )
    lang = models.CharField(
        verbose_name=_('Lang (API v2)'),
        help_text=_('If specified, override the default language, which is "fr"'),
        max_length=256,
        blank=True,
    )
    timezone = models.CharField(
        verbose_name=_('Timezone (API v2)'),
        help_text=_('Set the timezone for datetime fields'),
        max_length=256,
        blank=True,
    )
    filter_expression = models.TextField(
        verbose_name=_('Filter (API v1)'),
        help_text=_('Specify refine and exclude facet expressions separated lines'),
        blank=True,
    )
    sort = models.CharField(
        verbose_name=_('Sort field (API v1)'),
        help_text=_(
            'Sorts results by the specified field. A minus sign - may be used to perform an ascending sort.'
        ),
        max_length=256,
        blank=True,
    )
    limit = models.PositiveIntegerField(
        default=10,
        verbose_name=_('Limit'),
        help_text=_('Number of results to return in a single call'),
    )

    delete_view = 'opendatasoft-query-delete'
    edit_view = 'opendatasoft-query-edit'

    def get_api_versions(self):
        api_to_use = set()
        for field in self._meta.fields:
            if getattr(self, field.name) and getattr(self, field.name) != field.default:
                if '(API v1)' in field.verbose_name:
                    api_to_use.add('v1')
                if '(API v2)' in field.verbose_name:
                    api_to_use.add('v2')
        return list(api_to_use)

    def clean(self, *args, **kwargs):
        if len(self.get_api_versions()) > 1:
            raise ValidationError(_('You cannot mix parameters of several API versions.'))
        return super().clean(*args, **kwargs)

    def q(self, request, **kwargs):
        api_to_use = self.get_api_versions()
        if len(api_to_use) > 1:
            raise APIError('Query object should not provide both API v1 and v2 values')
        api_to_use = api_to_use[0] if api_to_use else 'v2'

        if api_to_use == 'v1':
            return self.resource.call_search(
                dataset=self.dataset,
                text_template=self.text_template,
                filter_expression='&'.join(
                    [x.strip() for x in str(self.filter_expression).splitlines() if x.strip()]
                ),
                sort=self.sort,
                limit=self.limit,
                id=kwargs.get('id'),
                q=kwargs.get('q'),
            )
        else:

            def strip_and_join(text):
                return ' '.join([x.strip() for x in str(text).splitlines() if x.strip()])

            def get_value(key):
                '''get configurated value if not overloaded by call'''
                value = kwargs.get(key, None)
                if value is None and key not in ['id', 'q']:
                    value = getattr(self, key, None)
                if value is None:
                    return None
                else:
                    return strip_and_join(value)

            params = {}
            for key in self.resource.records.endpoint_info.parameters:
                value = get_value(key)
                if value:
                    params[key] = value
            return self.resource.call_records(**params)

    def as_endpoint(self):
        endpoint = super().as_endpoint(path=self.resource.q.endpoint_info.name)
        api_to_use = self.get_api_versions()
        api_to_use = api_to_use[0] if len(api_to_use) == 1 else 'v2'
        if api_to_use == 'v1':
            basic_endpoint = self.resource.search.endpoint_info
        else:
            basic_endpoint = self.resource.records.endpoint_info
        endpoint.func = basic_endpoint.func
        endpoint.show_undocumented_params = False

        # Copy generic params descriptions from original endpoint
        # if they are not overloaded by the query
        if api_to_use == 'v1':
            for param in ('q', 'id'):
                endpoint.parameters[param] = basic_endpoint.parameters[param]
        else:
            for param in basic_endpoint.parameters:
                if (
                    param in ('q', 'id')
                    or not hasattr(Query, param)
                    or not getattr(self, param)
                    or not getattr(self, param) != getattr(Query, param).field.default
                ):
                    endpoint.parameters[param] = basic_endpoint.parameters[param]

        return endpoint
