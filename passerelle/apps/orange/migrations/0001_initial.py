from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('base', '0002_auto_20151009_0326'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrangeSMSGateway',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(verbose_name='Title', max_length=50)),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'keystore',
                    models.FileField(
                        help_text='Certificate and private key in PEM format',
                        upload_to=b'orange',
                        null=True,
                        verbose_name='Keystore',
                        blank=True,
                    ),
                ),
                (
                    'users',
                    models.ManyToManyField(
                        to='base.ApiUser',
                        related_name='+',
                        related_query_name='+',
                        blank=True,
                    ),
                ),
            ],
            options={
                'db_table': 'sms_orange',
                'verbose_name': 'Orange',
            },
            bases=(models.Model,),
        ),
    ]
