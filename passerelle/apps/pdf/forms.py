# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.urls import reverse
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _

from passerelle.utils.forms import ConditionField, TemplateField
from passerelle.utils.pdf import PDF

from . import models


class FieldsMappingEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.fill_form_file:
            return
        fields_mapping = self.instance.fields_mapping or {}
        with self.instance.fill_form_file as fd:
            pdf = PDF(fd)
        for page in pdf.pages:
            for i, field in enumerate(page.fields):
                name = f'field_{field.digest_id}'
                if field.widget_type == 'checkbox':
                    help_text = _('boolean expression')
                    field_class = ConditionField
                elif field.widget_type == 'text':
                    help_text = _('text template')
                    field_class = TemplateField
                elif field.widget_type == 'radio':
                    values = ', '.join('"%s"' % value for value in field.radio_possible_values)
                    help_text = _('text template, possibles values %s') % values
                    field_class = TemplateField
                elif field.widget_type in ('list', 'combo'):
                    ds_url = (
                        reverse(
                            'generic-endpoint',
                            kwargs={
                                'connector': 'pdf',
                                'endpoint': 'field-values',
                                'slug': self.instance.slug,
                            },
                        )
                        + '?digest_id='
                        + field.digest_id
                    )
                    help_text = mark_safe(
                        _('text template, possibles values <a href="%s">data source</a>') % ds_url
                    )
                    field_class = TemplateField
                else:
                    continue
                label = _('field {number}').format(number=i + 1)
                initial = fields_mapping.get(name, '')
                self.fields[name] = field_class(
                    label=label, required=False, initial=initial, help_text=help_text
                )
                self.fields[name].page_number = page.page_number
                self.fields[name].widget.attrs['tabindex'] = '0'
                self.fields[name].widget.attrs['class'] = '0'

    def save(self, commit=True):
        fields_mapping = {}
        for name in self.fields:
            value = self.cleaned_data.get(name)
            if value:
                fields_mapping[name] = value
        self.instance.fields_mapping = fields_mapping
        return super().save(commit=commit)

    class Meta:
        model = models.Resource
        fields = ()
