# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import io

import PIL.Image
import PIL.ImageDraw
from django.http import Http404, HttpResponse, HttpResponseNotModified
from django.utils.translation import gettext_lazy as _
from django.views.generic import UpdateView

from passerelle.base.views import ResourceView
from passerelle.utils.pdf import PDF

from . import forms, models


class FieldsMappingEditView(ResourceView, UpdateView):
    template_name = 'pdf/fields_mapping_edit.html'
    model = models.Resource
    form_class = forms.FieldsMappingEditForm

    def get(self, request, *args, **kwargs):
        resource = self.get_object()
        if not resource.fill_form_file:
            raise Http404
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        resource = self.get_object()
        with resource.fill_form_file as fd:
            pdf = PDF(fd)
        pages = []
        for page in pdf.pages:
            pages.append((page.page_number, page.fields_image_map(id_prefix='id_field_', id_suffix='_p')))
        context_data['pages'] = pages
        return context_data

    def get_success_url(self):
        return super().get_success_url() + '#'


class PageThumbnailView(ResourceView):
    model = models.Resource

    def make_thumbnail(self, page):
        # produce a thumbnail and add
        # * red rectangle over field's rectangles
        # * enumerated field names
        thumbnail = page.thumbnail_png()
        image = PIL.Image.open(io.BytesIO(thumbnail))
        draw = PIL.ImageDraw.Draw(image, 'RGBA')
        for i, field, area_rect in page.thumbnail_field_rects():
            draw.rectangle(area_rect, fill=(255, 0, 0, 50))
            x = area_rect.x1
            y = (area_rect.y1 + area_rect.y2) / 2 - 5
            if field.widget_type == 'checkbox':
                y -= 10
            draw.text((x, y), str(_('field %s') % (i + 1)), anchor='lb', fill=(0, 0, 0, 255))
        del draw
        output = io.BytesIO()
        image.save(output, 'PNG')
        return output.getvalue()

    def get(self, request, page_number, **kwargs):
        with self.get_object().fill_form_file as fd:
            pdf_content = fd.read()

        etag = hashlib.md5(pdf_content).hexdigest()
        if_none_match = request.headers.get('If-None-Match', '').split(',')
        if etag in if_none_match:
            # use browser cache
            response = HttpResponseNotModified()
        else:
            # produce the thumbnail
            pdf = PDF(pdf_content)
            try:
                page = pdf.page(int(page_number))
            except IndexError:
                raise Http404
            thumbnail_content = self.make_thumbnail(page)
            response = HttpResponse(thumbnail_content, content_type='image/png')
        response['ETag'] = etag
        response['Cache-Control'] = 'max-age=3600'
        return response
