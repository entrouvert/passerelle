# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

import pytest

from passerelle.contrib.toulouse_axel.utils import (
    get_booking,
    get_reference_year_from_date,
    get_week_dates_from_date,
)
from passerelle.contrib.utils.axel import json_date_format


@pytest.mark.parametrize(
    'value, expected',
    [
        ('0', False),
        ('1', True),
        ('2', None),
        ('foo', False),
    ],
)
def test_get_booking(value, expected):
    assert get_booking(value) is expected


@pytest.mark.parametrize(
    'value, expected',
    [
        ('2020-01-01', 2019),
        ('2020-07-31', 2019),
        ('2020-08-01', 2020),
        ('2020-12-31', 2020),
    ],
)
def test_get_reference_year_from_date(value, expected):
    in_date = datetime.datetime.strptime(value, json_date_format)
    assert get_reference_year_from_date(in_date) == expected


@pytest.mark.parametrize(
    'in_value, start_value, end_value',
    [
        ('2020-01-06', '2020-01-06', '2020-01-10'),  # monday
        ('2020-01-07', '2020-01-06', '2020-01-10'),  # tuesday
        ('2020-01-08', '2020-01-06', '2020-01-10'),  # wednesday
        ('2020-01-09', '2020-01-06', '2020-01-10'),  # thursday
        ('2020-01-10', '2020-01-06', '2020-01-10'),  # friday
        ('2020-01-11', '2020-01-06', '2020-01-10'),  # saturday
        ('2020-01-12', '2020-01-06', '2020-01-10'),  # sunday
    ],
)
def test_get_week_dates_from_date(in_value, start_value, end_value):
    in_date = datetime.datetime.strptime(in_value, json_date_format)
    start_date = datetime.datetime.strptime(start_value, json_date_format)
    end_date = datetime.datetime.strptime(end_value, json_date_format)
    assert get_week_dates_from_date(in_date) == (start_date, end_date)
