import copy
import json
import os
from decimal import Decimal

import pytest
import responses
from django.contrib.contenttypes.models import ContentType

from passerelle.base.models import AccessRight, ApiUser
from passerelle.contrib.solis_apa import integration
from passerelle.contrib.solis_apa.models import SolisAPA

TEST_BASE_DIR = os.path.join(os.path.dirname(__file__), 'data', 'solis_apa')


def json_get_data(filename):
    with open(os.path.join(TEST_BASE_DIR, filename)) as fd:
        return json.load(fd)


@pytest.fixture(autouse=True)
def setup(db):
    api = ApiUser.objects.create(username='all', keytype='', key='')
    solis = SolisAPA.objects.create(base_url='https://whateever.com/rec/', slug='test')
    obj_type = ContentType.objects.get_for_model(solis)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=solis.pk
    )


@pytest.fixture(
    params=[
        json_get_data('premiere_demande_apa_domicile.json'),
        json_get_data('premiere_demande_apa_etablissement.json'),
        json_get_data('premiere_demande_apa_etablissement_papier.json'),
    ]
)
def formdata(request):
    return request.param


@responses.activate
def test_suivi_error(app):
    responses.post(
        'https://whateever.com/rec/exportFlow?flow=ExportSuiviVisite&application=AsgTeleprocedureApa14',
        status=500,
    )
    resp = app.get('/solis-apa/test/suivi/visite/')
    assert resp.json['err_desc'] == 'suivi visite ws: error code 500'
    assert resp.json['err'] == 1


@responses.activate
def test_communes(app):
    fake_response = {
        'ReferentialOutputWS': {
            'Entries': {
                '@count': '718',
                '@name': 'commune',
                'Entry': [
                    {
                        'id': 'commune-14-1',
                        'value': 'ABLON',
                        'Keys': {
                            'Key': [
                                {'id': 'stdr.commune.code_a_com', 'value': 1},
                                {'id': 'stdr.commune.code_dep', 'value': 14},
                            ]
                        },
                        'Attributes': {
                            'Attribute': [
                                {'id': 'stdr.commune.nom_com', 'value': 'ABLON'},
                                {'id': 'stdr.commune.code_a_com', 'value': 1},
                                {'id': 'stdr.commune.code_dep', 'value': 14},
                                {'id': 'stdr.commune.cp_lieu', 'value': 14600},
                            ]
                        },
                        'ParentsEntries': '',
                    },
                    {
                        'id': 'commune-14-2',
                        'value': 'ACQUEVILLE',
                        'Keys': {
                            'Key': [
                                {'id': 'stdr.commune.code_a_com', 'value': 2},
                                {'id': 'stdr.commune.code_dep', 'value': 14},
                            ]
                        },
                        'Attributes': {
                            'Attribute': [
                                {'id': 'stdr.commune.nom_com', 'value': 'ACQUEVILLE'},
                                {'id': 'stdr.commune.code_a_com', 'value': 2},
                                {'id': 'stdr.commune.code_dep', 'value': 14},
                                {'id': 'stdr.commune.cp_lieu', 'value': 14220},
                            ]
                        },
                        'ParentsEntries': '',
                    },
                ],
            }
        }
    }

    responses.post(
        'https://whateever.com/rec/referential?referential=commune',
        json=fake_response,
    )

    resp = app.get('/solis-apa/test/communes?code_dep=14', status=200)
    assert resp.json['err'] == 0
    assert len(resp.json['data']) == 2
    assert resp.json['data'][1]['id'] == 'commune-14-2'
    assert resp.json['data'][1]['text'] == '14220 ACQUEVILLE'

    # jsonp call
    resp = app.get(
        '/solis-apa/test/communes?code_dep=14&callback=jQuery&q=14&page_limit=50&_=1234', status=200
    )

    assert resp.content_type == 'application/javascript'
    assert resp.text.startswith('jQuery({')
    assert json.loads(resp.text[7:-2]) == {
        'data': [
            {'id': 'commune-14-1', 'text': '14600 ABLON'},
            {'id': 'commune-14-2', 'text': '14220 ACQUEVILLE'},
        ],
        'err': 0,
    }


@responses.activate
def test_referential(app):
    fake_response = {
        'ReferentialOutputWS': {
            'Entries': {
                '@count': '3',
                '@name': 'organisme de tutelle',
                'Entry': [
                    {
                        'id': 'organisme_tuteur-3',
                        'value': 'CENTRE HOSPITALIER DE BAYEUX',
                        'Keys': {'Key': {'id': 'civi.organisme_tuteur.code_organisme_tuteur', 'value': 3}},
                        'ParentsEntries': '',
                    },
                    {
                        'id': 'organisme_tuteur-7',
                        'value': 'ASSOCIATION TUTELAIRE DU CALVADOS - ATC',
                        'Keys': {'Key': {'id': 'civi.organisme_tuteur.code_organisme_tuteur', 'value': 7}},
                        'ParentsEntries': '',
                    },
                    {
                        'id': 'organisme_tuteur-2',
                        'value': 'UDAF DU CALVADOS',
                        'Keys': {'Key': {'id': 'civi.organisme_tuteur.code_organisme_tuteur', 'value': 2}},
                        'ParentsEntries': '',
                    },
                ],
            }
        }
    }

    responses.post(
        'https://whateever.com/rec/referential?referential=organisme%20de%20tutelle',
        json=fake_response,
    )

    resp = app.get('/solis-apa/test/referential/organisme-de-tutelle/', status=200)
    assert resp.json['err'] == 0
    assert len(resp.json['data']) == 3
    assert resp.json['data'][1]['id'] == 'organisme_tuteur-7'
    assert resp.json['data'][1]['text'] == 'ASSOCIATION TUTELAIRE DU CALVADOS - ATC'


@responses.activate
def test_instegration_demande_apa_domicile(app):
    fake_response = {
        'ImportIdResults': {
            'Items': [
                {'key': 'indexDossier', 'value': 359043},
                {'key': 'indexBeneficiaire', 'value': 458238},
                {'key': 'indexDemande', 'value': 221155},
            ]
        }
    }

    responses.post(
        'https://whateever.com/rec/importFlow?flow=ImportIntegrationDemande&application=AsgTeleprocedureApa14',
        json=fake_response,
    )

    params = json_get_data('premiere_demande_apa_domicile.json')
    resp = app.post_json('/solis-apa/test/integration/', params=params, status=200)
    assert resp.json['data']['indexDossier'] == 359043
    assert resp.json['data']['indexBeneficiaire'] == 458238
    assert resp.json['data']['indexDemande'] == 221155

    params['fields']['anneerefrevenu'] = None
    resp = app.post_json('/solis-apa/test/integration/', params=params, status=200)
    assert resp.json['data']['indexDossier'] == 359043
    assert resp.json['data']['indexBeneficiaire'] == 458238
    assert resp.json['data']['indexDemande'] == 221155


@responses.activate
def test_integration_demande_apa_etablissement(app):
    fake_response = {
        'ImportIdResults': {
            'Items': [
                {'key': 'indexDossier', 'value': 359043},
                {'key': 'indexBeneficiaire', 'value': 458238},
                {'key': 'indexDemande', 'value': 221155},
            ]
        }
    }
    responses.post(
        'https://whateever.com/rec/importFlow?flow=ImportIntegrationDemande&application=AsgTeleprocedureApa14',
        json=fake_response,
    )

    resp = app.post_json(
        '/solis-apa/test/integration/',
        params=json_get_data('premiere_demande_apa_etablissement.json'),
        status=200,
    )

    assert resp.json['data']['indexDossier'] == 359043
    assert resp.json['data']['indexBeneficiaire'] == 458238
    assert resp.json['data']['indexDemande'] == 221155


@responses.activate
def test_integration_error(app):
    responses.post(
        'https://whateever.com/rec/importFlow?flow=ImportIntegrationDemande&application=AsgTeleprocedureApa14',
        status=500,
    )
    resp = app.post_json(
        '/solis-apa/test/integration/', params=json_get_data('premiere_demande_apa_etablissement.json')
    )
    assert resp.json['err_desc'] == 'integration ws: error code 500'
    assert resp.json['err'] == 1


def test_get_conjoint(formdata):
    data = integration.build_message(formdata)
    if formdata['display_id'] == '25-3':
        assert data['Conjoint']['indexIndividu'] == '458107'
        assert data['Conjoint']['bParticipeRevenus'] is False
        assert data['Conjoint']['dateNaissance'] == '1930-06-11'
        assert data['Conjoint']['nom'] == 'MCBEAL'
        assert data['Conjoint']['prenom'] == 'ALLY'
        assert data['Conjoint']['sexe'] == 'F'
        assert data['Conjoint']['situationFamiliale'] == 2
        assert data['RevenusImposition']['anneeReference'] == 2015
        assert Decimal(data['RevenusImposition']['revenuReference']) == Decimal(30000.0)
    elif formdata['display_id'] == '32-1':
        assert 'Conjoint' not in data
        assert data['RevenusImposition']['anneeReference'] == 2015
        assert Decimal(data['RevenusImposition']['revenuReference']) == Decimal(32000.0)
    else:
        assert 'Conjoint' not in data
        assert data['RevenusImposition']['anneeReference'] == 2016
        assert Decimal(data['RevenusImposition']['revenuReference']) == Decimal(1500)


def test_recipient_address(formdata):
    data = integration.build_message(formdata)
    if formdata['display_id'] == '25-3':
        # received values
        assert formdata['workflow']['data']['adresse_var_code_commune'] == '14000 CAEN'
        assert formdata['workflow']['data']['adresse_var_code_commune_raw'] == 'commune-14-118'
        assert formdata['workflow']['data']['adresse_var_code_lieu'] == 'AVENUE DU PARC BELLEVUE'
        assert formdata['workflow']['data']['adresse_var_code_lieu_raw'] == '0867'
        assert formdata['workflow']['data']['adresse_var_num_lieu'] == '1'
        assert formdata['fields']['code_commune'] == '14000 CAEN'
        assert formdata['fields']['code_commune_raw'] == 'commune-14-118'
        # built values
        assert data['Beneficiaire']['Adresse']['codeCommune'] == 118
        assert data['Beneficiaire']['Adresse']['codeDepartement'] == 14
        assert data['Beneficiaire']['Adresse']['codeLieu'] == '0867'
        assert data['Beneficiaire']['Adresse']['numeroLieu'] == '1'
    elif formdata['display_id'] == '32-1':
        # received values
        assert formdata['workflow']['data']['adresse_var_code_commune'] == '14930 ETERVILLE'
        assert formdata['workflow']['data']['adresse_var_code_commune_raw'] == 'commune-14-254'
        assert formdata['workflow']['data']['adresse_var_code_lieu'] == 'RUE DU JARDIN DE LA FERME'
        assert formdata['workflow']['data']['adresse_var_code_lieu_raw'] == 'Z001'
        assert formdata['workflow']['data']['adresse_var_num_lieu'] == '1'
        assert formdata['fields']['code_commune'] == '14930 ETERVILLE'
        assert formdata['fields']['code_commune_raw'] == 'commune-14-254'
        # built values
        assert data['Beneficiaire']['Adresse']['codeCommune'] == 254
        assert data['Beneficiaire']['Adresse']['codeDepartement'] == 14
        assert data['Beneficiaire']['Adresse']['codeLieu'] == 'Z001'
        assert data['Beneficiaire']['Adresse']['numeroLieu'] == '1'
    else:
        # received values
        assert formdata['fields']['papier_code_commune'] == '33370 FARGUES ST HILAIRE'
        assert formdata['fields']['papier_code_commune_raw'] == 'commune-33-165'
        assert formdata['fields']['papier_code_lieu'] == 'ROUTE DE MAISON ROUGE'
        assert formdata['fields']['papier_code_lieu_raw'] == '0001'
        assert formdata['fields']['papier_excode_commune'] == '14150 OUISTREHAM'
        assert formdata['fields']['papier_excode_commune_raw'] == 'commune-14-488'
        assert formdata['fields']['papier_excode_lieu'] == 'RUE DU PETIT BONHEUR'
        assert formdata['fields']['papier_excode_lieu_raw'] == '0730'
        assert formdata['fields']['papier_exnum_lieu'] == '2'
        assert formdata['fields']['papier_num_lieu'] == '20'
        # built values
        assert data['Beneficiaire']['Adresse']['codeCommune'] == 488
        assert data['Beneficiaire']['Adresse']['codeDepartement'] == 14
        assert data['Beneficiaire']['Adresse']['codeLieu'] == '0730'
        assert data['Beneficiaire']['Adresse']['numeroLieu'] == '2'


def test_missing_field(formdata):
    data = integration.build_message(formdata)
    assert 'lieuNaissance' in data['Beneficiaire']
    partial_formdata = copy.deepcopy(formdata)
    partial_formdata['fields'].pop('lieunbeneficiaire')
    data = integration.build_message(partial_formdata)
    assert 'lieuNaissance' not in data['Beneficiaire']
