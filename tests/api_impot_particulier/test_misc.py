# Passerelle

import re
from unittest import mock

import pytest
import requests
import responses
from django.core.cache import cache

from passerelle.apps.api_impot_particulier.models import Resource
from passerelle.utils.jsonresponse import APIError

from ..utils import make_resource

last_year = 2023


@responses.activate
def test_get_access_token_with_scope(resource):
    cache.clear()

    resource.api_url = 'https://dgfip/'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'
    resource.oauth_scope = 'scopes'

    auth = resource.make_requests_auth(resource.requests)

    responses.add(responses.POST, 'https://dgfip/token', json={'access_token': '1234'})
    assert auth.fetch_token_header() == 'Bearer 1234'
    assert len(responses.calls) == 1
    request = responses.calls[0].request
    assert request.body == 'grant_type=client_credentials&scope=scope'
    assert request.headers['authorization'] == 'Basic dXNlcjpwYXNz'


@responses.activate
def test_get_access_token_without_scope(resource):
    cache.clear()

    resource.api_url = 'https://dgfip/'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'
    resource.oauth_scopes = ''

    auth = resource.make_requests_auth(resource.requests)

    responses.add(responses.POST, 'https://dgfip/token', json={'access_token': '1234'})
    assert auth.fetch_token_header() == 'Bearer 1234'
    assert len(responses.calls) == 1
    assert responses.calls[0].request.body == 'grant_type=client_credentials'


@responses.activate
def test_get_access_token_connection_timeout(resource):
    cache.clear()

    resource.api_url = 'https://dgfip/'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'

    auth = resource.make_requests_auth(resource.requests)

    responses.add(responses.POST, 'https://dgfip/token', body=requests.ConnectTimeout())
    with pytest.raises(APIError, match=r'.*is unavailable'):
        auth.fetch_token_header()


@responses.activate
def test_get_tax_data(resource):
    cache.clear()

    resource.api_url = 'https://dgfip/impotparticulier/1.0'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'

    responses.add(responses.POST, 'https://dgfip/token', json={'access_token': '1234'})
    responses.add(responses.GET, 'https://dgfip/impotparticulier/1.0/123/truc/xyz/', json={'data': 1})

    assert resource.get_tax_data(
        correlation_id='abcd',
        endpoint_template='{var1}/truc/{var2}/',
        var1='123',
        var2='xyz',
        accept='application/prs.dgfip.part.situations.ir.assiettes.v1+json',
    ) == {'data': 1}

    assert len(responses.calls) == 2
    request = responses.calls[1].request
    assert request.headers['authorization'] == 'Bearer 1234'
    assert request.headers['x-correlation-id'] == 'abcd'
    assert request.headers['Accept'] == 'application/prs.dgfip.part.situations.ir.assiettes.v1+json'


@responses.activate
def test_get_tax_data_connection_timeout(resource):
    cache.clear()

    resource.api_url = 'https://dgfip/impotparticulier/1.0'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'

    responses.add(responses.POST, 'https://dgfip/token', json={'access_token': '1234'})
    responses.add(
        responses.GET, 'https://dgfip/impotparticulier/1.0/123/truc/xyz/', body=requests.ConnectTimeout()
    )

    with pytest.raises(APIError, match=r'.*is unavailable'):
        assert resource.get_tax_data(
            correlation_id='abcd',
            endpoint_template='{var1}/truc/{var2}/',
            var1='123',
            var2='xyz',
            accept='application/prs.dgfip.part.situations.ir.assiettes.v1+json',
        ) == {'data': 1}


@responses.activate
@pytest.mark.parametrize(
    'responses_kwargs,response',
    [
        ({'json': {'erreur': {'message': 'foobar'}}, 'status': 403}, {'message': 'foobar'}),
        ({'json': {'erreur': {'message': 'foobar'}}, 'status': 204}, {'message': 'foobar'}),
        ({'headers': {'codeapp': 'foobar'}, 'status': 403}, {'codeapp': 'foobar'}),
        ({'headers': {'codeapp': 'foobar'}, 'status': 204}, {'codeapp': 'foobar'}),
    ],
)
def test_get_tax_data_errors(responses_kwargs, response, resource):
    cache.clear()

    resource.api_url = 'https://dgfip/impotparticulier/1.0'
    resource.oauth_username = 'user'
    resource.oauth_password = 'pass'

    responses.add(responses.POST, 'https://dgfip/token', json={'access_token': '1234'})
    responses.add(
        responses.GET,
        'https://dgfip/impotparticulier/1.0/123/truc/xyz/',
        **responses_kwargs,
    )

    with pytest.raises(APIError) as ei:
        resource.get_tax_data(
            correlation_id='abcd',
            endpoint_template='{var1}/truc/{var2}/',
            var1='123',
            var2='xyz',
            accept='application/prs.dgfip.part.situations.ir.assiettes.v1+json',
        )
    assert ei.value.data == response


@pytest.fixture
def resource(db):
    return make_resource(
        Resource,
        slug='test',
        title='API Impot Particulier',
        description='API Impot Particulier',
        oauth_username='user',
        oauth_password='pass',
        oauth_scopes='scope',
    )


class TestGetMethods:
    @pytest.fixture(autouse=True)
    def setup(self):
        with responses._default_mock:
            with mock.patch('passerelle.apps.api_impot_particulier.models.uuid') as uuid_mock:
                uuid_mock.uuid4.return_value.hex = 'abcd'
                responses.add(
                    responses.POST, 'https://gw.dgfip.finances.gouv.fr/token', json={'access_token': '1234'}
                )
                yield None

    def test_spi_situations_ir_assiettes_annrev(self, resource):
        responses.add(
            responses.GET,
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/ir/assiettes/annrev/2023',
            json={'data': 2},
        )
        assert resource.get_spi_situations_ir_assiettes_annrev(
            numero_fiscal='1234', annee_de_revenu='2023'
        ) == {'data': 2}
        assert len(responses.calls) <= 2
        if len(responses.calls) == 2:
            request = responses.calls[0].request
            assert request.body == 'grant_type=client_credentials&scope=scope'
            assert request.headers['authorization'] == 'Basic dXNlcjpwYXNz'
        request = responses.calls[-1].request
        assert request.headers['authorization'] == 'Bearer 1234'
        assert request.headers['x-correlation-id'] == 'abcd'
        assert request.headers['Accept'] == 'application/prs.dgfip.part.situations.ir.assiettes.v1+json'

    def test_spi_situations_th_assiettes_principale_annrev(self, resource):
        responses.add(
            responses.GET,
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/th/assiettes/principale/annrev/2023',
            json={'data': 2},
        )
        assert resource.get_spi_situations_th_assiettes_principale_annrev(
            numero_fiscal='1234', annee_de_revenu='2023'
        ) == {'data': 2}
        assert len(responses.calls) <= 2
        if len(responses.calls) == 2:
            request = responses.calls[0].request
            assert request.body == 'grant_type=client_credentials&scope=scope'
            assert request.headers['authorization'] == 'Basic dXNlcjpwYXNz'
        request = responses.calls[-1].request
        assert request.headers['authorization'] == 'Bearer 1234'
        assert request.headers['x-correlation-id'] == 'abcd'
        assert request.headers['Accept'] == 'application/prs.dgfip.part.situations.th.assiettes.v1+json'

    def test_spi_situations_connection_error(self, resource):
        responses.add(
            responses.GET,
            re.compile('https://gw.dgfip.finances.gouv.fr/.*'),
            body=requests.ConnectTimeout(),
        )
        with pytest.raises(APIError, match=r'.*is unavailable'):
            resource.get_spi_situations_ir_assiettes_annrev(numero_fiscal='1234', annee_de_revenu='2023')

    def test_logging_success(self, resource, app, caplog):
        resource.set_log_level('DEBUG')
        responses.add(
            responses.GET,
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/ir/assiettes/annrev/2023',
            json={'foo': 'bar'},
        )
        response = app.get(
            '/api-impot-particulier/test/spi-situations-ir-assiettes-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': last_year},
        )
        assert response.json == {'data': {'foo': 'bar'}, 'err': 0}
        assert [(x.levelname, x.message[:20] + ' ... ' + x.message[-20:]) for x in caplog.records] == [
            ('INFO', "endpoint GET /api-im ... de_revenu=2023 ('') "),
            ('INFO', 'POST https://gw.dgfi ... uv.fr/token (=> 200)'),
            ('INFO', 'GET https://gw.dgfip ... annrev/2023 (=> 200)'),
            ('DEBUG', 'spi-situations-ir-as ... tes.v1+json) success'),
            ('DEBUG', "endpoint GET /api-im ... de_revenu=2023 ('') "),
        ]

    def test_logging_failure(self, resource, app, caplog):
        resource.set_log_level('DEBUG')
        responses.add(
            responses.GET,
            re.compile('https://gw.dgfip.finances.gouv.fr/.*'),
            body=requests.ConnectTimeout(),
        )
        response = app.get(
            '/api-impot-particulier/test/spi-situations-ir-assiettes-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': last_year},
        )
        assert response.json['err'] == 1
        assert [(x.levelname, x.message[:20] + ' ... ' + x.message[-20:]) for x in caplog.records] == [
            ('INFO', "endpoint GET /api-im ... de_revenu=2023 ('') "),
            ('INFO', 'POST https://gw.dgfi ... uv.fr/token (=> 200)'),
            ('INFO', 'GET https://gw.dgfip ... => ConnectTimeout())'),
            ('WARNING', 'spi-situations-ir-as ... ice is unavailable: '),
            ('WARNING', 'Error occurred while ... e processing request'),
        ]


class TestEndpoints:
    @pytest.fixture(autouse=True)
    def setup(self):
        with responses._default_mock:
            with mock.patch('passerelle.apps.api_impot_particulier.models.uuid') as uuid_mock:
                uuid_mock.uuid4.return_value.hex = 'abcd'
                responses.add(
                    responses.POST, 'https://gw.dgfip.finances.gouv.fr/token', json={'access_token': '1234'}
                )
                yield None

    @mock.patch(
        'passerelle.apps.api_impot_particulier.models.Resource.get_spi_situations_ir_assiettes_annrev'
    )
    def test_spi_situations_ir_assiettes_annrev(self, mock_get, app, resource):
        mock_get.return_value = 'ok'
        response = app.get(
            '/api-impot-particulier/test/spi-situations-ir-assiettes-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': last_year},
        )
        assert mock_get.call_args[1]['numero_fiscal'] == '1234'
        assert response.json == {'data': 'ok', 'err': 0}

    @mock.patch(
        'passerelle.apps.api_impot_particulier.models.Resource.get_spi_situations_ir_assiettes_annrev'
    )
    def test_spi_situations_ir_assiettes_annrev_strip(self, mock_get, app, resource):
        mock_get.return_value = 'ok'
        response = app.get(
            '/api-impot-particulier/test/spi-situations-ir-assiettes-annrev',
            params={'numero_fiscal': ' 1 2 3 4 ', 'annee_de_revenu': last_year},
        )
        assert mock_get.call_args[1]['numero_fiscal'] == '1234'
        assert response.json == {'data': 'ok', 'err': 0}

    def test_spi_situations_ir_assiettes_annrev_invalid(self, app, resource):
        response = app.get(
            '/api-impot-particulier/test/spi-situations-ir-assiettes-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': '2000'},
        )
        assert response.json['err'] == 1

    @mock.patch(
        'passerelle.apps.api_impot_particulier.models.Resource.get_spi_situations_th_assiettes_principale_annrev'
    )
    def test_spi_situations_th_assiettes_principale_annrev(self, mock_get, app, resource):
        mock_get.return_value = 'ok'
        response = app.get(
            '/api-impot-particulier/test/spi-situations-th-assiettes-principale-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': last_year},
        )
        assert mock_get.call_args[1]['numero_fiscal'] == '1234'
        assert response.json == {'data': 'ok', 'err': 0}


def test_oauth_token(app, resource, freezer):
    cache.clear()
    with responses.RequestsMock() as rsps:
        rsps.post('https://gw.dgfip.finances.gouv.fr/token', json={'access_token': '1234'})
        rsps.get(
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/th/assiettes/principale/annrev/2023',
            json={'data': 2},
        )
        resp = app.get(
            '/api-impot-particulier/test/spi-situations-th-assiettes-principale-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': '2023'},
        )
        assert resp.json == {'data': {'data': 2}, 'err': 0}

        # check the token request
        token_request = rsps.calls[0].request
        assert token_request.headers['Content-Type'] == 'application/x-www-form-urlencoded'
        assert token_request.headers['Authorization'] == 'Basic dXNlcjpwYXNz'
        assert token_request.body == f'grant_type=client_credentials&scope={resource.oauth_scopes}'
        # check that a token is sent on the API call
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer 1234'

    # check that the token is grabbed from the cache the second time
    with responses.RequestsMock() as rsps:
        rsps.get(
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/th/assiettes/principale/annrev/2023',
            json={'data': 2},
        )
        resp = app.get(
            '/api-impot-particulier/test/spi-situations-th-assiettes-principale-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': '2023'},
        )
        assert resp.json == {'data': {'data': 2}, 'err': 0}
        assert rsps.calls[0].request.headers['Authorization'] == 'Bearer 1234'

    # move time forward to invalidate token in cache and check that a new token is grabbed
    freezer.tick(302)
    with responses.RequestsMock() as rsps:
        rsps.post('https://gw.dgfip.finances.gouv.fr/token', json={'access_token': '6578'})
        rsps.get(
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/th/assiettes/principale/annrev/2023',
            json={'data': 2},
        )
        resp = app.get(
            '/api-impot-particulier/test/spi-situations-th-assiettes-principale-annrev',
            params={'numero_fiscal': '1234', 'annee_de_revenu': '2023'},
        )
        assert resp.json == {'data': {'data': 2}, 'err': 0}

        # check that a token is sent on the API call
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer 6578'


def test_oauth_token_cache(app, resource):
    cache.clear()
    with responses.RequestsMock() as rsps:
        rsps.post('https://gw.dgfip.finances.gouv.fr/token', json={'access_token': '1234'})
        rsps.get(
            'https://gw.dgfip.finances.gouv.fr/impotparticulier/1.0/spi/1234/situations/th/assiettes/principale/annrev/2023',
            json={'data': 2},
        )
        with mock.patch('passerelle.utils.http_authenticators.cache') as cache_mock:
            attrs = {'get.return_value': None}
            cache_mock.configure_mock(**attrs)
            resp = app.get(
                '/api-impot-particulier/test/spi-situations-th-assiettes-principale-annrev',
                params={'numero_fiscal': '1234', 'annee_de_revenu': '2023'},
            )
            assert resp.json == {'data': {'data': 2}, 'err': 0}
            key = f'TokenAuth:{resource.pk}:c9fa3e5e86ce55e7b336fc52775cb1356fcb7e0132d82c369ddadf421ae05034'
            cache_mock.get.assert_called_once_with(key)
            cache_mock.set.assert_called_once_with(key, 'Bearer 1234', 300)
