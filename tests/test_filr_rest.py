import base64

import pytest
import responses
from django.contrib.contenttypes.models import ContentType

from passerelle.apps.filr_rest.models import Filr
from passerelle.base.models import AccessRight, ApiUser


@pytest.fixture()
def connector(db):
    api = ApiUser.objects.create(username='all', keytype='', key='')
    connector = Filr.objects.create(
        base_url='http://filr.invalid/',
        basic_auth_username='foo',
        basic_auth_password='bar',
        slug='filr',
    )
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )
    return connector


def test_upload(app, connector):
    params = {
        'file': {
            'filename': 'bla',
            'content': base64.b64encode(b'who what').decode(),
            'content_type': 'text/plain',
        },
        'root_folder_id': '1234',
        'folder_name': 'folder_foo',
    }
    with responses.RequestsMock() as rsps:
        rsps.get(
            'http://filr.invalid/rest/folders/1234/library_folders?title=folder_foo',
            status=200,
            json={'items': [{'title': 'folder_foo', 'id': 5678}]},
        )
        rsps.post(
            'http://filr.invalid/rest/folders/5678/library_files?file_name=bla',
            status=200,
            json={'id': '09c1c3fb530f562401531018f4270000'},
        )
        resp = app.post_json('/filr-rest/filr/upload', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == {
            'folder_id': '5678',
            'file_info': {'id': '09c1c3fb530f562401531018f4270000'},
        }


def test_upload_with_folder_creation(app, connector):
    params = {
        'file': {
            'filename': 'bla',
            'content': base64.b64encode(b'who what').decode(),
            'content_type': 'text/plain',
        },
        'root_folder_id': '1234',
        'folder_name': 'folder_foo',
    }
    with responses.RequestsMock() as rsps:
        rsps.get(
            'http://filr.invalid/rest/folders/1234/library_folders?title=folder_foo',
            status=200,
            json={'items': []},
        )
        rsps.post('http://filr.invalid/rest/folders/1234/library_folders', status=200, json={'id': 82})
        rsps.post(
            'http://filr.invalid/rest/folders/82/library_files?file_name=bla',
            status=200,
            json={'id': '09c1c3fb530f562401531018f4270000'},
        )
        resp = app.post_json('/filr-rest/filr/upload', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == {
            'folder_id': '82',
            'file_info': {'id': '09c1c3fb530f562401531018f4270000'},
        }


def test_share_folder(app, connector):
    params = {
        'folder_id': '1234',
        'emails/0': 'foo@example.net',
        'emails/1': '',
        'emails/2': None,
        'emails/3': "not-a-mail but it's ok",
        'emails/4': 'bar@example.org',
        'days_to_expire': '30',
    }
    with responses.RequestsMock() as rsps:
        rsps.post('http://filr.invalid/rest/folders/1234/shares?notify=true', status=200, json={'id': 9})
        resp = app.post_json('/filr-rest/filr/share-folder', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == [{'id': 9}, {'id': 9}, {'id': 9}]

    params = {
        'folder_id': '1234',
        'emails/0': '',
        'emails/1': None,
        'days_to_expire': '30',
    }
    with responses.RequestsMock():
        resp = app.post_json('/filr-rest/filr/share-folder', params=params, status=400)
        assert resp.json['err_desc'] == 'no valid email in emails'
        assert resp.json['data'] is None


def test_delete_folder(app, connector):
    params = {
        'folder_id': '1234',
    }
    with responses.RequestsMock() as rsps:
        rsps.delete('http://filr.invalid/rest/folders/1234', status=204, content_type='application/json')
        resp = app.post_json('/filr-rest/filr/delete-folder', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == ''
