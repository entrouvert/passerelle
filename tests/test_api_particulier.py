# tests/test_api_particulier.py
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re

import pytest
import requests
import responses
from django.urls import reverse

from passerelle.apps.api_particulier.models import APIParticulier
from passerelle.base.models import ResourceLog
from tests.test_manager import login
from tests.utils import make_resource

CAF_FAMILLE = {
    'adresse': {
        'codePostalVille': '12345 CONDAT',
        'complementIdentiteGeo': 'ESCALIER B',
        'identite': 'Madame MARIE DUPONT',
        'numeroRue': '123 RUE BIDON',
        'pays': 'FRANCE',
    },
    'allocataires': [
        {'dateDeNaissance': '12111971', 'nomPrenom': 'MARIE DUPONT', 'sexe': 'F'},
        {'dateDeNaissance': '18101969', 'nomPrenom': 'JEAN DUPONT', 'sexe': 'M'},
    ],
    'annee': 2017,
    'enfants': [{'dateDeNaissance': '11122016', 'nomPrenom': 'LUCIE DUPONT', 'sexe': 'F'}],
    'mois': 4,
    'quotientFamilial': 1754,
}

COMPOSITION_FAMILIAL_V2 = {
    'quotientFamilial': 464,
    'mois': 12,
    'annee': 2023,
    'allocataires': [
        {
            'nomNaissance': 'CHAMPION',
            'nomUsuel': 'DU MONDE',
            'prenoms': 'JEAN-PASCAL ROMAIN',
            'anneeDateDeNaissance': '1980',
            'moisDateDeNaissance': '06',
            'jourDateDeNaissance': '12',
            'sexe': 'M',
        },
        {
            'nomNaissance': 'NIDOUILLET',
            'nomUsuel': None,
            'prenoms': 'JOSIANE',
            'anneeDateDeNaissance': '1981',
            'moisDateDeNaissance': '05',
            'jourDateDeNaissance': '02',
            'sexe': 'F',
        },
    ],
    'enfants': [
        {
            'nomNaissance': 'CHAMPION',
            'nomUsuel': None,
            'prenoms': 'AURELIE',
            'anneeDateDeNaissance': '2014',
            'moisDateDeNaissance': '05',
            'jourDateDeNaissance': '02',
            'sexe': 'F',
        },
        {
            'nomNaissance': 'CHAMPION',
            'nomUsuel': None,
            'prenoms': 'AURELIEN',
            'anneeDateDeNaissance': '2012',
            'moisDateDeNaissance': '04',
            'jourDateDeNaissance': None,
            'sexe': 'M',
        },
    ],
    'adresse': {
        'identite': 'DU MONDE JEAN-PASCAL',
        'complementInformation': 'APPARTEMENT 2',
        'complementInformationGeographique': None,
        'numeroLibelleVoie': None,
        'lieuDit': None,
        'codePostalVille': '81700 GARREVAQUES',
        'pays': 'FRANCE',
    },
}

SCOLARITES = {
    'eleve': {'nom': 'Martin', 'prenom': 'Justine', 'sexe': 'F', 'date_naissance': '2000-01-20'},
    'code_etablissement': '0890003V',
    'annee_scolaire': '2022-2023',
    'est_scolarise': True,
    'est_boursier': True,
    'status_eleve': {'code': 'ST', 'libelle': 'Scolaire'},
}

INTROSPECT = {
    '_id': '1d99db5a-a099-4314-ad2f-2707c6b505a6',
    'name': 'Application de sandbox',
    'scopes': [
        'cnaf_allocataires',
        'cnaf_enfants',
        'cnaf_adresse',
        'cnaf_quotient_familial',
        'mesri_statut_etudiant',
    ],
}


@pytest.fixture
def responses_mock():
    with responses.RequestsMock() as responses_mock:
        yield responses_mock


@pytest.fixture
def resource(db):
    return make_resource(
        APIParticulier,
        slug='test',
        title='API Particulier Prod',
        description='API Particulier Prod',
        platform='test',
        api_key='83c68bf0b6013c4daf3f8213f7212aa5',
    )


vector = [
    (['caf_famille', 'situation-familiale'], {'code_postal': 12, 'numero_allocataire': '0000015'}),
]


def test_error_500(app, resource, responses_mock):
    responses_mock.get(re.compile('.*'), status=500, json={'error': 500})

    def do(endpoint, params):
        resp = app.get('/api-particulier/test/%s' % endpoint, params=params)
        assert resp.status_code == 200
        assert resp.json['err'] == 1
        assert resp.json['data']['status_code'] == 500
        assert resp.json['data']['code'] == 'non-200'

    for endpoints, params in vector:
        for endpoint in endpoints:
            do(endpoint, params)


def test_not_json(app, resource, responses_mock):
    responses_mock.get(re.compile('.*'), status=200, body='')

    def do(endpoint, params):
        resp = app.get('/api-particulier/test/%s' % endpoint, params=params)
        assert resp.status_code == 200
        assert resp.json['err'] == 1
        assert 'returned non-JSON content' in resp.json['err_desc']
        assert resp.json['data']['code'] == 'non-json'

    for endpoints, params in vector:
        for endpoint in endpoints:
            do(endpoint, params)


def test_not_found(app, resource, responses_mock):
    responses_mock.get(
        re.compile('.*'),
        status=404,
        json={
            'error': 'not_found',
            'message': 'Les paramètres fournis sont incorrects ou ne correspondent pas à un avis',
        },
    )

    def do(endpoint, params):
        resp = app.get('/api-particulier/test/%s' % endpoint, params=params)
        assert resp.status_code == 200
        assert resp.json['err'] == 1
        assert 'incorrects ou ne correspondent pas' in resp.json['err_desc']
        assert resp.json['data']['code'] == 'not-found'

    for endpoints, params in vector:
        for endpoint in endpoints:
            do(endpoint, params)


def test_connection_error(app, resource, responses_mock):
    responses_mock.get(re.compile('.*'), body=requests.RequestException('connection timed-out'))

    def do(endpoint, params):
        resp = app.get('/api-particulier/test/%s' % endpoint, params=params)
        assert resp.status_code == 200
        assert resp.json['err'] == 1
        assert (
            resp.json['err_desc'] == 'API-particulier platform "test" connection error: connection timed-out'
        )

    for endpoints, params in vector:
        for endpoint in endpoints:
            do(endpoint, params)


def test_situation_familiale(app, resource, responses_mock):
    responses_mock.get(
        re.compile(r'^.*particulier\.api\.gouv\.fr/api/v2/composition-familiale($|\?.*)'), json=CAF_FAMILLE
    )
    params = {
        'code_postal': '99148',
        'numero_allocataire': '0000354',
        'user': 'John Doe',
    }
    resp = app.get('/api-particulier/test/situation-familiale', params=params)
    assert resp.json['data']['adresse']['codePostalVille'] == '12345 CONDAT'
    assert resp.json['data']['enfants'][0]['dateDeNaissance'] == '11122016'
    assert resp.json['data']['enfants'][0]['dateDeNaissance_iso'] == '2016-12-11'
    assert resp.json['data']['allocataires'][1]['dateDeNaissance'] == '18101969'
    assert resp.json['data']['allocataires'][1]['dateDeNaissance_iso'] == '1969-10-18'

    params['numero_allocataire'] = '11'
    resp = app.get('/api-particulier/test/situation-familiale', params=params)
    assert resp.status_code == 200
    assert resp.json['err'] == 1
    assert '7 digits' in resp.json['err_desc']

    params['numero_allocataire'] = '123456a'
    resp = app.get('/api-particulier/test/situation-familiale', params=params)
    assert resp.status_code == 200
    assert resp.json['err'] == 1
    assert '7 digits' in resp.json['err_desc']

    # last letter truncated automatically
    params['numero_allocataire'] = '1234567a'
    resp = app.get('/api-particulier/test/situation-familiale', params=params)
    assert resp.json['data']['adresse']['codePostalVille'] == '12345 CONDAT'
    # cleaned data is also inlcuded in the response
    assert resp.json['data']['numero_allocataire'] == '1234567'
    assert resp.json['data']['code_postal'] == params['code_postal']

    params['code_postal'] = ' '
    resp = app.get('/api-particulier/test/situation-familiale', params=params)
    assert resp.status_code == 200
    assert resp.json['err'] == 1
    assert 'missing' in resp.json['err_desc']


def test_composition_familiale_v2(app, resource, responses_mock):
    responses_mock.get(
        re.compile(r'^.*particulier\.api\.gouv\.fr/api/v2/composition-familiale-v2($|\?.*)'),
        json=COMPOSITION_FAMILIAL_V2,
    )
    resp = app.get(
        '/api-particulier/test/composition-familiale-v2',
        status=400,
    )
    assert resp.status_code == 400
    assert resp.json['err'] == 1
    assert resp.json['err_desc'] == "missing parameters: 'nomNaissance', 'codePaysLieuDeNaissance', 'sexe'."

    params = {
        'nomNaissance': 'ROUX',
        'codePaysLieuDeNaissance': '99100',
        'sexe': 'M',
    }
    resp = app.get(
        '/api-particulier/test/composition-familiale-v2',
        params=params,
    )
    assert resp.status_code == 200
    assert resp.json['err'] == 1
    assert resp.json['err_desc'] == (
        'Those fields are required when codeInseeLieuDeNaissance is not filled: '
        'nomCommuneNaissance, anneeDateDeNaissance, codeInseeDepartementNaissance'
    )

    params['codeInseeLieuDeNaissance'] = '08480'
    resp = app.get('/api-particulier/test/composition-familiale-v2', params=params)
    assert resp.status_code == 200
    assert resp.json['err'] == 0

    params.pop('codeInseeLieuDeNaissance')
    params.update(
        nomCommuneNaissance='GAP', anneeDateDeNaissance='1981', codeInseeDepartementNaissance='05100'
    )

    resp = app.get(
        '/api-particulier/test/composition-familiale-v2',
        params=params,
    )
    assert resp.status_code == 200
    assert resp.json['err'] == 0


def test_scolarites(app, resource, responses_mock):
    # https://github.com/etalab/siade_staging_data/tree/develop/payloads/api_particulier_v2_men_scolarites

    responses_mock.get(
        re.compile(r'^.*particulier\.api\.gouv\.fr/api/v2/scolarites($|\?.*)'), json=SCOLARITES
    )
    resp = app.get(
        '/api-particulier/test/scolarites',
        status=400,
    )
    assert resp.status_code == 400
    assert resp.json['err'] == 1
    assert resp.json['err_desc'] == (
        "missing parameters: 'nom', 'prenom', 'sexe', 'dateNaissance', 'codeEtablissement', 'anneeScolaire'."
    )

    params = {
        'nom': 'Martin',
        'prenom': 'Justine',
        'sexe': 'f',
        'dateNaissance': '2000-01-20',
        'codeEtablissement': '0890003V',
        'anneeScolaire': '2022',
    }

    resp = app.get(
        '/api-particulier/test/scolarites',
        params=params,
    )
    assert resp.status_code == 200
    assert resp.json['err'] == 0

    params['dateNaissance'] = '2000-01-2020'
    resp = app.get(
        '/api-particulier/test/scolarites',
        params=params,
        status=400,
    )
    assert resp.status_code == 400
    assert resp.json['err'] == 1
    assert resp.json['err_class'] == 'passerelle.views.InvalidParameterValue'


def test_scolarites_not_found(app, resource, responses_mock):
    responses_mock.get(
        re.compile('.*'),
        status=404,
        json={'error': 'not_found', 'message': 'Aucun étudiant n\'a pu être trouvé'},
    )
    params = {
        'nom': 'Martin',
        'prenom': 'Not found',
        'sexe': 'f',
        'dateNaissance': '2000-01-20',
        'codeEtablissement': '0890003V',
        'anneeScolaire': '2022',
    }
    resp = app.get(
        '/api-particulier/test/scolarites',
        params=params,
    )
    assert resp.status_code == 200
    assert resp.json['err'] == 1
    assert "Aucun étudiant n'a pu être trouvé" in resp.json['err_desc']


def test_detail_page(app, resource, admin_user):
    login(app)
    response = app.get(
        reverse(
            'view-connector',
            kwargs={
                'connector': 'api-particulier',
                'slug': 'test',
            },
        )
    )
    assert 'API Particulier Prod' in response.text
    assert 'family allowance' in response.text


@pytest.mark.parametrize(
    'status,body,should_log',
    [
        (
            404,
            '{"error": "not_found", "message": "Dossier allocataire inexistant. Le document ne peut être édité."}',
            False,
        ),
        (500, '{"error": "500"}', True),
        (200, '', True),
        (400, '{"error": "bad_request"}', True),
    ],
)
def test_api_particulier_dont_log_not_found(app, resource, responses_mock, status, body, should_log):
    responses_mock.get(re.compile('.*'), status=status, body=body)
    app.get(
        '/api-particulier/test/situation-familiale',
        params={
            'code_postal': '99148',
            'numero_allocataire': '0000354',
            'user': 'John Doe',
        },
    )
    logs = ResourceLog.objects.all()
    if should_log:
        assert logs.count() == 3
        assert logs.filter(levelno=logging.ERROR).count() == 1
    else:
        assert logs.count() == 2
        assert not logs.filter(levelno=logging.ERROR).exists()


def test_scopes(app, resource, responses_mock):
    responses_mock.get(re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'), json=INTROSPECT)
    assert not resource.accessible_scopes
    resp = app.get('/api-particulier/test/scopes')
    assert resp.json['data'] == [
        'cnaf_adresse',
        'cnaf_allocataires',
        'cnaf_enfants',
        'cnaf_quotient_familial',
        'mesri_statut_etudiant',
    ]
    assert len(APIParticulier.objects.get(slug=resource.slug).accessible_scopes) == 5


def test_scopes_error(app, resource, responses_mock):
    responses_mock.get(re.compile('.*'), status=500, json={'error': 500})
    resource.accessible_scopes = ['some', 'scopes']
    resource.save()
    assert len(APIParticulier.objects.get(slug=resource.slug).accessible_scopes) == 2
    resp = app.get('/api-particulier/test/scopes')
    assert resp.json['err']
    assert len(APIParticulier.objects.get(slug=resource.slug).accessible_scopes) == 2


def test_cron(resource, responses_mock):
    responses_mock.get(re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'), json=INTROSPECT)
    assert not resource.accessible_scopes
    resource.daily()
    assert len(resource.accessible_scopes) == 5

    responses_mock.replace(
        responses.GET,
        url=re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'),
        status=500,
        json={'error': 500},
    )
    resource.daily()
    assert len(resource.accessible_scopes) == 0


def test_manager_creation(db, app, admin_user, resource, responses_mock):
    responses_mock.get(re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'), json=INTROSPECT)

    app = login(app)
    path = '/manage/%s/add' % resource.get_connector_slug()

    resp = app.get(path)
    resp.form['slug'] = 'test2'
    resp.form['title'] = 'API Particulier Test2'
    resp.form['description'] = 'API Particulier Test2'
    resp.form['platform'] = 'test'
    resp.form['api_key'] = '83c68bf0b6013c4daf3f8213f7212aa5'

    resp = resp.form.submit()
    assert len(APIParticulier.objects.get(slug='test2').accessible_scopes) == 5
    resp = resp.follow()
    assert len(resp.html.find('ul', {'class': 'accessible-scopes'}).find_all('li')) == 5

    resp = app.get(path)
    resp.form['slug'] = 'test3'
    resp.form['title'] = 'API Particulier Test3'
    resp.form['description'] = 'API Particulier Test3'
    resp.form['platform'] = 'test'
    resp.form['api_key'] = '83c68bf0b6013c4daf3f8213f7212aa5'
    responses_mock.replace(
        responses.GET,
        url=re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'),
        status=500,
        json={'error': 500},
    )
    resp = resp.form.submit()
    assert not APIParticulier.objects.get(slug='test3').accessible_scopes
    resp = resp.follow()
    assert len(resp.html.find('ul', {'class': 'accessible-scopes'}).find_all('li')) == 0


def test_manager_edition(db, app, admin_user, resource, responses_mock):
    responses_mock.get(re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'), json=INTROSPECT)

    app = login(app)
    path = '/%s/%s/' % (resource.get_connector_slug(), resource.slug)

    assert not APIParticulier.objects.get(slug=resource.slug).accessible_scopes
    resp = app.get(path)
    assert len(resp.html.find('ul', {'class': 'accessible-scopes'}).find_all('li')) == 0

    path = '/manage/%s/%s/edit' % (resource.get_connector_slug(), resource.slug)
    resp = app.get(path)
    resp = resp.form.submit()
    assert len(APIParticulier.objects.get(slug=resource.slug).accessible_scopes) == 5
    resp = resp.follow()
    assert len(resp.html.find('ul', {'class': 'accessible-scopes'}).find_all('li')) == 5

    path = '/manage/%s/%s/edit' % (resource.get_connector_slug(), resource.slug)
    resp = app.get(path)
    responses_mock.replace(
        responses.GET,
        url=re.compile(r'^.*particulier\.api\.gouv\.fr/api/introspect$'),
        status=500,
        json={'error': 500},
    )
    resp = resp.form.submit()
    assert not APIParticulier.objects.get(slug=resource.slug).accessible_scopes
    resp = resp.follow()
    assert len(resp.html.find('ul', {'class': 'accessible-scopes'}).find_all('li')) == 0
