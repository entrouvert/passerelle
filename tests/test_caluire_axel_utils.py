# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

import pytest

from passerelle.contrib.caluire_axel.utils import get_reference_year_from_date
from passerelle.contrib.utils.axel import json_date_format


@pytest.mark.parametrize(
    'value, expected',
    [
        ('2021-01-01', 2020),
        ('2021-08-31', 2020),
        ('2021-09-01', 2021),
        ('2021-12-31', 2021),
    ],
)
def test_get_reference_year_from_date(value, expected):
    in_date = datetime.datetime.strptime(value, json_date_format)
    assert get_reference_year_from_date(in_date) == expected
