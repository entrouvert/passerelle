import datetime
from unittest import mock

import isodate
import pytest
from django.core.management import call_command
from django.core.management.base import CommandError

import tests.utils
from passerelle.base.models import Job, ResourceLog, SkipJob
from passerelle.utils.jsonresponse import APIError
from tests.test_base_adresse import BaseAdresse


@pytest.fixture
def base_adresse(db, monkeypatch):
    base_adresse = tests.utils.setup_access_rights(
        BaseAdresse.objects.create(slug='base-adresse', zipcode='73')
    )

    # mock a job method
    base_adresse.fail = False

    def job_method(self):
        if base_adresse.fail is not False:
            raise base_adresse.fail

    monkeypatch.setattr(BaseAdresse, 'job_method', job_method, raising=False)
    return base_adresse


def test_jobs(app, base_adresse, freezer):
    Job.objects.all().delete()  # remove jobs automatically added at connector creation

    freezer.move_to('2019-01-01 00:00:00')
    job = base_adresse.add_job('job_method')
    assert job.status == 'registered'

    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    base_adresse.fail = Exception('boom!')
    freezer.move_to('2019-01-01 12:00:00')
    job = base_adresse.add_job('job_method')
    assert job.status == 'registered'

    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'failed'
    assert Job.objects.get(id=job.id).status_details == {'error_summary': 'Exception: boom!'}
    assert ResourceLog.objects.all().count() == 1
    assert ResourceLog.objects.all()[0].message == ('error running job_method job (boom!)')

    base_adresse.fail = False

    ResourceLog.objects.all().delete()
    job = base_adresse.add_job('job_method')
    base_adresse.fail = Exception('hello')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'failed'
    assert Job.objects.get(id=job.id).status_details == {'error_summary': 'Exception: hello'}
    assert ResourceLog.objects.all().count() == 1
    assert ResourceLog.objects.all()[0].message == 'error running job_method job (hello)'

    job = base_adresse.add_job('job_method')
    base_adresse.fail = SkipJob()
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'

    # use after_timestamp with SkipJob
    freezer.move_to('2019-01-01 00:00:00')
    base_adresse.fail = SkipJob(after_timestamp=isodate.parse_datetime('2019-01-02T00:00:00+00:00'))
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    base_adresse.fail = False
    freezer.move_to('2019-01-01 12:00:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-02 01:00:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # use after_timestamp with SkipJob and seconds
    job = base_adresse.add_job('job_method')
    freezer.move_to('2019-01-01 00:00:00')
    base_adresse.fail = SkipJob(after_timestamp=3600)
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    base_adresse.fail = False
    freezer.move_to('2019-01-01 00:30:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-01 01:01:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # use after_timestamp with SkipJob and timedelta
    job = base_adresse.add_job('job_method')
    freezer.move_to('2019-01-01 00:00:00')
    base_adresse.fail = SkipJob(after_timestamp=3600)
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    base_adresse.fail = False
    freezer.move_to('2019-01-01 00:30:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-01 01:01:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # use after_timestamp with add_job
    freezer.move_to('2019-01-01 00:00:00')
    job = base_adresse.add_job(
        'job_method', after_timestamp=isodate.parse_datetime('2019-01-02T00:00:00+00:00')
    )
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-02 01:00:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # use after_timestamp with add_job and seconds
    freezer.move_to('2019-01-01 00:00:00')
    job = base_adresse.add_job('job_method', after_timestamp=3600)
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-01 01:01:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # use after_timestamp with add_job and seconds
    freezer.move_to('2019-01-01 00:00:00')
    job = base_adresse.add_job('job_method', after_timestamp=datetime.timedelta(seconds=3600))
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'registered'
    freezer.move_to('2019-01-01 01:01:00')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'completed'

    # don't run jobs if connector is down
    with mock.patch('passerelle.apps.base_adresse.models.BaseAdresse.down') as down:
        down.side_effect = lambda: True
        job = base_adresse.add_job('job_method')
        assert job.status == 'registered'
        base_adresse.jobs()
        assert Job.objects.get(id=job.id).status == 'registered'


def test_runjob(app, base_adresse, freezer):
    freezer.move_to('2019-01-01 00:00:00')
    job = base_adresse.add_job('job_method')
    assert job.status == 'registered'

    call_command('runjob', '--job-id=%s' % job.pk)
    assert Job.objects.get(id=job.id).status == 'completed'

    with pytest.raises(CommandError) as e:
        call_command('runjob', '--job-id=%s' % job.pk)
    assert 'cannot run job, status is completed' in str(e.value)

    date = datetime.date(year=2019, month=1, day=2)
    job = base_adresse.add_job('job_method', after_timestamp=date)

    with pytest.raises(CommandError) as e:
        call_command('runjob', '--job-id=%s' % job.pk)
    assert 'cannot run job, should be run after 2019-01-02 00:00:00+00:00' in str(e.value)


def test_jobs_api_error_log_level(app, base_adresse, freezer):
    Job.objects.all().delete()  # remove jobs automatically added at connector creation

    job = base_adresse.add_job('job_method')
    base_adresse.fail = Exception('hello')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'failed'
    assert ResourceLog.objects.all().count() == 1
    assert ResourceLog.objects.all()[0].level == 'error'

    job = base_adresse.add_job('job_method')
    base_adresse.fail = APIError('hello')
    base_adresse.jobs()
    assert Job.objects.get(id=job.id).status == 'failed'
    assert ResourceLog.objects.all().count() == 2
    assert ResourceLog.objects.all()[1].level == 'warning'
