# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import io
import re

import pytest
from PIL import Image

from passerelle.utils.pdf import PDF


@pytest.fixture
def pdf():
    with open('tests/data/cerfa_10072-02.pdf', 'rb') as fd:
        return PDF(content=fd)


def test_number_of_pages(pdf):
    assert pdf.number_of_pages == 5


def test_page(pdf):
    assert pdf.page(0) is not None
    assert pdf.page(0).media_box == (0, 0, 595.32, 841.92)


def test_page_len_fields(pdf):
    assert len(list(pdf.page(0).fields)) == 53


def test_page_fields(pdf):
    page = pdf.page(0)
    field = page.fields[0]
    assert field.name == 'topmostSubform[0].Page1[0].Case_à_cocher1[2]'
    assert field.widget_type == 'checkbox'
    assert field.rect == (550.292, 691.02, 558.292, 699.02)
    assert all(field.digest_id == field.digest_id.upper() for field in page.fields)
    assert all(len(field.digest_id) >= 25 for field in page.fields)
    # digests are unique
    assert len(page.fields) == len({field.digest_id for field in page.fields})
    assert page.fields[0] != page.fields[1]
    assert page.fields[0] == page.fields[0]


def test_thumbnail_png(pdf):
    png = pdf.page(0).thumbnail_png()
    assert png[:10] == b'\x89PNG\r\n\x1a\n\x00\x00'
    image = Image.open(io.BytesIO(png))
    assert (image.width, image.height) == (800, 1132)


def test_fields_image_map(pdf):
    image_map = pdf.page(0).fields_image_map()
    assert len(list(re.findall('area', image_map))) == 53


def test_field_set(pdf):
    for field in pdf.page(0).fields:
        if field.name == 'topmostSubform[0].Page1[0].Champ_de_texte1[0]':
            field.set('coucou')
        elif field.name == 'topmostSubform[0].Page1[0].Case_à_cocher1[0]':
            field.set(True)
    with io.BytesIO() as fd:
        pdf.write(fd)
        new_pdf = PDF(fd.getvalue())
    new_page = new_pdf.page(0)
    check = set()
    for field in new_page.fields:
        if field.name == 'topmostSubform[0].Page1[0].Champ_de_texte1[0]':
            check.add(1)
            assert field.value == 'coucou'
        elif field.name == 'topmostSubform[0].Page1[0].Case_à_cocher1[0]':
            check.add(2)
            assert field.value is True
        elif field.widget_type == 'checkbox':
            assert field.value is False
        elif field.widget_type == 'text':
            assert field.value == ''
        else:
            raise NotImplementedError
    assert check == {1, 2}


def test_radio_button():
    with open('tests/data/cerfa_14011-02.pdf', 'rb') as fd:
        pdf = PDF(content=fd)
    radio = [field for field in pdf.page(0).fields if field.name == 'topmostSubform[0].Page1[0].Gender[0]']
    assert len(radio) == 1
    radio = radio[0]
    assert radio.radio_possible_values == ['H', 'F']
    radio.set('H')
    assert radio.value == 'H'


def test_combo_box():
    with open('tests/data/cerfa_14011-02.pdf', 'rb') as fd:
        pdf = PDF(content=fd)
    combo = [field for field in pdf.page(0).fields if field.name == 'topmostSubform[0].Page1[0].Pays[0]']
    assert len(combo) == 1
    combo = combo[0]
    assert len(combo.combo_possible_values) == 235
    combo.set('X')
    assert combo.value is None
    combo.set('FRANCE')
    assert combo.value == 'FRANCE'
