# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import pytest
from django.core.files.storage import default_storage
from django.utils.encoding import force_bytes

from passerelle.utils.files import atomic_write


def test_atomic_write(tmpdir):
    target_dir = tmpdir.mkdir('target')
    filepath = str(target_dir.join('test'))

    with pytest.raises(Exception):
        with atomic_write(filepath) as fd:
            fd.write(force_bytes('coucou'))
            raise Exception()

    assert not os.path.exists(filepath)
    assert os.listdir(str(target_dir)) == []

    target_dir = default_storage.path('target')
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    filepath = os.path.join(target_dir, 'test')

    with atomic_write(filepath) as fd:
        fd.write(force_bytes('coucou'))
    assert os.path.exists(filepath)
    with open(filepath) as fd:
        assert fd.read() == 'coucou'
