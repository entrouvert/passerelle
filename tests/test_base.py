# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from passerelle.base.models import BaseResource
from tests.utils import ResponsesSoap


@pytest.fixture
def dummy_resource_class():
    class DummyResource(BaseResource):
        class logging_parameters:
            log_level = 30
            trace_emails = ''

        def down(self):
            return False

        class Meta:
            app_label = 'tests'

    return DummyResource


def test_soap_client_method(dummy_resource_class):
    with open('passerelle/contrib/toulouse_maelis/tools/wsdl/ActivityService.wsdl', 'rb') as fd:
        wsdl_url = 'https://example.org/ActivityService?wsdl'
        responses_soap = ResponsesSoap(
            wsdl_url=wsdl_url,
            wsdl_content=fd.read(),
        )
    with responses_soap():
        resource = dummy_resource_class(
            pk='x'
        )  # pk is necessary for the instance to be hashable and used a key in the soap_client cache
        assert (
            resource.soap_client(wsdl_url=wsdl_url).wsdl is not resource.soap_client(wsdl_url=wsdl_url).wsdl
        )

        dummy_resource_class.soap_client_wsdl_ttl = 300
        assert resource.soap_client(wsdl_url=wsdl_url).wsdl is resource.soap_client(wsdl_url=wsdl_url).wsdl
