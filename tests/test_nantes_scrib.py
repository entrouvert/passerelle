# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import pytest
import responses

import tests.utils
from passerelle.contrib.nantes_scrib.models import SOAPScrib

TEST_BASE_DIR = os.path.join(os.path.dirname(__file__), 'data', 'nantes_scrib')

RSPS_OK = """<?xml version="1.0" encoding="UTF-8"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
<S:Body><ns2:depotResponse xmlns:ns2="http://ws.scrib/">
  <return><codeRetour>true</codeRetour></return>
</ns2:depotResponse></S:Body></S:Envelope>"""

RSPS_ERR_TECH = """<?xml version="1.0" encoding="UTF-8"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
<S:Body><ns2:depotResponse xmlns:ns2="http://ws.scrib/">
  <return><codeErreur>ERR_TECH</codeErreur><codeRetour>false</codeRetour></return>
</ns2:depotResponse></S:Body></S:Envelope>"""

WCS_WSCALL = {
    # new request (no numBac)
    'demandeBacs/bac/0/bacCollecteDechetsMenagers': 'true',
    'demandeBacs/bac/0/bacCollecteSelective': 'false',
    'demandeBacs/bac/0/bacCollecteVerre': 'false',
    'demandeBacs/bac/0/numBacCollecteDechetsMenagers': '',
    'demandeBacs/bac/0/numBacCollecteSelective': None,
    'demandeBacs/bac/0/foobar': 'foobar',
    # error
    'demandeBacs/bac/1': 'not-a-dict',
    # renewal (numBac)
    'demandeBacs/bac/2/bacCollecteDechetsMenagers': 'false',
    'demandeBacs/bac/2/bacCollecteSelective': 1,
    'demandeBacs/bac/2/bacCollecteVerre': 'false',
    'demandeBacs/bac/2/numBacCollecteDechetsMenagers': '2',
    'demandeBacs/bac/2/numBacCollecteSelective': '576X9812',
    # no request (nothing is True)
    'demandeBacs/bac/3/bacCollecteDechetsMenagers': 'false',
    'demandeBacs/bac/3/bacCollecteSelective': '',
    'demandeBacs/bac/3/bacCollecteVerre': 'blah',
    'demandeBacs/bac/3/numBacCollecteDechetsMenagers': '',
    'demandeBacs/bac/3/numBacCollecteSelective': None,
    'demandeBacs/bac/3/numBacCollecteVerre': 0,
    'demandeBacs/bac/3/foobar': 'foobar',
    # error
    # others parameters
    'demandeBacs/codeCivel': '1140206',
    'demandeBacs/codePostal': '44000',
    'demandeBacs/commune': 'TestVille',
    'demandeBacs/email': 'test@example.org',
    'demandeBacs/libelleVoie': 'Route de Test',
    'demandeBacs/natureDemande': 'DREN',
    'demandeBacs/natureDemandeur': 'NPAR',
    'demandeBacs/nom': 'TESTNOM',
    'demandeBacs/nombrePersonnesFoyer': ' 2 ',
    'demandeBacs/numeroVoie': '  42 ',
    'demandeBacs/prenom': 'TestPrenom',
    'demandeBacs/raisonDemande': 'BBRU',
    'demandeBacs/typeDegradation': 'TDRO',
    'demandeBacs/telephone': '0707070707',
    'demandeBacs/typeHabitat': 'HCOL',
}


@pytest.fixture
def wsdl():
    with open(os.path.join(TEST_BASE_DIR, 'scrib.wsdl'), 'rb') as wsdl_file:
        return wsdl_file.read()


@pytest.fixture
def connector(db):
    return tests.utils.setup_access_rights(
        SOAPScrib.objects.create(slug='test', wsdl_url='https://scrib.example.net/scrib.wsdl')
    )


def test_scrib(wsdl, connector, app):
    with responses.RequestsMock() as rsps:
        rsps.get('https://scrib.example.net/scrib.wsdl', status=200, content_type='text/xml', body=wsdl)
        rsps.post(
            'https://scrib.example.net/scrib2/ws/demandeBac',
            status=200,
            content_type='text/xml',
            body=RSPS_OK,
        )

        resp = app.post_json('/nantes-scrib/test/depot', params=WCS_WSCALL)
        xml = rsps.calls[1].request.body.decode('utf-8')  # post payload
        assert resp.json == {
            'data': {'codeErreur': None, 'codeRetour': True, 'messageErreur': None},
            'err': 0,
        }
        # cleaned "bac" list, only two remains
        assert xml.count('<bac>') == 2
        assert (
            '<bac><bacCollecteDechetsMenagers>true</bacCollecteDechetsMenagers><foobar>foobar</foobar></bac>'
            in xml
        )
        assert (
            '<bac><bacCollecteSelective>true</bacCollecteSelective>'
            '<numBacCollecteSelective>576X9812</numBacCollecteSelective></bac>' in xml
        )
        assert '<nombrePersonnesFoyer>2</nombrePersonnesFoyer>' in xml
        assert '<numeroVoie>42</numeroVoie>' in xml
        assert '<raisonDemande>BBRU</raisonDemande>' in xml
        assert '<typeDegradation>TDRO</typeDegradation>' in xml

        # remove empty or non-integers fields
        WCS_WSCALL['demandeBacs/nombrePersonnesFoyer'] = '0'
        WCS_WSCALL['demandeBacs/numeroVoie'] = ' '
        WCS_WSCALL['demandeBacs/raisonDemande'] = '  '
        WCS_WSCALL['demandeBacs/typeDegradation'] = None
        resp = app.post_json('/nantes-scrib/test/depot', params=WCS_WSCALL)
        xml = rsps.calls[2].request.body.decode('utf-8')  # post payload
        assert xml.count('<bac>') == 2
        assert '<nombrePersonnesFoyer>0</nombrePersonnesFoyer>' in xml
        assert '<numeroVoie>' not in xml
        assert '<raisonDemande>' not in xml
        assert '<typeDegradation>' not in xml

    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://scrib.example.net/scrib2/ws/demandeBac', status=200, content_type='text/xml', body='booo'
        )
        resp = app.post_json('/nantes-scrib/test/depot', params=WCS_WSCALL)
        assert resp.json['err'] == 1
        assert resp.json['err_class'] == 'passerelle.utils.soap.SOAPInvalidContent'

    with responses.RequestsMock() as rsps:
        resp = app.post_json('/nantes-scrib/test/depot', params={}, status=400)
        assert resp.json['err'] == 1
        assert resp.json['err_desc'] == 'missing demandeBacs/bac list'

    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://scrib.example.net/scrib2/ws/demandeBac',
            status=200,
            content_type='text/xml',
            body=RSPS_ERR_TECH,
        )
        resp = app.post_json('/nantes-scrib/test/depot', params=WCS_WSCALL)
        assert resp.json == {
            'data': {'codeErreur': 'ERR_TECH', 'codeRetour': False, 'messageErreur': None},
            'err': 1,
        }
