import os

from passerelle.utils.conversion import to_pdf


def test_pdf_to_pdf_do_nothing():
    with open(os.path.join(os.path.dirname(__file__), 'data', 'minimal.pdf'), 'rb') as fd:
        pdf = fd.read()
    assert to_pdf(pdf) == pdf
    with open(os.path.join(os.path.dirname(__file__), 'data', 'minimal_bom.pdf'), 'rb') as fd:
        pdf = fd.read()
    assert to_pdf(pdf) == pdf
    with open(os.path.join(os.path.dirname(__file__), 'data', 'minimal_bomutf8.pdf'), 'rb') as fd:
        pdf = fd.read()
    assert to_pdf(pdf) == pdf
