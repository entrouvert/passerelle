import pytest
from httmock import HTTMock, all_requests

import tests.utils
from passerelle.apps.feeds.models import Feed

FEED_EXAMPLE = """<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/">
  <title>Actualités de Strasbourg</title>
  <link rel="alternate" href="http://www.strasbourg.eu/fr/actualites/-/asset_publisher/lG7u/rss" />
  <subtitle>Actualités de Strasbourg</subtitle>
  <entry>
    <title>30 juin - 1er juillet : conditions de circulation et de stationnement</title>
    <link rel="alternate" href="http://www.strasbourg.eu/fr/actualites/-/asset_publisher/lG7u/content/id/5722879" />
    <author>
      <name>Ville et Eurométropole de Strasbourg</name>
    </author>
    <id>http://www.strasbourg.eu/fr/actualites/-/asset_publisher/lG7u/content/id/5722879</id>
    <updated>2017-06-29T17:19:05Z</updated>
    <published>2017-06-29T16:46:24Z</published>
    <summary type="html">&lt;img src='http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/8a6e5fa7-a0be-4287-8034-eed14e86df39/Plan_Parlement.jpg' /&gt;&lt;div&gt;Dans le cadre de la cérémonie d'hommage à Helmut Kohl au Parlement européen, prenez connaissance du dispositif de sécurité mis en place avec notamment des restrictions de circulation et de stationnement le vendredi 30 juin et le samedi 1er juillet.&lt;/div&gt;</summary>
    <dc:creator>Ville et Eurométropole de Strasbourg</dc:creator>
    <dc:date>2017-06-29T16:46:24Z</dc:date>
  </entry>
  <entry>
    <title>A noter cette semaine : circulation, travaux, services publics, événements</title>
    <link rel="alternate" href="http://www.strasbourg.eu/fr/actualites/-/asset_publisher/lG7u/content/id/5715308" />
    <author>
      <name>Ville et Eurométropole de Strasbourg</name>
    </author>
    <id>http://www.strasbourg.eu/fr/actualites/-/asset_publisher/lG7u/content/id/5715308</id>
    <updated>2017-06-29T12:37:41Z</updated>
    <published>2017-06-26T07:33:59Z</published>
    <summary type="html">&lt;img src='http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/6f54e4f0-5c63-4b80-b2c2-df7afc7d213e/vue_de_Strasbourg.jpg' /&gt;&lt;div&gt;Informations utiles à savoir du 26 juin au 3 juillet 2017&lt;/div&gt;</summary>
    <dc:creator>Ville et Eurométropole de Strasbourg</dc:creator>
    <dc:date>2017-06-26T07:33:59Z</dc:date>
  </entry>
</feed>"""


@pytest.fixture
def connector(db):
    return tests.utils.setup_access_rights(Feed.objects.create(slug='test', url='http://example.net/'))


@all_requests
def up_mock(url, request):
    return {'status_code': 200, 'content': FEED_EXAMPLE, 'request': request}


@all_requests
def down_mock(url, request):
    return {'status_code': 404, 'content': 'down', 'request': request}


@all_requests
def down_500_mock(url, request):
    return {'status_code': 500, 'content': 'down', 'request': request}


def test_feed_availability(app, connector):
    assert connector.get_availability_status() is None

    with HTTMock(up_mock):
        connector.availability()
    assert connector.get_availability_status().up()

    with HTTMock(down_mock):
        connector.availability()
    assert connector.get_availability_status().down()
    assert '404' in connector.get_availability_status().message

    with HTTMock(down_500_mock):
        connector.availability()
    assert connector.get_availability_status().down()
    assert '500' in connector.get_availability_status().message


def test_availability_checks_disabled(app, connector):
    with HTTMock(up_mock):
        connector.availability()
    assert connector.get_availability_status().up()

    av = connector.availability_parameters
    av.run_check = False
    av.save()

    with HTTMock(down_mock):
        connector.availability()
    assert connector.get_availability_status().up()

    av.run_check = True
    av.save()
    with HTTMock(down_mock):
        connector.availability()
    assert connector.get_availability_status().down()
