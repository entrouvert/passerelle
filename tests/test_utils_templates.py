# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from passerelle.utils.templates import evaluate_condition, evaluate_template


def test_evaluate_condition():
    assert evaluate_condition('x == 1', {'x': 1}) is True
    assert evaluate_condition('x == 0', {'x': 1}) is False


def test_evaluate_template():
    assert evaluate_template('{% if foo %}bar{% endif %}', {'foo': True}) == 'bar'
