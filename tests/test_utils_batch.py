from passerelle.utils import batch


def test_batch():
    data = range(10)
    for i, d in enumerate(batch(data, 1)):
        assert list(d) == [i]
