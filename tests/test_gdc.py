import pytest

from passerelle.apps.gdc import models
from passerelle.utils.jsonresponse import APIError


def test_phpserialize_loads():
    with pytest.raises(APIError, match='Could not deserialize GDC response'):
        models.phpserialize_loads('aaa')
