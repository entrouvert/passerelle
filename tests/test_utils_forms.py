# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from django.core.exceptions import ValidationError

from passerelle.utils.forms import ConditionField, TemplateField


def test_condition_field():
    field = ConditionField()
    with pytest.raises(ValidationError):
        field.clean('x ==')
    assert field.clean('x == 1') == 'x == 1'


def test_template_field():
    field = TemplateField()
    with pytest.raises(ValidationError):
        field.clean('{% if foo %}bar')
    assert field.clean('{% if foo %}bar{% endif %}') == '{% if foo %}bar{% endif %}'
