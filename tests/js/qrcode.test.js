import 'qrcode/qrcode-reader.js'
import nunjucks from 'qrcode/nunjucks.min.js'
import { beforeEach, afterEach, expect, test, vi } from 'vitest'
import 'qrcode/zxing-browser.min.js'
import nacl from 'qrcode/nacl.min.js'
import { scanCertificates, fetchMetadata, tally } from 'qrcode/qrcode-certificate.js'

vi.mock('qrcode/qrcode-certificate.js', async (importOriginal) => {
  const mod = await importOriginal()
  return {
    scanCertificates: vi.fn(mod.scanCertificates),
    fetchMetadata: vi.fn(mod.fetchMetadata),
    tally: vi.fn(mod.tally),
  }
})

vi.hoisted(() => {
  global.gettext = (str) => str
  global.navigator.mediaDevices = true
})

global.nunjucks = nunjucks

beforeEach(() => {
  vi.useFakeTimers()
})

afterEach(() => {
  vi.useRealTimers()
})

const qrTest = test.extend({
  // eslint-disable-next-line no-empty-pattern
  fetchMock: async ({}, use) => {
    const fetchBackup = global.fetch
    global.fetch = vi.fn()
    await use(global.fetch)
    global.fetch = fetchBackup
  },
  // eslint-disable-next-line no-empty-pattern
  scanMock: async ({}, use) => {
    window.nacl = nacl

    await use(async (content, key) => {
      class MockCodeReader {
        async decodeFromVideoDevice (device, element, callback) {
          await callback({ text: content })
        }
      }

      const savedZXingBrowser = window.ZXingBrowser
      window.ZXingBrowser = { BrowserQRCodeReader: MockCodeReader }

      const mock = vi.fn()
      await scanCertificates(undefined, key, mock)

      window.ZXingBrowser = savedZXingBrowser
      return mock
    })

    window.nacl = undefined
  },
  // eslint-disable-next-line no-empty-pattern
  readerMock: async ({}, use) => {
    const createdReaders = []

    await use((content, attributes) => {
      const reader = document.createElement('qrcode-reader')
      reader.setAttribute('verify-key', 'verify-key')
      for (const [name, value] of Object.entries(attributes || {})) {
        reader.setAttribute(name, value)
      }
      reader.innerHTML = content

      let scan

      scanCertificates.mockImplementationOnce((element, key, callback) => {
        expect(element instanceof HTMLVideoElement).toBe(true)
        expect(key).toBe('verify-key')
        scan = callback
      })

      document.append(reader)
      createdReaders.push(reader)
      return { reader, scan }
    })

    for (const reader of createdReaders) {
      reader.remove()
    }
  },
  // eslint-disable-next-line no-empty-pattern
  serviceWorkerMock: async ({}, use) => {
    let postMessage
    const postMessageMock = vi.fn()
    const registerMock = vi.fn()

    global.navigator.serviceWorker = {
      register: registerMock,
      addEventListener (eventName, handler) {
        expect(eventName).toBe('message')
        postMessage = handler
      },
      ready: new Promise((resolve) => resolve({
        active: { postMessage: postMessageMock },
      })),
    }

    await use({
      postMessage: (data) => postMessage({data}),
      postMessageMock,
      registerMock,
    })

    global.navigator.serviceWorker = undefined
  },
})

qrTest('scan certificates', async ({ scanMock }) => {
  // private-key: 98f986e1afe2b546d264e45f00eb3f8d1b331bf3d2fa9e73bea3d8b2c9d90274
  const key = '15dbdd38a2d8a2db1b4dd985da8da2b4e6b785b28db3fe0b34a10cfb3ba0aeb3'

  const okCodeData
    = '3%73QJ0UK5G45S4XE0+GEUIKU662$D$K0RTM$4R7L7UX0V19FMFR5A++S6BNFR26 IJ7V15NE1NV2RN+T'
    + 'VH6%PHBDL*:01/69%EPEDUF7Y47EM6JA7MA79W5DOC$CC4S6G-CBW5C%6$Q6J*6JOCIPC4DC646FM6NE1'
    + 'AECPED-EDLFF QEGEC9VE634L%6 47%47C%6446D36K/EXVDAVCRWEV2C0/DUF7:967461R67:6OA7Y$5'
    + 'DE1HEC1WE..DF$DUF7/B8-ED:JCSTDR.C4LE1WE..DF$DUF771904E93DKOEXVDKPCF/DV CP9EIEC*ZC'
    + 'T34ZA8.Q6$Q6HB8A0'

  const ok = await scanMock(okCodeData, key)
  expect(ok).toHaveBeenCalledWith({
    'data': {
      'first_name': 'Georges',
      'last_name': 'Abitbol',
      'license_plate': 'HA-424-AH',
    },
    'uuid': '853b9497-6c1a-4ff1-8604-6dc7cca9003c',
    'validity': {
      'end': 1701467999,
      'start': 1698789600,
    },
  })

  const noValidity = await scanMock(
    ':U8$JSK1IVMJP$E06JCBCVQSIXFZ$HEDJ+S3:%1YN13BHJ$GFGLIN88AL5UNSFG8UG+YUV 2J6701809'
    + 'B3X3PP0TH9CN2R3IYEDB$DKWEOED0%EIEC ED3.DQ34%R83:6NW6XM8ME1.$E8UCE44QIC+96M.C9%6I'
    + 'M6E467W5LA76L6%47G%64W5ZJCYX6J*64EC6:6J$6',
    key)

  expect(noValidity).toHaveBeenCalledWith({
    'data': {
      'immatriculation': 'ED-755-CG',
    },
    'uuid': 'fb013fd6-3f08-4963-8964-1bf5e6daf786',
    'validity': {
      'end': undefined,
      'start': undefined,
    },
  })

  const invalidKey = '15dbdd38a2d8a2db1b4dd985da8da2b4e6b785b28db3fe0b34a10cfb3ba0aeb4'
  const invalidSignature = await scanMock(okCodeData, invalidKey)
  expect(invalidSignature).toHaveBeenCalledWith({
    error: 'This reader can\'t read this QR Code.',
  })

  const invalidData = await scanMock('invalid data', key)
  expect(invalidData).toHaveBeenCalledWith({
    error: 'Invalid QR Code.',
  })
})

qrTest('fetch certificate metadata', async ({ fetchMock }) => {
  const getMetadata = async (fetchImplementation) => {
    fetchMock.mockImplementationOnce(async (fetchUrl) => {
      expect(fetchUrl).toBe('https://orson-welles.io?certificate=certificate-uuid')
      return fetchImplementation()
    })
    const result = await fetchMetadata('certificate-uuid', 'https://orson-welles.io')
    expect(fetchMock).toHaveBeenCalledOnce()
    fetchMock.mockReset()
    return result
  }

  expect(await getMetadata(() => ({
    json: async () => ({ err: 0, data: { name: 'Georges' } }),
    ok: true,
  }))).toStrictEqual({items: {name: 'Georges'}})

  expect(await getMetadata(() => ({
    json: async () => ({ err: 1 }),
    ok: true,
  }))).toStrictEqual({
    error: 'An api error occured while fetching metadata.',
  })

  expect(await getMetadata(() => ({
    json: async () => ({ }),
    ok: false,
  }))).toStrictEqual({
    error: 'An api error occured while fetching metadata.',
  })

  expect(await getMetadata(() => {
    throw new Error()
  })).toStrictEqual({
    error: 'A network error occured while fetching metadata : check network connectivity.',
  })
})

qrTest('tally sends debits and parses results', async ({ fetchMock }) => {
  fetchMock.mockImplementationOnce((url, {body, headers, method}) => {
    expect(url).toBe('https://orson-welles.io')
    expect(method).toBe('POST')
    expect(headers).toStrictEqual({'Content-Type': 'application/json'})
    const json = JSON.parse(body)
    expect(json.events.length).toBe(2)
    expect(json.events[0].certificate).toBe('certificate-uuid')
    expect(json.events[0].credit).toBe(-10)
    expect(json.events[1].certificate).toBe('certificate-uuid/mma')
    expect(json.events[1].credit).toBe(-1)
    return {
      ok: true,
      json: async () => ({
        err: 0,
        data: { certificates: {
          'certificate-uuid': { credit: 1 },
          'certificate-uuid/mma': { credit: 12 },
          'unkwnow-certificate-uuid': {},
        } },
      }),
    }
  })

  const result = await tally('certificate-uuid', 'https://orson-welles.io', {'_default': 10, 'mma': 1})
  expect(result).toStrictEqual({
    accounts: {
      '_default': { credit: 1 },
      'mma': { credit: 12 },
    },
  })
})

qrTest('tally handles api errors', async ({ fetchMock }) => {
  fetchMock.mockImplementationOnce(() => ({
    ok: true,
    json: async () => ({ 'err': 1 }),
  }))
  const result = await tally('certificate-uuid', 'https://orson-welles.io', {'_default': ''})
  expect(result).toStrictEqual({ error: 'An api error occured while tallying the QR code.' })
})

qrTest('tally handles HTTP errors', async ({ fetchMock }) => {
  fetchMock.mockImplementationOnce(() => ({ ok: false, json: async () => ({}) }))
  const result = await tally('certificate-uuid', 'https://orson-welles.io', {'_default': ''})
  expect(result).toStrictEqual({ error: 'An api error occured while tallying the QR code.' })
})

qrTest('tally handles network errors', async ({ fetchMock }) => {
  fetchMock.mockImplementationOnce(() => { throw new Error() })
  const result = await tally('certificate-uuid', 'https://orson-welles.io', {'_default': ''})
  expect(result).toStrictEqual({
    error: 'A network error occured while tallying the QR code : check network connectivity.',
  })
})

qrTest('reader toggle fullscreen', async ({readerMock}) => {
  const { reader } = readerMock()
  const fullscreenButton = reader.shadowRoot.querySelector('.fullscreen-button')

  reader.requestFullscreen = vi.fn()
  document.exitFullscreen = vi.fn()

  fullscreenButton.dispatchEvent(new Event('click'))

  expect(reader.requestFullscreen).toHaveBeenCalled()
  expect(document.exitFullscreen).not.toHaveBeenCalled()

  vi.clearAllMocks()
  document.fullscreenElement = reader

  fullscreenButton.dispatchEvent(new Event('click'))

  expect(reader.requestFullscreen).not.toHaveBeenCalled()
  expect(document.exitFullscreen).toHaveBeenCalled()

  expect(reader.classList.contains('fullscreen')).toBe(false)

  document.fullscreenElement = reader
  reader.dispatchEvent(new Event('fullscreenchange'))
  expect(reader.classList.contains('fullscreen')).toBe(true)

  document.fullscreenElement = undefined
  reader.dispatchEvent(new Event('fullscreenchange'))
  expect(reader.classList.contains('fullscreen')).toBe(false)
})

qrTest('reader popup opens and closes', async ({readerMock}) => {
  const { reader, scan } = readerMock('nop')

  await scan({})

  expect(reader.classList).not.toContain('popup-closed')
  const popupButton = reader.shadowRoot.querySelector('.close-popup-button')
  popupButton.dispatchEvent(new Event('click'))
  expect(reader.classList).toContain('popup-closed')
})

qrTest('reader renders certificate informations', async ({readerMock}) => {
  vi.setSystemTime(new Date(1701467999000.0))
  const render = async (context, template) => {
    const { reader, scan } = readerMock(template)
    await scan({ uuid: 'certificate-uuid', ...context})

    const content = reader.shadowRoot.querySelector('.content')
    return content.innerText
  }


  expect(await render({}, '{{ now() }}')).toMatch('1701467999')
  expect(await render({}, '{{ now() | formatDate }}')).toMatch('12/1/2023, 10:59:59 PM')
  // just check gettext is available
  expect(await render({}, '{{ gettext("blah") }}')).toMatch('blah')

  vi.useRealTimers()
})

qrTest('reader fetch and renders metadata', async ({readerMock}) => {
  const { reader, scan } = readerMock('{{ metadata.items.name }}')
  const content = reader.shadowRoot.querySelector('.content')
  const closeButton = reader.shadowRoot.querySelector('.close-popup-button')
  reader.setAttribute('metadata-url', 'https://orson-welles.com')

  fetchMetadata.mockImplementation((uuid, url) => {
    expect(uuid).toBe('certificate-uuid')
    expect(url).toBe('https://orson-welles.com')
    return { items: { name: 'Georges' } }
  })

  await scan({ uuid: 'certificate-uuid'})
  expect(fetchMetadata).toHaveBeenCalledOnce()
  expect(content.innerText).toMatch('Georges')

  // closing popup and rescanning certificate should refetch metadata
  fetchMetadata.mockClear()
  closeButton.dispatchEvent(new Event('click'))

  await scan({ uuid: 'certificate-uuid'})
  expect(fetchMetadata).toHaveBeenCalledOnce()
  expect(content.innerText).toMatch('Georges')

  // scanning two times same certificate with popup opened shouldn't refetch
  // metadata
  fetchMetadata.mockClear()

  await scan({ uuid: 'certificate-uuid'})
  expect(fetchMetadata).not.toHaveBeenCalled()

  // check no error is raised when scanning invalid certificate
  await scan({ })
  expect(content.innerText).toBe('')
})

qrTest('tally url is disabled when certificate is out of validity', async ({readerMock}) => {
  const { reader, scan } = readerMock('{{ tallyUrl }}')
  const content = reader.shadowRoot.querySelector('.content')
  const now = new Date(1701467999000.0)
  vi.setSystemTime(now)
  reader.setAttribute('tally-url', 'https://orson-welles.com')

  await scan({ uuid: 'certificate-uuid'})
  expect(content.innerText).toMatch('https://orson-welles.com')

  await scan({ uuid: 'not-valid-yet', validity: { start: now.getTime() / 1000.0 + 1.0} })
  expect(content.innerText).toMatch('')

  await scan({ uuid: 'expired', validity: { end: now.getTime() / 1000.0 - 1.0} })
  expect(content.innerText).toMatch('')

  await scan({ uuid: 'valid',
    validity: {
      start: now.getTime() / 1000.0 - 1.0,
      end: now.getTime() / 1000.0 + 1.0,
    },
  })
  expect(content.innerText).toMatch('https://orson-welles.com')
})


qrTest('reader ineracts with service worker', async ({serviceWorkerMock, readerMock}) => {
  const { registerMock, postMessageMock, postMessage } = serviceWorkerMock
  const { reader } = readerMock(
    '{{ tally.name }}',
    { 'tally-url': 'https://bwah', 'service-worker': '/sw.js' },
  )

  await new Promise(resolve => resolve())
  expect(registerMock).toHaveBeenCalledWith('/sw.js', { scope: '/' })
  expect(postMessageMock).toHaveBeenCalledWith({refreshTally: 'https://bwah'})

  postMessageMock.mockClear()
  vi.runOnlyPendingTimers()
  expect(postMessageMock).toHaveBeenCalledWith({refreshTally: 'https://bwah'})

  const syncStatus = reader.shadowRoot.querySelector('.sync-status')
  const lastUpdate = reader.shadowRoot.querySelector('.sync-last-update')
  expect(syncStatus.classList).not.toContain('error')
  expect(syncStatus.classList).not.toContain('offline')
  expect(syncStatus.classList).not.toContain('online')

  const now = Math.floor((new Date().getTime()) / 1000)
  postMessage({syncStatus: { lastUpdate: now, offline: false } })
  expect(syncStatus.classList).not.toContain('error')
  expect(syncStatus.classList).not.toContain('offline')
  expect(syncStatus.classList).toContain('online')
  expect(lastUpdate.innerText).toBe('Up to date')

  postMessage({syncStatus: { lastUpdate: now - 3661, offline: true } })
  expect(syncStatus.classList).not.toContain('error')
  expect(syncStatus.classList).toContain('offline')
  expect(syncStatus.classList).not.toContain('online')
  expect(lastUpdate.innerText).toBe('01:01:01')

  postMessage({syncStatus: { lastUpdate: now - 3661, offline: true, error: true } })
  expect(syncStatus.classList).toContain('error')
  expect(syncStatus.classList).toContain('offline')
  expect(syncStatus.classList).not.toContain('online')
  expect(lastUpdate.innerText).toBe('01:01:01')
})


test('qr-title changes state', async () => {
  const title = document.createElement('qr-title')
  expect(title.classList).not.toContain('loading')
  expect(title.classList).not.toContain('error')

  title.setAttribute('loading', 'true')
  expect(title.classList).toContain('loading')

  title.setAttribute('loading', 'false')
  expect(title.classList).not.toContain('loading')

  title.setAttribute('error', 'true')
  expect(title.classList).toContain('error')

  title.setAttribute('error', 'false')
  expect(title.classList).not.toContain('error')
})

test('qr-error sets previous title state', async () => {
  const dom = document.createElement('div')
  dom.innerHTML = `
    <div>
      <qrcode-reader>
        <qr-title>First</qr-title>
        <qr-title>Second</qr-title>
        <qr-error>Error</qr-error>
      </qrcode-reader>
    </div>
  `
  document.appendChild(dom)
  const titles = dom.querySelectorAll('qr-title')
  const reader = dom.querySelector('qrcode-reader')

  // qr-error sets first preceding title
  expect(titles[0].getAttribute('error')).toBe(null)
  expect(titles[1].getAttribute('error')).toBe('true')
  expect(reader.classList).toContain('error')
})

test('qr-button trigger configured action', async () => {
  const dom = document.createElement('div')
  dom.innerHTML = `
    <div id="wrapper">
      <qr-button action="fetch-metadata">
        Test
      </qr-button>
  `
  const wrapper = dom.querySelector('#wrapper')
  const button = dom.querySelector('qr-button')
  const handlerMock = vi.fn()

  wrapper.addEventListener('fetch-metadata', handlerMock)
  button.dispatchEvent(new Event('click'))
  expect(handlerMock).toHaveBeenCalledOnce()
})

qrTest('qr-tallier shows and updates accounts', async () => {
  tally.mockImplementationOnce((uuid, tallyUrl, accounts) => {
    expect(uuid).toBe('certificate-uuid')
    expect(tallyUrl).toBe('https://example.com')
    expect(accounts).toStrictEqual({ 'mma': 3, 'karate': 0 })

    return {
      accounts: {
        'mma': {'credit': 0, 'revoked': false },
        'karate': {'credit': -1, 'revoked': true },
      },
    }
  })

  const tallier = document.createElement('qr-tallier')
  tallier.setAttribute('certificate-uuid', 'certificate-uuid')
  tallier.setAttribute('tally-url', 'https://example.com')
  tallier.setAttribute('accounts', '{ "mma": "MMA", "karate": "Karaté" }')

  const mmaAccount = tallier.shadowRoot.querySelector('qr-tally-account[account-id=mma]')
  const karateAccount = tallier.shadowRoot.querySelector('qr-tally-account[account-id=karate]')
  const tallyButton = tallier.shadowRoot.querySelector('#tally-button')

  expect(mmaAccount.getAttribute('account-label')).toBe('MMA')
  expect(karateAccount.getAttribute('account-label')).toBe('Karaté')

  mmaAccount.setAttribute('account-debit', 3)

  tallyButton.dispatchEvent(new Event('click'))
  await new Promise(resolve => resolve())

  expect(tally).toHaveBeenCalled()
  expect(mmaAccount.getAttribute('account-credit')).toBe('0')
  expect(mmaAccount.getAttribute('account-revoked')).toBe('false')
  expect(karateAccount.getAttribute('account-credit')).toBe('-1')
  expect(karateAccount.getAttribute('account-revoked')).toBe('true')
})

qrTest('qr-tallier shows unexpected tally error', async () => {
  tally.mockImplementationOnce(() => ({ error: 'error description' }))
  const tallier = document.createElement('qr-tallier')
  const errorElement = tallier.shadowRoot.querySelector('#global-error')
  const title = tallier.shadowRoot.querySelector('#title')
  const tallyButton = tallier.shadowRoot.querySelector('#tally-button')

  expect(errorElement.hidden).toBe(true)
  expect(title.getAttribute('error')).toBe(null)

  tallyButton.dispatchEvent(new Event('click'))
  await new Promise(resolve => resolve())

  expect(errorElement.innerText).toBe('error description')
  expect(errorElement.hidden).toBe(false)
  expect(tallyButton.innerText).toBe('Retry')
  expect(title.getAttribute('error')).not.toBe(null)
})

qrTest('qr-tallier shows error on insufficient credit', async () => {
  tally.mockImplementationOnce(() => ({ accounts: { 'mma': { 'credit': -1 } } }))

  const tallier = document.createElement('qr-tallier')
  const errorElement = tallier.shadowRoot.querySelector('#global-error')
  const title = tallier.shadowRoot.querySelector('#title')
  const tallyButton = tallier.shadowRoot.querySelector('#tally-button')
  tallier.setAttribute('accounts', '{ "mma": "MMA", "karate": "Karaté" }')

  tallyButton.dispatchEvent(new Event('click'))
  await new Promise(resolve => resolve())

  expect(errorElement.innerText).toBe('Invalid tally')
  expect(errorElement.hidden).toBe(false)
  expect(tallyButton.innerText).toBe('Retry')
  expect(title.getAttribute('error')).not.toBe(null)
})

qrTest('qr-tally-account allows debit change', async () => {
  const account = document.createElement('qr-tally-account')
  const addButton = account.shadowRoot.querySelector('#add-debit-button')
  const removeButton = account.shadowRoot.querySelector('#remove-debit-button')
  const debitElement = account.shadowRoot.querySelector('#debit')

  expect(removeButton.disabled).toBe(true)

  addButton.dispatchEvent(new Event('click'))
  expect(debitElement.innerText).toBe('1')
  expect(removeButton.disabled).toBe(false)

  removeButton.dispatchEvent(new Event('click'))
  expect(debitElement.innerText).toBe('0')
  expect(removeButton.disabled).toBe(true)
})

qrTest('qr-tally-account shows label', async () => {
  const account = document.createElement('qr-tally-account')
  account.setAttribute('account-label', 'Account label')
  const labelElement = account.shadowRoot.querySelector('#label')

  expect(labelElement.innerText).toBe('Account label')
})

qrTest('qr-tally-account shows credit informations', async () => {
  const account = document.createElement('qr-tally-account')
  const statusElement = account.shadowRoot.querySelector('#status-text')
  account.setAttribute('account-debit', '2')
  account.setAttribute('account-credit', '-2')

  expect(statusElement.innerText).toBe('Credit: 0')
  expect(account.classList).toContain('error')

  account.setAttribute('account-debit', '2')
  account.setAttribute('account-credit', '10')

  expect(statusElement.innerText).toBe('Credit: 10')
  expect(account.classList).not.toContain('error')
  expect(account.classList).toContain('success')
})

qrTest('qr-tally-account shows revoked certificate error', async () => {
  const account = document.createElement('qr-tally-account')
  const statusElement = account.shadowRoot.querySelector('#status-text')
  account.setAttribute('account-revoked', 'true')

  expect(statusElement.innerText).toBe('Account revoked')
  expect(statusElement.hidden).toBe(false)
})

qrTest('qr-tally-account shows default account name', async () => {
  const account = document.createElement('qr-tally-account')
  const labelElement = account.shadowRoot.querySelector('#label')
  account.setAttribute('account-id', '_default')
  account.setAttribute('account-label', '')

  expect(labelElement.innerText).toBe('Main account')
})

