import os

import pytest
import responses
from django.contrib.contenttypes.models import ContentType
from django.core.files import File

from passerelle.apps.sne.models import SNE
from passerelle.base.models import AccessRight, ApiUser


@pytest.fixture()
def connector(db):
    with open('%s/tests/data/sne/cert.pem' % os.getcwd()) as f:
        api = ApiUser.objects.create(username='all', keytype='', key='')
        connector = SNE.objects.create(
            wsdl_url='https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService/?wsdl',
            slug='test',
            client_certificate=File(f, 'cert.pem'),
            certificate_name='CERG1318-202209062200.XXX',
        )
        obj_type = ContentType.objects.get_for_model(connector)
        AccessRight.objects.create(
            codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
        )
    return connector


def setup_(rsps, settings):
    settings.CONNECTORS_SETTINGS = {
        'sne/test': {
            'requests_substitutions': [
                {
                    'url': 'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/',
                    'search': 'http://sne2-ws.j2ee.eco.edcs.fr:80',
                    'replace': 'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid',
                }
            ]
        }
    }
    with open('%s/tests/data/sne/DemandeLogementImplService.wsdl' % os.getcwd(), 'rb') as f:
        rsps.get(
            'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService/?wsdl',
            status=200,
            body=f.read(),
        )
    with open('%s/tests/data/sne/DemandeLogementImplService1.wsdl' % os.getcwd(), 'rb') as f:
        rsps.get(
            'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService?wsdl=1',
            status=200,
            body=f.read(),
        )
    with open('%s/tests/data/sne/DemandeLogementImplService.xsd' % os.getcwd(), 'rb') as f:
        rsps.get(
            'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService?xsd=1',
            status=200,
            body=f.read(),
        )
    with open('%s/tests/data/sne/xmlmime.xsd' % os.getcwd(), 'rb') as f:
        rsps.get('http://www.w3.org/2005/05/xmlmime', status=200, body=f.read())


def test_get_demande_logement(app, connector, settings):
    with responses.RequestsMock() as rsps:
        setup_(rsps, settings)
        with open('%s/tests/data/sne/response_ok' % os.getcwd(), 'rb') as f:
            rsps.post(
                'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService',
                status=200,
                body=f.read(),
                content_type='multipart/related; start="<rootpart*db03b44e-f563-4ce9-b06d-545faf9b26c0@example.jaxws.sun.com>"; type="application/xop+xml";'
                ' boundary="uuid:db03b44e-f563-4ce9-b06d-545faf9b26c0"; start-info="application/soap+xml"',
            )
        resp = app.get('/sne/test/get-demande-logement?demand_id=0690221008931G3163')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data']['interfaceNuu']['demande']['demandeLogement']['anru'] == 'false'


def test_get_demande_logement_does_not_exist(app, connector, settings):
    with responses.RequestsMock() as rsps:
        setup_(rsps, settings)
        with open('%s/tests/data/sne/response_does_not_exist' % os.getcwd(), 'rb') as f:
            rsps.post(
                'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService',
                status=200,
                body=f.read(),
                content_type='multipart/related; start="<rootpart*7902e9bd-21a8-4632-8760-d79a67eb89a1@example.jaxws.sun.com>"; type="application/xop+xml";'
                ' boundary="uuid:7902e9bd-21a8-4632-8760-d79a67eb89a1"; start-info="application/soap+xml"',
            )
        resp = app.get('/sne/test/get-demande-logement?demand_id=0690221008931G3164')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['err_desc'] == "La demande de logement n'existe pas dans le système."


def test_get_demande_logement_bad_guichet(app, connector, settings):
    with responses.RequestsMock() as rsps:
        setup_(rsps, settings)
        with open('%s/tests/data/sne/response_mauvais_guichet' % os.getcwd(), 'rb') as f:
            rsps.post(
                'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService',
                status=200,
                body=f.read(),
                content_type='multipart/related; start="<rootpart*7902e9bd-21a8-4632-8760-d79a67eb89a1@example.jaxws.sun.com>"; type="application/xop+xml";'
                ' boundary="uuid:7902e9bd-21a8-4632-8760-d79a67eb89a1"; start-info="application/soap+xml"',
            )
        resp = app.get('/sne/test/get-demande-logement?demand_id=0690221008931G3164')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert (
            json_resp['err_desc']
            == 'Votre guichet enregistreur ne couvre pas au moins une des communes souhaitées de la demande de logement.'
        )


def test_get_demande_logement_missing(app, connector, settings):
    with responses.RequestsMock() as rsps:
        resp = app.get('/sne/test/get-demande-logement?demand_id=')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['err_desc'] == 'demand_id must contains 18 characters'
        assert len(rsps.calls) == 0


def test_get_demande_logement_bad_length(app, connector, settings):
    with responses.RequestsMock() as rsps:
        resp = app.get('/sne/test/get-demande-logement?demand_id=1234')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['err_desc'] == 'demand_id must contains 18 characters'
        assert len(rsps.calls) == 0


def test_get_demande_bad_wsdl(app, connector, settings):
    with responses.RequestsMock() as rsps:
        rsps.get(
            'https://sne-ws-2.site-ecole.din.developpement-durable.gouv.invalid/services/DemandeLogementImplService/?wsdl',
            status=200,
            body='not xml',
        )
        resp = app.get('/sne/test/get-demande-logement?demand_id=0690221008931G3163')
        assert resp.json['err'] == 1
        assert resp.json['err_class'] == 'passerelle.utils.soap.SOAPInvalidContent'
        assert 'Invalid XML content received' in resp.json['err_desc']
