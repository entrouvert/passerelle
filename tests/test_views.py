from tests.test_manager import login


def test_get_does_not_crash_on_unknown_connector(app, db, admin_user):
    app = login(app)

    resp = app.get('/manage/opengis/noop/logs/', status='*')

    assert resp.status_code == 404
