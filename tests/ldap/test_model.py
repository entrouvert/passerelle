# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ldap
import pytest
from django.core.files.base import ContentFile
from ldaptools.slapd import has_slapd

pytestmark = pytest.mark.skipif(not has_slapd(), reason='slapd is not installed')


def test_get_connection(resource):
    resource.get_connection()


class TestCheckStatus:
    def test_nok(self, resource):
        with pytest.raises(ldap.LDAPError):
            resource.check_status()

    def test_ok(self, resource, ldap_server):
        resource.check_status()


class TestTLSAuthentication:
    @pytest.fixture
    def ldap_params(self, ldap_params, key, cert):
        ldap_params['ldap_url'] = ldap_params['ldap_url'].replace('ldap:', 'ldaps:')
        return {**ldap_params, 'tls': (str(key), str(cert))}

    @pytest.fixture
    def ldap_configure(self, ldap_object, cert):
        conn = ldap_object.get_connection_admin()
        conn.modify_s(
            'cn=config',
            [
                (ldap.MOD_ADD, 'olcTLSCACertificateFile', str(cert).encode()),
                (ldap.MOD_ADD, 'olcTLSVerifyClient', b'demand'),
            ],
        )

    @pytest.fixture
    def resource_params(self, resource_params, cert_content, key_content):
        return {
            **resource_params,
            'ldap_tls_cert': ContentFile(cert_content, name='cert.pem'),
            'ldap_tls_key': ContentFile(key_content, name='key.pem'),
            'ldap_tls_cacert': ContentFile(cert_content, name='cert.pem'),
        }

    def test_ok(self, resource, ldap_server):
        resource.check_status()


class TestLdapSearch:
    def test_nok(self, resource):
        with pytest.raises(ldap.LDAPError):
            list(resource.ldap_search('o=orga', ldap.SCOPE_SUBTREE, 'objectClass=*', ['*']))

    def test_ok(self, resource, ldap_server):
        entries = list(resource.ldap_search('o=orga', ldap.SCOPE_SUBTREE, 'objectClass=*', ['*']))
        assert entries == [('o=orga', {'o': 'orga', 'objectclass': 'organization'})]
