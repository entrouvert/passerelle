# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from unittest import mock

import pytest
import responses
import responses.matchers
from django.test import override_settings

import tests.utils
from passerelle.apps.proxy.models import Resource
from passerelle.base.models import ResourceLog


@pytest.fixture
def proxy(db):
    return tests.utils.setup_access_rights(
        Resource.objects.create(slug='echo', upstream_base_url='https://example.org/')
    )


@pytest.fixture
def endpoint(proxy):
    return tests.utils.generic_endpoint_url('proxy', 'request', slug=proxy.slug)


@pytest.fixture
def mocked_responses():
    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsp:
        yield rsp


def test_get(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[
            responses.matchers.query_string_matcher(''),
        ],
    )

    resp = app.get(endpoint + '/foo/bar', status=200)
    assert resp.text == 'ok'


def test_status(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(url='https://example.org/foo/bar', body='bad request', status=400)
    resp = app.get(endpoint + '/foo/bar', status=400)
    assert resp.text == 'bad request'


def test_post(mocked_responses, app, proxy, endpoint):
    mocked_responses.post(
        url='https://example.org/post',
        body='ok',
        match=[
            responses.matchers.json_params_matcher({'foo': 'bar'}),
        ],
    )
    resp = app.post_json(endpoint + '/post', params={'foo': 'bar'}, status=200)
    assert resp.text == 'ok'


def no_header_matcher(header_name):
    def inner(request):
        if header_name in request.headers:
            return False, f'Found header "{header_name} in requests headers.'
        return True, None

    return inner


def test_headers(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[
            responses.matchers.header_matcher({'User-Agent': 'test-ua'}),
            no_header_matcher('x-foo'),
            no_header_matcher('dontpass'),
        ],
    )
    resp = app.get(endpoint + '/foo/bar', headers={'user-agent': 'test-ua', 'dontpass': 'x'}, status=200)
    assert resp.text == 'ok'


def test_forced_headers(mocked_responses, app, proxy, endpoint):
    proxy.forced_headers = '''
x-foo :  bar
badentry

# comment: do not use me
    '''
    proxy.save()

    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[
            responses.matchers.header_matcher({'x-foo': 'bar'}),
            no_header_matcher('dontpass'),
        ],
    )
    resp = app.get(endpoint + '/foo/bar', headers={'user-agent': 'test', 'dontpass': 'x'}, status=200)
    assert resp.text == 'ok'


def test_query_parameters(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[
            responses.matchers.query_param_matcher({'param1': '1', 'param2': '2'}, strict_match=True),
        ],
    )

    resp = app.get(endpoint + '/foo/bar?param1=1&param2=2', status=200)
    assert resp.text == 'ok'


def test_publik_signature_is_removed(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[
            responses.matchers.query_param_matcher({'param1': '1', 'param2': '2'}, strict_match=True),
        ],
    )
    resp = app.get(
        endpoint + '/foo/bar?param1=1&param2=2&orig=coucou&algo=foo&nonce=bar&timestamp=xxx&signature=okok',
        status=200,
    )
    assert resp.text == 'ok'


def test_basic_auth(mocked_responses, app, proxy, endpoint):
    proxy.auth_type = 'basic_auth'
    proxy.basic_auth_username = 'test-login'
    proxy.basic_auth_password = 'test-pass'
    proxy.save()

    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.header_matcher({'Authorization': 'Basic dGVzdC1sb2dpbjp0ZXN0LXBhc3M='})],
    )
    resp = app.get(endpoint + '/foo/bar', status=200)
    assert resp.text == 'ok'


def test_oauth2(mocked_responses, app, proxy, endpoint):
    proxy.auth_type = 'oauth2'
    proxy.oauth2_token_url = 'https://auth.local/token'
    proxy.oauth2_username = 'test-login'
    proxy.oauth2_password = 'test-pass'
    proxy.save()

    mocked_responses.post(
        url='https://auth.local/token',
        json={'access_token': '1234', 'expires_in': 4060},
        match=[responses.matchers.header_matcher({'Authorization': 'Basic dGVzdC1sb2dpbjp0ZXN0LXBhc3M='})],
    )
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.header_matcher({'Authorization': 'Bearer 1234'})],
    )
    with mock.patch('passerelle.utils.http_authenticators.cache') as cache_mock:
        attrs = {'get.return_value': None}
        cache_mock.configure_mock(**attrs)
        resp = app.get(endpoint + '/foo/bar', status=200)
        assert resp.text == 'ok'
        key = f'TokenAuth:{proxy.pk}:2a6b90a83ea3deedb30f228ac414fda8318d2604ecccc26ffbe7e131d6311a94'
        cache_mock.get.assert_called_once_with(key)
        cache_mock.set.assert_called_once_with(key, 'Bearer 1234', 4000)


def test_amazon_cognito(mocked_responses, app, proxy, endpoint):
    proxy.auth_type = 'amazon_cognito_access_token'
    proxy.oauth2_token_url = 'https://auth.local/token'
    proxy.oauth2_username = 'test-login'
    proxy.oauth2_password = 'test-pass'
    proxy.amazon_cognito_client_id = 'clientid'
    proxy.save()

    mocked_responses.post(
        url='https://auth.local/token',
        json={'AuthenticationResult': {'AccessToken': '1234', 'ExpiresIn': 4060, 'TokenType': 'Bearer'}},
        match=[responses.matchers.header_matcher({'Content-Type': 'application/x-amz-json-1.1'})],
    )
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.header_matcher({'Authorization': 'Bearer 1234'})],
    )
    with mock.patch('passerelle.utils.http_authenticators.cache') as cache_mock:
        attrs = {'get.return_value': None}
        cache_mock.configure_mock(**attrs)
        resp = app.get(endpoint + '/foo/bar', status=200)
        assert resp.text == 'ok'
        key = f'AmazonCognito:{proxy.pk}:7bfb2ff33b0edfcdc8e7fe29381113db5dcc042ea8ceb661ef21c71f80cbf857'
        cache_mock.get.assert_called_once_with(key)
        cache_mock.set.assert_called_once_with(key, 'Bearer 1234', 3540)


def test_timeout(mocked_responses, app, proxy, endpoint):
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.request_kwargs_matcher({'timeout': 5})],
    )
    resp = app.get(endpoint + '/foo/bar', status=200)
    assert resp.text == 'ok'

    proxy.http_timeout = 10
    proxy.save()
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.request_kwargs_matcher({'timeout': 10})],
    )
    resp = app.get(endpoint + '/foo/bar', status=200)

    proxy.http_timeout = 0  # system default = settings.REQUESTS_TIMEOUT
    proxy.save()
    mocked_responses.get(
        url='https://example.org/foo/bar',
        body='ok',
        match=[responses.matchers.request_kwargs_matcher({'timeout': 25})],
    )
    resp = app.get(endpoint + '/foo/bar', status=200)
    assert resp.text == 'ok'


def test_add_edit_auth(app, admin_user):
    resp = app.get('/manage/', status=200, user=admin_user)
    resp = resp.click('Add Connector')
    assert 'Proxy' in resp.text
    resp = resp.click('Proxy')
    form = resp.form
    form['title'] = 'Test Connector'
    form['slug'] = 'test-connector'
    form['description'] = 'Connector for a simple test'
    form['upstream_base_url'] = 'https://none.local'
    resp = form.submit()
    assert resp.status_int == 302
    assert resp.location.endswith('/proxy/test-connector/')
    resp = resp.follow()
    assert 'Proxy - Test Connector' in resp.text

    resp = resp.click('Edit')
    form = resp.form

    # amazon cognito auth is disabled by default
    with pytest.raises(ValueError):
        form['auth_type'] = 'amazon_cognito_access_token'

    # we use basic auth
    form['auth_type'] = 'basic_auth'
    resp = form.submit()
    err = 'This field is required for basic authentication'
    for field in ('basic_auth_username', 'basic_auth_password'):
        assert err == resp.pyquery(f'#id_{field}').parent().text()
    form['basic_auth_username'] = 'username'
    form['basic_auth_password'] = 'password'

    resp = form.submit()
    resp = resp.follow()

    # we use oauth2
    resp = resp.click('Edit')
    form = resp.form
    form['auth_type'] = 'oauth2'
    resp = form.submit()
    oauth_fields = ('oauth2_token_url', 'oauth2_username', 'oauth2_password')
    err = 'This field is required for OAuth2 authentication'
    for field in oauth_fields:
        assert err == resp.pyquery(f'#id_{field}').parent().text()
    form['oauth2_token_url'] = 'https://auth.local'
    form['oauth2_username'] = 'username'
    form['oauth2_password'] = 'password'

    resp = form.submit()
    resp = resp.follow()

    # we use cognito
    with override_settings(PASSERELLE_APP_PROXY_COGNITO_ENABLED=True):
        cognito_fields = oauth_fields + ('amazon_cognito_client_id',)
        for auth_type in ('amazon_cognito_access_token', 'amazon_cognito_id_token'):
            resp = resp.click('Edit')
            form = resp.form
            form['auth_type'] = auth_type
            for field in cognito_fields:
                form[field] = ''
            resp = form.submit()
            err = 'This field is required for Amazon Cognito authentication'
            for field in cognito_fields:
                assert err == resp.pyquery(f'#id_{field}').parent().text()
            form['oauth2_token_url'] = 'https://auth.local'
            form['oauth2_username'] = 'username'
            form['oauth2_password'] = 'password'
            form['amazon_cognito_client_id'] = 'clientid'

            resp = form.submit()
            resp = resp.follow()

    resp = app.get('/manage/', status=200)
    assert 'Test Connector' in resp.text
    assert 'status-' not in resp.text


def test_check_status(app, admin_user, proxy, endpoint, mocked_responses):
    status_url = tests.utils.generic_endpoint_url('proxy', 'up', slug=proxy.slug)
    assert not proxy.has_check_status()
    proxy.status_url = 'https://status.example.org'
    assert proxy.has_check_status()
    proxy.save()
    assert proxy.availability_parameters.run_check

    mocked_responses.get(
        url='https://status.example.org',
    )

    proxy.availability()
    assert not proxy.down()

    resp = app.get(status_url, status=200)
    assert resp.status_code == 200

    mocked_responses.get(
        url='https://status.example.org',
        body='ko',
        status=500,
    )
    proxy.availability()
    assert proxy.down()

    ResourceLog.objects.all().delete()

    resp = app.get(status_url, status=200)
    assert resp.status_code == 200
    assert 'check the logs' not in resp.text
    assert ResourceLog.objects.count() == 0

    resp = app.get(status_url, status=200, user=admin_user)
    assert resp.status_code == 200
    assert 'check the logs' in resp.text
    assert ResourceLog.objects.count() == 1
    rl = ResourceLog.objects.first()
    assert rl.extra['error_summary'] == [
        'requests.exceptions.HTTPError: 500 Server Error: Internal Server Error for url: '
        'https://status.example.org/\n'
    ]

    # logout
    app.cookiejar.clear()

    # test with text
    mocked_responses.reset()

    proxy.status_text = 'ok'
    assert proxy.has_check_status()
    proxy.save()

    mocked_responses.get(
        url='https://status.example.org',
        body='ok',
    )
    proxy.availability()
    assert not proxy.down()

    mocked_responses.get(
        url='https://status.example.org',
        body='ko',
        status=200,
    )
    proxy.availability()
    assert proxy.down()

    assert proxy.has_check_status()
    resp = app.get('/proxy/echo/')
    resp.mustcontain('Down')

    proxy.status_url = ''
    assert not proxy.has_check_status()
    proxy.save()

    resp = app.get('/proxy/echo/')
    resp.mustcontain(no='Down')
