from unittest import mock

import pytest
import responses
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache

from passerelle.apps.r2p.models import R2P
from passerelle.base.models import AccessRight, ApiUser


@pytest.fixture()
def connector(db):
    api = ApiUser.objects.create(username='all', keytype='', key='')
    connector = R2P.objects.create(
        api_url='https://r2p.invalid', slug='test', oauth_username='foo', oauth_password='bar'
    )
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )
    return connector


def mock_tocken(rsps, token='1234'):
    rsps.post(
        'https://r2p.invalid/token',
        status=200,
        json={'access_token': token},
    )


def test_token(app, connector, freezer):
    cache.clear()
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/1257553562447',
            status=200,
            json={
                'personnePhysique': {
                    'identifiant': {'spi': '1257553562447'},
                    'etatCivil': {'cdSexe': '1', 'lbNomNaissance': 'SIMON'},
                }
            },
        )
        resp = app.get('/r2p/test/personne-by-spi?spi=1257553562447')
        assert resp.json['err'] == 0
        token_request = rsps.calls[0].request
        assert token_request.headers['Content-Type'] == 'application/x-www-form-urlencoded'
        assert token_request.headers['Authorization'] == 'Basic Zm9vOmJhcg=='
        assert (
            token_request.body
            == 'grant_type=client_credentials&scope=api_r2p_recherche_personne_physique+api_r2p_recherche_personne_physique+api_r2p_resolution_spi_degrade'
        )
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer 1234'
        rsps.reset()

    with responses.RequestsMock() as rsps:
        # no more call to obtain a token, get it from cache
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/1257553562447',
            status=200,
            json={
                'personnePhysique': {
                    'identifiant': {'spi': '1257553562447'},
                    'etatCivil': {'cdSexe': '1', 'lbNomNaissance': 'SIMON'},
                }
            },
        )
        resp = app.get('/r2p/test/personne-by-spi?spi=1257553562447')
        assert resp.json['err'] == 0
        assert rsps.calls[0].request.headers['Authorization'] == 'Bearer 1234'

    # move time forward to invalidate token in cache and check that a new token is grabbed
    freezer.tick(302)
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps, '5678')
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/1257553562447',
            status=200,
            json={
                'personnePhysique': {
                    'identifiant': {'spi': '1257553562447'},
                    'etatCivil': {'cdSexe': '1', 'lbNomNaissance': 'SIMON'},
                }
            },
        )
        resp = app.get('/r2p/test/personne-by-spi?spi=1257553562447')
        assert resp.json['err'] == 0
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer 5678'
        rsps.reset()


def test_oauth_token_cache(app, connector):
    cache.clear()
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/1257553562447',
            status=200,
            json={
                'personnePhysique': {
                    'identifiant': {'spi': '1257553562447'},
                    'etatCivil': {'cdSexe': '1', 'lbNomNaissance': 'SIMON'},
                }
            },
        )
        with mock.patch('passerelle.utils.http_authenticators.cache') as cache_mock:
            attrs = {'get.return_value': None}
            cache_mock.configure_mock(**attrs)
            resp = app.get('/r2p/test/personne-by-spi?spi=1257553562447')
            assert resp.json['err'] == 0
            key = f'TokenAuth:{connector.pk}:50c22fee4ea29c3c4598d99f9f65bf32d97e541db56a1b8125f6d822049da3f9'
            cache_mock.get.assert_called_once_with(key)
            cache_mock.set.assert_called_once_with(key, 'Bearer 1234', 300)


@pytest.mark.parametrize('hide_spi', [True, False])
def test_personne_by_spi(app, connector, hide_spi):
    connector.hide_spi = hide_spi
    connector.save()
    cache.clear()
    data = {
        'personnePhysique': {
            'identifiant': {'spi': '1257553562447'},
            'etatCivil': {'cdSexe': '1', 'lbNomNaissance': 'SIMON'},
        }
    }
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        rsps.get('https://r2p.invalid/r2p/v1/personne/1257553562447', status=200, json=data)
        resp = app.get('/r2p/test/personne-by-spi?spi=1257553562447')
        json_resp = resp.json
        assert json_resp['err'] == 0
        if hide_spi:
            data['personnePhysique']['identifiant']['spi'] = 'ANONYMIZED'
        assert json_resp['data'] == data


def test_personne_by_spi_error(app, connector):
    cache.clear()
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/123',
            status=400,
            json={'erreur': {'code': '40015', 'message': '[spi] Format du SPI erroné'}},
        )
        resp = app.get('/r2p/test/personne-by-spi?spi=123')
        json_resp = resp.json
        assert json_resp['err'] == 1
        assert json_resp['err_class'] == 'passerelle.utils.jsonresponse.APIError'
        assert (
            json_resp['err_desc']
            == '400 Client Error: Bad Request for url: https://r2p.invalid/r2p/v1/personne/123'
        )
        assert json_resp['data']['erreur']['code'] == '40015'
        assert json_resp['data']['erreur']['message'] == '[spi] Format du SPI erroné'


@pytest.mark.parametrize('hide_spi', [True, False])
def test_personne_by_criteria(app, connector, hide_spi):
    connector.hide_spi = hide_spi
    connector.save()
    cache.clear()
    data = {
        'personnePhysique': {
            'identifiant': {'spi': '7540305732558'},
            'etatCivil': {
                'cdSexe': '1',
                'lbNomNaissance': 'garcia',
                'lbPrenomNaissance': 'samuel',
                'lbNomUsage': 'garcia',
                'lbPrenomUsage': 'samuel',
                'anneeNaissance': '1943',
                'moisNaissance': '06',
                'jourNaissance': '15',
                'cdPaysNaissance': '99100',
                'cdDeptNaissance': '75',
                'cdTOMNaissance': None,
                'cdCommuneNaissance': '109',
            },
            'adresse': {
                'cdPays': '99100',
                'lbPays': 'FRANCE',
                'cdDepartement': '75',
                'lbDepartement': 'PARIS',
                'cdTOM': None,
                'lbTOM': None,
                'cdCommune': '112',
                'lbCommune': 'PARIS',
                'cdVoie': '0881',
                'lbVoie': 'RUE DE BERCY',
                'numeroVoie': '139',
                'indiceDeRepetition': None,
            },
        }
    }
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        params = {
            'prenom': 'samuel',
            'nom': 'garcia',
            'sexe': '1',
            'naisDate': '15/06/1943',
            'naisCodePays': '99100',
            'naisCodeCommune': '109',
            'naisCodeDept': '75',
        }
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne',
            status=200,
            match=[responses.matchers.query_param_matcher(params)],
            json=data,
        )
        resp = app.get('/r2p/test/personne-by-criteria', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        if hide_spi:
            data['personnePhysique']['identifiant']['spi'] = 'ANONYMIZED'
        assert json_resp['data'] == data


def test_personne_by_criteria_error(app, connector):
    cache.clear()
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        params = {
            'prenom': 'samuel',
            'nom': 'garcia',
            'sexe': '1',
        }
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne',
            status=400,
            match=[responses.matchers.query_param_matcher(params)],
            json={'erreur': {'code': '40005', 'message': '[naisDate] Date de naissance absente'}},
        )
        resp = app.get('/r2p/test/personne-by-criteria', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 1
        assert json_resp['data'] == {
            'erreur': {'code': '40005', 'message': '[naisDate] Date de naissance absente'}
        }


@pytest.mark.parametrize('hide_spi', [True, False])
def test_spi_by_criteria(app, connector, hide_spi):
    connector.hide_spi = hide_spi
    connector.save()
    cache.clear()
    data = {
        'personnePhysique': {
            'identifiant': {'spi': '7540305732558'},
            'etatCivil': {
                'cdSexe': '1',
                'lbNomNaissance': 'garcia',
                'lbPrenomNaissance': 'samuel',
                'lbNomUsage': 'garcia',
                'lbPrenomUsage': 'samuel',
                'anneeNaissance': '1943',
                'moisNaissance': '06',
                'jourNaissance': '15',
                'cdPaysNaissance': '99100',
                'cdDeptNaissance': '75',
                'cdTOMNaissance': None,
                'cdCommuneNaissance': '109',
            },
            'adresse': {
                'cdPays': '99100',
                'lbPays': 'FRANCE',
                'cdDepartement': '75',
                'lbDepartement': 'PARIS',
                'cdTOM': None,
                'lbTOM': None,
                'cdCommune': '112',
                'lbCommune': 'PARIS',
                'cdVoie': '0881',
                'lbVoie': 'RUE DE BERCY',
                'numeroVoie': '139',
                'indiceDeRepetition': None,
            },
        }
    }
    with responses.RequestsMock() as rsps:
        mock_tocken(rsps)
        params = {
            'prenom': 'samuel',
            'nom': 'garcia',
            'sexe': '1',
            'naisDate': '15/06/1943',
            'naisCodePays': '99100',
            'naisCodeCommune': '109',
            'naisCodeDept': '75',
        }
        rsps.get(
            'https://r2p.invalid/r2p/v1/personne/spi',
            status=200,
            match=[responses.matchers.query_param_matcher(params)],
            json=data,
        )
        resp = app.get('/r2p/test/spi-by-criteria', params=params)
        json_resp = resp.json
        assert json_resp['err'] == 0
        if hide_spi:
            data['personnePhysique']['identifiant']['spi'] = 'ANONYMIZED'
        assert json_resp['data'] == data
