# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from passerelle.contrib.utils.axel import encode_datetime, parse_datetime


def test_parse_datetime():
    # wrong format
    assert parse_datetime('foo') is None
    assert parse_datetime('2019-12-12') is None
    assert parse_datetime('2019-12-12T12:01:72') is None
    # ok
    assert parse_datetime('2019-12-12T12:01:42').isoformat() == '2019-12-12T12:01:42+00:00'


def test_encode_datetime():
    assert encode_datetime(parse_datetime('2019-12-12T23:40:42')) == '13/12/2019 00:40:42'
