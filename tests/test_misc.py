import datetime
from unittest.mock import patch

import pytest
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.db import connection
from django.test.utils import CaptureQueriesContext
from django.urls import reverse
from django.utils import timezone

from passerelle.apps.opengis.models import OpenGIS
from passerelle.apps.proxy.models import Resource as ProxyResource
from passerelle.base.models import ResourceLog
from passerelle.utils.jsonresponse import APIError
from tests.test_manager import login


def test_get_description_url_fields(db):
    connector = OpenGIS(slug='plop', wms_service_url='http://www.example.net')
    assert 'http://www.example.net' in [x[1] for x in connector.get_description_fields()]

    connector = OpenGIS(slug='plop', wms_service_url='http://username:secret@www.example.net')
    assert 'http://***:***@www.example.net' in [x[1] for x in connector.get_description_fields()]

    connector = OpenGIS(slug='plop', wms_service_url='http://username@example.net:secret@www.example.net')
    assert 'http://***:***@www.example.net' in [x[1] for x in connector.get_description_fields()]


def test_get_description_secret_fields(db):
    connector = ProxyResource(slug='plop', oauth2_password='secret1', basic_auth_password='secret2')
    assert 'secret1' not in [x[1] for x in connector.get_description_fields()]
    assert 'secret2' not in [x[1] for x in connector.get_description_fields()]


def test_log_cleaning(app, db, admin_user, settings):
    ResourceLog.objects.all().delete()
    connector = OpenGIS(slug='plop', wms_service_url='http://www.example.net')
    connector.save()
    connector.logger.error('hello1')
    connector.logger.error('hello2')

    assert ResourceLog.objects.all().count() == 2

    ResourceLog.objects.update(timestamp=timezone.now() - datetime.timedelta(days=10))
    connector.logger.error('hello3')
    assert ResourceLog.objects.all().count() == 3

    settings.LOG_RETENTION_DAYS = 11
    call_command('cron', 'daily')
    assert ResourceLog.objects.all().count() == 3
    settings.LOG_RETENTION_DAYS = 10
    call_command('cron', 'daily')
    assert ResourceLog.objects.all().count() == 1

    ResourceLog.objects.all().delete()
    connector.logger.error('hello1')
    connector.logger.error('hello2')
    assert ResourceLog.objects.all().count() == 2
    ResourceLog.objects.update(timestamp=timezone.now() - datetime.timedelta(days=10))
    connector.logger.error('hello3')
    assert ResourceLog.objects.all().count() == 3

    url = reverse(
        'logging-parameters',
        kwargs={
            'resource_type': ContentType.objects.get_for_model(connector).id,
            'resource_pk': connector.id,
        },
    )
    app = login(app)
    resp = app.get(url)
    assert not resp.html.find('input', {'name': 'log_retention_days'}).has_attr('value')
    resp.form['log_retention_days'] = '11'
    resp.form.submit()
    call_command('cron', 'daily')
    assert ResourceLog.objects.all().count() == 3

    resp = app.get(url)
    assert int(resp.html.find('input', {'name': 'log_retention_days'})['value']) == 11
    resp.form['log_retention_days'] = '10'
    resp.form.submit()
    call_command('cron', 'daily')
    assert ResourceLog.objects.all().count() == 1

    resp = app.get(connector.get_absolute_url())
    # click burger menu & confirm deletion
    resp.click('Delete').form.submit()
    assert ResourceLog.objects.all().count() == 0


def test_log_cleaning_multiple_delay(transactional_db, freezer):
    connectors = [
        OpenGIS.objects.create(slug=f'plop{i}', wms_service_url='http://www.example.net') for i in range(5)
    ]

    for i in range(2):
        logging_parameters = connectors[i].logging_parameters
        logging_parameters.log_retention_days = 10
        logging_parameters.save()

    for i in range(5):
        connectors[i].logger.error('hello1')
        connectors[i].logger.error('hello2')

    # check VACUUM ANALYZE is called only 1 time
    with CaptureQueriesContext(connection) as context:
        call_command('cron', 'daily')
        assert str(list(context)).count('VACUUM ANALYZE base_resourcelog') == 1

    assert ResourceLog.objects.all().count() == 10

    # move 1 week into the future
    freezer.tick(datetime.timedelta(days=7.1))

    call_command('cron', 'daily')

    # 6 log lines have been deleted (from plop2, 3 and 4)
    assert ResourceLog.objects.all().count() == 4
    # only logs of plop0 and plop1 have been kept
    assert set(ResourceLog.objects.values_list('slug', flat=True)) == {'plop0', 'plop1'}


@pytest.fixture
def email_handler():
    import logging

    from django.utils.log import AdminEmailHandler

    root = logging.getLogger()
    handler = AdminEmailHandler(include_html=True)
    handler.level = logging.ERROR
    root.handlers.append(handler)
    try:
        yield
    finally:
        root.handlers.remove(handler)


@pytest.mark.parametrize(
    'exc,amounts',
    (
        (ValueError('coin'), [1, 2, 1]),
        (APIError(log_error=False, http_status=500), [0, 0, 0]),
        (APIError(log_error=True, http_status=500), [0, 1, 0]),
    ),
)
def test_trace_emails(app, settings, dummy_csv_datasource, exc, amounts, email_handler, mailoutbox):
    from tests.utils import generic_endpoint_url

    settings.ADMINS = [('admin', 'admin@example.net')]

    logging_parameters = dummy_csv_datasource.logging_parameters
    logging_parameters.save()

    assert not mailoutbox

    with patch.object(dummy_csv_datasource.__class__, 'execute_query', side_effect=exc, autospec=True):
        app.get(
            generic_endpoint_url(
                connector='csvdatasource', endpoint='query/dummy-query/', slug=dummy_csv_datasource.slug
            ),
            status=500,
        )

        amount = amounts.pop(0)
        assert len([m.to for m in mailoutbox]) == amount
        if amount:
            # first mail is sent via ProxyLogger
            assert mailoutbox[0].to == ['admin@example.net']
        if amount > 1:
            # second mail is sent via sentry logger
            assert mailoutbox[1].to == ['admin@example.net']

        mailoutbox.clear()

        logging_parameters.trace_emails = 'john.doe@example.net'
        logging_parameters.save()
        app.get(
            generic_endpoint_url(
                connector='csvdatasource', endpoint='query/dummy-query/', slug=dummy_csv_datasource.slug
            ),
            status=500,
        )

        amount = amounts.pop(0)
        assert len([m.to for m in mailoutbox]) == amount
        if amount:
            # first mail is sent via ProxyLogger
            assert mailoutbox[0].to == ['john.doe@example.net']
        if amount > 1:
            # second mail is sent via sentry logger
            assert mailoutbox[1].to == ['admin@example.net']

        mailoutbox.clear()

        logging_parameters.trace_emails = '-'
        logging_parameters.save()
        app.get(
            generic_endpoint_url(
                connector='csvdatasource', endpoint='query/dummy-query/', slug=dummy_csv_datasource.slug
            ),
            status=500,
        )

        amount = amounts.pop(0)
        assert len([m.to for m in mailoutbox]) == amount
        if amount:
            # mail is sent via sentry logger
            assert mailoutbox[0].to == ['admin@example.net']
