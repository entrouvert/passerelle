# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2023 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest
from django.core.files.base import ContentFile

from passerelle.apps.pdf.models import Resource
from tests.test_manager import login
from tests.utils import setup_access_rights


@pytest.fixture
def pdf(db):
    return setup_access_rights(Resource.objects.create(slug='test', title='test', description='test'))


@pytest.fixture
def cerfa_content():
    with open('tests/data/cerfa_10072-02.pdf', 'rb') as fd:
        return fd.read()


def test_media(app, admin_user, simple_user, pdf, cerfa_content):
    pdf.fill_form_file.save('form.pdf', ContentFile(cerfa_content))

    # refuse anonymous or simple user
    resp = app.get('/media/pdf/test/form.pdf', status=302)
    assert resp.location == '/login/?next=/media/pdf/test/form.pdf'

    app = login(app, username='user', password='user')
    resp = app.get('/media/pdf/test/form.pdf', status=403)

    # allow manager access
    app = login(app, username='admin', password='admin')
    resp = app.get('/media/pdf/test/form.pdf')
    assert resp.content.startswith(b'%PDF')
    assert resp.headers['content-type'] == 'application/pdf'

    # bad requests: 404 or 400
    resp = app.get('/media/pdf/plop/there-is-not-file-here.pdf', status=404)
    resp = app.get('/media/pdf/bad-slug/form.pdf', status=404)
    resp = app.get('/media/pdf/', status=404)
    resp = app.get('/media/pdf', status=404)
    resp = app.get('/media/', status=404)
    resp = app.get('/media/../etc/passwd', status=400)
    resp = app.get('/media/../../../../../../../../etc/passwd', status=400)
