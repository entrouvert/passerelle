import datetime
import logging
import os

import pytest
import responses
from django.contrib.contenttypes.models import ContentType

import tests.utils
from passerelle.apps.mgdis.models import Mgdis
from passerelle.base.models import AccessRight, ApiUser, ResourceLog

pytestmark = pytest.mark.django_db

base_dir = os.path.dirname(__file__)

MGDIS_RESPONSE = {
    'data': {
        'tiers': [
            {
                'id': 'cOy8DSnSH',
                'referenceAdministrative': '00000180',
                'status': {'id': 'TEMPORARY', 'text': 'En cours de cr\u00e9ation'},
                'famille': {'id': ''},
                'demandes': [
                    {
                        'id': 'pFuGTUYDQ',
                        'text': 'Demande de financement - Associations',
                        'datetime': '2024-01-18T10:37:33.896Z',
                        'status': {'id': 'REQUESTED', 'text': 'En cours de saisie'},
                        'teleservice': {'text': 'Demande de financement - Associations', 'id': 'F_FINASSO'},
                        'form_status_is_endpoint': False,
                        'draft': True,
                        'url': 'https://mgdis.example.net/aides/#/mgdis-tenant/connecte/dashboard/pFuGTUYDQ/recapitulatif',
                        'action_usager': False,
                        'compte_demandeur': True,
                        'tiers_beneficiaire': True,
                    }
                ],
            },
            {
                'id': 'L7dYZD1_o',
                'text': 'Monsieur Foo Bar',
                'referenceAdministrative': '00000181',
                'status': {'id': 'TEMPORARY', 'text': 'En cours de cr\u00e9ation'},
                'famille': {'text': 'Particulier', 'id': '01'},
                'mail': 'foot@example.net',
                'telephone': '06 07 08 09 00',
                'demandes': [
                    {
                        'id': 'BgpRIrS0X',
                        'text': 'Transport - Allocation individuelle de transport - Monsieur Foo Bar',
                        'datetime': '2024-01-18T10:37:58.355Z',
                        'status': {'id': 'REQUESTED', 'text': 'En cours de saisie'},
                        'teleservice': {
                            'text': 'Transport - Allocation individuelle de transport',
                            'id': 'F_TRANSPAIT',
                        },
                        'url': 'https://mdgis.example.net/aides/#/mdgis-tenant/connecte/dashboard/BgpRIrS0X/recapitulatif',
                        'action_usager': False,
                        'compte_demandeur': True,
                        'tiers_beneficiaire': True,
                    }
                ],
            },
        ]
    },
    'err': 0,
}

MGDIS_ERROR_RESPONSE = {'data': {}, 'err': 500, 'err_desc': "Cet utilisateur n'est pas connu"}


@pytest.fixture
def mgdis():
    conn = Mgdis.objects.create(slug='test', base_url='https://mgdis.example.net/', tenant_id='mgdis-tenant')
    api = ApiUser.objects.create(username='all', keytype='', key='')
    obj_type = ContentType.objects.get_for_model(conn)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=conn.pk
    )

    return conn


@responses.activate
def test_get_user_demands(app, mgdis):
    endpoint = tests.utils.generic_endpoint_url('mgdis', 'demands', slug=mgdis.slug)
    assert endpoint == '/mgdis/test/demands'

    responses.add(
        responses.GET,
        f'{mgdis.base_url}pda-semi-public-api/api/tenants/{mgdis.tenant_id}/gru-publik/mes-demandes',
        json=MGDIS_RESPONSE,
        status=200,
    )

    resp = app.get(endpoint, params={'NameID': ''})
    assert len(responses.calls) == 0
    assert resp.json['err'] == 0
    assert len(resp.json['data']) == 0

    resp = app.get(endpoint, params={'NameID': 'xyz'})
    assert len(responses.calls) == 1
    assert resp.json['err'] == 0
    assert len(resp.json['data']) == 2
    for item in resp.json['data']:
        assert 'name' in item
        assert 'url' in item
        assert 'datetime' in item and datetime.datetime.strptime(item['datetime'], '%Y-%m-%d %H:%M:%S')
        assert 'form_receipt_datetime' in item
        assert 'status' in item

    resp = app.get(endpoint, params={'NameID': 'xyz', 'status': 'done'})
    assert len(resp.json['data']) == 0

    resp = app.get(endpoint, params={'NameID': 'xyz', 'tiers_id': 'L7dYZD1_o'})
    assert len(resp.json['data']) == 1


@responses.activate
def test_mgdis_invalid_response(app, mgdis):
    endpoint = tests.utils.generic_endpoint_url('mgdis', 'demands', slug=mgdis.slug)
    responses.add(
        responses.GET,
        f'{mgdis.base_url}pda-semi-public-api/api/tenants/{mgdis.tenant_id}/gru-publik/mes-demandes',
        body='<h2>Error 500</h2>',
        status=200,
    )

    resp = app.get(endpoint, params={'NameID': 'xyz'})
    assert resp.json['err'] == 1
    assert resp.json['err_desc'] == 'MGDIS error: unparsable response'


@responses.activate
def test_mgdis_error_response(app, mgdis):
    endpoint = tests.utils.generic_endpoint_url('mgdis', 'demands', slug=mgdis.slug)
    responses.add(
        responses.GET,
        f'{mgdis.base_url}pda-semi-public-api/api/tenants/{mgdis.tenant_id}/gru-publik/mes-demandes',
        json=MGDIS_ERROR_RESPONSE,
        status=200,
    )

    resp = app.get(endpoint, params={'NameID': 'xyz'})
    assert resp.json['err'] != 0
    assert not resp.json['data']


@responses.activate
def test_mgdis_invalid_response_status(app, mgdis, caplog):
    endpoint = tests.utils.generic_endpoint_url('mgdis', 'demands', slug=mgdis.slug)
    responses.add(
        responses.GET,
        f'{mgdis.base_url}pda-semi-public-api/api/tenants/{mgdis.tenant_id}/gru-publik/mes-demandes',
        json={'code': 403, 'message': 'Access is denied'},
        status=403,
    )

    resp = app.get(endpoint, params={'NameID': 'xyz'})
    assert resp.json['err'] != 0
    assert 'Invalid MGDIS response (403): ' in resp.json['err_desc']
    assert not resp.json['data']
    assert not ResourceLog.objects.filter(levelno=logging.ERROR).exists()
