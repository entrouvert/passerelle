import os

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

KNOWN_SERVICES = {
    'wcs': {
        'wcs1': {
            'url': 'http://example.org/',
            'verif_orig': 'wcs1',
            'secret': 'abcde',
        }
    }
}

# include all contrib apps
INSTALLED_APPS += (  # noqa pylint: disable=undefined-variable
    'passerelle.contrib.adict',
    'passerelle.contrib.caluire_axel',
    'passerelle.contrib.dpark',
    'passerelle.contrib.esirius_swi',
    'passerelle.contrib.fake_family',
    'passerelle.contrib.gdema',
    'passerelle.contrib.grandlyon_streetsections',
    'passerelle.contrib.greco',
    'passerelle.contrib.grenoble_gru',
    'passerelle.contrib.isere_ens',
    'passerelle.contrib.isere_esrh',
    'passerelle.contrib.iws',
    'passerelle.contrib.lille_urban_card',
    'passerelle.contrib.nancypoll',
    'passerelle.contrib.nantes_scrib',
    'passerelle.contrib.planitech',
    'passerelle.contrib.rsa13',
    'passerelle.contrib.sigerly',
    'passerelle.contrib.solis_apa',
    'passerelle.contrib.solis_afi_mss',
    'passerelle.contrib.strasbourg_eu',
    'passerelle.contrib.stub_invoices',
    'passerelle.contrib.teamnet_axel',
    'passerelle.contrib.tcl',
    'passerelle.contrib.toulouse_axel',
    'passerelle.contrib.toulouse_foederis',
    'passerelle.contrib.toulouse_maelis',
    'passerelle.contrib.toulouse_smart',
    'passerelle.contrib.lille_kimoce',
)

# enable applications that are otherwise disabled
PASSERELLE_APP_BDP_ENABLED = True
PASSERELLE_APP_GDC_ENABLED = True
PASSERELLE_APP_STRASBOURG_EU_ENABLED = True
PASSERELLE_APP_TOULOUSE_MAELIS_ENABLED = True

TCL_URL_TEMPLATE = 'http://tcl.example.net/%s'
TCL_GEOJSON_URL_TEMPLATE = 'http://tcl.example.net/geojson/%s'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'dummy': {'BACKEND': 'django.core.cache.backends.dummy.DummyCache'},
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': ('passerelle-test-%s' % os.environ.get('RAND_TEST', '')),
        },
    }
}

LOGGED_REQUESTS_MAX_SIZE = 4999

LEGACY_URLS_MAPPING = {'old.org': 'new.org'}

PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher']
