# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from unittest import mock

import pytest
import responses
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.urls import reverse
from requests.exceptions import ReadTimeout

from passerelle.apps.sivin.models import Resource
from passerelle.base.models import AccessRight, ApiUser

pytestmark = pytest.mark.django_db

EURO5_INDEXES = [
    ('0555*0651C Euro 4', False),
    ('0555*0651G Euro 5', True),
    ('0555*0651G EURO 5', True),
    ('0555*0874G (EURO 5)', True),
    ('134/2014EURO4', False),
    ('134/2014EURO5', True),
    ('175/2007*195/2013EURO6', True),
    ('200/55*2008/74EURO5', True),
    ('2005/55*51euro5', True),
    ('2006/51G-EUROV (G)', True),
    ('2006/96euro4', False),
    ('59/2009*2016/1718EURO6', True),
    ('595/2009*2018/932DEUROVI', True),
    ('595/2009*2018/932EURO', False),
    ('595/2009*627/2014EUROIV', False),
    ('70/220*1999/102EURO3', False),
    ('EURO 3', False),
    ('EURO III', False),
    ('EURO2', False),
    ('EURO5G', True),
    ('EURO6B', True),
    ('2001/100A', False),
]

VEHICLE_DETAILS = {
    'carrosserie': 'BERLINE',
    'clEnvironPrf': '70/220 2001/100EURO3',
    'codifVin': 'VF7FCKFVB26857835',
    'genreVCG': 'VP',
    'immatSiv': 'FS032GM',
    'genreVPrf': 'VP',
    'date1erCir': '2003-11-21',
    'nSiren': '000000000',
}

VEHICLE_THEORICAL_FINITION = {
    'carrosserie': 'COMBISPACE',
    'clEnvironPrf': '2001/100A',
    'codifVin': 'VF7GJRHYK93204774',
    'genreVCG': 'VP',
    'immatSiv': '01XT0747',
    'genreVPrf': 'VP',
    'date1erCir': '2004-11-19',
    'nSiren': '000000000',
}

EXPIRED_TOKEN_MESSAGE = (
    '<ams:fault xmlns:ams="http://wso2.org/apimanager/security">'
    '<ams:code>900901</ams:code><ams:message>Invalid Credentials</ams:message>'
    '<ams:description>'
    'Invalid Credentials. Make sure you have provided the correct security credentials'
    '</ams:description>'
    '</ams:fault>'
)


def mock_token(rsps, token_value='token_value'):
    rsps.post(
        'https://api.rec.sivin.fr/token',
        status=200,
        json={'access_token': token_value, 'scope': 'default', 'token_type': 'Bearer', 'expires_in': 660},
    )


@pytest.fixture
def conn():
    api_user = ApiUser.objects.create(username='sivin', keytype='API', key='sivinkey')
    connector = Resource.objects.create(
        title='Test', slug='test', consumer_key='key', consumer_secret='secret', environment='test'
    )
    obj_type = ContentType.objects.get_for_model(Resource)
    AccessRight.objects.create(
        codename='can_access', apiuser=api_user, resource_type=obj_type, resource_pk=connector.pk
    )
    return connector


def test_no_api_key(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consultervehiculeparvin', 'slug': conn.slug},
    )
    app.get(url, params={'vin': 'vin'}, status=403)


def test_wrong_credentials(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consultervehiculeparvin', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://api.rec.sivin.fr/token',
            json={'error_description': 'Client Authentication failed.', 'error': 'invalid_client'},
            status=401,
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'vin': 'VF1BA0E0514143067'}).json
    assert resp['err']
    assert resp['err_desc'] == 'Could not obtain a token. Service is unavailable'


def test_token(app, conn, freezer):
    cache.clear()
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterflotteparsiren', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren', json={'vins': ['VF1FB30A511331122']}
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'})
        assert resp.json['err'] == 0
        token_request = rsps.calls[0].request
        assert token_request.headers['Content-Type'] == 'application/x-www-form-urlencoded'
        assert token_request.headers['Authorization'] == 'Basic a2V5OnNlY3JldA=='
        assert token_request.body == 'grant_type=client_credentials'
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer token_value'
        rsps.reset()

    with responses.RequestsMock() as rsps:
        # no more call to obtain a token, get it from cache
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren', json={'vins': ['VF1FB30A511331122']}
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'})
        assert resp.json['err'] == 0
        assert rsps.calls[0].request.headers['Authorization'] == 'Bearer token_value'

    # move time forward to invalidate token in cache and check that a new token is grabbed
    freezer.tick(602)
    with responses.RequestsMock() as rsps:
        mock_token(rsps, token_value='5678')
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren', json={'vins': ['VF1FB30A511331122']}
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'})
        assert resp.json['err'] == 0
        assert rsps.calls[1].request.headers['Authorization'] == 'Bearer 5678'
        rsps.reset()


def test_check_status(app, conn, freezer):
    cache.clear()
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'up', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        resp = app.get(url)
        assert resp.json['err'] == 0
        assert conn.check_status() is None


def test_get_details_by_vin(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consultervehiculeparvin', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post('https://api.rec.sivin.fr/sivin/v2/consultervehiculeparvin', json=VEHICLE_DETAILS)
        resp = app.get(url, params={'apikey': 'sivinkey', 'vin': 'VF1BA0E0514143067'}).json
        assert not resp['err']
        assert resp['data'] == VEHICLE_DETAILS


def test_get_vehicles_by_siren(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterflotteparsiren', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren', json={'vins': ['VF1FB30A511331122']}
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'}).json
    assert not resp['err']
    for item in resp['data']:
        assert 'id' in item
        assert 'text' in item


def test_get_with_expired_token(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterflotteparsiren', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren',
            body=EXPIRED_TOKEN_MESSAGE,
            status=500,
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'}).json
    assert resp['err']
    assert resp['err_desc'] == f"b'{EXPIRED_TOKEN_MESSAGE}'"


@pytest.mark.parametrize('immat,sent_immat', [('747-xT 01', '01XT0747'), ('FD-734-hR', 'FD734HR')])
def test_get_vehicle_theorical_finition(app, conn, immat, sent_immat):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterfinitiontheoriqueparimmat', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterfinitiontheoriqueparimmat',
            json=VEHICLE_THEORICAL_FINITION,
        )
        resp = app.get(url, params={'apikey': 'sivinkey', 'immat': immat}).json
        assert json.loads(rsps.calls[-1].request.body) == {'immat': sent_immat}
    assert not resp['err']
    assert 'is_euro5' in resp['data']
    resp['data'].pop('is_euro5')
    assert resp['data'] == VEHICLE_THEORICAL_FINITION


def test_connection_timeout(app, conn):
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterflotteparsiren', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        rsps.post('https://api.rec.sivin.fr/token', body=ReadTimeout('timeout'))
        resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'}).json
    assert resp['err']
    assert resp['err_desc'] == 'Could not obtain a token. Service is unavailable.'


@pytest.mark.parametrize('clEnvironPrf, is_euro5', EURO5_INDEXES)
def test_compute_euro_index(app, conn, clEnvironPrf, is_euro5):
    assert conn.is_euro5(clEnvironPrf) == is_euro5


def test_oauth_token_cache(app, conn):
    cache.clear()
    url = reverse(
        'generic-endpoint',
        kwargs={'connector': 'sivin', 'endpoint': 'consulterflotteparsiren', 'slug': conn.slug},
    )
    with responses.RequestsMock() as rsps:
        mock_token(rsps)
        rsps.post(
            'https://api.rec.sivin.fr/sivin/v2/consulterflotteparsiren', json={'vins': ['VF1FB30A511331122']}
        )
        with mock.patch('passerelle.utils.http_authenticators.cache') as cache_mock:
            attrs = {'get.return_value': None}
            cache_mock.configure_mock(**attrs)
            resp = app.get(url, params={'apikey': 'sivinkey', 'siren': '000399634'})
            assert resp.json['err'] == 0
            key = f'TokenAuth:{conn.id}:917fc0baeed60bb95148eb70719fff4527b1702a017f6ad5d0e0147212a27580'
            cache_mock.get.assert_called_once_with(key)
            cache_mock.set.assert_called_once_with(key, 'Bearer token_value', 600)
