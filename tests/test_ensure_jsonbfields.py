from django.core.management import call_command
from django.db import connection


def test_ensure_jsonb_fields(db):
    with connection.cursor() as cursor:
        query = "SELECT table_name, column_name, data_type FROM information_schema.columns WHERE column_name IN ('_dialect_options', 'billing_regies')"
        cursor.execute(query)

        # make sure the data_type is correct
        for line in cursor.fetchall():
            assert line[2] == 'jsonb'

        # alter columns
        cursor.execute(
            'ALTER TABLE csvdatasource_csvdatasource ALTER COLUMN _dialect_options TYPE text USING _dialect_options::text'
        )

    call_command('ensure_jsonb')

    with connection.cursor() as cursor:
        query = "SELECT table_name, column_name, data_type FROM information_schema.columns WHERE column_name IN ('_dialect_options', 'billing_regies')"
        cursor.execute(query)

        # check the data_type is correct
        for line in cursor.fetchall():
            assert line[2] == 'jsonb'
