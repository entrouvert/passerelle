import base64

import pytest
import responses
from django.contrib.contenttypes.models import ContentType

from passerelle.apps.esup_signature.models import EsupSignature
from passerelle.base.models import AccessRight, ApiUser


@pytest.fixture()
def connector(db):
    api = ApiUser.objects.create(username='all', keytype='', key='')
    connector = EsupSignature.objects.create(
        base_url='https://esup-signature.invalid/',
        slug='esup-signature',
    )
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )
    return connector


def test_check_status(app, connector):
    with responses.RequestsMock() as rsps:
        rsps.get('https://esup-signature.invalid/ws/version', status=200, json={'err': 0, 'data': '29.29.28'})
        connector.availability()
        assert not connector.down()
        resp = app.get('/esup-signature/esup-signature/up/')
        assert resp.json['err'] == 0
    with responses.RequestsMock() as rsps:
        rsps.get(
            'https://esup-signature.invalid/ws/version',
            status=500,
        )
        connector.availability()
        assert connector.down()
        resp = app.get('/esup-signature/esup-signature/up/')
        assert resp.json['err'] == 1


def test_new(app, connector):
    params = {
        'file': {
            'filename': 'bla',
            'content': base64.b64encode(b'who what').decode(),
            'content_type': 'text/plain',
        },
        'recipients_emails/0': 'foo@invalid',
        'recipients_emails/1': 'bar@invalid',
        'eppn': 'baz@invalid',
        'title': 'a title',
    }
    with responses.RequestsMock() as rsps:
        query_params = {
            'recipientsEmails': ['foo@invalid', 'bar@invalid'],
            'eppn': 'baz@invalid',
            'title': 'a title',
            'signType': 'pdfImageStamp',
            'pending': True,
            'allSignToComplete': False,
            'userSignFirst': False,
            'forceAllSign': False,
        }
        rsps.post(
            'https://esup-signature.invalid/ws/signrequests/new',
            match=[responses.matchers.query_param_matcher(query_params)],
            status=200,
            json=9,
        )
        resp = app.post_json('/esup-signature/esup-signature/new', params=params)
        assert len(rsps.calls) == 1
        assert rsps.calls[0].request.headers['Content-Type'].startswith('multipart/form-data')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == 9


def test_new_with_workflow(app, connector):
    params = {
        'file': {
            'filename': 'bla',
            'content': base64.b64encode(b'who what').decode(),
            'content_type': 'text/plain',
        },
        'workflow_id': '99',
        'create_by_eppn': 'aa@foo.com',
        'title': 'a title',
        'recipients_emails/0': '0*xx@foo.com',
        'recipients_emails/1': '0*yy@foo.com',
        'recipients_emails/2': '1*zz@foo.com',
        'all_sign_to_completes/0': '12',
        'all_sign_to_completes/1': '13',
        'target_emails/0': 'xx@foo.com',
        'target_emails/1': 'yy@foo.com',
        'target_emails/2': 'zz@foo.com',
        'signrequest_params_jsonstring': 'List [ OrderedMap { "xPos": 100, "yPos": 100, "signPageNumber": 1 }, '
        'OrderedMap { "xPos": 200, "yPos": 200, "signPageNumber": 1 } ]',
        'target_urls/0': 'smb://foo.bar/location-1/',
        'target_urls/1': 'smb://foo.bar/location-2/',
    }
    with responses.RequestsMock() as rsps:
        query_params = {
            'createByEppn': 'aa@foo.com',
            'title': 'a title',
            'recipientsEmails': ['0*xx@foo.com', '0*yy@foo.com', '1*zz@foo.com'],
            'allSignToCompletes': ['12', '13'],
            'targetEmails': ['xx@foo.com', 'yy@foo.com', 'zz@foo.com'],
            'signRequestParamsJsonString': 'List [ OrderedMap { "xPos": 100, "yPos": 100, "signPageNumber": 1 }, '
            'OrderedMap { "xPos": 200, "yPos": 200, "signPageNumber": 1 } ]',
            'targetUrls': ['smb://foo.bar/location-1/', 'smb://foo.bar/location-2/'],
        }
        rsps.post(
            'https://esup-signature.invalid/ws/workflows/99/new',
            match=[responses.matchers.query_param_matcher(query_params)],
            status=200,
            json=9,
        )
        resp = app.post_json('/esup-signature/esup-signature/new-with-workflow', params=params)
        assert len(rsps.calls) == 1
        assert rsps.calls[0].request.headers['Content-Type'].startswith('multipart/form-data')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == 9


def test_status(app, connector):
    with responses.RequestsMock() as rsps:
        rsps.get('https://esup-signature.invalid/ws/signrequests/1', status=200, json={'status': 'completed'})
        resp = app.get('/esup-signature/esup-signature/status?signrequests_id=1')
        assert resp.json['err'] == 0
        assert resp.json['data']['status'] == 'completed'


def test_audit_trail(app, connector):
    with responses.RequestsMock() as rsps:
        rsps.get(
            'https://esup-signature.invalid/ws/signrequests/audit-trail/1',
            status=200,
            json={'id': 1, 'documentCheckSum': 'abc'},
        )
        resp = app.get('/esup-signature/esup-signature/audit-trail?signrequests_id=1')
        assert resp.json['err'] == 0
        assert resp.json['data']['id'] == 1
        assert resp.json['data']['documentCheckSum'] == 'abc'


def test_get_last_file(app, connector):
    with responses.RequestsMock() as rsps:
        headers = {'Content-Type': 'text/plain', 'Content-Disposition': 'attachment; filename=foo.txt'}
        rsps.get(
            'https://esup-signature.invalid/ws/signrequests/get-last-file/1',
            status=200,
            body='who hwat',
            headers=headers,
        )
        resp = app.get('/esup-signature/esup-signature/get-last-file?signrequests_id=1')
        assert resp.text == 'who hwat'


def test_forced_headers(app, connector):
    connector.forced_headers = 'X-Foo:bar'
    connector.save()
    with responses.RequestsMock() as rsps:
        headers = {'Content-Type': 'text/plain', 'Content-Disposition': 'attachment; filename=foo.txt'}
        rsps.get(
            'https://esup-signature.invalid/ws/signrequests/get-last-file/1',
            status=200,
            body='who hwat',
            headers=headers,
        )
        app.get('/esup-signature/esup-signature/get-last-file?signrequests_id=1')
        headers = rsps.calls[0].request.headers
        assert headers['X-Foo'] == 'bar'


def test_workflows(app, connector):
    with responses.RequestsMock() as rsps:
        rsps.get(
            'https://esup-signature.invalid/ws/workflows/all',
            status=200,
            json=[{'id': 0, 'description': 'foo'}, {'id': 1, 'description': 'bar'}],
        )
        resp = app.get('/esup-signature/esup-signature/workflows')
        json_resp = resp.json
        assert json_resp['err'] == 0
        assert json_resp['data'] == [
            {'id': 0, 'description': 'foo', 'text': 'foo'},
            {'id': 1, 'description': 'bar', 'text': 'bar'},
        ]
