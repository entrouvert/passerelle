# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy
import json
import os
from urllib.parse import unquote

import pytest
import responses
from django.test import override_settings
from requests.exceptions import ConnectionError

import tests.utils
from passerelle.apps.opendatasoft.models import OpenDataSoft, Query
from passerelle.utils import import_site
from tests.test_manager import login

pytestmark = pytest.mark.django_db


def get_json_file(filename):
    test_base_dir = os.path.join(os.path.dirname(__file__), 'data', 'opendatasoft')
    with open(os.path.join(test_base_dir, filename), 'rb') as desc:
        return json.load(desc)


API_V1_SEARCH = get_json_file('api_v1_search.json')
API_V1_ID_SEARCH = get_json_file('api_v1_search_id.json')
API_V2_Q_SEARCH = get_json_file('api_v2_records.json')
API_V2_DATASET = 'base-adresse-locale-tours-metropole-val-de-loire'
API_V2_FACETS = get_json_file('api_v2_facets.json')
API_V2_Q_SEARCH = get_json_file('api_v2_records.json')
API_V2_Q_SEARCH_WITH_DIST = get_json_file('api_v2_records_ordered.json')
API_V2_ID_SEARCH = get_json_file('api_v2_records_id.json')
API_V2_ODSQL_ERROR = get_json_file('api_v2_odsql_error.json')
API_V2_404_ERROR = get_json_file('api_v2_404_error.json')
API_V2_401_ERROR = get_json_file('api_v2_401_error.json')


@pytest.fixture
def connector():
    return tests.utils.setup_access_rights(
        OpenDataSoft.objects.create(
            slug='my_connector',
            service_url='http://www.example.net',
            api_key='my_secret',
        )
    )


@pytest.fixture
def query(connector):
    return Query.objects.create(
        resource=connector,
        name='Référenciel adresses de test',
        slug='my_query',
        description='Rechercher une adresse',
        dataset='referentiel-adresse-test',
        text_template='{{numero}} {{nom_rue}} {{nom_commun}}',
        filter_expression='''
refine.source=Ville et Eurométropole de Strasbourg
exclude.numero=42
exclude.numero=43
''',
        sort='-nom_rue',
        limit=3,
    )


@pytest.fixture
def query_api_v2(connector):
    return Query.objects.create(
        resource=connector,
        name='Référentiel adresses',
        slug='my_query',
        description='Rechercher une adresse',
        dataset=API_V2_DATASET,
        text_template='{{numero}} {{nom_rue}} {{nom_commun}} {{ geo_shape.geometry.coordinates }}',
        select="""
        numero,
        nom_rue,
        nom_commun
        """,
        where="""
        commune_nom='Rochecorbon'
        AND voie_nom='Chemin de la Planche'
        """,
        order_by='voie_nom asc, numero desc',
        exclude="""
        source:'Tours Métropole Val de Loire',
        source:'other',
        """,
        limit=3,
    )


def test_views(db, admin_user, app, connector):
    app = login(app)
    resp = app.get('/opendatasoft/my_connector/', status=200)
    resp = resp.click('New Query')
    resp.form['name'] = 'my query'
    resp.form['slug'] = 'my-query'
    resp.form['dataset'] = 'my-dataset'
    resp = resp.form.submit()
    resp = resp.follow()
    assert resp.html.find('div', {'id': 'panel-queries'}).ul.li.a.text == 'my query'


def test_export_import(query):
    assert OpenDataSoft.objects.count() == 1
    assert Query.objects.count() == 1
    serialization = {'resources': [query.resource.export_json()]}
    OpenDataSoft.objects.all().delete()
    assert OpenDataSoft.objects.count() == 0
    assert Query.objects.count() == 0
    import_site(serialization, overwrite=True)
    assert OpenDataSoft.objects.count() == 1
    assert Query.objects.count() == 1


def test_query_view_available_parameters(admin_user, app, query):
    app = login(app)

    def available_parameters(resp):
        params = resp.html.find_all('ul', {'class': 'get-params'})[-1]
        return [x.text for x in params.find_all('span', {'class': 'param-name'})]

    resp = app.get('/opendatasoft/my_connector/')
    assert available_parameters(resp) == ['id', 'q']


def test_api_v2_query_view_available_parameters(admin_user, app, query_api_v2):
    app = login(app)

    def available_parameters(resp):
        params = resp.html.find_all('ul', {'class': 'get-params'})[-1]
        return [x.text for x in params.find_all('span', {'class': 'param-name'})]

    resp = app.get('/opendatasoft/my_connector/')
    assert available_parameters(resp) == [
        'id',
        'q',
        'q_filter',
        'geo_point',
        'lat',
        'lon',
        'dist',
        'output',
        'group_by',
        'refine',
        'offset',
        'lang',
        'timezone',
    ]

    # add parameters to the query
    query_api_v2.refine = "commune_nom:'Rochecorbon'"
    query_api_v2.lang = 'fr'
    query_api_v2.timezone = 'UTC'
    query_api_v2.save()
    resp = app.get('/opendatasoft/my_connector/')
    assert available_parameters(resp) == [
        'id',
        'q',
        'q_filter',
        'geo_point',
        'lat',
        'lon',
        'dist',
        'output',
        'group_by',
        'offset',
    ]

    # remove parameters from the query, and set default values
    query_api_v2.select = ''
    query_api_v2.where = ''
    query_api_v2.order_by = ''
    query_api_v2.refine = ''
    query_api_v2.limit = 10
    query_api_v2.save()
    resp = app.get('/opendatasoft/my_connector/')
    assert available_parameters(resp) == [
        'id',
        'q',
        'q_filter',
        'geo_point',
        'lat',
        'lon',
        'dist',
        'output',
        'select',
        'where',
        'group_by',
        'order_by',
        'refine',
        'limit',
        'offset',
    ]

    # provide only API v1 parameters
    query_api_v2.lang = ''
    query_api_v2.timezone = ''
    query_api_v2.exclude = ''
    query_api_v2.filter_expression = 'some content'
    query_api_v2.save()
    resp = app.get('/opendatasoft/my_connector/')
    assert available_parameters(resp) == ['id', 'q']


def test_api_version_display_in_query_views(admin_user, app, query_api_v2):
    app = login(app)
    resp = app.get(query_api_v2.edit_url())

    # show which API will be used
    assert '(API v2)' in resp.html.find('div', {'id': 'appbar'}).text

    # provide parameters from both API v1 and v2
    resp.form['filter_expression'] = 'some content'
    resp = resp.form.submit()
    assert (
        'There were errors processing your form. You cannot mix parameters of several API versions.'
        in ' '.join([x.text for x in resp.html.find('div', {'class': 'errornotice'}).find_all('p')])
    )

    # empty API v2 parameters
    resp.form['select'] = ''
    resp.form['where'] = ''
    resp.form['order_by'] = ''
    resp.form['refine'] = ''
    resp.form['exclude'] = ''
    resp = resp.form.submit()
    assert not resp.html.find('div', {'class': 'errornotice'})
    resp = app.get(query_api_v2.edit_url())
    assert '(API v1)' in resp.html.find('div', {'id': 'appbar'}).text


def test_api_v2_query_view(admin_user, app, query_api_v2):
    app = login(app)

    def html_errors(resp):
        errors = {}
        errornotice_html = resp.html.find('div', {'class': 'errornotice'})
        if errornotice_html and errornotice_html.p.text == 'There were errors processing your form.':
            if len(errornotice_html.find_all()) > 1:
                errors['TOP'] = errornotice_html.find_all()[1].text
            else:
                errors['TOP'] = errornotice_html.p.text
        for widget_html in resp.html.find_all('div', {'class': 'widget'}):
            error_html = widget_html.find('div', {'class': 'error'})
            if error_html:
                errors[widget_html.div.label.text] = error_html.p.text.strip()
        return errors

    # lat, lon and geo_point requiring each others
    resp = app.get(query_api_v2.edit_url())
    resp.form['lat'] = '44.55'
    resp = resp.form.submit()
    assert html_errors(resp) == {
        'TOP': '"geo_point", "lat" and "lon" are requiring each other',
        'Geo point 2D (API v2):': '"geo_point", "lat" and "lon" are requiring each other',
        'Longitude (API v2):': '"geo_point", "lat" and "lon" are requiring each other',
    }
    resp.form['lon'] = '1.57'
    resp = resp.form.submit()
    assert html_errors(resp) == {
        'TOP': '"geo_point", "lat" and "lon" are requiring each other',
        'Geo point 2D (API v2):': '"geo_point", "lat" and "lon" are requiring each other',
    }
    resp.form['geo_point'] = 'geo_point_2d'
    resp = resp.form.submit()
    assert not html_errors(resp)

    # GeoJSON output required geo_point
    resp = app.get(query_api_v2.edit_url())
    resp.form['lat'] = ''
    resp.form['lon'] = ''
    resp.form['geo_point'] = ''
    resp.form['output'] = 'geojson'
    resp = resp.form.submit()
    assert html_errors(resp) == {
        'TOP': 'GeoJson output format needs require geo_point parameter',
        'Geo point 2D (API v2):': 'GeoJson output format needs require geo_point parameter',
    }
    resp.form['geo_point'] = 'geo_point_2d'
    resp = resp.form.submit()
    assert not html_errors(resp)

    # Distance requiered a value and a unit
    resp = app.get(query_api_v2.edit_url())
    resp.form['dist'] = 'plop'
    resp = resp.form.submit()
    assert html_errors(resp.html) == {
        'TOP': 'There were errors processing your form.',
        'Distance (API v2):': 'Bad dist format, expect float value followed by unit',
    }
    resp.form['dist'] = '2euros'
    resp = resp.form.submit()
    assert html_errors(resp.html) == {
        'TOP': 'There were errors processing your form.',
        'Distance (API v2):': 'Bad dist unit, expect mi, yd, ft, m, cm, km or mm',
    }
    resp.form['dist'] = ' 1 km '
    resp = resp.form.submit()
    assert not html_errors(resp)

    # Dist is reformatted
    resp = app.get(query_api_v2.edit_url())
    assert resp.html.find('input', {'id': 'id_dist'}).attrs['value'] == '1.0km'


@responses.activate
def test_search_empty_contents(app, connector):
    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'search', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/search'

    # error returned by opendadasoft server
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json={'error': "The query is invalid : Field 00 doesn't exist"},
        status=200,
    )
    resp = app.get(endpoint, status=200)
    assert resp.json['err']
    assert resp.json['err_desc'] == "The query is invalid : Field 00 doesn't exist"


@responses.activate
def test_search(app, connector):
    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'search', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/search'
    params = {
        'dataset': 'referentiel-adresse-test',
        'text_template': '{{numero}} {{nom_rue}} {{nom_commun}}',
        'sort': '-nom_rue',
        'limit': 3,
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_SEARCH,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'sort': '-nom_rue',
        'rows': '3',
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    # check order is kept
    assert [x['id'] for x in resp.json['data']] == [
        'e00cf6161e52a4c8fe510b2b74d4952036cb3473',
        '7cafcd5c692773e8b863587b2d38d6be82e023d8',
        '0984a5e1745701f71c91af73ce764e1f7132e0ff',
    ]
    # check text results
    assert [x['text'] for x in resp.json['data']] == [
        "33 RUE DE L'AUBEPINE Strasbourg",
        "19 RUE DE L'AUBEPINE Lipsheim",
        "29 RUE DE L'AUBEPINE Strasbourg",
    ]
    # check additional attributes
    assert [x['numero'] for x in resp.json['data']] == ['33', '19', '29']


@responses.activate
def test_search_using_q(app, connector):
    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'search', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/search'
    params = {
        'dataset': 'referentiel-adresse-test',
        'text_template': '{{numero}} {{nom_rue}} {{nom_commun}}',
        'sort': '-nom_rue',
        'limit': '3',
        'q': "rue de l'aubepine",
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_SEARCH,
        status=200,
    )

    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'rows': '3',
        'q': "rue de l'aubepine",
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    # check order is kept
    assert [x['id'] for x in resp.json['data']] == [
        'e00cf6161e52a4c8fe510b2b74d4952036cb3473',
        '7cafcd5c692773e8b863587b2d38d6be82e023d8',
        '0984a5e1745701f71c91af73ce764e1f7132e0ff',
    ]
    # check text results
    assert [x['text'] for x in resp.json['data']] == [
        "33 RUE DE L'AUBEPINE Strasbourg",
        "19 RUE DE L'AUBEPINE Lipsheim",
        "29 RUE DE L'AUBEPINE Strasbourg",
    ]
    # check additional attributes
    assert [x['numero'] for x in resp.json['data']] == ['33', '19', '29']

    # check operators are removed
    params['q'] = 'please, do NOT send boolean operators like And, OR and nOt'
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent['q'] == 'please do send boolean operators like'

    params['q'] = 'field operators are almost ignored too:, -, ==, >, <, >=, <=, [start_date TO end_date]'
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent['q'] == 'field operators are almost ignored too start_date TO end_date'


@responses.activate
def test_search_using_id(app, connector):
    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'search', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/search'
    params = {
        'dataset': 'referentiel-adresse-test',
        'text_template': '{{numero}} {{nom_rue}} {{nom_commun}}',
        'id': '7cafcd5c692773e8b863587b2d38d6be82e023d8',
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_ID_SEARCH,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'q': 'recordid:7cafcd5c692773e8b863587b2d38d6be82e023d8',
    }
    assert len(resp.json['data']) == 1
    assert resp.json['data'][0]['text'] == "19 RUE DE L'AUBEPINE Lipsheim"


@responses.activate
def test_query_q(app, query):
    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'limit': 3,
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_SEARCH,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'refine.source': 'Ville et Eurométropole de Strasbourg',
        'exclude.numero': ['42', '43'],
        'sort': '-nom_rue',
        'rows': '3',
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    # check order is kept
    assert [x['id'] for x in resp.json['data']] == [
        'e00cf6161e52a4c8fe510b2b74d4952036cb3473',
        '7cafcd5c692773e8b863587b2d38d6be82e023d8',
        '0984a5e1745701f71c91af73ce764e1f7132e0ff',
    ]
    # check text results
    assert [x['text'] for x in resp.json['data']] == [
        "33 RUE DE L'AUBEPINE Strasbourg",
        "19 RUE DE L'AUBEPINE Lipsheim",
        "29 RUE DE L'AUBEPINE Strasbourg",
    ]
    # check additional attributes
    assert [x['numero'] for x in resp.json['data']] == ['33', '19', '29']


@responses.activate
def test_query_q_using_q(app, query):
    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'q': "rue de l'aubepine",
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_SEARCH,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'refine.source': 'Ville et Eurométropole de Strasbourg',
        'exclude.numero': ['42', '43'],
        'rows': '3',
        'q': "rue de l'aubepine",
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    # check order is kept
    assert [x['id'] for x in resp.json['data']] == [
        'e00cf6161e52a4c8fe510b2b74d4952036cb3473',
        '7cafcd5c692773e8b863587b2d38d6be82e023d8',
        '0984a5e1745701f71c91af73ce764e1f7132e0ff',
    ]
    # check text results
    assert [x['text'] for x in resp.json['data']] == [
        "33 RUE DE L'AUBEPINE Strasbourg",
        "19 RUE DE L'AUBEPINE Lipsheim",
        "29 RUE DE L'AUBEPINE Strasbourg",
    ]
    # check additional attributes
    assert [x['numero'] for x in resp.json['data']] == ['33', '19', '29']


@responses.activate
def test_query_q_using_id(app, query):
    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'id': '7cafcd5c692773e8b863587b2d38d6be82e023d8',
    }
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=API_V1_ID_SEARCH,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'apikey': 'my_secret',
        'dataset': 'referentiel-adresse-test',
        'refine.source': 'Ville et Eurométropole de Strasbourg',
        'exclude.numero': ['42', '43'],
        'rows': '3',
        'q': 'recordid:7cafcd5c692773e8b863587b2d38d6be82e023d8',
    }
    assert len(resp.json['data']) == 1
    assert resp.json['data'][0]['text'] == "19 RUE DE L'AUBEPINE Lipsheim"


def test_opendatasoft_query_unicity(admin_user, app, connector, query):
    connector2 = OpenDataSoft.objects.create(
        slug='my_connector2',
        api_key='my_secret',
    )
    Query.objects.create(
        resource=connector2,
        name='Foo Bar',
        slug='foo-bar',
    )

    app = login(app)
    resp = app.get('/manage/opendatasoft/%s/query/new/' % connector.slug)
    resp.form['slug'] = query.slug
    resp.form['name'] = 'Foo Bar'
    resp.form['dataset'] = 'my-dataset'
    resp = resp.form.submit()
    assert resp.status_code == 200
    assert Query.objects.filter(resource=connector).count() == 1
    assert 'A query with this slug already exists' in resp.text
    resp.form['slug'] = 'foo-bar'
    resp.form['name'] = query.name
    resp.form['dataset'] = 'my-dataset'
    resp = resp.form.submit()
    assert Query.objects.filter(resource=connector).count() == 1
    assert resp.status_code == 200
    assert 'A query with this name already exists' in resp.text
    resp.form['slug'] = 'foo-bar'
    resp.form['name'] = 'Foo Bar'
    resp.form['dataset'] = 'my-dataset'
    resp = resp.form.submit()
    assert resp.status_code == 302
    assert Query.objects.filter(resource=connector).count() == 2
    new_query = Query.objects.latest('pk')
    assert new_query.resource == connector

    resp = app.get('/manage/opendatasoft/%s/query/%s/' % (connector.slug, new_query.pk))
    resp.form['slug'] = query.slug
    resp.form['name'] = 'Foo Bar'
    resp = resp.form.submit()
    assert resp.status_code == 200
    assert 'A query with this slug already exists' in resp.text
    resp.form['slug'] = 'foo-bar'
    resp.form['name'] = query.name
    resp = resp.form.submit()
    assert resp.status_code == 200
    assert 'A query with this name already exists' in resp.text
    resp.form['slug'] = 'foo-bar'
    resp.form['name'] = 'Foo Bar'
    resp = resp.form.submit()
    assert resp.status_code == 302


@responses.activate
def test_query_q_having_original_fields(app, query):
    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'id': '7cafcd5c692773e8b863587b2d38d6be82e023d8',
    }
    content = copy.deepcopy(API_V1_ID_SEARCH)
    content['records'][0]['fields']['id'] = 'original id'
    content['records'][0]['fields']['text'] = 'original text'
    query.text_template = '{{id}} - {{original_id}} - {{original_text}}'
    query.save()
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json=content,
        status=200,
    )
    resp = app.get(endpoint, params=params, status=200)
    assert resp.json['data'][0]['original_id'] == 'original id'
    assert resp.json['data'][0]['original_text'] == 'original text'
    assert (
        resp.json['data'][0]['text']
        == '7cafcd5c692773e8b863587b2d38d6be82e023d8 - original id - original text'
    )


@responses.activate
def test_call_search_errors(app, connector):
    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'search', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/search'

    # Connection error
    responses.add(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        body=ConnectionError('Remote end closed connection without response'),
    )
    resp = app.get(endpoint)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'OpenDataSoft error: Remote end closed connection without response'

    # API error, provides HTTP status code
    responses.replace(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        json={'error': 'Unknown dataset: foo'},
        status=404,
    )
    resp = app.get(endpoint)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'Unknown dataset: foo'

    # HTTP error
    responses.replace(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        body='Not found',
        status=404,
    )
    resp = app.get(endpoint)
    assert resp.json['err']
    assert 'OpenDataSoft error: 404 Client Error: Not Found' in resp.json['err_desc']

    # bad JSON response
    responses.replace(
        responses.GET,
        'http://www.example.net/api/records/1.0/search/',
        body='not a json content',
        status=200,
    )
    resp = app.get(endpoint)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'OpenDataSoft error: bad JSON response'


@responses.activate
def test_facets(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/alsh-ville-de-tours/facets/',
        json=API_V2_FACETS,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'facets', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/facets'
    params = {
        'dataset': 'alsh-ville-de-tours',
    }
    resp = app.get(endpoint, params=params)
    assert responses.calls[-1].request.headers['Authorization'] == 'ApiKey my_secret'
    assert not resp.json['err']
    assert resp.json['data'] == [{'id': 'assemblee', 'text': 'assemblee'}]


@responses.activate
def test_facet_values(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/alsh-ville-de-tours/facets/',
        json=API_V2_FACETS,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'facet-values', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/facet-values'
    params = {
        'dataset': 'alsh-ville-de-tours',
        'facet': 'assemblee',
    }
    resp = app.get(endpoint, params=params)
    assert responses.calls[-1].request.headers['Authorization'] == 'ApiKey my_secret'
    assert not resp.json['err']
    assert resp.json['data'] == [
        {'id': 'centre-ouest', 'text': 'Centre Ouest'},
        {'id': 'sud', 'text': 'Sud'},
        {'id': 'centre-est', 'text': 'Centre Est'},
        {'id': 'nord-est', 'text': 'Nord Est'},
        {'id': 'nord-ouest', 'text': 'Nord Ouest'},
    ]

    params['facet'] = 'plop'
    resp = app.get(endpoint, params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'no "plop" facet on "alsh-ville-de-tours" dataset'


@responses.activate
def test_records(app, connector, settings):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'text_template': '{{numero}} {{voie_nom}} {{commune_nom}} {{ geo_shape.geometry.coordinates }}',
    }
    with override_settings(LANGUAGE_CODE='fr-fr', TIME_ZONE='Europe/Paris'):
        resp = app.get(endpoint, params=params)
    assert responses.calls[-1].request.headers['Authorization'] == 'ApiKey my_secret'
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': '*',
        'lang': 'fr',
        'timezone': 'Europe/Paris',
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    assert [x['numero'] for x in resp.json['data']] == [7, 55, 79]
    assert [x['text'] for x in resp.json['data']] == [
        '7 Rue du Change Tours [0.6819339935, 47.3938586796]',
        '55 Rue du Commerce Tours [0.6839766385, 47.3949591722]',
        '79 Rue du Commerce Tours [0.6829819595, 47.3948045776]',
    ]
    assert resp.json['data'][0]['original_id'] == 'legacy id'
    assert resp.json['data'][0]['original_text'] == 'legacy text'


@responses.activate
def test_query_api_v2(app, query_api_v2, settings):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'text_template': '{{numero}} {{voie_nom}} {{commune_nom}} {{ geo_shape.geometry.coordinates }}',
    }
    with override_settings(LANGUAGE_CODE='fr-fr', TIME_ZONE='Europe/Paris'):
        resp = app.get(endpoint, params=params)
    assert responses.calls[-1].request.headers['Authorization'] == 'ApiKey my_secret'
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'lang': 'fr',
        'timezone': 'Europe/Paris',
        'select': 'numero, nom_rue, nom_commun',
        'where': "commune_nom='Rochecorbon' AND voie_nom='Chemin de la Planche'",
        'order_by': 'voie_nom asc, numero desc',
        'exclude': "source:'Tours Métropole Val de Loire', source:'other',",
        'limit': '3',
    }
    assert not resp.json['err']
    assert len(resp.json['data']) == 3
    assert [x['numero'] for x in resp.json['data']] == [7, 55, 79]
    assert [x['text'] for x in resp.json['data']] == [
        '7 Rue du Change Tours [0.6819339935, 47.3938586796]',
        '55 Rue du Commerce Tours [0.6839766385, 47.3949591722]',
        '79 Rue du Commerce Tours [0.6829819595, 47.3948045776]',
    ]
    assert resp.json['data'][0]['original_id'] == 'legacy id'
    assert resp.json['data'][0]['original_text'] == 'legacy text'

    # provide parameters from both API v1 and v2
    query_api_v2.filter_expression = 'some content'
    query_api_v2.save()
    resp = app.get(endpoint, params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'Query object should not provide both API v1 and v2 values'


@pytest.mark.parametrize('ending', ('records', 'q/my_query/'))
@responses.activate
def test_api_v2_get_errors(app, query_api_v2, ending):
    endpoint = '/opendatasoft/my_connector/' + ending
    url = 'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET

    def app_get(params=None):
        if not params:
            params = {}
        if ending == 'records':
            params['dataset'] = API_V2_DATASET
        return app.get(endpoint, params=params)

    # Connection error
    responses.add(
        responses.GET,
        url,
        body=ConnectionError('Remote end closed connection without response'),
    )
    resp = app_get()
    assert resp.json['err']
    assert resp.json['err_desc'] == 'OpenDataSoft: Remote end closed connection without response'

    # not a JSON response
    responses.replace(
        responses.GET,
        url,
        body='not a json content',
    )
    resp = app_get()
    assert resp.json['err']
    assert resp.json['err_desc'] == 'OpenDataSoft: not a JSON response'

    # HTTP 404, unknown dataset
    # Connection error
    responses.add(
        responses.GET,
        url,
        json=API_V2_404_ERROR,
        status=404,
    )
    resp = app_get()
    assert resp.json['err']
    assert 'dataset assainissement-eau does not exist' in resp.json['err_desc']

    # HTTP 401, Unauthorized
    # Connection error
    responses.add(
        responses.GET,
        url,
        json=API_V2_401_ERROR,
        status=401,
    )
    resp = app_get()
    assert resp.json['err']
    assert resp.json['err_desc'] == 'OpenDataSoft: API key is not valid'

    # HTTP 400, ODSQL error
    responses.add(
        responses.GET,
        url,
        json=API_V2_ODSQL_ERROR,
        status=400,
    )
    resp = app_get(params={'select': 'plop'})
    assert resp.json['err']
    assert 'ODSQL query is malformed: Unknown field' in resp.json['err_desc']

    # API error on search parameters
    params = {'lon': '1.57'}
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == '"geo_point", "lat" and "lon" are requiring each other'

    params = {'geo_point': 'geo_point_2d', 'lon': '1.57', 'lat': 'plop'}
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'Bad lon/lat coordinates format'

    params = {'geo_point': 'geo_point_2d', 'lon': '1.57', 'lat': '44.55', 'dist': 'plop'}
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'Bad dist format, expect float value followed by unit'

    params['dist'] = '2euros'
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'Bad dist unit, expect mi, yd, ft, m, cm, km or mm'

    # API error on geojson output format
    params = {'output': 'plop'}
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'unknown output format: plop'

    params = {'output': 'geojson'}
    resp = app_get(params=params)
    assert resp.json['err']
    assert resp.json['err_desc'] == 'GeoJson output format needs require geo_point parameter'


@pytest.mark.parametrize('ending', ('records', 'q/my_query/'))
@responses.activate
def test_api_v2_geojson(app, query_api_v2, ending):
    endpoint = '/opendatasoft/my_connector/' + ending
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    def app_get(params=None):
        if not params:
            params = {}
        if ending == 'records':
            params['dataset'] = API_V2_DATASET
        return app.get(endpoint, params=params)

    params = {'geo_point': 'geo_point_2d', 'output': 'geojson'}
    resp = app_get(params=params)
    assert resp.json['type'] == 'FeatureCollection'
    assert len(resp.json['features']) == 2
    assert resp.json['features'][0]['type'] == 'Feature'
    assert resp.json['features'][0]['geometry'] == {
        'coordinates': [0.6819339935, 47.3938586796],
        'type': 'Point',
    }
    assert list(resp.json['features'][0]['properties'].keys()) == [
        '_id',
        '_timestamp',
        '_size',
        'cle_interop',
        'numero',
        'suffixe',
        'voie_nom',
        'commune_insee',
        'commune_nom',
        'date_der_maj',
        'x',
        'y',
        'long',
        'lat',
        'position',
        'source',
        'certification_commune',
        'geo_point_2d',
        'geo_shape',
        'original_id',
        'original_text',
        'id',
        'text',
    ]


@responses.activate
def test_records_providing_id(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/410ffe5c6bcd8249413c43ff47cc0528408f0b47'
        % API_V2_DATASET,
        json=API_V2_ID_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'text_template': '{{numero}} {{voie_nom}} {{commune_nom}}',
        'id': '410ffe5c6bcd8249413c43ff47cc0528408f0b47',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']
    assert [x['numero'] for x in resp.json['data']] == [55]
    assert [x['text'] for x in resp.json['data']] == [
        '55 Rue du Commerce Tours',
    ]


@responses.activate
def test_query_api_v2_providing_id(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/410ffe5c6bcd8249413c43ff47cc0528408f0b47'
        % API_V2_DATASET,
        json=API_V2_ID_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'text_template': '{{numero}} {{voie_nom}} {{commune_nom}}',
        'id': '410ffe5c6bcd8249413c43ff47cc0528408f0b47',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']
    assert [x['numero'] for x in resp.json['data']] == [55]
    assert [x['text'] for x in resp.json['data']] == [
        '55 Rue du Commerce Tours',
    ]


@responses.activate
def test_records_providing_q(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'q': "l'église",
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': '*',
        'where': 'search("l\'église")',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']


@responses.activate
def test_query_api_v2_providing_q(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'q': "l'église",
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent['where'] == [
        "commune_nom='Rochecorbon' AND voie_nom='Chemin de la Planche'",
        'search("l\'église")',
    ]
    assert not resp.json['err']


@pytest.mark.parametrize('q_filter', ('search', 'suggest', 'startswith', 'plop'))
@responses.activate
def test_records_providing_q_filter(app, connector, q_filter):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'q_filter': q_filter,
        'q': "l'église",
    }
    resp = app.get(endpoint, params=params)
    if q_filter != 'plop':
        params_sent = responses.calls[-1].request.params
        assert params_sent['where'] == '%s("l\'église")' % q_filter
        assert not resp.json['err']
    else:
        assert resp.json['err']
        assert resp.json['err_desc'] == 'unknown q_filter function: plop'


@pytest.mark.parametrize('q_filter', ('search', 'suggest', 'startswith', 'plop'))
@responses.activate
def test_query_api_v2_providing_q_filter(app, query_api_v2, q_filter):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'q_filter': q_filter,
        'q': "l'église",
    }
    resp = app.get(endpoint, params=params)
    if q_filter != 'plop':
        params_sent = responses.calls[-1].request.params
        assert params_sent['where'] == [
            "commune_nom='Rochecorbon' AND voie_nom='Chemin de la Planche'",
            '%s("l\'église")' % q_filter,
        ]
        assert not resp.json['err']
    else:
        assert resp.json['err']
        assert resp.json['err_desc'] == 'unknown q_filter function: plop'


@responses.activate
def test_records_providing_where(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'where': "within_distance(geo_point_2d,GEOM'POINT(0.756702 47.414164)',+10m)",
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': '*',
        'where': "within_distance(geo_point_2d,GEOM'POINT(0.756702 47.414164)',+10m)",
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']


@responses.activate
def test_query_api_v2_providing_where(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'where': "within_distance(geo_point_2d,GEOM'POINT(0.756702 47.414164)',+10m)",
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent['where'] == "within_distance(geo_point_2d,GEOM'POINT(0.756702 47.414164)',+10m)"
    assert not resp.json['err']


@responses.activate
def test_records_providing_geo_point(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH_WITH_DIST,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'geo_point': 'geo_point_2d',
        'lon': '0.756702',
        'lat': '47.414164',
        'dist': '10m',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': ['*', "distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)') as dist"],
        'where': "within_distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)', 10.0m)",
        'order_by': 'dist',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']
    assert [x['dist'] for x in resp.json['data']] == [0, 0, 2.1340095246826745, 8.758915045308246]

    # querying providing several where and order_by clauses
    params.update(
        {
            'where': "commune_nom='Rochecorbon'",
            'order_by': 'voie_nom asc, numero desc',
        }
    )
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': ['*', "distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)') as dist"],
        'where': [
            "commune_nom='Rochecorbon'",
            "within_distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)', 10.0m)",
        ],
        'order_by': [
            'voie_nom asc, numero desc',
            'dist',
        ],
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']


@responses.activate
def test_query_api_v2_providing_geo_point(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH_WITH_DIST,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'geo_point': 'geo_point_2d',
        'lon': '0,756702',
        'lat': '47.414164',
        'dist': '10 m',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'lang': 'en',
        'timezone': 'UTC',
        'select': [
            'numero, nom_rue, nom_commun',
            "distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)') as dist",
        ],
        'where': [
            "commune_nom='Rochecorbon' AND voie_nom='Chemin de la Planche'",
            "within_distance(geo_point_2d, GEOM'POINT(0.756702 47.414164)', 10.0m)",
        ],
        'order_by': ['voie_nom asc, numero desc', 'dist'],
        'exclude': "source:'Tours Métropole Val de Loire', source:'other',",
        'limit': '3',
    }
    assert not resp.json['err']
    assert [x['dist'] for x in resp.json['data']] == [0, 0, 2.1340095246826745, 8.758915045308246]


@responses.activate
def test_records_providing_group_by(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'select': 'count(*)',
        'group_by': 'voie_nom',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': 'count(*)',
        'group_by': 'voie_nom',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']


@responses.activate
def test_query_api_v2_providing_group_by(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'
    params = {
        'select': 'count(*)',
        'group_by': 'voie_nom',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent['select'] == 'count(*)'
    assert params_sent['group_by'] == 'voie_nom'
    assert not resp.json['err']


@responses.activate
def test_records_providing_other_opendatasoft_parameters(app, connector):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = tests.utils.generic_endpoint_url('opendatasoft', 'records', slug=connector.slug)
    assert endpoint == '/opendatasoft/my_connector/records'
    params = {
        'dataset': 'base-adresse-locale-tours-metropole-val-de-loire',
        'limit': '2',
        'offset': '3',
        'refine': "commune_nom:'Fondettes'",
        'exclude': "source:'Tours Métropole Val de Loire'",
        'lang': 'fr',
        'timezone': 'Europe/Paris',
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': '*',
        'limit': '2',
        'offset': '3',
        'refine': "commune_nom:'Fondettes'",
        'exclude': "source:'Tours Métropole Val de Loire'",
        'lang': 'fr',
        'timezone': 'Europe/Paris',
    }
    assert not resp.json['err']


@responses.activate
def test_query_api_v2_providing_other_opendatasoft_parameters(app, query_api_v2):
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    endpoint = '/opendatasoft/my_connector/q/my_query/'

    # do not provide parameter : use the ones configurated by on the query
    resp = app.get(endpoint, params={})
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'select': 'numero, nom_rue, nom_commun',
        'where': "commune_nom='Rochecorbon' AND voie_nom='Chemin de la Planche'",
        'order_by': 'voie_nom asc, numero desc',
        'exclude': "source:'Tours Métropole Val de Loire', source:'other',",
        'limit': '3',
        'lang': 'en',
        'timezone': 'UTC',
    }
    assert not resp.json['err']

    # provide other values for parameters when calling
    params = {
        # not already provided by the query configuration
        'refine': "commune_nom:'Rochecorbon'",
        'offset': '3',
        'limit': '4',
        'lang': 'pt',
        'timezone': 'Africa/Casablanca',
        # already provided by the query configuration
        'select': 'numero, nom_rue',
        'where': "voie_nom='Chemin de la Planche'",
        'order_by': 'voie_nom asc',
        'exclude': "source:'other'",
    }
    resp = app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    assert params_sent == {
        'include_app_metas': 'True',
        'lang': 'pt',
        'timezone': 'Africa/Casablanca',
        'select': 'numero, nom_rue',
        'where': "voie_nom='Chemin de la Planche'",
        'order_by': 'voie_nom asc',
        'refine': "commune_nom:'Rochecorbon'",
        'exclude': "source:'other'",
        'limit': '4',
        'offset': '3',
    }
    assert not resp.json['err']


@responses.activate
def test_api_v2_odsql_injection(admin_user, app, query_api_v2):
    '''try to bypass 'publik is true' condition using odsql injection'''
    endpoint = '/opendatasoft/my_connector/q/my_query/'
    responses.add(
        responses.GET,
        'http://www.example.net/api/explore/v2.1/catalog/datasets/%s/records/' % API_V2_DATASET,
        json=API_V2_Q_SEARCH,
    )

    query_api_v2.where = 'publik is true'
    query_api_v2.save()

    # try using q parameter to bypass above condition
    params = {
        'q': '") or publik is false OR search("',
    }
    app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    url_called = responses.calls[-1].request.url
    assert params_sent['where'] == ['publik is true', 'search("\\") or publik is false OR search(\\"")']
    assert 'where=search("\\")+or+publik+is+false+OR+search(\\"")' in unquote(url_called)

    # try using others parameters to bypass above condition
    params = {
        'select': '*&where=publik is false',
    }
    app.get(endpoint, params=params)
    params_sent = responses.calls[-1].request.params
    url_called = responses.calls[-1].request.url
    assert params_sent['where'] == 'publik is true'
    assert 'false' not in url_called

    # try using dataset to call other webservice
    params = {'dataset': '%s/facets/?plop=' % API_V2_DATASET}
    resp = app.get(endpoint, params=params)
    url_called = responses.calls[-1].request.url
    assert 'facets' not in url_called
    assert not resp.json['err']
