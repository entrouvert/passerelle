# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import urllib.parse
import uuid
from datetime import datetime, timedelta, timezone
from unittest import mock

import pytest
import responses
from django.conf import settings
from requests.exceptions import RequestException

from passerelle.apps.qrcode.models import Certificate, QRCodeConnector, Reader
from passerelle.base.signature import check_url
from tests.utils import generic_endpoint_url, setup_access_rights


@pytest.fixture()
def connector(db):
    return setup_access_rights(
        QRCodeConnector.objects.create(
            slug='test',
        )
    )


@pytest.fixture
def mock_wcs():
    settings.KNOWN_SERVICES = {
        'wcs': {
            'default': {
                'title': 'test',
                'url': 'https://wcs.com',
                'secret': 'xxx',
                'orig': 'passerelle',
            },
            'other': {
                'title': 'test',
                'url': 'https://other-wcs.com',
                'secret': 'blah',
                'orig': 'passerelle',
            },
        }
    }


def test_save_campaign(app, connector, freezer):
    freezer.move_to('2022-01-01T10:00:00')
    now = datetime.now(timezone.utc)
    endpoint = generic_endpoint_url('qrcode', 'save-campaign', slug=connector.slug)

    result = app.post_json(
        endpoint,
        params={'metadata': {'toto': 'tata'}},
    )

    campaign_uuid = result.json['data']['uuid']
    campaign = connector.campaigns.get(uuid=campaign_uuid)
    assert campaign
    assert campaign.metadata == {'toto': 'tata'}
    assert campaign.created == now
    assert campaign.modified == now

    freezer.move_to('2022-01-01T10:00:00')
    now = datetime.now(timezone.utc)
    result = app.post_json(
        f'{endpoint}/{campaign_uuid}',
        params={'metadata': {'Georges': 'ravioles'}},
    )

    campaign.refresh_from_db()
    assert campaign.metadata == {'Georges': 'ravioles'}
    assert campaign.modified == now


def test_save_certificate(app, connector):
    endpoint = generic_endpoint_url('qrcode', 'save-certificate', slug=connector.slug)
    campaign = connector.campaigns.create()

    result = app.post_json(
        endpoint,
        params={
            'campaign': str(campaign.uuid),
            'data': {
                'first_name': 'Georges',
                'last_name': 'Abitbol',
            },
            'metadata': {'puissance_intellectuelle': 'BAC +2'},
            'validity_start': '2022-01-01 10:00:00+00:00',
            'validity_end': '2023-01-01 10:00:00+00:00',
        },
    )

    assert result.json['err'] == 0

    certificate_uuid = result.json['data']['uuid']
    assert result.json['data']['qrcode_url'] == f'http://testserver/qrcode/test/get-qrcode/{certificate_uuid}'
    certificate = campaign.certificates.get(uuid=certificate_uuid)

    assert certificate.data['first_name'] == 'Georges'
    assert certificate.data['last_name'] == 'Abitbol'

    assert certificate.metadata['puissance_intellectuelle'] == 'BAC +2'
    assert certificate.validity_start == datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert certificate.validity_end == datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)

    result = app.post_json(
        f'{endpoint}/{certificate_uuid}',
        params={
            'campaign': str(campaign.uuid),
            'data': {
                'first_name': 'Robert',
                'last_name': 'Redford',
            },
            'validity_start': '2024-01-01T10:00:00+00:00',
            'validity_end': '2025-01-01T10:00:00+00:00',
        },
    )

    certificate.refresh_from_db()
    assert certificate.data['first_name'] == 'Robert'
    assert certificate.data['last_name'] == 'Redford'
    assert certificate.validity_start == datetime(2024, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert certificate.validity_end == datetime(2025, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)

    result = app.post_json(
        f'{endpoint}/{certificate_uuid}',
        params={
            'campaign': str(uuid.UUID('00000000-0000-0000-0000-000000000000')),
        },
        status=400,
    )

    assert result.json['err'] == 1
    assert result.json['err_desc'] == "You can't change the campaign of an existing certificate"

    result = app.post_json(
        f'{endpoint}/',
        params={},
        status=400,
    )

    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Campaign is mandatory when creating new certificate'


def test_get_certificate(app, connector):
    campaign = connector.campaigns.create()
    certificate = campaign.certificates.create(
        data={
            'first_name': 'Georges',
            'last_name': 'Abitbol',
        },
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )

    endpoint = generic_endpoint_url('qrcode', 'get-certificate', slug=connector.slug)
    result = app.get(f'{endpoint}/{certificate.uuid}')

    assert result.json == {
        'err': 0,
        'data': {
            'uuid': str(certificate.uuid),
            'data': {'first_name': 'Georges', 'last_name': 'Abitbol'},
            'validity_start': '2022-01-01T10:00:00+00:00',
            'validity_end': '2023-01-01T10:00:00+00:00',
            'qrcode_url': f'http://testserver/qrcode/test/get-qrcode/{certificate.uuid}',
            'status': {},
        },
    }


def test_get_qrcode(app, connector):
    campaign = connector.campaigns.create(
        key='5e8176e50d45b67e9db875d6006edf3ba805ff4ef4d945327012db4c797be1be'
    )
    certificate = campaign.certificates.create(
        uuid=uuid.UUID('12345678-1234-5678-1234-567812345678'),
        data={
            'first_name': 'Georges',
            'last_name': 'Abitbol',
        },
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )
    endpoint = generic_endpoint_url('qrcode', 'get-qrcode', slug=connector.slug)

    response = app.get(f'{endpoint}/{certificate.uuid}')
    assert response.headers['Content-Type'] == 'image/png'
    with open('tests/data/qrcode/test-qrcode.png', 'rb') as expected_qrcode:
        # just check images are the same. Decoded content is tested javascript-side.
        assert response.body == expected_qrcode.read()


def test_save_reader(app, connector):
    endpoint = generic_endpoint_url('qrcode', 'save-reader', slug=connector.slug)
    campaign = connector.campaigns.create()

    result = app.post_json(endpoint, params={})
    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'campaign parameter is mandatory when creating a reader'

    result = app.post_json(
        endpoint,
        params={
            'campaign': str(campaign.uuid),
            'validity_start': '2022-01-01 10:00:00+00:00',
            'validity_end': '2023-01-01 10:00:00+00:00',
            'readable_metadatas': 'name,last_name',
            'metadata': {'toto': 'tata'},
        },
    )

    assert result.json['err'] == 0

    reader_uuid = result.json['data']['uuid']
    assert result.json['data']['url'] == f'http://testserver/qrcode/test/open-reader/{reader_uuid}'
    reader = campaign.readers.get(uuid=reader_uuid)

    assert reader.readable_metadatas == 'name,last_name'
    assert reader.validity_start == datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert reader.validity_end == datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert reader.metadata == {'toto': 'tata'}

    result = app.post_json(
        f'{endpoint}/{reader_uuid}',
        params={
            'validity_start': '2024-01-01T10:00:00+00:00',
            'validity_end': '2025-01-01T10:00:00+00:00',
            'metadata': {'Georges': 'ravioles'},
        },
    )

    reader.refresh_from_db()
    assert reader.validity_start == datetime(2024, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert reader.validity_end == datetime(2025, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc)
    assert reader.metadata == {'Georges': 'ravioles'}


def test_get_reader(app, connector):
    campaign = connector.campaigns.create()
    reader = campaign.readers.create(
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )

    endpoint = generic_endpoint_url('qrcode', 'get-reader', slug=connector.slug)
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert result.json == {
        'err': 0,
        'data': {
            'uuid': str(reader.uuid),
            'validity_start': '2022-01-01T10:00:00+00:00',
            'validity_end': '2023-01-01T10:00:00+00:00',
            'url': f'http://testserver/qrcode/test/open-reader/{reader.uuid}',
        },
    }


def test_open_reader(app, connector, freezer):
    campaign = connector.campaigns.create()
    reader = campaign.readers.create(
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )

    endpoint = generic_endpoint_url('qrcode', 'open-reader', slug=connector.slug)
    freezer.move_to('2022-01-01T09:59:59')
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert 'Reader isn\'t usable yet' in result.body.decode('utf-8')

    freezer.move_to('2022-01-01T10:00:00')
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert result.pyquery(f'qrcode-reader[verify-key="{campaign.hex_verify_key}"]')
    assert not result.pyquery('qrcode-reader[tally-url]')

    campaign.metadata = {'tally': True}
    campaign.save()
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert result.pyquery(f'qrcode-reader[verify-key="{campaign.hex_verify_key}"][tally-url]')

    campaign.metadata = {'tally': False}
    campaign.save()

    reader.metadata = {'tally': True}
    reader.save()
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert result.pyquery(f'qrcode-reader[verify-key="{campaign.hex_verify_key}"][tally-url]')

    campaign.metadata = {'template': 'campaign template'}
    campaign.save()

    result = app.get(f'{endpoint}/{reader.uuid}')
    assert result.pyquery('qrcode-reader').text() == 'campaign template'

    reader.metadata = {'template': 'reader template'}
    reader.save()
    result = app.get(f'{endpoint}/{reader.uuid}')
    assert result.pyquery('qrcode-reader').text() == 'reader template'

    freezer.move_to('2023-01-01T10:00:01')
    result = app.get(f'{endpoint}/{reader.uuid}')

    assert 'Reader has expired.' in result.body.decode('utf-8')


def test_read_metadata(app, connector, freezer):
    campaign = connector.campaigns.create()
    reader = campaign.readers.create(
        readable_metadatas='first_name,puissance_intellectuelle',
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )

    certificate = campaign.certificates.create(
        uuid=uuid.UUID('12345678-1234-5678-1234-567812345678'),
        metadata={'first_name': 'Georges', 'last_name': 'Abitbol', 'puissance_intellectuelle': 'BAC +2'},
        validity_start=datetime(2022, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
        validity_end=datetime(2023, 1, 1, 10, 0, 0, 0, tzinfo=timezone.utc),
    )

    endpoint = generic_endpoint_url('qrcode', 'read-metadata', slug=connector.slug)
    freezer.move_to('2022-01-01T09:59:59')
    result = app.get(f'{endpoint}/{reader.uuid}', params={'certificate': certificate.uuid})

    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Reader isn\'t usable yet.'

    freezer.move_to('2022-01-01T10:00:00')
    result = app.get(f'{endpoint}/{reader.uuid}', params={'certificate': certificate.uuid})

    assert result.json['err'] == 0
    assert result.json['data'] == {'first_name': 'Georges', 'puissance_intellectuelle': 'BAC +2'}

    freezer.move_to('2023-01-01T10:00:01')
    result = app.get(f'{endpoint}/{reader.uuid}', params={'certificate': certificate.uuid})

    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Reader has expired.'


def test_add_event(app, connector, freezer):
    freezer.move_to('2023-01-01T10:00:01')
    campaign = connector.campaigns.create(metadata={'tally': True})
    certificate = campaign.certificates.create()
    now = datetime.now(timezone.utc)

    endpoint = generic_endpoint_url('qrcode', 'add-event', slug=connector.slug)
    app.post_json(
        endpoint,
        params={
            'certificate': str(certificate.uuid),
            'metadata': {'toto': 'tata'},
        },
    )

    certificate.refresh_from_db()
    events = list(certificate.events.all())
    assert len(events) == 1
    assert events[0].metadata == {'toto': 'tata'}
    assert events[0].received == now
    assert events[0].happened == now

    # Check credit is converted to integer
    certificate = campaign.certificates.create()
    app.post_json(
        endpoint,
        params={
            'certificate': str(certificate.uuid),
            'metadata': {'credit': '10'},
        },
    )

    event = certificate.events.all()[0]
    assert event.metadata['credit'] == 10

    certificate = campaign.certificates.create()
    result = app.post_json(
        endpoint,
        params={
            'certificate': str(certificate.uuid),
            'metadata': {'credit': 'Moukraines à la glaviouse'},
        },
        status=400,
    )

    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Invalid credit value: Moukraines à la glaviouse'


def test_get_events(app, connector, freezer):
    freezer.move_to('2023-01-01T10:00:00')
    now = datetime.now(timezone.utc)

    swimming_pool = connector.campaigns.create()
    sausage_fair = connector.campaigns.create()

    john = swimming_pool.certificates.create()
    jack = sausage_fair.certificates.create()

    swimming_pool_entry = swimming_pool.readers.create()
    sausage_fair_entry = sausage_fair.readers.create()

    john_buys_swimming_pool_tickets_event = john.events.create(happened=now, metadata={'credit': 10})

    freezer.move_to('2023-02-01T10:00:00')
    now = datetime.now(timezone.utc)
    john_goes_to_swimming_pool_event = john.events.create(
        reader=swimming_pool_entry, happened=now, metadata={'credit': -1}
    )
    jack_goes_to_sausage_fair_event = jack.events.create(
        reader=sausage_fair_entry, happened=now, metadata={'credit': -1}
    )

    john_buys_swimming_pool_tickets = {
        'uuid': str(john_buys_swimming_pool_tickets_event.uuid),
        'certificate': str(john.uuid),
        'reader': None,
        'received': '2023-01-01T10:00:00+00:00',
        'happened': '2023-01-01T10:00:00+00:00',
        'metadata': {'credit': 10},
    }

    john_goes_to_swimming_pool = {
        'uuid': str(john_goes_to_swimming_pool_event.uuid),
        'certificate': str(john.uuid),
        'reader': str(swimming_pool_entry.uuid),
        'received': '2023-02-01T10:00:00+00:00',
        'happened': '2023-02-01T10:00:00+00:00',
        'metadata': {'credit': -1},
    }

    jack_goes_to_sausage_fair = {
        'uuid': str(jack_goes_to_sausage_fair_event.uuid),
        'certificate': str(jack.uuid),
        'reader': str(sausage_fair_entry.uuid),
        'received': '2023-02-01T10:00:00+00:00',
        'happened': '2023-02-01T10:00:00+00:00',
        'metadata': {'credit': -1},
    }

    endpoint = generic_endpoint_url('qrcode', 'events', slug=connector.slug)

    result = app.get(endpoint)
    assert result.json == {
        'err': 0,
        'data': [
            john_buys_swimming_pool_tickets,
            john_goes_to_swimming_pool,
            jack_goes_to_sausage_fair,
        ],
    }

    result = app.get(f'{endpoint}?campaign={swimming_pool.uuid}')
    assert result.json == {
        'err': 0,
        'data': [
            john_buys_swimming_pool_tickets,
            john_goes_to_swimming_pool,
        ],
    }

    result = app.get(f'{endpoint}?campaign=zbra')
    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Invalid UUID: zbra'

    result = app.get(f'{endpoint}?certificate={john.uuid}')
    assert result.json == {
        'err': 0,
        'data': [
            john_buys_swimming_pool_tickets,
            john_goes_to_swimming_pool,
        ],
    }

    result = app.get(f'{endpoint}?certificate=zbra')
    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Invalid UUID: zbra'

    result = app.get(f'{endpoint}?reader={swimming_pool_entry.uuid}')
    assert result.json == {
        'err': 0,
        'data': [john_goes_to_swimming_pool],
    }

    result = app.get(f'{endpoint}?reader=zbra')
    assert result.json['err'] == 1
    assert result.json['err_desc'] == 'Invalid UUID: zbra'

    result = app.get(f'{endpoint}?since=2023-02-01T10:00:00%2B00:00')
    assert result.json == {
        'err': 0,
        'data': [john_goes_to_swimming_pool, jack_goes_to_sausage_fair],
    }

    result = app.get(f'{endpoint}?until=2023-01-01T10:00:00%2B00:00')
    assert result.json == {
        'err': 0,
        'data': [
            john_buys_swimming_pool_tickets,
        ],
    }

    result = app.get(f'{endpoint}?tally=True')
    assert result.json == {
        'err': 0,
        'data': [john_goes_to_swimming_pool, jack_goes_to_sausage_fair],
    }

    result = app.get(f'{endpoint}?tally=False')
    assert result.json == {
        'err': 0,
        'data': [
            john_buys_swimming_pool_tickets,
        ],
    }


def test_get_events_order(app, connector, freezer):
    campaign = connector.campaigns.create()
    certificate = campaign.certificates.create()

    # check events are sorted by 'received' date, not by 'created'
    now = datetime.now(timezone.utc)
    second_event = certificate.events.create(received=now, happened=now + timedelta(minutes=2))
    first_event = certificate.events.create(received=now + timedelta(minutes=1), happened=now)

    endpoint = generic_endpoint_url('qrcode', 'events', slug=connector.slug)
    result = app.get(endpoint)
    events = result.json['data']
    assert events[0]['uuid'] == str(first_event.uuid)
    assert events[1]['uuid'] == str(second_event.uuid)


def test_merge_events(app, connector, freezer):
    freezer.move_to('2023-01-01T10:00:00')
    now = datetime.now(timezone.utc)
    campaign = connector.campaigns.create(metadata={'max_event_history': 2})
    reader = campaign.readers.create()
    certificate = campaign.certificates.create()

    certificate.events.create(happened=now, metadata={'credit': 10})

    freezer.move_to('2023-01-02T10:00:00')
    now = datetime.now(timezone.utc)
    certificate.events.create(happened=now, metadata={'credit': -1}, reader=reader)
    certificate.events.create(happened=now, metadata={'pass': True})
    certificate.events.create(happened=now, metadata={'revoked': True})

    freezer.move_to('2023-01-03T10:00:00')
    certificate.events.create(happened=now, metadata={'credit': -1, 'pass': False, 'revoked': False})
    connector.daily()
    assert len(certificate.events.all()) == 5

    freezer.move_to('2023-01-04T10:00:01')
    connector.daily()
    assert len(certificate.events.all()) == 2
    merged_event = certificate.events.order_by('received')[0]
    assert merged_event.received == datetime.fromisoformat('2023-01-02T10:00:00+00:00')
    assert merged_event.happened == datetime.fromisoformat('2023-01-02T10:00:00+00:00')
    assert merged_event.metadata == {'credit': 9, 'pass': True, 'revoked': True}

    # check accounts are kept separated
    freezer.move_to('2023-01-01T10:00:00')
    certificate = campaign.certificates.create()
    now = datetime.now(timezone.utc)
    certificate.events.create(happened=now, metadata={'credit': 10, 'account': 'swimming_pool'})
    certificate.events.create(
        happened=now, metadata={'credit': -1, 'account': 'swimming_pool'}, reader=reader
    )
    certificate.events.create(happened=now, metadata={'credit': 20, 'account': 'bridge_club'})
    certificate.events.create(happened=now, metadata={'credit': -2, 'account': 'bridge_club'}, reader=reader)
    freezer.move_to('2023-01-05T10:00:00')
    connector.daily()

    assert len(certificate.events.all()) == 2
    event = certificate.events.get(metadata__account='swimming_pool')
    assert event.received == datetime.fromisoformat('2023-01-01T10:00:00+00:00')
    assert event.happened == datetime.fromisoformat('2023-01-01T10:00:00+00:00')
    assert event.metadata['credit'] == 9
    event = certificate.events.get(metadata__account='bridge_club')
    assert event.received == datetime.fromisoformat('2023-01-01T10:00:00+00:00')
    assert event.happened == datetime.fromisoformat('2023-01-01T10:00:00+00:00')
    assert event.metadata['credit'] == 18


def test_merge_untallied_certificate(app, connector, freezer):
    freezer.move_to('2023-01-01T10:00:00')
    now = datetime.now(timezone.utc)
    campaign = connector.campaigns.create(metadata={'max_event_history': 2})
    certificate = campaign.certificates.create()

    certificate.events.create(happened=now, metadata={'credit': 10})
    certificate.events.create(happened=now, metadata={'credit': 10})

    freezer.move_to('2023-01-04T10:00:01')
    connector.daily()
    assert len(certificate.events.all()) == 1
    merged_event = certificate.events.all()[0]
    assert merged_event.metadata == {'credit': 20, 'pass': False, 'revoked': False}


class TestCertificate:
    def test_get_certificate_status(self, app, connector):
        campaign = connector.campaigns.create()
        certificate = campaign.certificates.create()
        certificate.events.create(happened=datetime.now(timezone.utc), metadata={'credit': 10})
        certificate.events.create(
            happened=datetime.now(timezone.utc), metadata={'credit': 10, 'account': None}
        )
        certificate.events.create(
            happened=datetime.now(timezone.utc), metadata={'account': 'Swimming Pool', 'credit': 10}
        )
        certificate.events.create(
            happened=datetime.now(timezone.utc), metadata={'account': 'Swimming Pool', 'credit': -1}
        )
        certificate.events.create(
            happened=datetime.now(timezone.utc), metadata={'account': 'Bridge Club', 'pass': True}
        )
        certificate.events.create(
            happened=datetime.now(timezone.utc), metadata={'account': 'Bridge Club', 'revoked': True}
        )

        endpoint = generic_endpoint_url('qrcode', 'get-certificate', slug=connector.slug)
        result = app.get(f'{endpoint}/{certificate.uuid}')

        assert result.json['data']['status'] == {
            '_default': {
                'pass': False,
                'revoked': False,
                'credit': 20,
            },
            'Swimming Pool': {
                'pass': False,
                'revoked': False,
                'credit': 9,
            },
            'Bridge Club': {
                'pass': True,
                'revoked': True,
                'credit': 0,
            },
        }


class TestTally:
    @pytest.fixture
    def campaign(self, connector):
        return connector.campaigns.create(metadata={'tally': True})

    @pytest.fixture
    def certificate(self, campaign):
        return campaign.certificates.create()

    @pytest.fixture
    def reader(self, campaign):
        return campaign.readers.create()

    @pytest.fixture(autouse=True)
    def freezer(self, freezer):
        freezer.move_to('2022-01-10T00:00:00')
        return freezer

    @pytest.fixture(autouse=True)
    def tally(self, app, connector, reader):
        endpoint = generic_endpoint_url('qrcode', 'tally', slug=connector.slug)

        def tally(data, status=200):
            now = datetime.now(timezone.utc)
            result = app.post_json(f'{endpoint}/{reader.uuid}', params=data, status=status)
            if status == 200:
                assert result.json['err'] == 0
                assert result.json['data']['timestamp'] == int(datetime.timestamp(now))
                return result.json['data']['certificates']
            else:
                return result.json

        return tally

    @pytest.fixture
    def tally_certificate(self, connector, certificate, tally):
        now = datetime.now(timezone.utc)

        def tally_certificate(account=None, status=200, credit=None):
            account_key = str(certificate.uuid) if account is None else f'{certificate.uuid}/{account}'
            event_data = {
                'uuid': str(uuid.uuid4()),
                'certificate': account_key,
                'timestamp': int(datetime.timestamp(now)),
            }

            if credit:
                event_data['credit'] = credit

            try:
                return tally(
                    {
                        'since': 0,
                        'events': [event_data],
                    },
                    status=status,
                )
            finally:
                connector.jobs()

        return tally_certificate

    def test_default_credit(self, certificate, tally):
        result = tally({'since': 0})
        assert result[str(certificate.uuid)]['credit'] == 0

    def test_not_enough_credit(self, certificate, tally_certificate):
        result = tally_certificate()
        assert result[str(certificate.uuid)]['credit'] == -1

        result = tally_certificate(credit=-10)
        assert result[str(certificate.uuid)]['credit'] == -10

        # no credit: event is not saved
        events = list(certificate.events.all())
        assert len(events) == 0

    def test_revoked(self, certificate, tally_certificate):
        # Check revoked certificate are not tallied
        now = datetime.now(timezone.utc)
        certificate.events.create(happened=now, metadata={'revoked': True, 'credit': 1})
        result = tally_certificate()

        assert result[str(certificate.uuid)]['revoked'] is True
        assert result[str(certificate.uuid)]['credit'] == 1
        assert len(certificate.events.all()) == 1

    def test_pass(self, certificate, tally_certificate):
        # check activating pass mode on certificate doesn't decrement credits
        now = datetime.now(timezone.utc)
        certificate.events.create(happened=now, metadata={'pass': True, 'credit': 10})
        result = tally_certificate()
        events = list(certificate.events.all())

        assert result[str(certificate.uuid)]['credit'] == 10
        assert result[str(certificate.uuid)]['pass'] is True
        assert len(events) == 2
        assert events[1].metadata['credit'] == 0

    def test_invalid_certificate(self, certificate, tally):
        now = datetime.now(timezone.utc)
        result = tally(
            {
                'since': 0,
                'events': [
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': 'deadbeef-0000-0000-0000-000000000000',
                        'timestamp': int(datetime.timestamp(now)),
                    }
                ],
            }
        )
        assert result[str(certificate.uuid)]['credit'] == 0
        assert 'deadbeef-0000-0000-0000-000000000000' not in result

    def test_deduct_credit(self, reader, certificate, tally_certificate):
        now = datetime.now(timezone.utc)
        # credit certificate
        certificate.events.create(happened=now, metadata={'credit': 1})
        result = tally_certificate()
        events = list(certificate.events.all())

        assert result[str(certificate.uuid)]['credit'] == 0
        assert len(events) == 2
        assert events[1].reader == reader
        assert events[1].happened == now
        assert events[1].received == now
        assert events[1].metadata == {'credit': -1}

    def test_custom_credit(self, reader, certificate, tally_certificate):
        now = datetime.now(timezone.utc)
        # credit certificate
        certificate.events.create(happened=now, metadata={'credit': 10})
        result = tally_certificate(credit=-9)
        events = list(certificate.events.all())

        assert result[str(certificate.uuid)]['credit'] == 1
        assert len(events) == 2
        assert events[1].reader == reader
        assert events[1].happened == now
        assert events[1].received == now
        assert events[1].metadata == {'credit': -9}

    def test_bad_custom_credit(self, reader, certificate, tally_certificate):
        now = datetime.now(timezone.utc)
        certificate.events.create(happened=now, metadata={'credit': 2})
        result = tally_certificate(credit=0)
        assert result[str(certificate.uuid)]['credit'] == 1

        result = tally_certificate(credit=1)
        assert result[str(certificate.uuid)]['credit'] == 0

    def test_multiple_events(self, certificate, tally):
        # check events are filtered by account
        now = datetime.now(timezone.utc)
        certificate.events.create(happened=now, metadata={'credit': 15})
        result = tally(
            {
                'since': 0,
                'events': [
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': str(certificate.uuid),
                        'timestamp': int(datetime.timestamp(now)),
                        'credit': -10,
                    },
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': str(certificate.uuid),
                        'timestamp': int(datetime.timestamp(now)) + 1,
                        'credit': -5,
                    },
                ],
            }
        )
        events = list(certificate.events.order_by('id').all())

        assert result == {f'{certificate.uuid}': {'credit': 0, 'revoked': False, 'pass': False}}

        assert len(events) == 3
        assert events[1].metadata['credit'] == -10
        assert events[2].metadata['credit'] == -5

    def test_duplicate_event(self, certificate, tally):
        now = datetime.now(timezone.utc)
        credit_event = certificate.events.create(happened=now, metadata={'credit': 10})
        tally_event = certificate.events.create(happened=now, metadata={'credit': -1})
        result = tally(
            {
                'since': 0,
                'events': [
                    {
                        'uuid': str(tally_event.uuid),  # use already existing event
                        'certificate': str(certificate.uuid),
                        'timestamp': int(datetime.timestamp(now)),
                    }
                ],
            }
        )

        # duplicate events should be ignored
        assert result[str(certificate.uuid)]['credit'] == 9
        assert list(certificate.events.all()) == [credit_event, tally_event]

    def test_since(self, freezer, certificate, tally):
        yesterday = datetime.now(timezone.utc)
        certificate.events.create(happened=yesterday, metadata={'credit': 10})
        freezer.tick(timedelta(days=1))

        result = tally({'since': datetime.timestamp(yesterday + timedelta(minutes=1))})
        assert str(certificate.uuid) in result

        result = tally({'since': datetime.timestamp(yesterday + timedelta(minutes=1, seconds=1))})
        assert str(certificate.uuid) not in result

        # specifying a certificate should return it even if filtered by since
        now = datetime.now(timezone.utc)
        result = tally(
            {
                'since': datetime.timestamp(yesterday + timedelta(minutes=1, seconds=1)),
                'events': [
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': str(certificate.uuid),
                        'timestamp': int(datetime.timestamp(now)),
                    }
                ],
            }
        )
        assert str(certificate.uuid) in result

    def test_asynchronous(self, certificate, tally):
        now = datetime.now(timezone.utc)
        result = tally(
            {
                'since': 0,
                'events': [
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': str(certificate.uuid),
                        'timestamp': int(datetime.timestamp(now)),
                    }
                ],
                'asynchronous': True,
            }
        )
        events = list(certificate.events.all())

        # asynchronous tallying should save overdraft events even if credit is insufficient
        assert result[str(certificate.uuid)]['credit'] == -1
        assert len(events) == 1
        assert events[0].metadata['credit'] == -1

    def test_expired_certificate(self, certificate, tally):
        certificate.validity_end = datetime.now(timezone.utc)
        certificate.save()

        assert str(certificate.uuid) in tally({'since': 0})

        certificate.validity_end = datetime.now(timezone.utc) - timedelta(seconds=1)
        certificate.save()

        assert str(certificate.uuid) not in tally({'since': 0})

    def test_inactive_certificate(self, certificate, tally):
        certificate.validity_start = datetime.now(timezone.utc)
        certificate.save()

        assert str(certificate.uuid) in tally({'since': 0})

        certificate.validity_start = datetime.now(timezone.utc) + timedelta(seconds=1)
        certificate.save()

        assert str(certificate.uuid) not in tally({'since': 0})

    def test_last_tally(self, freezer, campaign, reader, certificate, tally_certificate):
        reader.metadata = {'label': 'Swimming pool'}
        reader.save()
        bridge_club_reader = campaign.readers.create(metadata={'label': 'Bridge Club'})
        now = datetime.now(timezone.utc)

        # check last tally is based on happened timestamp rather than created timestamp, or insertion order
        certificate.events.create(happened=now - timedelta(days=1), metadata={'credit': 3})
        certificate.events.create(happened=now, reader=bridge_club_reader, metadata={'credit': -1})
        certificate.events.create(happened=now - timedelta(seconds=1), reader=reader, metadata={'credit': -1})
        result = tally_certificate()

        assert result[str(certificate.uuid)]['last_tally']['timestamp'] == datetime.timestamp(now)
        assert result[str(certificate.uuid)]['last_tally']['reader']['label'] == 'Bridge Club'

    class TestAccounts:
        def test_tally(self, reader, certificate, tally_certificate):
            # check events are filtered by account
            reader.metadata = {'accounts': {'swimming-pool': 'Swimming Pool'}}
            reader.save()
            now = datetime.now(timezone.utc)
            certificate.events.create(happened=now, metadata={'credit': 10, 'account': 'bridge-club'})
            certificate.events.create(happened=now, metadata={'credit': 1, 'account': 'swimming-pool'})
            result = tally_certificate('swimming-pool')

            assert result == {
                f'{certificate.uuid}/swimming-pool': {'credit': 0, 'revoked': False, 'pass': False}
            }
            events = list(certificate.events.order_by('id'))
            assert len(events) == 3
            assert events[2].metadata['account'] == 'swimming-pool'

        def test_event_with_none_account(self, certificate, tally_certificate):
            now = datetime.now(timezone.utc)
            certificate.events.create(happened=now, metadata={'credit': 10})
            certificate.events.create(happened=now, metadata={'credit': 10, 'account': None})
            result = tally_certificate()
            assert result[str(certificate.uuid)]['credit'] == 19

        def test_since_filter_account(self, reader, certificate, tally, freezer):
            now = datetime.now(timezone.utc)
            reader.metadata = {'accounts': {'swimming-pool': 'Swimming Pool'}}
            reader.save()
            certificate.events.create(happened=now, metadata={'credit': 1, 'account': 'bridge-club'})

            # since should only take into accounts events that occured on reader's accounts
            result = tally({'since': 0})
            assert result == {
                f'{str(certificate.uuid)}/swimming-pool': {'credit': 0, 'revoked': False, 'pass': False}
            }

            result = tally({'since': datetime.timestamp(now + timedelta(minutes=1))})
            assert result == {}

        def test_tally_unhautorized_account(self, reader, certificate, tally_certificate):
            # check events are filtered by account
            reader.metadata = {'accounts': {'swimming-pool': 'Swimming Pool'}}
            reader.save()
            now = datetime.now(timezone.utc)
            certificate.events.create(happened=now, metadata={'credit': 1, 'account': 'bridge-club'})
            result = tally_certificate('bridge-club', status=401)

            assert result['err'] == 1
            assert (
                result['err_desc'] == f'Reader {reader.uuid} is not authorized to tally account bridge-club'
            )
            assert len(certificate.events.all()) == 1

        def test_slash_in_account_name(self, app, reader, certificate, tally_certificate):
            now = datetime.now(timezone.utc)
            certificate.events.create(happened=now, metadata={'credit': 1, 'account': 'swimming/pool'})
            reader.metadata = {'accounts': {'swimming/pool': 'Swimming Pool'}}
            reader.save()

            result = tally_certificate('swimming/pool')

            assert result == {
                f'{str(certificate.uuid)}/swimming/pool': {
                    'credit': 0,
                    'revoked': False,
                    'pass': False,
                },
            }

        def test_save_on_single_invalid_account(self, reader, certificate, tally):
            reader.metadata = {'accounts': {'swimming-pool': 'Swimming Pool', 'bridge-club': 'Bridge Club'}}
            reader.save()
            now = datetime.now(timezone.utc)
            certificate.events.create(happened=now, metadata={'credit': 1, 'account': 'swimming-pool'})
            tally_body = {
                'since': 0,
                'events': [
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': f'{str(certificate.uuid)}/swimming-pool',
                        'timestamp': int(datetime.timestamp(now)),
                    },
                    {
                        'uuid': str(uuid.uuid4()),
                        'certificate': f'{str(certificate.uuid)}/bridge-club',
                        'timestamp': int(datetime.timestamp(now)),
                    },
                ],
            }

            result = tally(tally_body)

            assert result == {
                f'{certificate.uuid}/swimming-pool': {'credit': 0, 'revoked': False, 'pass': False},
                f'{certificate.uuid}/bridge-club': {'credit': -1, 'revoked': False, 'pass': False},
            }
            # No event save if a single one in the batch fails
            assert len(certificate.events.all()) == 1

            result = tally(tally_body | {'asynchronous': True})

            assert result == {
                f'{certificate.uuid}/swimming-pool': {'credit': 0, 'revoked': False, 'pass': False},
                f'{certificate.uuid}/bridge-club': {'credit': -1, 'revoked': False, 'pass': False},
            }
            # Except if the request comes from the service worker
            assert len(certificate.events.all()) == 3

    class TestTriggerWCS:
        @pytest.fixture
        def certificate(self, certificate, mock_wcs):
            trigger_url = 'https://wcs.com/trigger/blah'
            certificate.metadata = {'trigger_url': trigger_url}
            certificate.events.create(happened=datetime.now(timezone.utc), metadata={'credit': 1})
            certificate.save()
            yield certificate

        @responses.activate
        def test_trigger(self, reader, certificate, tally_certificate):
            responses.post(certificate.metadata['trigger_url'])
            result = tally_certificate()
            tally_event = certificate.events.order_by('id').all()[1]
            request_data = json.loads(responses.calls[0].request.body)

            assert str(certificate.uuid) in result
            assert (
                urllib.parse.parse_qs(urllib.parse.urlparse(responses.calls[0].request.url).query)['orig'][0]
                == 'passerelle'
            )

            assert len(responses.calls) == 1
            assert responses.calls[0].response.url.startswith('https://wcs.com/trigger/blah')
            assert check_url(
                responses.calls[0].response.url, settings.KNOWN_SERVICES['wcs']['default']['secret']
            )
            assert len(certificate.events.all()) == 2

            assert request_data == {
                'certificate': str(certificate.uuid),
                'happened': '2022-01-10T00:00:00+00:00',
                'metadata': {'credit': -1},
                'reader': str(reader.uuid),
                'received': '2022-01-10T00:00:00+00:00',
                'uuid': str(tally_event.uuid),
            }

        @responses.activate
        def test_dont_trigger_inactive_certificate(self, certificate, tally_certificate):
            # Check trigger isn't called if certificate isn't valid or expired
            certificate.validity_start = datetime.now(timezone.utc) + timedelta(seconds=1)
            certificate.save()
            tally_certificate()
            assert len(responses.calls) == 0

        @responses.activate
        def test_dont_trigger_expired_certificate(self, certificate, tally_certificate):
            # Check trigger isn't called if certificate isn't valid or expired
            certificate.validity_end = datetime.now(timezone.utc) - timedelta(seconds=1)
            certificate.save()
            tally_certificate()
            assert len(responses.calls) == 0

        @mock.patch('passerelle.base.models.ProxyLogger.error')
        @responses.activate
        def test_trigger_error(self, mock_log_error, certificate, tally_certificate):
            responses.post(certificate.metadata['trigger_url'], body=RequestException('kaboume'))
            result = tally_certificate()

            # certificate should be returned anyway in case of trigger error
            assert str(certificate.uuid) in result
            mock_log_error.assert_called_with(
                'Error while triggering w.c.s. for certificate %s : %s', certificate.uuid, mock.ANY
            )

        @mock.patch('passerelle.base.models.ProxyLogger.error')
        @responses.activate
        def test_bad_trigger_url(self, mock_log_error, certificate, tally_certificate):
            certificate.metadata = {'trigger_url': 'https://invalid-wcs-url.com/trigger/blah'}
            certificate.save()
            result = tally_certificate()

            # certificate should be returned anyway in case of trigger error
            assert str(certificate.uuid) in result
            mock_log_error.assert_called_with(
                "Can't find a suitable configured WCS service for url %s",
                'https://invalid-wcs-url.com/trigger/blah',
            )


MISSING = object()


@pytest.mark.parametrize('value', [MISSING, None, ''], ids=['missing', 'null', 'empty string'])
class TestOptional:
    def test_certificate_validity_start(self, value, app, connector):
        campaign = connector.campaigns.create()
        params = {
            'campaign': str(campaign.uuid),
            'data': {
                'first_name': 'Georges',
                'last_name': 'Abitbol',
            },
            'validity_end': '2023-01-01 10:00:00+00:00',
        }
        if value is not MISSING:
            params['validity_start'] = value

        app.post_json('/qrcode/test/save-certificate/', params=params)
        assert Certificate.objects.get().validity_start is None

    def test_certificate_validity_end(self, value, app, connector):
        campaign = connector.campaigns.create()
        params = {
            'campaign': str(campaign.uuid),
            'data': {
                'first_name': 'Georges',
                'last_name': 'Abitbol',
            },
            'validity_start': '2023-01-01 10:00:00+00:00',
        }
        if value is not MISSING:
            params['validity_end'] = value

        app.post_json('/qrcode/test/save-certificate/', params=params)
        assert Certificate.objects.get().validity_end is None

    def test_reader_validity_start(self, value, app, connector):
        campaign = connector.campaigns.create()
        params = {
            'campaign': str(campaign.uuid),
            'validity_end': '2023-01-01 10:00:00+00:00',
        }
        if value is not MISSING:
            params['validity_start'] = value

        app.post_json('/qrcode/test/save-reader/', params=params)
        assert Reader.objects.get().validity_start is None

    def test_reader_validity_end(self, value, app, connector):
        campaign = connector.campaigns.create()
        params = {
            'campaign': str(campaign.uuid),
            'validity_start': '2023-01-01 10:00:00+00:00',
        }
        if value is not MISSING:
            params['validity_end'] = value

        app.post_json('/qrcode/test/save-reader/', params=params)
        assert Reader.objects.get().validity_end is None
