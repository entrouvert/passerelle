#!/usr/bin/python

import os
import shutil
import subprocess
import sys

from setuptools import Command, find_packages, setup
from setuptools.command.build import build as _build
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.sdist import sdist
from setuptools.errors import CompileError


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result.replace('.dirty', '+dirty')
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        curdir = os.getcwd()
        try:
            from django.core.management import call_command

            for path, dirs, files in os.walk('passerelle'):
                if 'locale' not in dirs:
                    continue
                os.chdir(os.path.realpath(path))
                call_command('compilemessages')
        except ImportError:
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations\n')
        finally:
            os.chdir(curdir)


class compile_scss(Command):
    description = 'compile scss files into css files'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sass_bin = None
        for program in ('sassc', 'sass'):
            sass_bin = shutil.which(program)
            if sass_bin:
                break
        if not sass_bin:
            raise CompileError(
                'A sass compiler is required but none was found.  See sass-lang.com for choices.'
            )

        for path, dirnames, filenames in os.walk('passerelle'):
            for filename in filenames:
                if not filename.endswith('.scss'):
                    continue
                if filename.startswith('_'):
                    continue
                subprocess.check_call(
                    [
                        sass_bin,
                        '%s/%s' % (path, filename),
                        '%s/%s' % (path, filename.replace('.scss', '.css')),
                    ]
                )


class build(_build):
    sub_commands = [('compile_translations', None), ('compile_scss', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


setup(
    name='passerelle',
    version=get_version(),
    license='AGPLv3',
    description='Passerelle provides an uniform access to multiple data sources and services.',
    url='https://dev.entrouvert.org/projects/passerelle/',
    download_url='http://repos.entrouvert.org/passerelle.git/',
    author="Entr'ouvert",
    author_email='info@entrouvert.com',
    packages=find_packages(os.path.dirname(__file__) or '.'),
    scripts=['manage.py'],
    include_package_data=True,
    install_requires=[
        'caldav == 0.11.0',
        'icalendar == 4.0.3',
        'recurring-ical-events == 2.0.1',
        'django >= 3.2, <4.3',
        'django-model-utils',
        'requests',
        'urllib3<2',
        'gadjo',
        'phpserialize',
        'suds',
        # pyexcel-xlsx is unmaintained (no change since 2020) and is incompatible
        # with a change introduced in openpyxl 3.1.0
        'openpyxl<3.1',
        'pyexcel-io',
        'pyexcel-ods',
        'pyexcel-xls',
        'pyexcel-xlsx',
        'cmislib-maykin',
        'pyproj',
        'feedparser<6' if sys.version_info < (3, 9) else 'feedparser>=6',
        'lxml',
        'python-dateutil',
        'Pillow',
        'jsonschema',
        'zeep >= 3.2',
        'pycryptodomex',
        'unidecode',
        'paramiko',
        'pdfrw',
        'httplib2',
        'xmlschema',
        'pytz',
        'vobject',
        'Levenshtein',
        'python-ldap',
        'pyOpenSSL',
        'roman',
        'cryptography',
        'xmltodict',
        'phonenumbers',
        'qrcode',
        'pillow',
        'pynacl',
    ],
    cmdclass={
        'build': build,
        'compile_scss': compile_scss,
        'compile_translations': compile_translations,
        'install_lib': install_lib,
        'sdist': eo_sdist,
    },
    package_data={'passerelle': ['*.xsd']},
)
