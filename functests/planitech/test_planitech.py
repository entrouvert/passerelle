import pprint
import random
import urllib.parse

import requests


def test_main(conn):
    # get days
    query_string = urllib.parse.urlencode(
        {'start_days': 1, 'end_days': 90, 'start_time': '10:00', 'end_time': '11:00', 'display': 'date'}
    )
    url = conn + '/getfreegaps?%s' % query_string
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    data = res['data']
    assert data

    # get places
    query_string = urllib.parse.urlencode(
        {'start_days': 1, 'end_days': 90, 'start_time': '10:00', 'end_time': '11:00', 'display': 'place'}
    )
    url = conn + '/getfreegaps?%s' % query_string
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    data = res['data']
    assert data
    place = data[random.randint(0, len(data) - 1)]['id']

    # get days on one place
    query_string = urllib.parse.urlencode(
        {
            'start_days': 1,
            'end_days': 90,
            'start_time': '10:00',
            'end_time': '11:00',
            'place_id': place,
            'display': 'date',
        }
    )
    url = conn + '/getfreegaps?%s' % query_string
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    data = res['data']
    assert data

    activities = call_generic(conn, 'getactivities')
    activity_id = activities[random.randint(0, len(activities) - 1)]['identifier']

    resa_types = call_generic(conn, 'getreservationtypes')
    resa_type_id = resa_types[random.randint(0, len(resa_types) - 1)]['identifier']

    chosen_date = data[0]['id']
    # create reservation
    params = {
        'date': chosen_date,
        'start_time': '10:00',
        'end_time': '11:00',
        'place_id': place,
        'price': 200,
        'name_id': 'john-doe',
        'type_id': resa_type_id,
        'first_name': 'jon',
        'last_name': 'doe',
        'activity_id': activity_id,
        'email': 'jon.doe@localhost',
        'object': 'reservation object',
        'vat_rate': 200,
    }
    print('Create reservation parameters \n')
    pprint.pprint(params)
    print('\n')
    url = conn + '/createreservation'
    resp = requests.post(url, json=params)
    resp.raise_for_status()
    res = resp.json()
    print('Create reservation response \n')
    pprint.pprint(res)
    print('\n')
    assert res['err'] == 0
    data = res['data']
    assert 'reservation_id' in data
    reservation_id = data['reservation_id']

    # confirm reservation
    params = {'reservation_id': reservation_id, 'status': 'standard'}
    url = conn + '/updatereservation'
    resp = requests.post(url, json=params)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    data = res['data']
    assert data

    # FORBIDDEN BY PLANITECH - NO RESERVATION CANCELATION ?
    # cancel reservation
    # params = {
    #     'reservation_id': reservation_id, 'status': 'invalid'
    # }
    # url = conn + '/updatereservation'
    # resp = requests.post(url, json=params)
    # resp.raise_for_status()
    # res = resp.json()
    # assert res['err'] == 0
    # data = res['data']
    # assert data


def call_generic(conn, endpoint):
    print('%s \n' % endpoint)
    url = conn + '/%s' % endpoint
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    data = res['data']
    print('%s \n' % endpoint)
    pprint.pprint(data)
    print('\n')
    return data


def test_getusers(conn):
    call_generic(conn, 'getusers')


def test_getactivities(conn):
    call_generic(conn, 'getactivities')


def test_getactivitytyes(conn):
    call_generic(conn, 'getactivitytypes')


def test_getreservationtypes(conn):
    call_generic(conn, 'getreservationtypes')
