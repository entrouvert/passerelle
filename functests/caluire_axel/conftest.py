import pytest


def pytest_addoption(parser):
    parser.addoption('--url', help='Url of a passerelle Caluire Axel connector instance')
    parser.addoption('--nameid', help='Publik Name ID')
    parser.addoption('--firstname', help='first name of a user')
    parser.addoption('--lastname', help='Last name of a user')
    parser.addoption('--family', help='Family ID')


@pytest.fixture(scope='session')
def conn(request):
    return request.config.getoption('--url')


@pytest.fixture(scope='session')
def user(request):
    return {
        'name_id': request.config.getoption('--nameid'),
        'first_name': request.config.getoption('--firstname'),
        'last_name': request.config.getoption('--lastname'),
        'family': request.config.getoption('--family'),
    }
