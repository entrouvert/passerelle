import pytest


def pytest_addoption(parser):
    parser.addoption('--url', help='Url of a passerelle Vivaticket connector instance')


@pytest.fixture(scope='session')
def conn(request):
    return request.config.getoption('--url')
