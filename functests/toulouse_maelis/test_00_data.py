import pytest
import requests

from .conftest import diff


@pytest.mark.parametrize(
    'ref',
    [
        'ape-indicators',
        'category',
        'child-indicator',
        'civility',
        'country',
        'county',
        'csp',
        'dietcode',
        'document',
        'exemption-reasons',
        #'nursery',
        'organ',
        'pai',
        'quality',
        'quotient',
        #'regie',
        'rl-indicator',
        'school-levels',
        'school-years',
        'situation',
        'street',
        'vaccin',
    ],
)
def test_referentials(conn, referentials, ref):
    url = conn + '/read-%s-list' % ref
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert len(res['data']) > 1
    for item in res['data']:
        assert 'id' in item
        assert 'text' in item
    if ref not in ['street', 'county', 'nursery']:
        assert diff(res['data'], 'test_read_%s_list.json' % ref)
