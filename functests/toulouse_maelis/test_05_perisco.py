import datetime

import pytest
import requests

from .conftest import link, unlink


def test_perisco(perisco_subscribe_info):
    assert perisco_subscribe_info['info']['activity']['libelle1'] == 'TEST TEMPS DU MIDI 22/23'


def test_perisco_adulte(perisco_subscribe_adulte_info):
    assert perisco_subscribe_adulte_info['info']['activity']['libelle1'] == 'TEST RESTAURATION ADULTE 22/23'


def test_perisco_agenda(conn, create_data, perisco_subscribe_info):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    # subscription
    url = conn + '/add-person-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': perisco_subscribe_info['activity']['id'],
        'unit_id': perisco_subscribe_info['unit']['id'],
        'place_id': perisco_subscribe_info['place']['id'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # find first available booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['bart_num'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) > 0
    booking = None
    for booking in resp.json()['data']:
        if booking['disabled'] is False:
            break
    else:
        raise Exception('no booking available')
    assert booking['details']['activity_id'] == perisco_subscribe_info['activity']['id']
    assert booking['details']['activity_label'] == 'Temps du midi'
    assert booking['prefill'] is False

    # book activity
    url = conn + '/update-child-agenda?NameID=%s' % create_data['name_id']
    payload = {
        'child_id': create_data['bart_num'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [booking['id']],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json() == {
        'updated': True,
        'count': 1,
        'changes': [
            {
                'booked': True,
                'activity_id': booking['details']['activity_id'],
                'activity_label': 'Temps du midi',
                'day': booking['details']['day_str'],
            }
        ],
        'err': 0,
    }

    # check booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['bart_num'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert [x['prefill'] for x in resp.json()['data'] if x['id'] == booking['id']][0] is True


def test_perisco_agenda_adulte(conn, create_data2, perisco_subscribe_adulte_info):
    unlink(conn, create_data2['name_id'])
    link(conn, create_data2)

    # subscription
    url = conn + '/add-person-subscription?NameID=%s' % create_data2['name_id']
    payload = {
        'person_id': create_data2['rl1_num'],
        'activity_id': perisco_subscribe_adulte_info['activity']['id'],
        'unit_id': perisco_subscribe_adulte_info['unit']['id'],
        'place_id': perisco_subscribe_adulte_info['place']['id'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # find first available booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data2['name_id']
    params = {
        'child_id': create_data2['rl1_num'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) > 0
    booking = None
    for booking in resp.json()['data']:
        if booking['disabled'] is False:
            break
    else:
        raise Exception('no booking available')
    assert booking['details']['activity_id'] == perisco_subscribe_adulte_info['activity']['id']
    assert booking['details']['activity_label'] == 'RESTAURATION ADULTE'
    assert booking['prefill'] is False

    # book activity
    url = conn + '/update-child-agenda?NameID=%s' % create_data2['name_id']
    payload = {
        'child_id': create_data2['rl1_num'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
        'booking_list': [booking['id']],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json() == {
        'updated': True,
        'count': 1,
        'changes': [
            {
                'booked': True,
                'activity_id': booking['details']['activity_id'],
                'activity_label': 'RESTAURATION ADULTE',
                'day': booking['details']['day_str'],
            }
        ],
        'err': 0,
    }

    # check booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data2['name_id']
    params = {
        'child_id': create_data2['rl1_num'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert [x['prefill'] for x in resp.json()['data'] if x['id'] == booking['id']][0] is True


def test_perisco_recurrent_week(conn, create_data, perisco_subscribe_info, reference_year):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    # no subscribed activity
    url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
    params = {
        'person_id': create_data['maggie_num'],
        'nature': 'PERISCO',
        'school_year': '%s-%s' % (reference_year, reference_year + 1),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 0

    # subscription
    url = conn + '/add-person-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['maggie_num'],
        'activity_id': perisco_subscribe_info['activity']['id'],
        'unit_id': perisco_subscribe_info['unit']['id'],
        'place_id': perisco_subscribe_info['place']['id'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
    params = {
        'person_id': create_data['maggie_num'],
        'nature': 'PERISCO',
        'school_year': '%s-%s' % (reference_year, reference_year + 1),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 1
    assert resp.json()['data'][0]['id'] == perisco_subscribe_info['activity']['id']
    assert [(x['text'], x['libelle'], x['libelle2']) for x in resp.json()['data']] == [
        ('Temps du midi', 'TEST TEMPS DU MIDI 22/23', 'Temps du midi'),
    ]

    # get recurent-week gabarit
    url = conn + '/get-recurrent-week?NameID=%s' % create_data['name_id']
    params = {
        'person_id': create_data['maggie_num'],
        'activity_id': perisco_subscribe_info['activity']['id'],
        'ref_date': datetime.date.today().strftime('%Y-%m-%d'),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert [(x['id'], x['day']) for x in resp.json()['data']] == [
        ('1-X', 'Lundi'),
        ('2-X', 'Mardi'),
        ('4-X', 'Jeudi'),
        ('5-X', 'Vendredi'),
    ]

    # no booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['maggie_num'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert not any(x['prefill'] for x in resp.json()['data'])

    # set recurent-week gabarit
    url = conn + '/update-recurrent-week?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['maggie_num'],
        'activity_id': perisco_subscribe_info['activity']['id'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
        'recurrent_week': ['1-X', '2-X'],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'

    # there is now some bookings
    url = conn + '/read-child-agenda?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['maggie_num'],
        'start_date': perisco_subscribe_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert any(x['prefill'] for x in resp.json()['data'])


def test_perisco_recurrent_week_adulte(conn, create_data2, perisco_subscribe_adulte_info, reference_year):
    unlink(conn, create_data2['name_id'])
    link(conn, create_data2)

    # no subscribed activity
    url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data2['name_id']
    params = {
        'person_id': create_data2['rl2_num'],
        'nature': 'PERISCO',
        'school_year': '%s-%s' % (reference_year, reference_year + 1),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 0

    # subscription
    url = conn + '/add-person-subscription?NameID=%s' % create_data2['name_id']
    payload = {
        'person_id': create_data2['rl2_num'],
        'activity_id': perisco_subscribe_adulte_info['activity']['id'],
        'unit_id': perisco_subscribe_adulte_info['unit']['id'],
        'place_id': perisco_subscribe_adulte_info['place']['id'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data2['name_id']
    params = {
        'person_id': create_data2['rl2_num'],
        'nature': 'PERISCO',
        'school_year': '%s-%s' % (reference_year, reference_year + 1),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 1
    assert resp.json()['data'][0]['id'] == perisco_subscribe_adulte_info['activity']['id']
    assert [(x['text'], x['libelle'], x['libelle2']) for x in resp.json()['data']] == [
        ('RESTAURATION ADULTE', 'TEST RESTAURATION ADULTE 22/23', 'RESTAURATION ADULTE')
    ]

    # get recurent-week gabarit
    url = conn + '/get-recurrent-week?NameID=%s' % create_data2['name_id']
    params = {
        'person_id': create_data2['rl2_num'],
        'activity_id': perisco_subscribe_adulte_info['activity']['id'],
        'ref_date': datetime.date.today().strftime('%Y-%m-%d'),
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert [(x['id'], x['day']) for x in resp.json()['data']] == [
        ('1-X', 'Lundi'),
        ('2-X', 'Mardi'),
        ('3-X', 'Mercredi'),
        ('4-X', 'Jeudi'),
        ('5-X', 'Vendredi'),
    ]

    # no booking
    url = conn + '/read-child-agenda?NameID=%s' % create_data2['name_id']
    params = {
        'child_id': create_data2['rl2_num'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert not any(x['prefill'] for x in resp.json()['data'])

    # set recurent-week gabarit
    url = conn + '/update-recurrent-week?NameID=%s' % create_data2['name_id']
    payload = {
        'person_id': create_data2['rl2_num'],
        'activity_id': perisco_subscribe_adulte_info['activity']['id'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
        'recurrent_week': ['1-X', '2-X'],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'

    # there is now some bookings
    url = conn + '/read-child-agenda?NameID=%s' % create_data2['name_id']
    params = {
        'child_id': create_data2['rl2_num'],
        'start_date': perisco_subscribe_adulte_info['unit']['dateStart'][:10],
        'end_date': perisco_subscribe_adulte_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert any(x['prefill'] for x in resp.json()['data'])
