import datetime

import pytest
import requests


@pytest.fixture(scope='session')
def school_year(conn):
    url = conn + '/read-school-years-list'
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    year = res['data'][0]['text']
    return year


@pytest.fixture(scope='session')
def exemption(conn):
    # get an exemption code
    url = conn + '/read-exemption-reasons-list'
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    return res['data'][0]['id']


def test_displaying_school_subscribed(conn, create_data, school_year, exemption):
    """
    Read-family ramène les inscriptions aux date de visualisation paramétrées
    sur le référential YearSchool
    """
    school_year = str(int(school_year) + 1)

    # create a 7 year-old child
    url = conn + '/create-child?NameID=%s' % create_data['name_id']
    payload = {
        'sexe': 'F',
        'firstname': 'Claris',
        'lastname': create_data['lastname'],
        'birth': {'dateBirth': '2016-09-12'},
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    claris_id = str(resp.json()['data']['child_id'])

    # book
    url = conn + '/create-child-school-pre-registration'
    payload = {
        'numPerson': claris_id,
        'schoolYear': school_year,
        'levelCode': 'CE1',
        'dateSubscribe': school_year + '-01-01',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data']['returnMessage'] is None
    assert resp.json()['data']['subscribeSchoolBean']['schoolName'] == 'DUPONT PIERRE ELEMENTAIRE'
    assert resp.json()['data']['subscribeSchoolBean']['adresse'] == '101 GRANDE-RUE SAINT MICHEL'

    # get Claris school from read-family
    url = conn + '/read-school-years-list'
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()['data']
    date_start = [x['dateStartYearSchool'] for x in res if x['text'] == school_year][0]
    assert date_start[10] > datetime.datetime.now().strftime('%Y-%m-%d')

    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    schools = [x['subscribeSchoolList'] for x in res['data']['childList'] if x['num'] == claris_id][0]
    assert len(schools) == 0  # school is filtered, but it is related to an hidden school year
    # field, not dateStartYearSchool, checked before : #2425


def test_school_pre_registration_by_sector(conn, create_data, school_year, exemption):
    """
    Pré-inscription de l'enfant de 7 ans dans son secteur
    """
    # create a 7 year-old child
    url = conn + '/create-child?NameID=%s' % create_data['name_id']
    payload = {
        'sexe': 'F',
        'firstname': 'Sego',
        'lastname': create_data['lastname'],
        'birth': {'dateBirth': '2016-05-09'},
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    sego_id = str(resp.json()['data']['child_id'])

    # assert there is a school at this address
    url = conn + '/read-schools-for-address-and-level'
    params = {
        'id_street': '2317',
        'num': '4',
        'year': school_year,
        'level': 'CE1',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 1
    assert resp.json()['data'][0]['text'] == 'DUPONT PIERRE ELEMENTAIRE'

    # assert there is a school at child address
    url = conn + '/read-schools-for-child-and-level'
    params = {
        'child_id': sego_id,
        'year': school_year,
        'level': 'CE1',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert len(resp.json()['data']) == 1
    assert resp.json()['data'][0]['text'] == 'DUPONT PIERRE ELEMENTAIRE'
    school_id = resp.json()['data'][0]['idSchool']
    assert school_id == '2435'

    # book
    url = conn + '/create-child-school-pre-registration'
    payload = {
        'numPerson': sego_id,
        'schoolYear': school_year,
        'levelCode': 'CE1',
        'dateSubscribe': school_year + '-01-01',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data']['returnMessage'] is None
    assert resp.json()['data']['subscribeSchoolBean']['schoolName'] == 'DUPONT PIERRE ELEMENTAIRE'
    assert resp.json()['data']['subscribeSchoolBean']['adresse'] == '101 GRANDE-RUE SAINT MICHEL'

    # get Sego school from read-family
    url = conn + '/read-school-years-list'
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()['data']
    date_start = [x['dateStartYearSchool'] for x in res if x['text'] == school_year][0]
    assert date_start[10] > datetime.datetime.now().strftime('%Y-%m-%d')
    # school is filtered, but it is related to an hidden school year
    # field, not dateStartYearSchool, see #2425

    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    schools = [x['subscribeSchoolList'] for x in res['data']['childList'] if x['num'] == sego_id][0]
    assert len(schools) == 1
    assert schools[0]['schoolName'] == 'DUPONT PIERRE ELEMENTAIRE'

    """
    Pré-inscription d'un enfant de 5 ans en CP avec rappprochement de fratrie pour celui de 7 ans :
    rapprochement dans le secteur de l'enfant.
    """
    # get Sego school
    url = conn + '/read-child-school-informations?NameID=%s' % create_data['name_id']
    params = {
        'child_id': sego_id,
        'year': school_year,
        'level': 'CE1',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    schools = data['childSubscribeSchoolInformation']['subscribeSchoolYearList']
    assert len(schools) == 1
    assert schools[0]['subscribeSchool']['school']['idSchool'] == school_id
    assert schools[0]['subscribeSchool']['perim']['idPerim'] == '2707'

    url = conn + '/create-child-school-pre-registration-with-sibling'
    payload = {
        'numPerson': create_data['maggie_num'],
        'schoolYear': school_year,
        'levelCode': 'GS',
        'datePresubscribe': school_year + '-01-01',
        'idSchoolRequested': school_id,
        'numPersonSibling': sego_id,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert 'returnMessage' not in resp.json()
    assert resp.json()['data']['schoolName'] == 'CALAS MATERNELLE'
    assert resp.json()['data']['adresse'] == '47 RUE ACHILLE VIADIEU'  # same sector


def test_school_pre_registration_by_exemption(conn, create_data, school_year, exemption):
    """
    Pré-inscription de l'enfant de 9 ans en dérogation :
    c'est une dérogation avec sélection du motif sur un établissement hors secteur
    """
    # school list
    url = conn + '/read-child-school-informations?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['bart_num'],
        'year': school_year,
        'level': 'CM1',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    schools = data['childSubscribeSchoolInformation']['subscribeSchoolInformation']['derogSchoolList']
    assert len(schools) > 1
    school_id = schools[0]['id']

    # book
    url = conn + '/create-child-school-pre-registration-with-exemption'
    payload = {
        'numPerson': create_data['bart_num'],
        'schoolYear': school_year,
        'levelCode': 'CM1',
        'datePresubscribe': school_year + '-01-01',
        'idRequestSchool1': school_id,
        'derogReasonCode': exemption,
        'derogComment': 'bla',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert 'returnMessage' not in resp.json()
    assert resp.json()['data']['schoolName'] == 'AMIDONNIERS ELEMENTAIRE'
    assert resp.json()['data']['adresse'] == '123 ALL DE BRIENNE'

    """
    Pré-inscription de l'autre enfant de 5 ans en CP
    avec rapprochement de fratrie pour celui de 9 ans :
    rapprochement hors du secteur de l'enfant.
    """

    # check E124 error
    # get a school that do not provide a level in its sector
    url = conn + '/read-child-school-informations?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['hugo_num'],
        'year': school_year,
        'level': 'GS',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert [
        x['idSchool']
        for x in data['childSubscribeSchoolInformation']['subscribeSchoolInformation']['derogSchoolList']
        if x['text'] == 'DIEUZAIDE JEAN MATERNELLE'
    ] == ['2437']

    # try to book on a sector that do not provide the requested level
    url = conn + '/create-child-school-pre-registration-with-sibling'
    payload = {
        'numPerson': create_data['hugo_num'],
        'schoolYear': school_year,
        'levelCode': 'CP',
        'datePresubscribe': school_year + '-01-01',
        'idSchoolRequested': '2437',
        'numPersonSibling': create_data['bart_num'],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 1
    assert resp.json()['err_class'] == 'passerelle.utils.soap.SOAPFault'
    assert 'E124' in resp.json()['err_desc']

    # get Bart school
    url = conn + '/read-child-school-informations?NameID=%s' % create_data['name_id']
    params = {
        'child_id': create_data['bart_num'],
        'year': school_year,
        'level': 'CM1',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    schools = data['childSubscribeSchoolInformation']['subscribeSchoolYearList']
    assert len(schools) == 1
    assert schools[0]['subscribeSchool']['school']['idSchool'] == school_id
    assert schools[0]['subscribeSchool']['perim']['idPerim'] == '2663'

    # book
    url = conn + '/create-child-school-pre-registration-with-sibling'
    payload = {
        'numPerson': create_data['hugo_num'],
        'schoolYear': school_year,
        'levelCode': 'GS',
        'datePresubscribe': school_year + '-01-01',
        'idSchoolRequested': school_id,
        'numPersonSibling': create_data['bart_num'],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert 'returnMessage' not in resp.json()
    assert resp.json()['data']['schoolName'] == 'AMIDONNIERS MATERNELLE'
    assert resp.json()['data']['adresse'] == '125 ALL DE BRIENNE'
