import pytest
import requests


def test_basket_subscribe_extrasco(conn, create_data, extrasco_subscribe_info, reference_year):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True

    def get_baskets():
        url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
        resp = requests.get(url)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def subscribe(person_id):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
        payload = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info['activity']['id'],
            'unit_id': extrasco_subscribe_info['unit']['id'],
            'place_id': extrasco_subscribe_info['place']['id'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    def subscriptions(person_id):
        url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'nature': 'EXTRASCO',
            'school_year': '%s-%s' % (reference_year, reference_year + 1),
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def get_bookings(person_id):
        url = conn + '/read-activity-agenda?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info['activity']['id'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    # no subscription
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []

    # empty basket
    assert get_baskets() == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert data['basket']['codeRegie'] == 105
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len({x['idIns'] for x in data['basket']['lignes']}) == 1  # 3 sur Larden

    subs = subscriptions(create_data['bart_num'])
    assert len(subs) == 1
    assert len(subs[0]['subscribesUnit']) == 1
    assert subscriptions(create_data['maggie_num']) == []

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['codeRegie'] == 105
    assert data[0]['text'] == 'ENFANCE LOISIRS'
    assert len(data[0]['lignes']) == 1  # 3 sur Larden
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 1

    # get 3 idIns because we subscribe a generic unit
    assert len({x['idIns'] for x in data[0]['lignes']}) == 1  # 3 sur Larden
    basket_id = data[0]['id']

    # cannot subscribe Bart twice
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 1
    assert 'E1019' in resp.json()['err_desc']
    assert len(get_baskets()) == 1

    # delete basket
    # should be call by user or by cron job
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'
    assert get_baskets() == []
    assert subscriptions(create_data['bart_num']) == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len(subscriptions(create_data['bart_num'])) == 1

    # subscribe Maggie
    resp = subscribe(create_data['maggie_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    subs = subscriptions(create_data['maggie_num'])
    assert len(subs) == 1
    assert len(subs[0]['subscribesUnit']) == 1

    # delete (generic) basket line for Bart
    data = get_baskets()
    assert len(data) == 1
    assert len(data[0]['lignes']) == 2  # 6 sur Larden
    basket_id = data[0]['id']
    # line for the generic unit for Bart
    line_id = [
        y['id']
        for x in data
        for y in x['lignes']
        if y['personneInfo']['numPerson'] == int(create_data['bart_num'])
        if y['inscription']['idUnit'] == extrasco_subscribe_info['unit']['id']
    ][0]
    url = conn + '/delete-basket-line?NameID=%s' % create_data['name_id']
    payload = {
        'basket_id': basket_id,
        'line_id': line_id,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['codeRegie'] == 105
    assert len({x['personneInfo']['numPerson'] for x in data['lignes']}) == 1
    assert len({x['idIns'] for x in data['lignes']}) == 1  # 3 sur Larden
    data = get_baskets()
    assert len(data) == 1
    assert len(get_baskets()) == 1
    assert len(data[0]['lignes']) == 1  # 3 sur Larden
    assert subscriptions(create_data['bart_num']) == []
    assert len(subscriptions(create_data['maggie_num'])) == 1

    # re-subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    assert len(subscriptions(create_data['bart_num'])) == 1

    # add bookings to Bart
    slots = [x['id'] for x in extrasco_subscribe_info['info']['agenda'] if x['disabled'] is False]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [True, True]
    assert len([x['prefill'] for x in get_bookings(create_data['bart_num']) if x['prefill'] is True]) == 2

    # add bookings to Maggie
    slots = [':'.join([create_data['maggie_num']] + x.split(':')[1:]) for x in slots]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['maggie_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [True, True]
    assert len([x['prefill'] for x in get_bookings(create_data['maggie_num']) if x['prefill'] is True]) == 2

    # delete basket
    # should be call by user or by cron job
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'
    assert get_baskets() == []
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []


@pytest.mark.xfail(run=False)
def test_basket_subscribe_extrasco2(conn, create_data, extrasco_subscribe_info2, reference_year):
    """Subscribing to a generic unit"""
    assert extrasco_subscribe_info2['info']['controlResult']['controlOK'] is True

    def get_baskets():
        url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
        resp = requests.get(url)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def subscribe(person_id):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
        payload = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info2['activity']['id'],
            'unit_id': extrasco_subscribe_info2['unit']['id'],
            'place_id': extrasco_subscribe_info2['place']['id'],
            'start_date': extrasco_subscribe_info2['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info2['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    def subscriptions(person_id):
        url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'nature': 'EXTRASCO',
            'school_year': '%s-%s' % (reference_year, reference_year + 1),
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def get_bookings(person_id):
        url = conn + '/read-activity-agenda?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info2['activity']['id'],
            'start_date': extrasco_subscribe_info2['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info2['unit']['dateEnd'][:10],
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    # no subscription
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []

    # empty basket
    assert get_baskets() == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert data['basket']['codeRegie'] == 105
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len({x['idIns'] for x in data['basket']['lignes']}) == 1  # 3 expected

    subs = subscriptions(create_data['bart_num'])
    assert len(subs) == 1
    assert len(subs[0]['subscribesUnit']) == 2
    assert [x['libelle'] for x in subs[0]['subscribesUnit']] == [
        'PUBLIK ADL MATERNELLE Lardenne JUIN 22/23 (NEPAS UTILISER)',
        'PUBLIK ADL MATER JOURNEE AVEC REPAS',
    ]
    assert subscriptions(create_data['maggie_num']) == []

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['codeRegie'] == 105
    assert data[0]['text'] == 'ENFANCE LOISIRS'
    assert len(data[0]['lignes']) == 1  # 3 expected
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 1

    # we should get 3 idIns because we subscribe a generic unit
    assert len({x['idIns'] for x in data[0]['lignes']}) == 1  # 3 expected
    basket_id = data[0]['id']

    # cannot subscribe Bart twice
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 1
    assert 'E1019' in resp.json()['err_desc']
    assert len(get_baskets()) == 1

    # delete basket
    # should be call by user or by cron job
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'
    assert get_baskets() == []
    assert subscriptions(create_data['bart_num']) == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len(subscriptions(create_data['bart_num'])) == 1

    # subscribe Maggie
    resp = subscribe(create_data['maggie_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    assert len(subscriptions(create_data['maggie_num'])) == 1

    # delete (generic) basket line for Bart
    data = get_baskets()
    assert len(data) == 1
    assert len(data[0]['lignes']) == 2  # 6 sur Larden
    basket_id = data[0]['id']
    # line for the generic unit for Bart
    line_id = [
        y['id']
        for x in data
        for y in x['lignes']
        if y['personneInfo']['numPerson'] == int(create_data['bart_num'])
        if y['inscription']['idUnit'] == extrasco_subscribe_info2['unit']['id']
    ][0]
    url = conn + '/delete-basket-line?NameID=%s' % create_data['name_id']
    payload = {
        'basket_id': basket_id,
        'line_id': line_id,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['codeRegie'] == 105
    assert len({x['personneInfo']['numPerson'] for x in data['lignes']}) == 1
    assert len({x['idIns'] for x in data['lignes']}) == 1  # 3 sur Larden
    data = get_baskets()
    assert len(data) == 1
    assert len(get_baskets()) == 1
    assert len(data[0]['lignes']) == 1  # 3 sur Larden
    assert subscriptions(create_data['bart_num']) == []
    assert len(subscriptions(create_data['maggie_num'])) == 1

    # re-subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    assert len(subscriptions(create_data['bart_num'])) == 1

    # add bookings to Bart
    slots = [x['id'] for x in extrasco_subscribe_info2['info']['agenda'] if x['disabled'] is False]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info2['activity']['id'],
        'start_date': extrasco_subscribe_info2['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info2['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [True, True]
    assert len([x['prefill'] for x in get_bookings(create_data['bart_num']) if x['prefill'] is True]) == 2

    # add bookings to Maggie
    slots = [':'.join([create_data['maggie_num']] + x.split(':')[1:]) for x in slots]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['maggie_num'],
        'activity_id': extrasco_subscribe_info2['activity']['id'],
        'start_date': extrasco_subscribe_info2['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info2['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [True, True]
    assert len([x['prefill'] for x in get_bookings(create_data['maggie_num']) if x['prefill'] is True]) == 2

    # delete basket
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'
    assert get_baskets() == []
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []


def test_basket_subscribe_loisirs(conn, create_data, loisirs_subscribe_info, reference_year):
    assert loisirs_subscribe_info['info']['controlResult']['controlOK'] is True

    def get_baskets():
        url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
        resp = requests.get(url)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def subscribe(person_id):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
        payload = {
            'person_id': person_id,
            'activity_id': loisirs_subscribe_info['activity']['id'],
            'unit_id': loisirs_subscribe_info['unit']['id'],
            'place_id': loisirs_subscribe_info['place']['id'],
            'start_date': loisirs_subscribe_info['unit']['dateStart'][:10],
            'end_date': loisirs_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    def subscriptions(person_id):
        url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'nature': 'LOISIRS',
            'school_year': '%s-%s' % (reference_year, reference_year + 1),
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return [
            x
            for x in resp.json()['data']
            if x['libelle'] == 'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES'
        ]

    # no subscription
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []

    # empty basket
    assert get_baskets() == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert data['basket']['codeRegie'] == 109
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len({x['idIns'] for x in data['basket']['lignes']}) == 1
    subs = subscriptions(create_data['bart_num'])
    assert len(subs) == 1
    assert len(subs[0]['subscribesUnit']) == 2
    assert [x['libelle'] for x in subs[0]['subscribesUnit']] == [
        'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES',
        'MERCREDI - 15h30/17h - 8/15Ans',
    ]
    assert subscriptions(create_data['maggie_num']) == []

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['codeRegie'] == 109
    assert data[0]['text'] == 'SPORT'
    assert len(data[0]['lignes']) == 1
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 1
    assert len({x['idIns'] for x in data[0]['lignes']}) == 1
    assert data[0]['lignes'][0]['montant'] == 88.5
    basket_id = data[0]['id']

    # cannot subscribe Bart twice
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 1
    assert 'E1019' in resp.json()['err_desc']
    assert len(get_baskets()) == 1

    # subscribe Maggie
    resp = subscribe(create_data['maggie_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    subs = subscriptions(create_data['maggie_num'])
    assert len(subs) == 1
    assert len(subs[0]['subscribesUnit']) == 2

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['id'] == basket_id
    assert data[0]['codeRegie'] == 109
    assert data[0]['text'] == 'SPORT'
    assert len(data[0]['lignes']) == 2
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 2
    assert len({x['idIns'] for x in data[0]['lignes']}) == 2
    assert all(x['montant'] == 88.5 for x in data[0]['lignes'])

    # delete basket line for Bart
    data = get_baskets()
    assert len(data) == 1
    assert len(data[0]['lignes']) == 2
    basket_id = data[0]['id']
    # line for Bart
    line_id = [
        y['id']
        for x in data
        for y in x['lignes']
        if y['personneInfo']['numPerson'] == int(create_data['bart_num'])
    ][0]
    url = conn + '/delete-basket-line?NameID=%s' % create_data['name_id']
    payload = {
        'basket_id': basket_id,
        'line_id': line_id,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['codeRegie'] == 109
    assert len({x['personneInfo']['numPerson'] for x in data['lignes']}) == 1
    assert len({x['idIns'] for x in data['lignes']}) == 1
    data = get_baskets()
    assert len(data) == 1
    assert len(get_baskets()) == 1
    assert len(data[0]['lignes']) == 1
    assert subscriptions(create_data['bart_num']) == []
    assert len(subscriptions(create_data['maggie_num'])) == 1

    # delete basket
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['data'] == 'ok'
    assert get_baskets() == []
    assert subscriptions(create_data['maggie_num']) == []
