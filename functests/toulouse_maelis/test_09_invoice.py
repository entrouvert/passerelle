import datetime

import pytest
import requests

from .conftest import diff, link, unlink


def test_direct_debit_order(conn, create_data):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    url = conn + '/add-rl1-direct-debit-order?NameID=%s' % create_data['name_id']
    payload = {
        'codeRegie': '102',
        'bank/bankBIC': 'BDFEFR2T',
        'bank/bankIBAN': 'FR7630001007941234567890185',
        'bank/bankRUM': 'xxx',
        'bank/dateStart': '2023-01-01',
        'bank/bankAddress': '75049 PARIS cedex 01',
        'bank/civility': 'x',
        'bank/lastName': 'Ewing',
        'bank/firstName': 'John Ross',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    res = resp.json()
    assert res['data'] == 'ok'

    url = conn + '/get-rl1-direct-debit-order?NameID=%s' % create_data['name_id']
    params = {
        'codeRegie': '102',
        'dateRef': '2023-01-01',
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    res = resp.json()
    res['data']['numPerson'] = 'N/A'
    assert diff(res['data'], 'test_get_rl1_direct_debit_order.json')


def test_pay_invoice_loisirs(conn, create_data, loisirs_subscribe_info, reference_year):
    assert loisirs_subscribe_info['info']['controlResult']['controlOK'] is True

    def get_baskets():
        url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
        resp = requests.get(url)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def subscribe(person_id):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
        payload = {
            'person_id': person_id,
            'activity_id': loisirs_subscribe_info['activity']['id'],
            'unit_id': loisirs_subscribe_info['unit']['id'],
            'place_id': loisirs_subscribe_info['place']['id'],
            'start_date': loisirs_subscribe_info['unit']['dateStart'][:10],
            'end_date': loisirs_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    # empty basket
    assert get_baskets() == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert data['basket']['codeRegie'] == 109
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len({x['idIns'] for x in data['basket']['lignes']}) == 1

    # subscribe Maggie
    resp = subscribe(create_data['maggie_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['codeRegie'] == 109
    assert data[0]['text'] == 'SPORT'
    assert len(data[0]['lignes']) == 2
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 2
    assert len({x['idIns'] for x in data[0]['lignes']}) == 2
    basket_id = data[0]['id']

    # validate basket de generate an invoice
    url = conn + '/validate-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data['idInsLst']) == 2
    assert len(data['factureLst']) == 1
    assert len(data['factureLst'][0]['lineInvoiceList']) == 2
    assert data['factureLst'][0]['regie']['code'] == 109
    invoice_num = data['factureLst'][0]['numInvoice']
    invoice_id = data['factureLst'][0]['idInvoice']
    assert get_baskets() == []

    # get invoices paid
    url = conn + '/regie/109/invoices/history?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json() == {'data': [], 'err': 0}

    # get invoices to be paid
    url = conn + '/regie/109/invoices?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data) == 1
    assert data[0]['amount'] == '177'  # ou juste > 0 ?
    assert data[0]['online_payment'] is True
    assert data[0]['paid'] is False
    assert len({x['idIns'] for x in data[0]['maelis_item']['lineInvoiceList']}) == 2
    assert data[0]['maelis_item']['idInvoice'] == invoice_id
    assert data[0]['maelis_item']['numInvoice'] == invoice_num

    # payInvoice
    url = conn + '/regie/109/invoice/%s-%s/pay/' % (create_data['family_id'], invoice_num)
    payload = {
        'transaction_date': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
        'transaction_id': 'xxx',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    res = resp.json()
    assert res['data'] == 'ok'

    # get invoices to be paid
    url = conn + '/regie/109/invoices?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json() == {'has_invoice_for_payment': True, 'data': [], 'err': 0}

    # get invoices paid
    url = conn + '/regie/109/invoices/history?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data) == 1
    assert data[0]['amount'] == '0'
    assert data[0]['total_amount'] == '177'  # ou juste > 0 ?
    assert data[0]['online_payment'] is False
    assert data[0]['paid'] is True
    assert len({x['idIns'] for x in data[0]['maelis_item']['lineInvoiceList']}) == 2
    assert data[0]['maelis_item']['idInvoice'] == invoice_id
    assert data[0]['maelis_item']['numInvoice'] == invoice_num


def test_payinvoice_extrasco(conn, create_data, extrasco_subscribe_info, reference_year):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True

    def get_baskets():
        url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
        resp = requests.get(url)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def subscribe(person_id):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
        payload = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info['activity']['id'],
            'unit_id': extrasco_subscribe_info['unit']['id'],
            'place_id': extrasco_subscribe_info['place']['id'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    def subscriptions(person_id):
        url = conn + '/read-subscribe-activity-list?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'nature': 'EXTRASCO',
            'school_year': '%s-%s' % (reference_year, reference_year + 1),
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def get_bookings(person_id):
        url = conn + '/read-activity-agenda?NameID=%s' % create_data['name_id']
        params = {
            'person_id': person_id,
            'activity_id': extrasco_subscribe_info['activity']['id'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    # no subscription
    assert subscriptions(create_data['bart_num']) == []
    assert subscriptions(create_data['maggie_num']) == []

    # empty basket
    assert get_baskets() == []

    # subscribe Bart
    resp = subscribe(create_data['bart_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert data['basket']['codeRegie'] == 105
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 1
    assert len({x['idIns'] for x in data['basket']['lignes']}) == 1

    assert len(subscriptions(create_data['bart_num'])) == 1
    assert subscriptions(create_data['maggie_num']) == []

    # basket
    data = get_baskets()
    assert len(data) == 1
    assert data[0]['codeRegie'] == 105
    assert len(data[0]['lignes']) == 1
    assert len({x['personneInfo']['numPerson'] for x in data[0]['lignes']}) == 1

    assert len({x['idIns'] for x in data[0]['lignes']}) == 1
    basket_id = data[0]['id']

    # subscribe Maggie
    resp = subscribe(create_data['maggie_num'])
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert data['controlResult'] == {'controlOK': True, 'message': None}
    assert len({x['personneInfo']['numPerson'] for x in data['basket']['lignes']}) == 2
    assert len(subscriptions(create_data['maggie_num'])) == 1

    # add bookings to Bart
    slots = [x['id'] for x in extrasco_subscribe_info['info']['agenda'] if x['disabled'] is False]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert len([x['prefill'] for x in get_bookings(create_data['bart_num']) if x['prefill'] is True]) > 0

    # add bookings to Maggie
    slots = [':'.join([create_data['maggie_num']] + x.split(':')[1:]) for x in slots]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['maggie_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert len([x['prefill'] for x in get_bookings(create_data['maggie_num']) if x['prefill'] is True]) > 0

    # validate basket
    url = conn + '/validate-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data['idInsLst']) == 2
    assert len(data['factureLst']) == 1
    assert get_baskets() == []
    assert len(data['factureLst'][0]['lineInvoiceList']) == 2
    assert data['factureLst'][0]['regie']['code'] == 105
    invoice_num = data['factureLst'][0]['numInvoice']
    invoice_id = data['factureLst'][0]['idInvoice']

    # get invoices paid
    url = conn + '/regie/105/invoices/history?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json() == {'data': [], 'err': 0}

    # get invoices to be paid
    url = conn + '/regie/105/invoices?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data) == 1
    assert int(data[0]['amount']) > 0
    assert data[0]['online_payment'] is True
    assert data[0]['paid'] is False
    assert len({x['idIns'] for x in data[0]['maelis_item']['lineInvoiceList']}) == 2
    assert data[0]['maelis_item']['idInvoice'] == invoice_id
    assert data[0]['maelis_item']['numInvoice'] == invoice_num

    # payInvoice
    url = conn + '/regie/105/invoice/%s-%s/pay/' % (create_data['family_id'], invoice_num)
    payload = {
        'transaction_date': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
        'transaction_id': 'xxx',
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    res = resp.json()
    assert res['data'] == 'ok'

    # get invoices to be paid
    url = conn + '/regie/105/invoices?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json() == {'has_invoice_for_payment': True, 'data': [], 'err': 0}

    # get invoices history
    url = conn + '/regie/105/invoices/history?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    data = resp.json()['data']
    assert len(data) == 1
    assert data[0]['amount'] == '0'
    assert int(data[0]['total_amount']) > 0
    assert data[0]['online_payment'] is False
    assert data[0]['paid'] is True
    assert len({x['idIns'] for x in data[0]['maelis_item']['lineInvoiceList']}) == 2
    assert data[0]['maelis_item']['idInvoice'] == invoice_id
    assert data[0]['maelis_item']['numInvoice'] == invoice_num
