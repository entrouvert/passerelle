import pytest
import requests


def test_catalog_personnalise_extrasco(extrasco_subscribe_info):
    assert (
        extrasco_subscribe_info['info']['activity']['libelle1']
        == 'PUBLIK ADL ELEMENTAIRE Maourine JUIN 22/23(NE PAS UTILISER)'
    )
    assert extrasco_subscribe_info['info']['calendarGeneration']['code'] == 'NOT_REQUIRED'
    assert extrasco_subscribe_info['info']['billingInformation'] == {
        'modeFact': 'PRESENCE',
        'quantity': None,
        'unitPrice': 11.5,
    }
    assert extrasco_subscribe_info['info']['activity']['blocNoteList'] == [
        {
            'note': 'Lien vers le réglement intérieur :\r\nhttps://portail-parsifal.test.entrouvert.org/media/uploads/2023/03/23/flyer-sejour.pdf\r\nLien vers arrêté municipal :\r\nhttps://portail-parsifal.test.entrouvert.org/media/uploads/2023/04/05/arrete-municipal.pdf',
            'numIndex': 1,
        }
    ]
    assert (
        extrasco_subscribe_info['info']['agenda'][0]['details']['activity_label']
        == 'ADL ELEMENTAIRE Maourine Juin'
    )


def test_catalog_personnalise_extrasco2(extrasco_subscribe_info2):
    assert (
        extrasco_subscribe_info2['info']['activity']['libelle1']
        == 'PUBLIK ADL MATERNELLE Lardenne JUIN 22/23 (NEPAS UTILISER)'
    )
    assert extrasco_subscribe_info2['info']['calendarGeneration']['code'] == 'FORBIDDEN'
    assert extrasco_subscribe_info2['info']['billingInformation'] == {
        'modeFact': 'PRESENCE',
        'quantity': None,
        'unitPrice': 11.5,
    }
    assert extrasco_subscribe_info2['info']['activity']['blocNoteList'] == [
        {
            'note': 'Lien vers le réglement intérieur :\r\nhttps://portail-parsifal.test.entrouvert.org/media/uploads/2023/03/23/flyer-sejour.pdf\r\nLien vers arrêté municipal :\r\nhttps://portail-parsifal.test.entrouvert.org/media/uploads/2023/04/05/arrete-municipal.pdf',
            'numIndex': 1,
        }
    ]


def test_direct_subscribe(conn, create_data, extrasco_subscribe_info, reference_year):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True

    url = conn + '/add-person-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['hugo_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'unit_id': extrasco_subscribe_info['unit']['id'],
        'place_id': extrasco_subscribe_info['place']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # no idIns provided to remove subscription later
    assert resp.json()['data'] == {'controlOK': True, 'message': None}


def test_subscribe_with_conveyance(conn, create_data, extrasco_subscribe_info):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True

    assert extrasco_subscribe_info['info']['conveyance'] is not None
    morning = [
        x['id'] for x in extrasco_subscribe_info['info']['conveyance']['morningJourney']['depositPlaceList']
    ]
    afternoon = [
        x['id'] for x in extrasco_subscribe_info['info']['conveyance']['afternoonJourney']['depositPlaceList']
    ]
    assert len(morning) > 0
    assert len(afternoon) > 0

    url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'unit_id': extrasco_subscribe_info['unit']['id'],
        'place_id': extrasco_subscribe_info['place']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'conveyanceSubscribe/idPlaceMorning': morning[0],
        'conveyanceSubscribe/idPlaceAfternoon': afternoon[0],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    basket_id = resp.json()['data']['basket']['id']

    # remove subscription
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0


def test_subscribe_with_recurrent_week(conn, create_data, extrasco_subscribe_info):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True
    assert [(x['id'], x['day']) for x in extrasco_subscribe_info['info']['recurrent_week']] == [
        ('1-X', 'Lundi'),
        ('2-X', 'Mardi'),
        ('3-X', 'Mercredi'),
        ('4-X', 'Jeudi'),
        ('5-X', 'Vendredi'),
    ]

    url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'unit_id': extrasco_subscribe_info['unit']['id'],
        'place_id': extrasco_subscribe_info['place']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'recurrent_week': ['1-X', '2-X'],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    basket_id = resp.json()['data']['basket']['id']

    # there is now some bookings
    url = conn + '/read-activity-agenda?NameID=%s' % create_data['name_id']
    params = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert any(x['prefill'] for x in resp.json()['data'])

    # check quantity into basket
    url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    line = resp.json()['data'][0]['lignes'][0]
    assert line['prixUnit'] == 11.5
    assert line['qte'] > 0
    assert line['montant'] == line['prixUnit'] * line['qte']

    # remove subscription
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0


def test_subscribe_with_agenda(conn, create_data, extrasco_subscribe_info):
    assert extrasco_subscribe_info['info']['controlResult']['controlOK'] is True

    def get_bookings():
        url = conn + '/read-activity-agenda?NameID=%s' % create_data['name_id']
        params = {
            'person_id': create_data['bart_num'],
            'activity_id': extrasco_subscribe_info['activity']['id'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']

    def get_perisco_bookings():
        url = conn + '/read-child-agenda?NameID=%s' % create_data['name_id']
        params = {
            'child_id': create_data['bart_num'],
            'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        }
        resp = requests.get(url, params=params)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return [
            item
            for item in resp.json()['data']
            if item['details']['activity_id'] == extrasco_subscribe_info['activity']['id']
        ]

    # subscribe without providing calendar
    url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'unit_id': extrasco_subscribe_info['unit']['id'],
        'place_id': extrasco_subscribe_info['place']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    basket_id = resp.json()['data']['basket']['id']

    # no booking
    assert not any(x['prefill'] for x in get_bookings())
    assert not any(x['prefill'] for x in get_perisco_bookings())

    # book using info calendar gabarit (booking registered from w.c.s. form)
    assert len(extrasco_subscribe_info['info']['agenda']) > 0
    assert not any(x['prefill'] for x in extrasco_subscribe_info['info']['agenda'])
    slots = [x['id'] for x in extrasco_subscribe_info['info']['agenda'] if x['disabled'] is False]
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [slots[0], slots[-1]],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [True, True]

    # there is now 2 bookings
    assert len([x['prefill'] for x in get_bookings() if x['prefill'] is True]) == 2
    perisco_bookings = get_perisco_bookings()
    assert len([x['prefill'] for x in perisco_bookings if x['prefill'] is True]) == 2
    assert perisco_bookings[0]['details']['activity_label'] == 'ADL ELEMENTAIRE Maourine Juin'

    # check quantity into basket
    url = conn + '/get-baskets?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    line = resp.json()['data'][0]['lignes'][0]
    assert (line['prixUnit'], line['qte'], line['montant']) == (11.5, 0.0, 0.0)

    # unbook slots
    url = conn + '/update-activity-agenda/?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': extrasco_subscribe_info['activity']['id'],
        'start_date': extrasco_subscribe_info['unit']['dateStart'][:10],
        'end_date': extrasco_subscribe_info['unit']['dateEnd'][:10],
        'booking_list': [],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    assert resp.json()['updated'] is True
    assert [x['booked'] for x in resp.json()['changes']] == [False, False]
    assert not any(x['prefill'] for x in get_bookings())

    # remove subscription
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0


@pytest.mark.xfail(run=False)
def test_daily_capacity(conn, create_data2, extrasco_subscribe_info3):
    assert extrasco_subscribe_info3['info']['controlResult']['controlOK'] is True

    def subscribe(child):
        url = conn + '/add-person-basket-subscription?NameID=%s' % create_data2['name_id']
        payload = {
            'person_id': create_data2['%s_num' % child],
            'activity_id': extrasco_subscribe_info3['activity']['id'],
            'unit_id': extrasco_subscribe_info3['unit']['id'],
            'place_id': extrasco_subscribe_info3['place']['id'],
            'start_date': extrasco_subscribe_info3['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info3['unit']['dateEnd'][:10],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        assert resp.json()['err'] == 0
        return resp.json()['data']['basket']['id']

    def book(child, slot):
        url = conn + '/update-activity-agenda/?NameID=%s' % create_data2['name_id']
        payload = {
            'person_id': create_data2['%s_num' % child],
            'activity_id': extrasco_subscribe_info3['activity']['id'],
            'start_date': extrasco_subscribe_info3['unit']['dateStart'][:10],
            'end_date': extrasco_subscribe_info3['unit']['dateEnd'][:10],
            'booking_list': [slot],
        }
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        return resp

    # subscribe all family childs
    basket_id = subscribe('bart')
    for child in 'lisa', 'maggie', 'hugo':
        assert subscribe(child) == basket_id

    # book all childs on the same day
    assert len(extrasco_subscribe_info3['info']['agenda']) > 0
    assert not any(x['prefill'] for x in extrasco_subscribe_info3['info']['agenda'])
    slots = [x['id'] for x in extrasco_subscribe_info3['info']['agenda'] if x['disabled'] is False]
    for child in 'bart', 'lisa', 'maggie':
        resp = book(child, slots[-1])
        assert resp.json()['err'] == 0
        assert resp.json()['updated'] is True
        assert [x['booked'] for x in resp.json()['changes']] == [True]
    resp = book('hugo', slots[-1])
    assert resp.json()['err'] == 1
    assert resp.json()['err_desc'] == 0

    # # remove subscriptions
    # url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    # payload = {'basket_id': basket_id}
    # resp = requests.post(url, json=payload)
    # resp.raise_for_status()
    # assert resp.json()['err'] == 0
