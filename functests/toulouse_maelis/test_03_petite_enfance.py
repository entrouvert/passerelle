import datetime

import pytest
import requests

from .conftest import link, unlink


def test_create_nursery_demand_on_existing_child(conn, create_data):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    url = conn + '/get-nursery-geojson'
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    nurseries = resp.json()['features']
    assert len(nurseries) >= 2

    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    nb_childs = len(res['data']['childList'])
    assert sorted(x['code'] for x in res['data']['indicatorList']) == []

    url = conn + '/read-child?NameID=%s&child_id=%s' % (create_data['name_id'], create_data['maggie_num'])
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert sorted(x['code'] for x in res['data']['indicatorList']) == []

    url = conn + '/create-nursery-demand'
    payload = {
        'family_id': create_data['family_id'],
        'family_indicators/0/code': 'APE_FIRSTC',
        'family_indicators/0/isActive': True,
        'child_id': create_data['maggie_num'],
        'demand_indicators/0/code': 'APE_COMPO1',
        'demand_indicators/0/isActive': True,
        'start_date': datetime.date.today().strftime('%Y-%m-%d'),
        'number_of_days': '2',
        'start_hour_Mon': '08:00',
        'end_hour_Mon': '',
        'comment': 'bla',
        'accept_other_nurseries': True,
        'nursery1/idActivity': nurseries[0]['properties']['activity_id'],
        'nursery1/idUnit': nurseries[0]['properties']['unit_id'],
        'nursery1/idPlace': nurseries[0]['properties']['place_id'],
        'nursery2/idActivity': nurseries[1]['properties']['activity_id'],
        'nursery2/idUnit': nurseries[1]['properties']['unit_id'],
        'nursery2/idPlace': nurseries[1]['properties']['place_id'],
        'nursery3/idActivity': '',
        'nursery3/idUnit': '',
        'nursery3/idPlace': '',
        # indicators
        'child_indicators/0/code': 'APE_HBOTH',
        'child_indicators/0/isActive': True,
        'child_indicators/1/code': 'APE_HPAR',
        'child_indicators/1/isActive': True,
        'child_indicators/2/code': 'APE_COMPO3',
        'child_indicators/2/isActive': True,
        'child_indicators/3/code': 'APE_MULTIACC',
        'child_indicators/3/isActive': True,
        'family_indicators/0/code': 'APE_COMPO4',
        'family_indicators/0/isActive': True,
        'family_indicators/1/code': 'APE_NAIM',
        'family_indicators/1/isActive': True,
        'family_indicators/2/code': 'APE_FIRSTC',
        'family_indicators/2/isActive': True,
        'family_indicators/3/code': 'APE_COMPO2',
        'family_indicators/3/isActive': True,
        'family_indicators/4/code': 'APE_HAND',
        'family_indicators/4/isActive': True,
        'demand_indicators/0/code': 'APE_FRAT',
        'demand_indicators/0/isActive': True,
        'demand_indicators/1/code': 'APE_COMPO1',
        'demand_indicators/1/isActive': True,
        'demand_indicators/2/code': 'APE_HFRAT',
        'demand_indicators/2/isActive': True,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json() == {'data': None, 'err': 0}

    # no child added
    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert len(res['data']['childList']) == nb_childs

    # check indicators
    assert sorted(x['code'] for x in res['data']['indicatorList']) == [
        'APE_COMPO2',
        'APE_COMPO4',
        'APE_FIRSTC',
        'APE_HAND',
        'APE_NAIM',
    ]

    url = conn + '/read-child?NameID=%s&child_id=%s' % (create_data['name_id'], create_data['maggie_num'])
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert sorted(x['code'] for x in res['data']['indicatorList']) == [
        'APE_COMPO3',
        'APE_HBOTH',
        'APE_HPAR',
        'APE_MULTIACC',
    ]


def test_create_nursery_demand_adding_new_child(conn, create_data):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    url = conn + '/get-nursery-geojson'
    resp = requests.get(url)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    nurseries = resp.json()['features']
    assert len(nurseries) >= 2

    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    nb_childs = len(res['data']['childList'])
    assert 'NELSON' not in [x['firstname'] for x in res['data']['childList']]

    url = conn + '/create-nursery-demand'
    payload = {
        'family_id': create_data['family_id'],
        'child_first_name': 'Nelson',
        'child_last_name': 'Muntz',
        'child_birthdate': '2013-10-31',
        'child_gender': 'G',
        'start_date': datetime.date.today().strftime('%Y-%m-%d'),
        'nursery1/idActivity': nurseries[0]['properties']['activity_id'],
        'nursery1/idUnit': nurseries[0]['properties']['unit_id'],
        'nursery1/idPlace': nurseries[0]['properties']['place_id'],
        'nursery2/idActivity': nurseries[1]['properties']['activity_id'],
        'nursery2/idUnit': nurseries[1]['properties']['unit_id'],
        'nursery2/idPlace': nurseries[1]['properties']['place_id'],
        'nursery3/idActivity': '',
        'nursery3/idUnit': '',
        'nursery3/idPlace': '',
        # indicators
        'child_indicators/0/code': 'APE_HBOTH',
        'child_indicators/0/isActive': True,
        'child_indicators/1/code': 'APE_HPAR',
        'child_indicators/1/isActive': True,
        'child_indicators/2/code': 'APE_COMPO3',
        'child_indicators/2/isActive': True,
        'child_indicators/3/code': 'APE_MULTIACC',
        'child_indicators/3/isActive': True,
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert res['err'] == 0
    child_id = resp.json()['data']
    assert child_id is not None

    # a new child is created on family
    url = conn + '/read-family?NameID=%s' % create_data['name_id']
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert len(res['data']['childList']) == nb_childs + 1
    assert 'NELSON' in [x['firstname'] for x in res['data']['childList']]
    assert res['data']['childList'][nb_childs]['num'] == child_id

    # check child indicators
    url = conn + '/read-child?NameID=%s&child_id=%s' % (create_data['name_id'], child_id)
    resp = requests.get(url)
    resp.raise_for_status()
    res = resp.json()
    assert res['err'] == 0
    assert res['data']['firstname'] == 'NELSON'
    assert sorted(x['code'] for x in res['data']['indicatorList']) == [
        'APE_COMPO3',
        'APE_HBOTH',
        'APE_HPAR',
        'APE_MULTIACC',
    ]
