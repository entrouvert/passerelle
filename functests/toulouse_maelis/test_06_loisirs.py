import datetime

import pytest
import requests

from .conftest import get_subscription_info, link, unlink

# LOISIR is like EXTRACO (most tests are redondants) but :
# * there is no calendar (days) to provide.
# * there is a general catalog to display


def test_catalog_general_loisirs(conn, update_data):
    unlink(conn, update_data['name_id'])
    link(conn, update_data)
    url = conn + '/read-activity-list'
    params = {'ref_date': datetime.date.today().strftime('%Y-%m-%d')}

    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    labels = [x['text'] for x in resp.json()['data']]
    assert (
        'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES, MERCREDI - 13h45/17h - 8/15Ans, ARGOULETS'
        in labels
    )
    assert (
        'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES, MERCREDI - 14h/16h30 - 10/15Ans, LA RAMEE'
        in labels
    )
    assert (
        'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES, MERCREDI - 15h30/17h - 8/15Ans, ARGOULETS'
        in labels
    )
    assert 'Promenade forêt enchantée, TEST promenade forêt enchantée, TERRITOIRE OUEST' in labels
    assert 'Vitrail Fusing 1/2 Je Adultes, Inscription annuelle, Centre Culturel ALBAN MINVILLE' in labels

    for item in resp.json()['data']:
        if (
            item['text']
            == 'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES, MERCREDI - 13h45/17h - 8/15Ans, ARGOULETS'
        ):
            assert item['criterias'] == {
                'service': {'text': 'Service', 'data': {'sports': 'Sports'}, 'order': ['sports']},
                'nature': {
                    'text': "Nature de l'activité",
                    'data': {'1': 'Activités Régulières'},
                    'order': ['1'],
                },
                'type': {
                    'text': "Type de l'activité",
                    'data': {'activites-aquatiques': 'Activités Aquatiques'},
                    'order': ['activites-aquatiques'],
                },
                'public': {
                    'text': 'Public',
                    'data': {'1': 'Enfant (3-11 ans)', '2': 'Ado (12-17 ans)'},
                    'order': ['1', '2'],
                },
                'day': {'text': 'Jours', 'data': {'3': 'Mercredi'}, 'order': ['3']},
                'place': {'text': 'Lieu', 'data': {'A10053179757': 'ARGOULETS'}, 'order': ['A10053179757']},
            }
            assert item['activity']['activityPortail']['blocNoteList'] == [
                {
                    'note': "Activité ayant lieu le Mercredi, merci de choisir votre tranche horraire en fonction de l'âge de votre enfant.",
                    'numIndex': 1,
                }
            ]
        if item['text'] == 'Promenade forêt enchantée, TEST promenade forêt enchantée, TERRITOIRE OUEST':
            assert item['criterias'] == {
                'service': {'text': 'Service', 'data': {'sports': 'Sports'}, 'order': ['sports']},
                'nature': {
                    'text': "Nature de l'activité",
                    'data': {'1': 'Activités Régulières'},
                    'order': ['1'],
                },
                'type': {
                    'text': "Type de l'activité",
                    'data': {'activite-pedestre': 'Activité Pédestre'},
                    'order': ['activite-pedestre'],
                },
                'public': {'text': 'Public', 'data': {'5': 'Sénior (60 ans et plus)'}, 'order': ['5']},
                'day': {
                    'text': 'Jours',
                    'data': {'1': 'Lundi', '2': 'Mardi', '3': 'Mercredi', '4': 'Jeudi', '5': 'Vendredi'},
                    'order': ['1', '2', '3', '4', '5'],
                },
                'place': {
                    'text': 'Lieu',
                    'data': {'A10056517597': 'TERRITOIRE OUEST'},
                    'order': ['A10056517597'],
                },
            }
            assert item['activity']['activityPortail']['blocNoteList'] == [
                {'note': 'Activité de promenade en forêt.', 'numIndex': 1}
            ]


def test_catalog_personnalise_loisirs(loisirs_subscribe_info):
    assert (
        loisirs_subscribe_info['info']['activity']['libelle1']
        == 'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES'
    )
    assert loisirs_subscribe_info['info']['calendarGeneration']['code'] == 'REQUIRED'
    assert [(x['id'], x['day']) for x in loisirs_subscribe_info['info']['recurrent_week']] == []
    assert loisirs_subscribe_info['info']['billingInformation'] == {
        'modeFact': 'FORFAIT',
        'quantity': 1.0,
        'unitPrice': 88.5,
    }


def test_catalog_personnalise_loisirs_not_allowed(conn, create_data, reference_year):
    unlink(conn, create_data['name_id'])
    link(conn, create_data)
    try:
        get_subscription_info(
            'LOISIRS',
            'TEST ECOLE DES SPORTS 22/23 SEMESTRE 2 - MULTIACTIVITES',
            'MERCREDI - 15h30/17h - 8/15Ans',
            'ARGOULETS',
            conn,
            create_data['name_id'],
            create_data['rl1_num'],
            reference_year,
        )
    except Exception:
        return
    assert False, 'Adult can subscribe to child activity'


def test_direct_subscribe(conn, create_data, loisirs_subscribe_info, reference_year):
    assert loisirs_subscribe_info['info']['controlResult']['controlOK'] is True
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    url = conn + '/add-person-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['hugo_num'],
        'activity_id': loisirs_subscribe_info['activity']['id'],
        'unit_id': loisirs_subscribe_info['unit']['id'],
        'place_id': loisirs_subscribe_info['place']['id'],
        'start_date': loisirs_subscribe_info['unit']['dateStart'][:10],
        'end_date': loisirs_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # no idIns provided to remove subscription later
    assert resp.json()['data'] == {'controlOK': True, 'message': None}


def test_direct_subscribe_out_town(conn, create_data2, loisirs_subscribe_info2, reference_year):
    assert loisirs_subscribe_info2['info']['controlResult']['controlOK'] is True
    unlink(conn, create_data2['name_id'])
    link(conn, create_data2)

    url = conn + '/add-person-subscription?NameID=%s' % create_data2['name_id']
    payload = {
        'person_id': create_data2['hugo_num'],
        'activity_id': loisirs_subscribe_info2['activity']['id'],
        'unit_id': loisirs_subscribe_info2['unit']['id'],
        'place_id': loisirs_subscribe_info2['place']['id'],
        'start_date': loisirs_subscribe_info2['unit']['dateStart'][:10],
        'end_date': loisirs_subscribe_info2['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # no idIns provided to remove subscription later
    assert resp.json()['data'] == {'controlOK': True, 'message': None}


def test_subscribe_to_basket(conn, create_data, loisirs_subscribe_info, reference_year):
    assert loisirs_subscribe_info['info']['controlResult']['controlOK'] is True
    unlink(conn, create_data['name_id'])
    link(conn, create_data)

    url = conn + '/add-person-basket-subscription?NameID=%s' % create_data['name_id']
    payload = {
        'person_id': create_data['bart_num'],
        'activity_id': loisirs_subscribe_info['activity']['id'],
        'unit_id': loisirs_subscribe_info['unit']['id'],
        'place_id': loisirs_subscribe_info['place']['id'],
        'start_date': loisirs_subscribe_info['unit']['dateStart'][:10],
        'end_date': loisirs_subscribe_info['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    basket_id = resp.json()['data']['basket']['id']

    # remove subscription
    url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    payload = {'basket_id': basket_id}
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0


@pytest.mark.xfail(run=False)
def test_global_capacity(conn, create_data2, loisirs_subscribe_info3, reference_year):
    assert loisirs_subscribe_info3['info']['controlResult']['controlOK'] is True
    unlink(conn, create_data2['name_id'])
    link(conn, create_data2)

    # subscribe Bart
    url = conn + '/add-person-subscription?NameID=%s' % create_data2['name_id']
    # url = conn + '/add-person-basket-subscription?NameID=%s' % create_data2['name_id']
    payload = {
        'person_id': create_data2['bart_num'],
        'activity_id': loisirs_subscribe_info3['activity']['id'],
        'unit_id': loisirs_subscribe_info3['unit']['id'],
        'place_id': loisirs_subscribe_info3['place']['id'],
        'start_date': loisirs_subscribe_info3['unit']['dateStart'][:10],
        'end_date': loisirs_subscribe_info3['unit']['dateEnd'][:10],
    }
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    # basket_id = resp.json()['data']['basket']['id']

    # subscribe Lisa
    payload['person_id'] = create_data2['lisa_num']
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # subscribe Maggie
    payload['person_id'] = create_data2['maggie_num']
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 0

    # can't subscribe Huggo
    payload['person_id'] = create_data2['hugo_num']
    resp = requests.post(url, json=payload)
    resp.raise_for_status()
    assert resp.json()['err'] == 1
    assert resp.json()['err_desc'] == ''

    # check capacity on main catalog
    url = conn + '/read-activity-list'
    params = {'ref_date': datetime.date.today().strftime('%Y-%m-%d')}
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    assert resp.json()['err'] == 0
    for item in resp.json()['data']:
        if item['activity']['libelle'] == 'PUBLIK Vitrail Fusing 1/2 Je Adultes 2022/2023 - Mardi 14h-1':
            import pdb

            pdb.set_trace()

    # # remove subscriptions
    # url = conn + '/delete-basket?NameID=%s' % create_data['name_id']
    # payload = {'basket_id': basket_id}
    # resp = requests.post(url, json=payload)
    # resp.raise_for_status()
    # assert resp.json()['err'] == 0
