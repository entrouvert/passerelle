import pytest


def pytest_addoption(parser):
    parser.addoption('--url', help='Url of a passerelle Toulouse Axel connector instance')
    parser.addoption('--nameid', help='Publik Name ID')
    parser.addoption('--firstname', help='first name of a user')
    parser.addoption('--lastname', help='Last name of a user')
    parser.addoption('--dob', help='Date of birth of a user')
    parser.addoption('--dui', help='DUI number')


@pytest.fixture(scope='session')
def conn(request):
    return request.config.getoption('--url')


@pytest.fixture(scope='session')
def user(request):
    return {
        'name_id': request.config.getoption('--nameid'),
        'first_name': request.config.getoption('--firstname'),
        'last_name': request.config.getoption('--lastname'),
        'dob': request.config.getoption('--dob'),
        'dui': request.config.getoption('--dui'),
    }
