# This file is sourced by "execfile" from passerelle.settings

import os

# Debian defaults
DEBUG = False

PROJECT_NAME = 'passerelle'
PASSERELLE_MANAGE_COMMAND = '/usr/bin/passerelle-manage'

#
# hobotization (multitenant)
#
exec(open('/usr/lib/hobo/debian_config_common.py').read())

# disable django-mellon autologin
MELLON_OPENED_SESSION_COOKIE_NAME = None

# suds logs are buggy
LOGGING['loggers']['suds'] = {
    'level': 'ERROR',
    'filters': ['require_debug_true'],
    'propagate': True,
}
# cmislib logs are useless
LOGGING['loggers']['cmislib'] = {
    'level': 'ERROR',
    'filters': ['require_debug_true'],
    'propagate': True,
}

# paramiko transport logs are buggy
LOGGING['loggers']['paramiko.transport'] = {
    'level': 'ERROR',
    'filters': ['force_debug'],
    'propagate': True,
}

# silence pdfrw
LOGGING['loggers']['pdfrw'] = {
    'propagate': False,
}

exec(open('/etc/%s/settings.py' % PROJECT_NAME).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
