import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    include: ['tests/js/**/*.test.js'],
    watchExclude: ['**'],
    alias: {
      qrcode: fileURLToPath(new URL('./passerelle/apps/qrcode/static/qrcode/js', import.meta.url)),
      vitest: process.env.NODE_PATH + '/vitest',
    },
    environment: 'happy-dom',
    chaiConfig: {
      truncateThreshold: Infinity,
    },
  },
})

