import os
import shlex
import tempfile
from pathlib import Path

import nox


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'WebTest',
        'django-webtest',
        'git+https://git.entrouvert.org/entrouvert/publik-django-templatetags.git',
        'httmock',
        'pytest',
        'pytest-freezer',
        'pyquery',
        'responses',
        'mohawk',
        'ldaptools',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session()
@nox.parametrize('django', ['>=4.2,<4.3'])
def tests(session, django):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-django',
        'pytest-xdist',
        'pytest-localserver',
        'pytest-sftpserver',
        'mock<4',
        'psycopg2-binary',
        'python-dateutil',
        'django-model-utils',
        'lxml',
        'vobject',
        'bleach',
        'emoji',
        'phonenumbers',
        'dnspython',
        'requests_toolbelt',
        'Pillow<9.5.0',
        'jsonschema==4.10.3',
        'python-ldap<=3.2',  # align with Debian <= 11 (buster, bullseye)
        'cryptography<39',
        django_version=django,
    )

    session.run('python', 'manage.py', 'compilemessages', silent=True)

    session.log('Checking migrations...')
    check_migrations(
        session,
        env={
            'DJANGO_SETTINGS_MODULE': 'passerelle.settings',
            'PASSERELLE_SETTINGS_FILE': 'tests/settings.py',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
        },
    )

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov-context',
            'test',
            '--cov=passerelle/',
            '--cov=tests/',
            '--cov-config',
            '.coveragerc',
            '-v',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]

    if not session.interactive:
        args += ['-v', '--numprocesses', '12']

    args += session.posargs + ['tests/']

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'passerelle.settings',
            'PASSERELLE_SETTINGS_FILE': 'tests/settings.py',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
        },
    )


@nox.session
def pylint(session):
    setup_venv(session, 'django-mellon>=1.55', 'pylint<3', 'astroid<3', 'pylint-django', 'nox')
    pylint_command = ['pylint', '--jobs', '12', '-f', 'parseable', '--rcfile', 'pylint.rc']

    if not session.posargs:
        pylint_command += ['passerelle/', 'tests/', 'noxfile.py']
    else:
        pylint_command += session.posargs

    if not session.interactive:
        session.run(
            'bash',
            '-c',
            f'{shlex.join(pylint_command)} | tee pylint.out ; test $PIPESTATUS -eq 0',
            external=True,
        )
    else:
        session.run(*pylint_command)


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def js_tests(session):
    session.install('nodeenv')
    session.run('nodeenv', '--prebuilt', '--python-virtualenv')
    session.run('npm', 'install', '-g', 'vitest@<1.1.0', 'happy-dom@=13.4.0')
    session.run(
        'npx',
        'vitest',
        '--run',
        env={
            'NODE_PATH': os.path.join(os.getcwd(), '.nox/js_tests/lib/node_modules'),
            'LC_ALL': 'en_US.UTF-8',
        },
    )


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
        'passerelle/static/css/style.css',
        'passerelle/apps/*/static/*/css/style.css',
        'passerelle/contrib/*/static/*/css/style.css',
        # may appear on jenkins when jobs runs in parallel
        'merge-junit-results.py',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))


def check_migrations(session, env):
    with tempfile.NamedTemporaryFile(mode='w') as fd:
        print(
            '''\
import django
from django.apps import apps
from django.conf import settings
from django.core.management import call_command

settings.DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}

django.setup()

app_labels = [app.label for app in apps.get_app_configs() if app.label not in ['admin', 'auth', 'contenttypes']]

call_command('makemigrations', *app_labels, dry_run=True, no_input=True, verbosity=1, check=True)
''',
            file=fd,
            flush=True,
        )
        session.run('python3', fd.name, env=env, silent=True)
